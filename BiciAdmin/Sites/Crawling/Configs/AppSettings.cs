﻿using System;
using System.Collections.Generic;

namespace Crawling.Configs
{
    public class AppSettings
    {
        public CrawTools CrawTools { get; set; }
        public ShopInfo ShopInfo { get; set; }
        public KiotVietSettings KiotVietSettings { get; set; }
    }

    public class CrawTools
    {
        public int NumberDateTracking { get; set; }
        public string TimeCrawlingOnDay { get; set; }
        public string TokenPass { get; set; }
        public string UrlBase { get; set; }
        public string VariantColors { get; set; }
        public List<string> ColorTag { get; set; }
    }

    public class ShopInfo
    {
        public string ShopName { get; set; }
        public string SubDomain { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
    }

    public class KiotVietSettings
    {
        public string TokenEndPoint { get; set; }
        public string PublicAPIEndPoint { get; set; }
        public string ClientId { get; set; }
        public string ClientSecret { get; set; }
        public string Retail { get; set; }
        public long CashierId { get; set; }
        public TokenInfo TokenGlobal { get; set; }
    }

    public class TokenInfo
    {
        public string access_token { get; set; }
        public int expires_in { get; set; }
        public string token_type { get; set; }
        public DateTime token_time { get; set; }
    }
}

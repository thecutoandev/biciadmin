﻿using System;
using Autofac;
using Autofac.Extensions.DependencyInjection;
using DataCore.Data;
using DataCore.Data.Infrastructure;
using DataCore.Data.Repositories;
using DataCore.Models;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;
using System.Linq;
using Quartz;
using Quartz.Impl;
using Quartz.Spi;
using Crawling.Jobs;
using Crawling.Services;
using MainService = Services.APIServices;
using MainInterfaces = Services.Interfaces;
using DataCore.Models;
using KiotService.Services;
using Bugsnag.AspNet.Core;

namespace Crawling.Configs
{
    public class BootStrapper
    {
        //public static DatabaseFactory mainDBContext;
        public static IServiceProvider serviceProvider;
        public static IConfiguration Configuration;
        public static bool IsSyncingData = false;
        public static IContainer SetupAutofacContainer(IServiceCollection services, IConfiguration configuration)
        {
            var builder = new ContainerBuilder();

            builder.RegisterType<UnitOfWork>().As<IUnitOfWork>().InstancePerDependency();
            builder.RegisterType<DatabaseFactory>().As<IDatabaseFactory>().InstancePerDependency();
            //builder.RegisterType<MainService.OrderPickingService>().As<MainInterfaces.IOrderPickingService>().InstancePerDependency();
            //Scan repositories for components

            builder.RegisterAssemblyTypes(typeof(CollectionRepository).Assembly)
                  .Where(instance => instance.Name.EndsWith("Repository", StringComparison.Ordinal))
                  .AsImplementedInterfaces();

            builder.RegisterAssemblyTypes(typeof(OrderService).Assembly)
                        .Where(instance => instance.Name.EndsWith("Service", StringComparison.Ordinal))
                        .AsImplementedInterfaces();

            builder.RegisterAssemblyTypes(typeof(MainService.OrderPickingService).Assembly)
                        .Where(instance => instance.Name.EndsWith("Service", StringComparison.Ordinal))
                        .AsImplementedInterfaces();

            builder.RegisterAssemblyTypes(typeof(KOrderService).Assembly)
                        .Where(instance => instance.Name.EndsWith("Service", StringComparison.Ordinal))
                        .AsImplementedInterfaces();

            // Init required components.
            if (null != services)
            {
                var connectionString = configuration.GetConnectionString("MainDBContext");
                var assembly = typeof(MainDBContext).Namespace;
                services.AddDbContext<MainDBContext>(options => options.UseSqlServer(connectionString));
                services.AddIdentity<User, IdentityRole>()
                        .AddEntityFrameworkStores<MainDBContext>()
                        .AddDefaultTokenProviders();

                // Set mysql connection
                //IMySQLContext mysqlContext = new MySQLContext(configuration);
                //services.AddSingleton(mysqlContext);


                // Add Quartz services
                services.AddSingleton<IJobFactory, SingletonJobFactory>();
                services.AddSingleton<ISchedulerFactory, StdSchedulerFactory>();

                // Get app settings
                var appSettingsSection = configuration.GetSection("AppSettings");
                services.Configure<AppSettings>(appSettingsSection);
                services.Configure<DataCore.Models.AppSettings>(appSettingsSection);
                var timeSchedule = configuration.GetValue<string>("AppSettings:CrawTools:TimeCrawlingOnDay");
                services.AddSingleton<CrawlingJob>();
                services.AddSingleton(new SyncSchedule(
                    jobType: typeof(CrawlingJob),
                    cronExpression: timeSchedule));
                services.AddScoped(provider => provider.GetRequiredService<IOptions<AppSettings>>().Value);
                services.AddScoped(provider => provider.GetRequiredService<IOptions<DataCore.Models.AppSettings>>().Value);
                services.AddHostedService<QuartzHostedService>();

                // Add bugsnag
                services.AddBugsnag(config =>
                {
                    config.ApiKey = "07507924b2dcbc86419a2883b429db16";
                });
                // Set automaper
                builder.Populate(services);
            }
            var container = builder.Build();
            Configuration = configuration;
            
            return container;
        }


        public static void InitApplicationBuilder(IApplicationBuilder app)
        {
            var scope = app.ApplicationServices.GetRequiredService<IServiceScopeFactory>().CreateScope();
            serviceProvider = scope.ServiceProvider;
        }
    }
}

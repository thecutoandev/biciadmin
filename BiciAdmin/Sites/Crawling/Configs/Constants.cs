﻿using System;
namespace Crawling.Configs
{
    public class Constants
    {
        public const string APP_SETTINGS_NAME = "AppSettings";
        public const string CRAWLING_NUMBER_DATE_TRACKING = "AppSetings:CrawTools:NumberDateTracking";

        public enum OrderStatus
        {
            WaitingApprove = 0,
            WaitingProccess = 1,
            PreparingOrder = 2,
            CompleteOrder = 3,
            Canceled =4
        }
    }
}

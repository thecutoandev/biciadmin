﻿using System.Collections.Generic;
using Crawling.Configs;
using Crawling.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using AppSettings = Crawling.Configs.AppSettings;

namespace Crawling.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SyncController : ControllerBase
    {
        private readonly AppSettings appSettings;
        private readonly IOrderService orderService;
        private readonly IProductService productService;
        private readonly IBlogService blogService;
        public SyncController(IOptions<AppSettings> appSettings,
                              IOrderService orderService,
                              IProductService productService,
                              IBlogService blogService)
        {
            this.appSettings = appSettings.Value;
            this.orderService = orderService;
            this.productService = productService;
            this.blogService = blogService;
            //this._userRepository = userRepository;
        }

        [HttpPost]
        public IActionResult SyncAllData()
        {
            try
            {
                if (BootStrapper.IsSyncingData)
                {
                    return BadRequest("Data is syncing, please wait to complete sync.");
                }
                BootStrapper.IsSyncingData = true;
                var listServices = new List<IService>();
                listServices.Add(productService);
                listServices.Add(orderService);
                listServices.Add(blogService);
                foreach (var service in listServices)
                {
                    var result = service.SyncData().Result;
                }
                BootStrapper.IsSyncingData = false;
                return Ok();
            }
            catch (System.Exception ex)
            {
                BootStrapper.IsSyncingData = false;
                return BadRequest(ex.Message);
            }
        }

        [AllowAnonymous]
        [HttpGet]
        [Route("orders")]
        public IActionResult SyncOrders()
        {
            try
            {
                if (BootStrapper.IsSyncingData)
                {
                    return BadRequest("Data is syncing, please wait to complete sync.");
                }
                BootStrapper.IsSyncingData = true;
                var result = orderService.SyncData().Result;
                BootStrapper.IsSyncingData = false;
                return Ok();
            }
            catch (System.Exception ex)
            {
                BootStrapper.IsSyncingData = false;
                return BadRequest(ex.Message);
            }
        }

        [HttpGet]
        [Route("products")]
        public IActionResult SyncProducts()
        {
            try
            {
                if (BootStrapper.IsSyncingData)
                {
                    return BadRequest("Data is syncing, please wait to complete sync.");
                }
                BootStrapper.IsSyncingData = true;
                var result = productService.SyncData().Result;
                BootStrapper.IsSyncingData = false;
                return Ok();
            }
            catch (System.Exception ex)
            {
                BootStrapper.IsSyncingData = false;
                return BadRequest(ex.Message);
            }
        }

        [AllowAnonymous]
        [HttpGet]
        [Route("product/{id}")]
        public IActionResult SyncSingleProduct(long id)
        {
            try
            {
                productService.SyncSingleProduct(null, null, null, id);
                return Ok();
            }
            catch (System.Exception ex)
            {
                BootStrapper.IsSyncingData = false;
                return BadRequest(ex.Message);
            }
        }


        [AllowAnonymous]
        [HttpGet]
        [Route("status")]
        public IActionResult SyncStatus()
        {
            try
            {
                var msg = "Hệ thống sync đang khả dụng";
                if (BootStrapper.IsSyncingData)
                {
                    msg = "Hệ thống đang sync data. Không thể thực hiện thao tác sync khi chưa hoàn thành.";
                }
                return Ok(msg);
            }
            catch (System.Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
    }
}

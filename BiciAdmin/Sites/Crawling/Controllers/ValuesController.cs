﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Crawling.Configs;
using Crawling.Services;
using DataCore.Data.Repositories;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;

namespace Crawling.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ValuesController : ControllerBase
    {
        private readonly AppSettings appSettings;
        private readonly IProductService productService;
        public ValuesController(IOptions<AppSettings> appSettings, IProductService productService)
        {
            this.appSettings = appSettings.Value;
            this.productService = productService;
        }
        // GET api/values
        [HttpGet]
        public async Task<ActionResult<IEnumerable<string>>> GetAsync()
        {
            //var srv = new CategoryCrawlService();
            var list = new List<string>();
            var listServices = new List<IService>();
            listServices.Add(new ProductService(appSettings));
            foreach (var service in listServices)
            {
                var success = await service.SyncData();
            }
            return list;
        }

        // GET api/values/5
        [HttpGet("{id}")]
        public ActionResult<string> Get(int id)
        {
            return "value";
        }

        // POST api/values
        [HttpPost]
        public void Post([FromBody] string value)
        {
        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}

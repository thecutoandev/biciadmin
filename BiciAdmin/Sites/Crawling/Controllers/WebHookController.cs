﻿using System.Collections.Generic;
using Crawling.Configs;
using Crawling.Model;
using Crawling.Services;
using DataCore.Data.Repositories;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;

namespace Crawling.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class WebHookController : ControllerBase
    {
        private readonly AppSettings appSettings;
        private readonly IUserRepository _userRepository;
        private readonly IProductService productService;
        private readonly IOrderService orderService;

        public WebHookController(IOptions<AppSettings> appSettings,
            IProductService productService,
            IOrderService orderService)
        {
            this.appSettings = appSettings.Value;
            this.productService = productService;
            this.orderService = orderService;
        }

        [HttpPost]
        [Route("SyncProduct")]
        public IActionResult SyncProductData(HProduct product)
        {
            try
            {
                var result = this.productService.SyncSingleProduct(product);
                return Ok("Crawling data is " + result);
            }
            catch (System.Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpPost]
        [Route("SyncOrder")]
        public IActionResult SyncOrderData(HOrder order)
        {
            try
            {
                //var order = new OrderService(appSettings, _userRepository);
                var result = orderService.SyncSingleData(order);
                return Ok("Crawling data is " + result);
            }
            catch (System.Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
    }
}

﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Crawling.Configs;
using Crawling.Services;
using Microsoft.Extensions.Options;
using Quartz;
using MainInterfaces = Services.Interfaces;

namespace Crawling.Jobs
{
    [DisallowConcurrentExecution]
    public class CrawlingJob : IJob
    {
        private readonly IOrderService orderService;
        private readonly IProductService productService;
        private readonly MainInterfaces.IOrderPickingService orderPickingService;
        private readonly AppSettings appSettings;
        public CrawlingJob(IOptions<AppSettings> appSettings,
                           IProductService productService,
                           IOrderService orderService,
                           MainInterfaces.IOrderPickingService orderPickingService)
        {
            this.productService = productService;
            this.orderService = orderService;
            this.appSettings = appSettings.Value;
            this.orderPickingService = orderPickingService;
        }

        public Task Execute(IJobExecutionContext context)
        {
            try
            {
                if (BootStrapper.IsSyncingData)
                {
                    return Task.CompletedTask;
                }
                BootStrapper.IsSyncingData = true;
                var listServices = new List<IService>();
                listServices.Add(productService);
                listServices.Add(orderService);
                //listServices.Add(new BlogService(appSettings));
                foreach (var service in listServices)
                {
                    service.SyncData();
                }
                orderPickingService.AutoMultiPick();
            }
#if DEBUG
            catch (System.Exception ex)
#else
            catch
#endif
            {
                BootStrapper.IsSyncingData = false;
            }
            finally
            {
                BootStrapper.IsSyncingData = false;
            }
            return Task.CompletedTask;
        }
    }
}

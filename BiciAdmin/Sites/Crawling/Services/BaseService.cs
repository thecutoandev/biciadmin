﻿using Crawling.Configs;
using DataCore.Data;
using DataCore.Data.Infrastructure;
using Microsoft.Extensions.DependencyInjection;
using System.Collections.Generic;
using System;
using Microsoft.Extensions.Options;
using System.Net.Http;
using System.Net;
using System.Linq;
using AppSettings = Crawling.Configs.AppSettings;

namespace Crawling.Services
{
    public abstract class BaseService
    {
        protected readonly IDatabaseFactory databaseFactory;
        protected readonly IUnitOfWork unitOfWork;
        protected readonly AppSettings appSetting;
        protected readonly string cookies;

        public BaseService(AppSettings appSetting, IUnitOfWork unitOfWork)
        {
            //MainDBContext dbContext = BootStrapper.serviceProvider.GetService<MainDBContext>();
            //databaseFactory = new DatabaseFactory(dbContext);
            //unitOfWork = new UnitOfWork(databaseFactory);
            cookies = string.Empty;
            this.appSetting = appSetting;
            this.unitOfWork = unitOfWork;
        }

        protected virtual void MappingData<T>(List<T> dataSource)
        {
            throw new NotImplementedException();
        }

        protected virtual string DetectCookies()
        {
            CookieContainer cookieContainer = new CookieContainer();
            HttpClientHandler handler = new HttpClientHandler();
            handler.CookieContainer = cookieContainer;
            var http = new HttpClient(handler);
            var values = new Dictionary<string, string>();
            values.Add("ShopName", appSetting.ShopInfo.ShopName);
            values.Add("SubDomain", appSetting.ShopInfo.SubDomain);
            values.Add("UserName", appSetting.ShopInfo.UserName);
            values.Add("Password", appSetting.ShopInfo.Password);

            var content = new FormUrlEncodedContent(values);
            var response = http.PostAsync(appSetting.CrawTools.UrlBase + "/admin/auth/login", content).Result;
            var strResult = response.Content.ReadAsStringAsync().Result;
            var text = strResult.Contains("phan thi ngoc tho");
            var cookies = cookieContainer.GetCookies(new Uri(this.appSetting.CrawTools.UrlBase)).Cast<Cookie>().ToArray() ;
            var cookiesString = new List<string>();
            foreach (var cookie in cookies)
            {
                cookiesString.Add(cookie.ToString());
            }

            return string.Join(';', cookiesString);
        }


        protected void SaveChanges()
        {
            unitOfWork.Commit();
        }
    }
}

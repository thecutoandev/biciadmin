﻿using System;
using System.Text;
using Crawling.Configs;
using Settings = Crawling.Configs.Constants;

namespace Crawling.Services
{
    public class GenericProductQuery
    {
        public static string List()
        {
            var sqlBuilder = new StringBuilder();
            sqlBuilder.AppendLine("SELECT  tBrand.term_id      AS brand_id,");
            sqlBuilder.AppendLine("	       tPrice.meta_value   AS product_price,");
            sqlBuilder.AppendLine("	       p.ID                AS ref_web_id, ");
            sqlBuilder.AppendLine("        p.post_title        AS product_name, ");
            sqlBuilder.AppendLine("        p.post_content      AS product_content, ");
            sqlBuilder.AppendLine("	       p.guid              AS link, ");
            sqlBuilder.AppendLine("	       p.post_date_gmt     AS created_date, ");
            sqlBuilder.AppendLine("        p.post_modified_gmt AS updated_date ");
            sqlBuilder.AppendLine("FROM    wp_posts AS p");
            //-- Mapping brand data...
            sqlBuilder.AppendLine("LEFT JOIN wp_term_relationships AS rBrand");
            sqlBuilder.AppendLine("          ON p.ID = rBrand.object_id");
            sqlBuilder.AppendLine("LEFT JOIN wp_term_taxonomy AS xBrand");
            sqlBuilder.AppendLine("          ON rBrand.term_taxonomy_id = xBrand.term_taxonomy_id");
            sqlBuilder.AppendLine("LEFT JOIN wp_terms AS tBrand");
            sqlBuilder.AppendLine("          ON xBrand.term_id = tBrand.term_id");
            //-- Mapping price
            sqlBuilder.AppendLine("LEFT JOIN wp_postmeta AS tPrice");
            sqlBuilder.AppendLine("          ON tPrice.post_id = p.ID");
            sqlBuilder.AppendLine("WHERE p.post_type     = 'product' AND");
            sqlBuilder.AppendLine("	     p.post_status   = 'publish' AND");
            sqlBuilder.AppendLine("      xBrand.taxonomy = 'thuong_hieu_san_pham' AND");
            sqlBuilder.AppendLine("      tPrice.meta_key = '_price'");
            sqlBuilder.AppendLine("ORDER BY p.ID");

            // Filter by updated date.
            var dateFromSettings = BootStrapper.Configuration[Settings.CRAWLING_NUMBER_DATE_TRACKING];
            if (!string.IsNullOrEmpty(dateFromSettings) && dateFromSettings != "0")
            {
                var numberDate = int.Parse(dateFromSettings);
                var dateForQuery = DateTime.Today.AddDays(numberDate).ToString("yyyy-MM-dd HH:mm:ss");
                sqlBuilder.AppendLine($"AND   p.post_modified_gmt   >= '{dateForQuery}'");
            }
            var sql = sqlBuilder.ToString();
            return sql;
        }

        public enum ColumnIndex
        {
            brandId = 0,
            productPrice = 1,
            refWebId = 2,
            productName = 3,
            productContent = 4,
            productLink = 5,
            createdDate = 6,
            updatedDate = 7
        }
    }
}

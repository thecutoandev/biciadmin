﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DataCore.Data.Repositories;
using DataCore.Models;
using MySql.Data.MySqlClient;

namespace Crawling.Services
{
    public class GenericProductCrawlService : BaseService, IService
    {
        private readonly IGenericProductRepository _genericProductRepository;
        private readonly IBrandRepository _brandRepository;
        private List<GenericProduct> genericProducts;
        private List<Brand> brands;

        public GenericProductCrawlService()
        {
            _genericProductRepository = new GenericProductRepository(this.databaseFactory);
            _brandRepository = new BrandRepository(this.databaseFactory);
            genericProducts = new List<GenericProduct>();
            brands = new List<Brand>();
        }

        /// <summary>
        /// Prepare data from mysql server and sync all data to sql server.
        /// </summary>
        /// <param name="sqlConnection">Connection for mysql</param>
        /// <returns>true: sync data is successfully | false: is failed.</returns>
        public async Task<bool> SyncData(MySqlConnection sqlConnection)
        {
            try
            {
                var lisGenericProducts = new List<GenericProduct>();
                var command = new MySqlCommand(GenericProductQuery.List(), sqlConnection);
                using (var sqlReader = command.ExecuteReader())
                {
                    while (sqlReader.Read())
                    {
                        lisGenericProducts.Add(new GenericProduct()
                        {
                            ref_web_id = sqlReader.GetInt32((int)GenericProductQuery.ColumnIndex.refWebId),
                            name = sqlReader.GetString((int)GenericProductQuery.ColumnIndex.productName),
                            product_name = sqlReader.GetString((int)GenericProductQuery.ColumnIndex.productName),
                            brand_id = sqlReader.GetInt32((int)GenericProductQuery.ColumnIndex.brandId),
                            product_content = sqlReader.GetString((int)GenericProductQuery.ColumnIndex.productContent),
                            link = sqlReader.GetString((int)GenericProductQuery.ColumnIndex.productLink),
                            product_price = ConvertDecimal(sqlReader.GetFieldValue<string>((int)GenericProductQuery.ColumnIndex.productPrice)),
                            created_date = sqlReader.GetDateTime((int)GenericProductQuery.ColumnIndex.createdDate),
                            updated_date = sqlReader.GetDateTime((int)GenericProductQuery.ColumnIndex.updatedDate),
                        });
                    }
                }
                MappingData<GenericProduct>(lisGenericProducts);
                return true;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return false;
            }
        }

        private decimal ConvertDecimal(object input)
        {
            try
            {
                var inputText = input.ToString();
                return decimal.Parse(inputText);
            }
            catch
            {
                return 0;
            }
        }

        /// <summary>
        /// Insert or update data from mysql to sql server
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="dataSource"></param>
        protected override void MappingData<T>(List<T> dataSource)
        {
            var mySqlData = dataSource.Cast<GenericProduct>().ToList();
            // Get exists data
            var listIds = mySqlData.Select(model => model.ref_web_id).ToArray();
            var listBrandIds = mySqlData.Select(model => model.brand_id).ToArray();
            brands = _brandRepository.Query(model => listBrandIds.Contains(model.ref_brand_id)).ToList();
            genericProducts = _genericProductRepository.Query(model => listIds.Contains(model.ref_web_id)).ToList();
            // Insert root categories
            foreach (var rootCat in mySqlData)
            {
                //mySqlData.Remove(rootCat);
                var dbId = AddOrUpdateModel(rootCat);
                //if (dbId != -1)
                //{
                //    rootCat.id = dbId;
                //    MappingSublist(rootCat, mySqlData);
                //}
            }
        }

        /// <summary>
        /// Update data when exists, create new when not exists.
        /// </summary>
        /// <param name="rootModel"></param>
        /// <returns></returns>
        private int AddOrUpdateModel(GenericProduct rootModel)
        {
            try
            {
                // Get brand id
                var sqlServerBrandId = 0;
                var sqlBrand = brands.FirstOrDefault(model => model.ref_brand_id == rootModel.brand_id);
                if (sqlBrand == null)
                {
                    sqlServerBrandId = sqlBrand.id;
                }
                rootModel.brand_id = sqlServerBrandId;

                // Check exists from sql server DB.
                var sqlServerItem = genericProducts.FirstOrDefault(model => model.ref_web_id == rootModel.ref_web_id);
                if (null == sqlServerItem)
                {
                    _genericProductRepository.Add(rootModel);
                }
                else
                {
                    rootModel.id = sqlServerItem.id;
                    rootModel.created_by = sqlServerItem.created_by;
                    rootModel.created_date = sqlServerItem.created_date;
                    _genericProductRepository.Update(rootModel);
                }
                SaveChanges();
                return rootModel.id;
            }
            catch (Exception ex)
            {
                System.Diagnostics.Trace.WriteLine(ex.Message);
                return -1;
            }
        }
    }
}

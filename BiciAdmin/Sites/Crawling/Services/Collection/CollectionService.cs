﻿using System;
using System.Threading.Tasks;
using System.Net.Http;
using Crawling.Model;
using System.Net.Http.Headers;
using DataCore.Data.Repositories;
using DataCore.Models;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;

namespace Crawling.Services
{
    public class CollectionService : BaseService, IService
    {
        private readonly HttpClient http;
        private readonly ICollectionRepository _collectionRepository;
        private readonly ICollectionMediaRepository _collectionMediaRepository;

        public CollectionService(Crawling.Configs.AppSettings appSettings)
            : base(appSettings)
        {
            _collectionRepository = new CollectionRepository(this.databaseFactory);
            _collectionMediaRepository = new CollectionMediaRepository(this.databaseFactory);

            var cookie = DetectCookies();

            http = new HttpClient();
            var urlService = appSetting.CrawTools.UrlBase + "admin/Collection/CollectionSearch";
            this.http.BaseAddress = new Uri(urlService);
            this.http.DefaultRequestHeaders.Authorization =
                new AuthenticationHeaderValue("Basic", appSetting.CrawTools.TokenPass);
            this.http.DefaultRequestHeaders.Add("Cookie", cookie);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public async Task<bool> SyncData()
        {
            var requestModel = new RequestModel()
            {
                Page = new Page()
                {
                    CurrentPage = 1,
                    PageSize = 1000
                }
            };
            HttpResponseMessage response = this.http.PostAsJsonAsync(this.http.BaseAddress, requestModel).Result;
            var datas = response.Content.ReadAsAsync<ResponseFullModel<HCollection>>().Result;

            foreach (var collection in datas.Data.Data)
            {
                var newCollection = _collectionRepository.Query(model => model.ref_web_id == collection.Id).FirstOrDefault();
                var isUpdate = true;
                if (newCollection == null)
                {
                    newCollection = new Collection();
                    isUpdate = false;
                }
                newCollection.name = collection.Title;
                newCollection.ref_web_id = collection.Id;
                newCollection.status = collection.Visibility ? byte.Parse("1") : byte.Parse("0");

                if (isUpdate)
                {
                    _collectionRepository.Update(newCollection);
                }
                else
                {
                    _collectionRepository.Add(newCollection);
                }
                SaveChanges();
                // Delete all media
                _collectionMediaRepository.Delete(model => model.collection_id == newCollection.id);
                SaveChanges();
                _collectionMediaRepository.Add(new CollectionMedia()
                {
                    collection_id = newCollection.id,
                    image_link = collection.ImageUrl
                });
                SaveChanges();
            }

            return true;
        }
    }
}

﻿using System;
using System.Threading.Tasks;
using System.Net.Http;
using Crawling.Model;
using System.Net.Http.Headers;
using DataCore.Data.Repositories;
using DataCore.Models;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;

namespace Crawling.Services
{
    public class CollectionProductService : BaseService, IService
    {
        private readonly HttpClient http;
        private readonly ICollectionRepository _collectionRepository;
        private readonly IGenericProductRepository _genericProductRepository;
        private readonly IProductCollectionRepository _productCollectionRepository;

        public CollectionProductService(Crawling.Configs.AppSettings appSettings)
            : base(appSettings)
        {
            _collectionRepository = new CollectionRepository(this.databaseFactory);
            _genericProductRepository = new GenericProductRepository(this.databaseFactory);
            _productCollectionRepository = new ProductCollectionRepository(this.databaseFactory);

            //var cookie = DetectCookies();

            http = new HttpClient();
            var urlService = appSetting.CrawTools.UrlBase + "admin/collects.json";
            this.http.BaseAddress = new Uri(urlService);
            this.http.DefaultRequestHeaders.Authorization =
                new AuthenticationHeaderValue("Basic", appSetting.CrawTools.TokenPass);
            //this.http.DefaultRequestHeaders.Add("Cookie", cookie);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public async Task<bool> SyncData()
        {
            HttpResponseMessage response = this.http.GetAsync(this.http.BaseAddress).Result;
            var datas = response.Content.ReadAsAsync<ResponseCollect>().Result;
            var collecIds = datas.collects.Select(model => model.collection_id).Distinct().ToArray();

            foreach (var cId in collecIds)
            {
                var collection = _collectionRepository.Query(model => model.ref_web_id == cId).FirstOrDefault();
                if (collection != null)
                {
                    // Delete all collection
                    _productCollectionRepository.Delete(model => model.collection_id == collection.id);
                    SaveChanges();
                }
                var listCollection = datas.collects.Where(model => model.collection_id == cId).ToArray();
                foreach (var collect in listCollection)
                {
                    var genericProduct = _genericProductRepository.Query(model => model.ref_web_id == collect.product_id).FirstOrDefault();
                    var collectEntity = _collectionRepository.Query(model => model.ref_web_id == collect.collection_id).FirstOrDefault();
                    var newCProduct = new ProductCollection()
                    {
                        collection_id = collectEntity.id,
                        product_id = genericProduct.id,
                    };
                    _productCollectionRepository.Add(newCProduct);
                }
                SaveChanges();
            }
            return true;
        }
    }
}
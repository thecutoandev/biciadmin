﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using Crawling.Model;
using DataCore.Data.Infrastructure;
using DataCore.Data.Repositories;
using DataCore.Models;

namespace Crawling.Services
{
    public interface IOrderService : IService
    {
        bool SyncSingleData(HOrder order, bool isFromWebHook = false);
    }

    public class OrderService : BaseService, IOrderService
    {
        private readonly HttpClient http;
        private readonly IGenericProductRepository _genericProductRepository;
        private readonly IGenericProductMediaRepository _genericProductMediaRepository;
        private readonly IProductMediaRepository _productMediaRepository;
        private readonly IProductRepository _productRepository;
        private readonly IProductAttributeRepository _productAttributeRepository;
        private readonly Dictionary<string, string> variantColors;
        private readonly IUserRepository _userRepository;
        private readonly IOrderRepository _orderRepository;
        private readonly IOrderItemRepository _orderItemRepository;
        private readonly IOrderSummaryRepository _orderSummaryRepository;
        private readonly IUserAddressRepository _userAddressRepository;
        private readonly ICityRepository _cityRepository;
        private readonly IDistrictRepository _districtRepository;
        private readonly IBatchJobsRepository _batchJobsRepository;
        private const int MAX_LIMIT = 50;
        private const string JOB_NAME = "ORDERS";
        private DateTime? JobTime = null;

        public OrderService(Crawling.Configs.AppSettings appSettings,
                            IGenericProductRepository genericProductRepository,
                            IUserRepository userRepository,
                            IGenericProductMediaRepository genericProductMediaRepository,
                            IProductRepository productRepository,
                            IProductMediaRepository productMediaRepository,
                            IProductAttributeRepository productAttributeRepository,
                            IOrderRepository orderRepository,
                            IUserAddressRepository userAddressRepository,
                            ICityRepository cityRepository,
                            IDistrictRepository districtRepository,
                            IOrderSummaryRepository orderSummaryRepository,
                            IOrderItemRepository orderItemRepository,
                            IBatchJobsRepository batchJobsRepository,
                            IUnitOfWork unitOfWork
                            )
            : base(appSettings, unitOfWork)
        {
            try
            {
                _genericProductRepository = genericProductRepository;
                _genericProductMediaRepository = genericProductMediaRepository;
                _productRepository = productRepository;
                _productMediaRepository = productMediaRepository;
                _productAttributeRepository = productAttributeRepository;
                _orderRepository = orderRepository;
                _userAddressRepository = userAddressRepository;
                _cityRepository = cityRepository;
                _districtRepository = districtRepository;
                _orderSummaryRepository = orderSummaryRepository;
                _orderItemRepository = orderItemRepository;
                _userRepository = userRepository;
                _batchJobsRepository = batchJobsRepository;

                http = new HttpClient();
                var urlService = appSetting.CrawTools.UrlBase + "admin/orders.json";
                this.http.BaseAddress = new Uri(urlService);
                this.http.DefaultRequestHeaders.Authorization =
                    new AuthenticationHeaderValue("Basic", appSetting.CrawTools.TokenPass);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public async Task<bool> SyncData()
        {
            long countData = 0;
            var batchHistory = new BatchJobs()
            {
                BeginAt = DateTime.Now,
                JobName = JOB_NAME,
                SuccessRecords = 0,
                Status = 1,
                EndAt = DateTime.Now
            };
            try
            {
                var pages = GetCountPages(ref countData);
                batchHistory.NumberOfRecords = countData;
                _batchJobsRepository.Add(batchHistory);
                SaveChanges();
                for (int page = 1; page < pages + 1; page++)
                {
                    var url = $"{this.http.BaseAddress.ToString()}?{GetQueryString()}&limit={MAX_LIMIT}&page={page}";
                    var destUri = new Uri(url);
                    HttpResponseMessage response = this.http.GetAsync(destUri).Result;
                    var datas = response.Content.ReadAsAsync<ResponseOrder>().Result;
                    foreach (var order in datas.orders)
                    {
                        var result = SyncSingleData(order);
                        if (result)
                        {
                            batchHistory.SuccessRecords += 1;
                        }
                        
                    }
                }
                batchHistory.Status = 2;
                return true;
            }
#if DEBUG
            catch (Exception ex)
#else
            catch
#endif
            {
                batchHistory.Status = 3;
                return false;
            }
            finally
            {
                batchHistory.EndAt = DateTime.Now;
                _batchJobsRepository.Update(batchHistory);
                SaveChanges();
            }
        }

        private int GetCountPages(ref long countData)
        {
            var urlService = appSetting.CrawTools.UrlBase + "admin/orders/count.json?" + GetQueryString();
            var baseUri = new Uri(urlService);
            HttpResponseMessage response = this.http.GetAsync(baseUri).Result;
            var countProducts = response.Content.ReadAsAsync<HProductCount>().Result;
            var pages = countProducts.count / MAX_LIMIT;
            countData = countProducts.count;
            return pages + 1;
        }

        private string GetQueryString()
        {
            var query = string.Empty;
            if (JobTime == null)
            {
                var history = _batchJobsRepository.Query(model => model.JobName == JOB_NAME && model.Status >= 2)
                                              .OrderByDescending(model => model.id)
                                              .FirstOrDefault();
                if (history != null)
                {
                    JobTime = history.EndAt;
                }
                else
                {
                    JobTime = new DateTime();
                }

            }
            query = "updated_at_min=" + $"{JobTime.Value.ToString("yyyy-MM-dd HH:mm")}";
            return query;
        }

        public bool SyncSingleData(HOrder order, bool isFromWebHook = false)
        {
            try
            {
                var modeInsert = false;
                var newOrder = _orderRepository.Query(model => model.code == order.order_number).FirstOrDefault();
                if (newOrder != null)
                {
                    //newOrder = new Order();
                    switch (newOrder.status)
                    {
                        case (int)Configs.Constants.OrderStatus.CompleteOrder:
                        case (int)Configs.Constants.OrderStatus.Canceled:
                            return true;
                        default:
                            break;
                    }
                    //modeInsert = true;
                }
                else
                {
                    if (order.cancelled_at != null)
                    {
                        return true;
                    }
                    modeInsert = true;
                    newOrder = new Order();
                }
                var customer = order.customer;
                var existsUser = _userRepository.Query(model => model.Id == customer.id.ToString()).FirstOrDefault();
                if (existsUser == null)
                {
                    var user = _userRepository.RegisterUser(new UserViewModel()
                    {
                        Id = customer.id.ToString(),
                        Email = customer.email,
                        FullName = customer.first_name + " " + customer.last_name,
                        UserName = customer.id.ToString(),
                        PhoneNumber = customer.phone,
                    }).Result;
                }

                var city = _cityRepository.Query(model => model.name == ("Thành phố " + order.shipping_address.province) ||
                                                          model.name == ("Tỉnh " + order.shipping_address.province)
                                                ).FirstOrDefault();
                if (city == null)
                {
                    city = CrawCity(order.shipping_address);
                }
                // Crawling data district.
                var district = _districtRepository.Query(model => model.name == order.shipping_address.district).FirstOrDefault();
                if (district == null)
                {
                    district = CrawDistrict(order.shipping_address, city.id);
                }

                var address = order.shipping_address.address1 + ", " + order.shipping_address.ward;
                address = address.ToLower();

                var userAddress = _userAddressRepository.Query(model => model.username == customer.id.ToString() &&
                                                                        model.address.ToLower() == address &&
                                                                        model.city_id == city.id &&
                                                                        model.district_id == district.id).FirstOrDefault();
                // Create user address if not exists
                if (userAddress == null)
                {
                    var newAddress = new UserAddress()
                    {
                        address = address,
                        city_id = city.id,
                        district_id = district.id,
                        username = customer.id.ToString(),
                        receiver_name = order.shipping_address.name,
                        receiver_phoneno = order.shipping_address.phone
                    };
                    _userAddressRepository.Add(newAddress);
                    SaveChanges();
                    userAddress = newAddress;
                }

                // Create or update order infomation
                // Set data
                if (newOrder == null)
                {
                    newOrder = new Order();
                }
                newOrder.ref_web_id = order.id;
                newOrder.code = order.order_number;
                newOrder.note = string.IsNullOrEmpty(order.note) ? string.Empty : order.note;
                newOrder.user_id = customer.id.ToString();
                newOrder.user_address_id = userAddress.id;
                newOrder.source = "Web";
                newOrder.created_date = order.created_at;

                if (order.confirmed_at != null)
                {
                    newOrder.status = (int)Configs.Constants.OrderStatus.WaitingProccess;
                }

                if (order.cancelled_at != null)
                {
                    newOrder.status = (int)Configs.Constants.OrderStatus.Canceled;
                }

                // Detect Mode
                if (modeInsert)
                {
                    _orderRepository.Add(newOrder);
                }
                else
                {
                    _orderRepository.Update(newOrder);
                }
                SaveChanges();



                // Create or update order summary.
                var newSummary = _orderSummaryRepository.Query(model => model.order_id == newOrder.id).FirstOrDefault();
                if (newSummary == null)
                {
                    newSummary = new OrderSummary()
                    {
                        order_id = newOrder.id,
                        sub_total = order.subtotal_price,
                        shipping_fee_amount = order.shipping_lines[0].price,
                        total_amount = order.subtotal_price + order.shipping_lines[0].price,
                    };

                    _orderSummaryRepository.Add(newSummary);
                }
                else
                {
                    newSummary.order_id = newOrder.id;
                    newSummary.sub_total = order.subtotal_price;
                    newSummary.shipping_fee_amount = order.shipping_lines[0].price;
                    newSummary.total_amount = order.subtotal_price + order.shipping_lines[0].price;
                    _orderSummaryRepository.Update(newSummary);
                }
                SaveChanges();

                // Create or update order items.
                _orderItemRepository.Delete(model => model.order_id == newOrder.id);
                SaveChanges();
                foreach (var orderItem in order.line_items)
                {
                    var product = _productRepository.Query(model => model.ref_web_id == orderItem.variant_id).FirstOrDefault();
                    var newItem = new OrderItem()
                    {
                        order_id = newOrder.id,
                        product_id = product == null ? orderItem.variant_id : product.id,
                        quantity = orderItem.quantity,
                        unit_price = orderItem.price,
                        amount = orderItem.price * orderItem.quantity
                    };
                    _orderItemRepository.Add(newItem);
                }
                SaveChanges();
                return true;
            }
#if DEBUG
            catch(Exception ex)
#else
            catch
#endif
            {
                return false;
            }
            finally
            {

            }
        }

        private City CrawCity(HAddress address)
        {
            try
            {
                //var getMaxIdCity = _cityRepository.GetAllQueryable().Max(model => model.id);
                var newCity = new City()
                {
                    //id = getMaxIdCity + 1,
                    name = $"Tỉnh {address.province}"
                };
                _cityRepository.Add(newCity);
                SaveChanges();
                return newCity;
            }
            catch
            {
                return new City();
            }
        }

        private District CrawDistrict(HAddress address, int cityId)
        {
            try
            {
                //var maxDistrictId = _districtRepository.GetAllQueryable().Max(model => model.id);
                var newCity = new District()
                {
                    //id = maxDistrictId + 1,
                    name = address.district,
                    city_id = cityId,
                };
                _districtRepository.Add(newCity);
                SaveChanges();
                return newCity;
            }
#if DEBUG
            catch (Exception ex)
#else
            catch
#endif
            {
                return new District();
            }
        }
    }
}

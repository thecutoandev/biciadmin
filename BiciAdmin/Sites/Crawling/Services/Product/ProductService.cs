﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Crawling.Model;
using DataCore.Data.Infrastructure;
using DataCore.Data.Repositories;
using DataCore.Models;
using Newtonsoft.Json;

namespace Crawling.Services
{
    public interface IProductService : IService
    {
        bool SyncSingleProduct(HProduct product, Dictionary<string, int> dataCategories = null, Dictionary<string, int> dataBrands = null, long productId = -1);
    }

    public class ProductService : BaseService, IProductService
    {
        private readonly HttpClient http;
        private readonly IGenericProductRepository _genericProductRepository;
        private readonly IGenericProductMediaRepository _genericProductMediaRepository;
        private readonly IProductMediaRepository _productMediaRepository;
        private readonly IProductRepository _productRepository;
        private readonly IProductAttributeRepository _productAttributeRepository;
        private readonly ICategoryRepository _categoryRepository;
        private readonly IProductTypeRepository _productTypeRepository;
        private readonly IBrandRepository _brandRepository;
        private readonly IBatchJobsRepository _batchJobsRepository;
        private readonly Dictionary<string, string> variantColors;
        private const int MAX_LIMIT = 50;
        private const string JOB_NAME = "PRODUCTS";
        private string[] productScopes = { "global", "web"};
        private DateTime? JobTime = null;

        public ProductService(Crawling.Configs.AppSettings appSettings,
            IGenericProductRepository genericProductRepository,
            IGenericProductMediaRepository genericProductMediaRepository,
            IProductRepository productRepository,
            IProductMediaRepository productMediaRepository,
            IProductAttributeRepository productAttributeRepository,
            ICategoryRepository categoryRepository,
            IBrandRepository brandRepository,
            IBatchJobsRepository batchJobsRepository,
            IProductTypeRepository productTypeRepository,
            IUnitOfWork unitOfWork
            )
            :base(appSettings, unitOfWork)
        {
            _genericProductRepository = genericProductRepository;
            _genericProductMediaRepository = genericProductMediaRepository;
            _productRepository = productRepository;
            _productMediaRepository = productMediaRepository;
            _productAttributeRepository = productAttributeRepository;
            _categoryRepository = categoryRepository;
            _brandRepository = brandRepository;
            _batchJobsRepository = batchJobsRepository;
            _productTypeRepository = productTypeRepository;

            variantColors = JsonConvert.DeserializeObject<Dictionary<string, string>>(appSetting.CrawTools.VariantColors);
            http = new HttpClient();
            var urlService = appSetting.CrawTools.UrlBase + "admin/products.json";
            this.http.BaseAddress = new Uri(urlService);
            this.http.DefaultRequestHeaders.Authorization =
                new AuthenticationHeaderValue("Basic", appSetting.CrawTools.TokenPass);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public async Task<bool> SyncData()
        {
            long countData = 0;
            var pages = GetCountPages(ref countData);
            var batchHistory = new BatchJobs()
            {
                BeginAt = DateTime.Now,
                JobName = JOB_NAME,
                NumberOfRecords = countData,
                SuccessRecords = 0,
                Status = 1,
                EndAt = DateTime.Now
            };
            _batchJobsRepository.Add(batchHistory);
            SaveChanges();

            try
            {
                for (int page = 1; page < pages + 1; page++)
                {
                    var url = $"{this.http.BaseAddress.ToString()}?{GetQueryString()}&limit={MAX_LIMIT}&page={page}";
                    var destUri = new Uri(url);
                    HttpResponseMessage response = this.http.GetAsync(destUri).Result;
                    var datas = response.Content.ReadAsAsync<ResponseProduct>().Result;
                    // Crwaling categories
                    var categoryNames = datas.products.Select(model => model.product_type).Distinct().ToList();
                    var dataCategories = CrawCategories(categoryNames);
                    // Crawling brands.
                    var brandNames = datas.products.Select(model => model.vendor).Distinct().ToList();
                    var dataBrands = CrawBrands(brandNames);

                    foreach (var product in datas.products)
                    {
                        var result = SyncSingleProduct(product, dataCategories, dataBrands);
                        if (result)
                        {
                            batchHistory.SuccessRecords += 1;
                        }
                    }
                }
                batchHistory.Status = 2;
                return true;
            }
            catch
            {
                batchHistory.Status = 3;
                return false;
            }
            finally
            {
                batchHistory.EndAt = DateTime.Now;
                _batchJobsRepository.Update(batchHistory);
                SaveChanges();
                GC.Collect();
            }
        }

        private int GetCountPages(ref long countData)
        {
            var urlService = appSetting.CrawTools.UrlBase + "admin/products/count.json?" + GetQueryString();
            var baseUri = new Uri(urlService);
            HttpResponseMessage response = this.http.GetAsync(baseUri.ToString()).Result;
            var countProducts = response.Content.ReadAsAsync<HProductCount>().Result;
            var pages = countProducts.count / MAX_LIMIT;
            countData = countProducts.count;
            return pages + 1;
        }

        public HProduct GetHProduct(long productId)
        {
            try
            {
                var urlService = $"{appSetting.CrawTools.UrlBase}admin/products/{productId}.json";
                var baseUri = new Uri(urlService);
                HttpResponseMessage response = this.http.GetAsync(baseUri.ToString()).Result;
                var productResp = response.Content.ReadAsAsync<HProductResp>().Result;
                return productResp.product;
            }
            catch (Exception ex)
            {
                return null;
            }
            
        }

        private string GetQueryString()
        {
            var query = string.Empty;
            if (JobTime == null)
            {
                var history = _batchJobsRepository.Query(model => model.JobName == JOB_NAME && model.Status >= 2)
                                              .OrderByDescending(model => model.id)
                                              .FirstOrDefault();
                if (history != null)
                {
                    JobTime = history.EndAt;
                }
                else
                {
                    JobTime = new DateTime();
                }
                
            }
            query = "updated_at_min=" + $"{JobTime.Value.ToString("yyyy-MM-dd HH:mm")}";
            return query;
        }


        public bool SyncSingleProduct(HProduct product,
                                      Dictionary<string, int> dataCategories = null,
                                      Dictionary<string, int> dataBrands = null,
                                      long hId = -1)
        {
            try
            {
                if (hId != -1)
                {
                    product = GetHProduct(hId);
                }
                var exData = _genericProductRepository.Query(model => model.ref_web_id == product.id).FirstOrDefault();
                if (exData != null)
                {
                    if (product.published_at == null || !productScopes.Contains(product.published_scope))
                    {
                        exData.hidden = true;
                    }
                }
                else
                {
                    if (product.published_at == null || !productScopes.Contains(product.published_scope))
                    {
                        return true;
                    }
                }

                if (dataCategories == null)
                {
                    var catNames = new List<string>();
                    catNames.Add(product.product_type);
                    dataCategories = CrawCategories(catNames);
                }

                if (dataBrands == null)
                {
                    var brandNames = new List<string>();
                    brandNames.Add(product.vendor);
                    dataCategories = CrawBrands(brandNames);
                }
                var isImageVariant = true;
                // get list id exists
                //var idFromHaravan = datas.products.Select(model => model.id).ToArray();
                
                //var exData = dataIds.FirstOrDefault(model => model.ref_web_id == product.id);
                GenericProduct entity;
                if (exData != null)
                {
                    entity = exData;
                    entity.ref_web_id = product.id;
                    entity.name = product.title;
                    entity.product_name = product.title;
                    entity.product_content = product.body_html;
                    entity.updated_date = product.updated_at;
                    _genericProductRepository.Update(entity);
                }
                else
                {
                    entity = new GenericProduct()
                    {
                        ref_web_id = product.id,
                        name = product.title,
                        product_content = product.body_html,
                        created_date = product.published_at,
                        updated_date = product.updated_at,
                        product_name = product.title,
                        category_id = dataCategories[product.product_type],
                        brand_id = dataBrands[product.vendor],
                        product_price = 0,
                        is_free_ship = true,
                        hidden = false
                    };
                    _genericProductRepository.Add(entity);
                }

                SaveChanges();
                if (entity.id != 0)
                {
                    _genericProductMediaRepository.Delete(model => model.generic_product_id == entity.id);
                    SaveChanges();
                    // Craw generic images.
                    foreach (var img in product.images.OrderBy(p => p.position))
                    {
                        var imgEntity = new GenericProductMedia()
                        {
                            generic_product_id = entity.id,
                            ref_wed_id = img.id,
                            image_link = img.src,
                            position = img.position
                        };
                        _genericProductMediaRepository.Add(imgEntity);
                    }
                    SaveChanges();

                    // Crawling product.
                    product.variants = product.variants == null ? new List<HVariant>() : product.variants;
                    if (product.variants.Count > 0)
                    {
                        var variantExists = product.variants.Select(model => model.id).ToArray();
                        var productExists = _productRepository.Query(model => variantExists.Contains(model.ref_web_id)).ToList();
                        // Craw variants
                        foreach (var variant in product.variants)
                        {
                            var existsEntity = productExists.FirstOrDefault(model => model.ref_web_id == variant.id);
                            Product variantEntity = new Product();
                            if (existsEntity != null)
                            {
                                variantEntity = existsEntity;
                            }
                            variantEntity.generic_product_id = entity.id;
                            variantEntity.product_code = variant.id.ToString();
                            variantEntity.product_name = variant.title;
                            variantEntity.created_date = variant.created_at;
                            variantEntity.updated_date = variant.updated_at;
                            variantEntity.ref_web_id = variant.id;
                            variantEntity.kiot_product_code = variant.sku;

                            // detect price
                            if (variant.compare_at_price != 0)
                            {
                                variantEntity.product_price = variant.compare_at_price;
                                variantEntity.selling_price = variant.price;
                            }
                            else
                            {
                                variantEntity.product_price = variant.price;
                                variantEntity.selling_price = variant.price;
                            }

                            // Detect generic price
                            if (entity.product_price == 0 || entity.product_price > variantEntity.selling_price)
                            {
                                entity.product_price = variantEntity.selling_price;
                            }

                            // If exists entity then update entity.
                            if (existsEntity != null)
                            {
                                _productRepository.Update(variantEntity);
                            }
                            else
                            {
                                _productRepository.Add(variantEntity);
                            }
                            SaveChanges();

                            // Crawling product images.
                            var productId = variantEntity.id;
                            _productMediaRepository.Delete(model => model.product_id == productId);
                            SaveChanges();

                            var listVariantImages = product.images.Where(model => model.variant_ids.Contains(variant.id)).ToArray();
                            if (listVariantImages.Length <= 0)
                            {
                                isImageVariant = false;
                            }

                            var posIndex = 1;
                            foreach (var img in listVariantImages)
                            {
                                var imgEntity = new ProductMedia()
                                {
                                    image_link = img.src,
                                    product_id = productId,
                                    position = posIndex
                                };
                                _productMediaRepository.Add(imgEntity);
                                posIndex++;
                            }
                            SaveChanges();

                            // Crawling product type.
                            CrawlingProductAttribute(product, variant, variantEntity, isImageVariant);
                        }
                    }
                    // Saving product price
                    _genericProductRepository.Update(entity);
                    SaveChanges();
                }
                return true;
            }
            catch
            {
                return false;
            }
            finally
            {
                GC.Collect();
            }
        }

        private Dictionary<string, int> CrawCategories(List<string> catNames)
        {

            try
            {
                Dictionary<string, int> dicCategories = new Dictionary<string, int>();
                var arrNames = catNames.ToArray();
                var existCategories = _productTypeRepository.Query(model => arrNames.Contains(model.name)).ToList();

                // Mapping data exists
                foreach (var item in existCategories)
                {
                    dicCategories.Add(item.name, item.id);
                    catNames.Remove(item.name);
                }

                foreach (var cat in catNames)
                {
                    var catEntity = new ProductType()
                    {
                        name = cat,
                        updated_date = DateTime.Now,
                        created_date = DateTime.Now
                    };
                    _productTypeRepository.Add(catEntity);
                    SaveChanges();
                    dicCategories.Add(catEntity.name, catEntity.id);
                }

                return dicCategories;
            }
            catch
            {
                return new Dictionary<string, int>();
            }
            
        }

        private Dictionary<string, int> CrawBrands(List<string> brandsNames)
        {

            try
            {
                Dictionary<string, int> dicCategories = new Dictionary<string, int>();
                var arrNames = brandsNames.ToArray();
                var existCategories = _brandRepository.Query(model => arrNames.Contains(model.name)).ToList();

                // Mapping data exists
                foreach (var item in existCategories)
                {
                    dicCategories.Add(item.name, item.id);
                    brandsNames.Remove(item.name);
                }

                foreach (var cat in brandsNames)
                {
                    var catEntity = new Brand()
                    {
                        name = cat,
                        created_date = DateTime.Now,
                        updated_date = DateTime.Now
                    };
                    _brandRepository.Add(catEntity);
                    SaveChanges();
                    dicCategories.Add(catEntity.name, catEntity.id);
                }

                return dicCategories;
            }
            catch
            {
                return new Dictionary<string, int>();
            }
            
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="product"></param>
        /// <param name="variant"></param>
        /// <param name="productEntity"></param>
        /// <param name="isVariantImages"></param>
        /// <returns></returns>
        private bool CrawlingProductAttribute(HProduct product, HVariant variant, Product productEntity, bool isVariantImages)
        {
            try
            {
                _productAttributeRepository.Delete(model => model.product_id == productEntity.id);
                SaveChanges();

                var colorType = product.options.FirstOrDefault(model => appSetting.CrawTools.ColorTag.Contains(model.name.ToLower()));
                if (colorType != null)
                {
                    var colorName = variant["option" + colorType.position].ToString();
                    var colorHex = GetHexColor(colorName);
                    _productAttributeRepository.Add(new ProductAttribute()
                    {
                        product_id = productEntity.id,
                        value = colorHex,
                        attribute_id = 3
                    });
                }
                else
                {
                    var deepDetectColor = false;
                    for (int position = 1; position < 4; position++)
                    {
                        var colorName = variant["option" + colorType.position].ToString();
                        var colorHex = GetHexColor(colorName);
                        if (!string.IsNullOrEmpty(colorHex))
                        {
                            _productAttributeRepository.Add(new ProductAttribute()
                            {
                                product_id = productEntity.id,
                                value = colorHex,
                                attribute_id = 3
                            });
                            deepDetectColor = true;
                            break;
                        }
                    }
                    if (deepDetectColor)
                    {
                        goto savedata;
                    }
                    if (!isVariantImages)
                    {
                        var dataText = new string[] { variant.option1, variant.option2, variant.option3 };
                        var textValue = string.Join('/', dataText.Where(str => !string.IsNullOrEmpty(str)));
                        _productAttributeRepository.Add(new ProductAttribute()
                        {
                            product_id = productEntity.id,
                            value = textValue,
                            attribute_id = 2
                        });
                    }
                    else
                    {
                        var variantImage = product.images.FirstOrDefault(model => model.variant_ids.Contains(variant.id));
                        if (variantImage != null)
                        {
                            _productAttributeRepository.Add(new ProductAttribute()
                            {
                                product_id = productEntity.id,
                                value = variantImage.src,
                                attribute_id = 1
                            });
                        }
                    }
                }
            savedata:
                SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }

        private string GetHexColor(string colorName)
        {
            try
            {
                colorName = colorName.Trim().ToLower();
                colorName = ConvertToUnSign(colorName);
                var colorHex = this.variantColors[colorName];
                return colorHex;
            }
            catch
            {
                return "#FFFFFF";
            }
        }

        private static string ConvertToUnSign(string text)
        {
            for (int i = 33; i < 48; i++)
            {
                if ((char)i == '-')
                {
                    continue;
                }
                text = text.Replace(((char)i).ToString(), "");
            }

            for (int i = 58; i < 65; i++)
            {
                if ((char)i == '-')
                {
                    continue;
                }
                text = text.Replace(((char)i).ToString(), "");
            }

            for (int i = 91; i < 97; i++)
            {
                if ((char)i == '-')
                {
                    continue;
                }
                text = text.Replace(((char)i).ToString(), "");
            }
            for (int i = 123; i < 127; i++)
            {
                if ((char)i == '-')
                {
                    continue;
                }
                text = text.Replace(((char)i).ToString(), "");
            }
            text = text.Replace(" ", "-");
            Regex regex = new Regex(@"\p{IsCombiningDiacriticalMarks}+");
            string strFormD = text.Normalize(System.Text.NormalizationForm.FormD);
            return regex.Replace(strFormD, String.Empty).Replace('\u0111', 'd').Replace('\u0110', 'D');
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DataCore.Data.Repositories;
using DataCore.Models;
using MySql.Data.MySqlClient;

namespace Crawling.Services
{
    public class CategoryCrawlService : BaseService, IService
    {
        private readonly ICategoryRepository _categoryRepository;

        public CategoryCrawlService()
        {
            _categoryRepository = new CategoryRepository(this.databaseFactory);
        }

        /// <summary>
        /// Prepare data from mysql server and sync all data to sql server.
        /// </summary>
        /// <param name="sqlConnection">Connection for mysql</param>
        /// <returns>true: sync data is successfully | false: is failed.</returns>
        public async Task<bool> SyncData(MySqlConnection sqlConnection)
        {
            try
            {
                var listCategories = new List<Category>();
                var command = new MySqlCommand(CategoryQuery.List(), sqlConnection);
                using (var sqlReader = command.ExecuteReader())
                {
                    while (sqlReader.Read())
                    {
                        listCategories.Add(new Category()
                        {
                            ref_category_id = sqlReader.GetInt32((int)CategoryQuery.ColumnIndex.refCategoryId),
                            name = sqlReader.GetString((int)CategoryQuery.ColumnIndex.name),
                            parent_category_id = sqlReader.GetInt32((int)CategoryQuery.ColumnIndex.parentCategoryId),
                            status = 1
                        });
                    }
                }
                MappingData<Category>(listCategories);
                return true;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return false;
            }
        }

        /// <summary>
        /// Insert or update data from mysql to sql server
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="dataSource"></param>
        protected override void MappingData<T>(List<T> dataSource)
        {
            var mySqlData = dataSource.Cast<Category>().ToList();

            // Insert root categories
            var rootCats = mySqlData.Where(cat => cat.parent_category_id == 0);
            foreach (var rootCat in rootCats)
            {
                //mySqlData.Remove(rootCat);
                var dbId = AddOrUpdateCategory(rootCat);
                if (dbId != -1)
                {
                    rootCat.id = dbId;
                    MappingSublist(rootCat, mySqlData);
                }
            }
        }

        private void MappingSublist(Category rootCat, List<Category> categories)
        {
            var list = categories.Where(cat => cat.parent_category_id == rootCat.ref_category_id);
            foreach (var item in list)
            {
                //categories.Remove(item);
                item.parent_category_id = rootCat.id;
                var dbId = AddOrUpdateCategory(item);
                if (dbId != -1)
                {
                    //item.id = -1;
                    MappingSublist(item, categories);
                }
            }
        }

        private int AddOrUpdateCategory(Category rootCat)
        {
            try
            {
                //rootCat.updated_by = "CrawlingTools";
                //rootCat.updated_date = DateTime.Now;
                var sqlServerItem = _categoryRepository.Query(cat => cat.ref_category_id == rootCat.ref_category_id).FirstOrDefault();
                if (null == sqlServerItem)
                {
                    //rootCat.created_by = "CrawlingTools";
                    //rootCat.created_date = DateTime.Now;
                    rootCat.status = 1;
                    _categoryRepository.Add(rootCat);
                }
                else
                {
                    rootCat.id = sqlServerItem.id;
                    rootCat.created_by = sqlServerItem.created_by;
                    rootCat.created_date = sqlServerItem.created_date;
                    _categoryRepository.Update(rootCat);
                }
                SaveChanges();
                return rootCat.id;
            }
            catch(Exception ex)
            {
                System.Diagnostics.Trace.WriteLine(ex.Message);
                return -1;
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DataCore.Data.Repositories;
using DataCore.Models;
using MySql.Data.MySqlClient;

namespace Crawling.Services
{
    public class GenericProductMediaCrawlService : BaseService, IService
    {
        private readonly IGenericProductRepository _genericProductRepository;
        private readonly IGenericProductMediaRepository _genericProductMediaRepository;
        private List<GenericProduct> genericProducts;
        private List<GenericProductMedia> genericProductMedias;

        public GenericProductMediaCrawlService()
        {
            _genericProductRepository = new GenericProductRepository(this.databaseFactory);
            _genericProductMediaRepository = new GenericProductMediaRepository(this.databaseFactory);
            genericProducts = new List<GenericProduct>();
            genericProductMedias = new List<GenericProductMedia>();
        }

        /// <summary>
        /// Prepare data from mysql server and sync all data to sql server.
        /// </summary>
        /// <param name="sqlConnection">Connection for mysql</param>
        /// <returns>true: sync data is successfully | false: is failed.</returns>
        public async Task<bool> SyncData(MySqlConnection sqlConnection)
        {
            try
            {
                var listMedias = new List<GenericProductMedia>();
                var command = new MySqlCommand(GenericProductMediaQuery.List(), sqlConnection);
                using (var sqlReader = command.ExecuteReader())
                {
                    while (sqlReader.Read())
                    {
                        listMedias.Add(new GenericProductMedia()
                        {
                            ref_wed_id = sqlReader.GetInt32((int)GenericProductMediaQuery.ColumnIndex.refWebId),
                            generic_product_id = sqlReader.GetInt32((int)GenericProductMediaQuery.ColumnIndex.genericProductId),
                            image_link = sqlReader.GetString((int)GenericProductMediaQuery.ColumnIndex.imageLink),
                        });
                    }
                }
                MappingData<GenericProductMedia>(listMedias);
                return true;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return false;
            }
        }

        /// <summary>
        /// Insert or update data from mysql to sql server
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="dataSource"></param>
        protected override void MappingData<T>(List<T> dataSource)
        {
            var mySqlData = dataSource.Cast<GenericProductMedia>().ToList();
            // Get exists data
            var listIds = mySqlData.Select(model => model.ref_wed_id).ToArray();
            var listGenericProductIds = mySqlData.Select(model => model.generic_product_id).Distinct().ToArray();
            genericProducts = _genericProductRepository.Query(model => listGenericProductIds.Contains(model.ref_web_id)).ToList();
            genericProductMedias = _genericProductMediaRepository.Query(model => listIds.Contains(model.ref_wed_id)).ToList();
            // Insert root categories
            foreach (var rootCat in mySqlData)
            {
                //mySqlData.Remove(rootCat);
                var dbId = AddOrUpdateModel(rootCat);
            }
        }

        /// <summary>
        /// Update data when exists, create new when not exists.
        /// </summary>
        /// <param name="rootModel"></param>
        /// <returns></returns>
        private int AddOrUpdateModel(GenericProductMedia rootModel)
        {
            try
            {
                // Get brand id
                var sqlProduct = genericProducts.FirstOrDefault(model => model.ref_web_id == rootModel.generic_product_id);
                if (sqlProduct == null)
                {
                    return -1;
                }
                rootModel.generic_product_id = sqlProduct.id;

                // Check exists from sql server DB.
                var sqlServerItem = genericProductMedias.FirstOrDefault(model => model.ref_wed_id == rootModel.ref_wed_id);
                if (null == sqlServerItem)
                {
                    _genericProductMediaRepository.Add(rootModel);
                }
                else
                {
                    rootModel.id = sqlServerItem.id;
                    _genericProductMediaRepository.Update(rootModel);
                }
                SaveChanges();
                return rootModel.id;
            }
            catch (Exception ex)
            {
                System.Diagnostics.Trace.WriteLine(ex.Message);
                return -1;
            }
        }
    }
}

﻿using System;
using System.Text;
using Crawling.Configs;
using Settings = Crawling.Configs.Constants;

namespace Crawling.Services
{
    public class GenericProductMediaQuery
    {
        public static string List()
        {
            var sqlBuilder = new StringBuilder();
            sqlBuilder.AppendLine("SELECT P.ID     AS generic_product_id, ");
            sqlBuilder.AppendLine("	      T.ID     AS ref_web_id,");
            sqlBuilder.AppendLine("       T.guid   AS image_link");
            sqlBuilder.AppendLine("FROM      wp_posts AS P");
            sqlBuilder.AppendLine("LEFT JOIN wp_posts AS T");
            sqlBuilder.AppendLine("          ON P.ID          = T.post_parent");
            sqlBuilder.AppendLine("WHERE        P.post_type   = 'product' ");
            sqlBuilder.AppendLine("         AND P.post_status = 'publish' ");
            sqlBuilder.AppendLine("         AND T.post_type   = 'attachment'");

            // Filter by updated date.
            var dateFromSettings = BootStrapper.Configuration[Settings.CRAWLING_NUMBER_DATE_TRACKING];
            if (!string.IsNullOrEmpty(dateFromSettings) && dateFromSettings != "0")
            {
                var numberDate = int.Parse(dateFromSettings);
                var dateForQuery = DateTime.Today.AddDays(numberDate).ToString("yyyy-MM-dd HH:mm:ss");
                sqlBuilder.AppendLine($"AND   P.post_modified_gmt   >= '{dateForQuery}'");
            }
            var sql = sqlBuilder.ToString();
            return sql;
        }

        public enum ColumnIndex
        {
            genericProductId = 0,
            refWebId = 1,
            imageLink = 2
        }
    }
}

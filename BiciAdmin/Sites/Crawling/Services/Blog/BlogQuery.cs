﻿using System;
using System.Text;
using Crawling.Configs;
using Settings = Crawling.Configs.Constants;
namespace Crawling.Services
{
    public class BlogQuery
    {
        public static string List()
        {
            var sqlBuilder = new StringBuilder();
            sqlBuilder.AppendLine("SELECT P.ID AS ref_web_id,");
            sqlBuilder.AppendLine("       P.post_title AS name,");
            sqlBuilder.AppendLine("       P.post_content AS content,");
            sqlBuilder.AppendLine("       P.guid AS link,");
            sqlBuilder.AppendLine("       P.post_date_gmt AS created_date,");
            sqlBuilder.AppendLine("       P.post_modified_gmt AS updated_date");
            sqlBuilder.AppendLine("FROM wp_posts AS P");
            sqlBuilder.AppendLine("WHERE P.post_status = 'publish'");
            sqlBuilder.AppendLine("AND   P.post_type   = 'post'");

            var dateFromSettings = BootStrapper.Configuration[Settings.CRAWLING_NUMBER_DATE_TRACKING];
            if (!string.IsNullOrEmpty(dateFromSettings) && dateFromSettings != "0")
            {
                var numberDate = int.Parse(dateFromSettings);
                var dateForQuery = DateTime.Today.AddDays(numberDate).ToString("yyyy-MM-dd HH:mm:ss");
                sqlBuilder.AppendLine($"AND   P.post_modified_gmt   >= '{dateForQuery}'");
            }
            var sql = sqlBuilder.ToString();
            return sql;
        }
        public enum ColumnIndex
        {
            ref_web_id = 0,
            name = 1,
            content = 2,
            link = 3,
            created_date = 4,
            updated_date = 5
        }
    }
}

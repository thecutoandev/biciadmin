﻿using System;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using Crawling.Model;
using DataCore.Data.Infrastructure;
using DataCore.Data.Repositories;
using DataCore.Models;

namespace Crawling.Services
{
    public interface IBlogService : IService { }
    public class BlogService : BaseService, IBlogService
    {
        private readonly HttpClient http;
        private readonly HttpClient httpArticle;
        private readonly ICollectionRepository _collectionRepository;
        private readonly ICollectionMediaRepository _collectionMediaRepository;

        private readonly IBlogCollectionRepository _blogCollectionRepository;
        private readonly IBlogRepository _blogRepository;
        private readonly ICollectionBlogRepository _collectionBlogRepository;
        private readonly IBatchJobsRepository _batchJobsRepository;
        private const int MAX_LIMIT = 50;
        private const string JOB_NAME = "BLOGS";
        private DateTime? JobTime = null;

        public BlogService(Crawling.Configs.AppSettings appSettings,
                            ICollectionRepository collectionRepository,
                            ICollectionMediaRepository collectionMediaRepository,
                            IBlogCollectionRepository blogCollectionRepository,
                            IBlogRepository blogRepository,
                            ICollectionBlogRepository collectionBlogRepository,
                            IBatchJobsRepository batchJobsRepository,
                            IUnitOfWork unitOfWork)
            : base(appSettings, unitOfWork)
        {
            _collectionRepository = collectionRepository;
            _collectionMediaRepository = collectionMediaRepository;
            _blogCollectionRepository = blogCollectionRepository;
            _blogRepository = blogRepository;
            _collectionBlogRepository = collectionBlogRepository;
            _batchJobsRepository = batchJobsRepository;

            //var cookie = DetectCookies();

            http = new HttpClient();
            var urlService = appSetting.CrawTools.UrlBase + "admin/blogs.json";
            this.http.BaseAddress = new Uri(urlService);
            this.http.DefaultRequestHeaders.Authorization =
                new AuthenticationHeaderValue("Basic", appSetting.CrawTools.TokenPass);
            //this.http.DefaultRequestHeaders.Add("Cookie", cookie);

            // Init http client for articles
            httpArticle = new HttpClient();
            var urlArticlesService = appSetting.CrawTools.UrlBase + "admin/blogs/{0}/articles.json";
            this.httpArticle.BaseAddress = new Uri(urlArticlesService);
            this.httpArticle.DefaultRequestHeaders.Authorization =
                new AuthenticationHeaderValue("Basic", appSetting.CrawTools.TokenPass);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public async Task<bool> SyncData()
        {
            long countData = 0;
            var batchHistory = new BatchJobs()
            {
                BeginAt = DateTime.Now,
                JobName = JOB_NAME,
                SuccessRecords = 0,
                Status = 1,
                EndAt = DateTime.Now
            };
            try
            {
                var pages = GetCountPages(ref countData);
                batchHistory.NumberOfRecords = countData;
                _batchJobsRepository.Add(batchHistory);
                SaveChanges();
                for (int page = 1; page < pages + 1; page++)
                {
                    var url = $"{this.http.BaseAddress.ToString()}?{GetQueryString()}&limit={MAX_LIMIT}&page={page}";
                    var destUri = new Uri(url);
                    HttpResponseMessage response = this.http.GetAsync(destUri).Result;
                    var datas = response.Content.ReadAsAsync<ResponseBlog>().Result.blogs;
                    foreach (var collection in datas)
                    {
                        var blogCollect = _blogCollectionRepository.Query(model => model.ref_web_id == collection.id).FirstOrDefault();
                        var isUpdate = true;
                        if (blogCollect == null)
                        {
                            blogCollect = new BlogCollection();
                            isUpdate = false;
                        }

                        blogCollect.ref_web_id = collection.id;
                        blogCollect.name = collection.title;

                        if (isUpdate)
                        {
                            _blogCollectionRepository.Update(blogCollect);
                        }
                        else
                        {
                            _blogCollectionRepository.Add(blogCollect);
                        }
                        SaveChanges();
                        batchHistory.SuccessRecords += 1;
                        CrawlingArticles(collection.id, blogCollect.id);
                    }
                    //SaveChanges();
                }
                batchHistory.Status = 2;
                return true;
            }
            catch
            {
                batchHistory.Status = 3;
                return false;
            }
            finally
            {
                batchHistory.EndAt = DateTime.Now;
                _batchJobsRepository.Update(batchHistory);
                SaveChanges();
            }
        }

        private int GetCountPages(ref long countData)
        {
            var urlService = appSetting.CrawTools.UrlBase + "admin/blogs/count.json?" + GetQueryString();
            var baseUri = new Uri(urlService);
            HttpResponseMessage response = this.http.GetAsync(baseUri).Result;
            var countProducts = response.Content.ReadAsAsync<HProductCount>().Result;
            var pages = countProducts.count / MAX_LIMIT;
            countData = countProducts.count;
            return pages + 1;
        }

        private string GetQueryString()
        {
            var query = string.Empty;
            if (JobTime == null)
            {
                var history = _batchJobsRepository.Query(model => model.JobName == JOB_NAME && model.Status >= 2)
                                              .OrderByDescending(model => model.id)
                                              .FirstOrDefault();
                if (history != null)
                {
                    JobTime = history.EndAt;
                }
                else
                {
                    JobTime = new DateTime();
                }

            }
            query = "updated_at_min=" + $"{JobTime.Value.ToString("yyyy-MM-dd HH:mm")}";
            return query;
        }

        // Crawling articles.
        private void CrawlingArticles(int blogId, int dbId)
        {
            var destUrl = string.Format(this.httpArticle.BaseAddress.ToString(), blogId);
            HttpResponseMessage response = this.httpArticle.GetAsync(new Uri(destUrl)).Result;
            var datas = response.Content.ReadAsAsync<ResponseArticle>().Result.articles;

            // Create collection blog.
            _collectionBlogRepository.Delete(model => model.collection_id == dbId);
            SaveChanges();
            foreach (var blog in datas)
            {
                var newBlog = _blogRepository.Query(model => model.ref_web_id == blog.id).FirstOrDefault();
                var isUpdate = true;
                if (newBlog == null)
                {
                    newBlog = new Blog();
                    isUpdate = false;
                }
                newBlog.ref_web_id = blog.id;
                newBlog.name = blog.title;
                newBlog.content = blog.body_html;
                newBlog.link = blog.handle;

                if (isUpdate)
                {
                    _blogRepository.Update(newBlog);
                }
                else
                {
                    _blogRepository.Add(newBlog);
                }
                SaveChanges();

                var newEntity = new CollectionBlog()
                {
                    ref_blog_id = blog.id,
                    ref_collection_id = blogId,
                    collection_id = dbId,
                    blog_id = newBlog.id
                };
                _collectionBlogRepository.Add(newEntity);
                SaveChanges();
            }
            //SaveChanges();
        }
    }
}

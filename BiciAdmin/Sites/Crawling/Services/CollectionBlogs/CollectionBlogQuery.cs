﻿using System;
using System.Text;
using Crawling.Configs;
using Settings = Crawling.Configs.Constants;

namespace Crawling.Services
{
    public class CollectionBlogQuery
    {
        public static string List()
        {
            var sqlBuilder = new StringBuilder();
            sqlBuilder.AppendLine("SELECT T.name AS cat_name,");
            sqlBuilder.AppendLine("       T.term_id AS cat_id,");
            sqlBuilder.AppendLine("       P.ID AS blog_id");
            sqlBuilder.AppendLine("FROM wp_posts AS P");
            // JOIN RELATIONSHIPS
            sqlBuilder.AppendLine("LEFT JOIN wp_term_relationships AS R");
            sqlBuilder.AppendLine("ON P.ID = R.object_id");
            // JOIN TAXONOMY
            sqlBuilder.AppendLine("LEFT JOIN wp_term_taxonomy AS X");
            sqlBuilder.AppendLine("ON R.term_taxonomy_id = X.term_taxonomy_id");
            // JOIN POSTS
            sqlBuilder.AppendLine("LEFT JOIN wp_terms AS T");
            sqlBuilder.AppendLine("ON X.term_id = T.term_id");
            // QUERY CONDITIONS
            sqlBuilder.AppendLine("WHERE X.taxonomy = 'category'");
            sqlBuilder.AppendLine("AND P.post_type = 'post'");
            sqlBuilder.AppendLine("AND P.post_status = 'publish'");

            var dateFromSettings = BootStrapper.Configuration[Settings.CRAWLING_NUMBER_DATE_TRACKING];
            if (!string.IsNullOrEmpty(dateFromSettings) && dateFromSettings != "0")
            {
                var numberDate = int.Parse(dateFromSettings);
                var dateForQuery = DateTime.Today.AddDays(numberDate).ToString("yyyy-MM-dd HH:mm:ss");
                sqlBuilder.AppendLine($"AND   P.post_modified_gmt   >= '{dateForQuery}'");
            }

            var sql = sqlBuilder.ToString();
            return sql;
        }
        public enum ColumnIndex
        {
            refCatName = 0,
            refBlogCollectionId = 1,
            refBlogId = 2,
        }
    }
}

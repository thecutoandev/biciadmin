﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DataCore.Data.Repositories;
using DataCore.Models;
using MySql.Data.MySqlClient;

namespace Crawling.Services
{
    public class CollectionBlogCrawlService : BaseService, IService
    {
        private readonly ICollectionBlogRepository _collectionBlogRepository;
        private readonly IBlogCollectionRepository _blogCollectionRepository;
        private readonly IBlogRepository _blogRepository;
        private List<Blog> blogs;
        private List<BlogCollection> blogCollections;

        public CollectionBlogCrawlService()
        {
            _collectionBlogRepository = new CollectionBlogRepository(this.databaseFactory);
            _blogCollectionRepository = new BlogCollectionRepository(this.databaseFactory);
            _blogRepository = new BlogRepository(this.databaseFactory);
            blogs = new List<Blog>();
            blogCollections = new List<BlogCollection>();
        }

        /// <summary>
        /// Prepare data from mysql server and sync all data to sql server.
        /// </summary>
        /// <param name="sqlConnection">Connection for mysql</param>
        /// <returns>true: sync data is successfully | false: is failed.</returns>
        public async Task<bool> SyncData(MySqlConnection sqlConnection)
        {
            try
            {
                var listDatas = new List<CollectionBlog>();
                var command = new MySqlCommand(CollectionBlogQuery.List(), sqlConnection);
                using (var sqlReader = command.ExecuteReader())
                {
                    while (sqlReader.Read())
                    {
                        listDatas.Add(new CollectionBlog()
                        {
                            ref_collection_id = sqlReader.GetInt32((int)CollectionBlogQuery.ColumnIndex.refBlogCollectionId),
                            ref_blog_id = sqlReader.GetInt32((int)CollectionBlogQuery.ColumnIndex.refBlogId)
                        });
                    }
                }
                MappingData<CollectionBlog>(listDatas);
                return true;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return false;
            }
        }

        /// <summary>
        /// Insert or update data from mysql to sql server
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="dataSource"></param>
        protected override void MappingData<T>(List<T> dataSource)
        {
            var mySqlData = dataSource.Cast<CollectionBlog>().ToList();

            // Get exists data blogs.
            var blogIds = mySqlData.Select(model => model.ref_blog_id).ToArray();
            blogs = _blogRepository.Query(model => blogIds.Contains(model.ref_web_id)).ToList();

            // Get exists data collections
            var collectionIds = mySqlData.Select(model => model.ref_collection_id).ToArray();
            blogCollections = _blogCollectionRepository.Query(model => collectionIds.Contains(model.ref_web_id)).ToList();

            // Insert root categories
            Parallel.ForEach(mySqlData, model =>
            {
                var dbId = AddOrUpdateModel(model);
            });
            //foreach (var rootCat in mySqlData)
            //{
            //    //mySqlData.Remove(rootCat);
            //    var dbId = AddOrUpdateModel(rootCat);
            //    //if (dbId != -1)
            //    //{
            //    //    rootCat.id = dbId;
            //    //    MappingSublist(rootCat, mySqlData);
            //    //}
            //}
        }

        private int AddOrUpdateModel(CollectionBlog rootModel)
        {
            try
            {
                // Get blog id
                var blog = blogs.FirstOrDefault(model => model.ref_web_id == rootModel.ref_blog_id);
                if (null == blog)
                {
                    throw new KeyNotFoundException();
                }
                rootModel.blog_id = blog.id;

                // Get collection id
                var collection = blogCollections.FirstOrDefault(model => model.ref_web_id == rootModel.ref_collection_id);
                if (null == collection)
                {
                    throw new KeyNotFoundException();
                }
                rootModel.collection_id = collection.id;

                var sqlServerItem = _collectionBlogRepository.Query(b => b.ref_blog_id == rootModel.ref_blog_id & b.ref_collection_id == rootModel.ref_collection_id).FirstOrDefault();
                if (null == sqlServerItem)
                {
                    _collectionBlogRepository.Add(rootModel);
                }
                else
                {
                    rootModel.id = sqlServerItem.id;
                    _collectionBlogRepository.Update(rootModel);
                }
                SaveChanges();
                return rootModel.id;
            }
            catch (Exception ex)
            {
                System.Diagnostics.Trace.WriteLine(ex.Message);
                return -1;
            }
        }
    }
}

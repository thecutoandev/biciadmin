﻿using System;
using System.Threading.Tasks;

namespace Crawling.Services
{
    public interface IService
    {
        Task<bool> SyncData();
    }
}

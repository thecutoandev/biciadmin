﻿using System;
using System.Text;

namespace Crawling.Services
{
    public class BrandQuery
    {
        public static string List()
        {
            var sqlBuilder = new StringBuilder();
            sqlBuilder.AppendLine("SELECT T.term_id AS ref_category_id,");
            sqlBuilder.AppendLine("       T.name AS name,");
            sqlBuilder.AppendLine("       X.parent AS parent_category_id");
            sqlBuilder.AppendLine("FROM wp_terms AS T");
            sqlBuilder.AppendLine("INNER JOIN wp_term_taxonomy AS X");
            sqlBuilder.AppendLine("ON T.term_id = X.term_id");
            sqlBuilder.AppendLine("WHERE X.taxonomy = 'thuong_hieu_san_pham'");
            sqlBuilder.AppendLine("ORDER BY X.parent");
            var sql = sqlBuilder.ToString();
            return sql;
        }
        public enum ColumnIndex
        {
            refBrandId = 0,
            name = 1,
            parentCategoryId = 2,
        }
    }
}

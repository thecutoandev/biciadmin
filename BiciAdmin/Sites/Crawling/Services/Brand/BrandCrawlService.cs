﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DataCore.Data.Repositories;
using DataCore.Models;
using MySql.Data.MySqlClient;

namespace Crawling.Services
{
    public class BrandCrawlService : BaseService, IService
    {
        private readonly IBrandRepository _brandRepository;
        private List<Brand> brands;

        public BrandCrawlService()
        {
            _brandRepository = new BrandRepository(this.databaseFactory);
            brands = new List<Brand>();
        }

        /// <summary>
        /// Prepare data from mysql server and sync all data to sql server.
        /// </summary>
        /// <param name="sqlConnection">Connection for mysql</param>
        /// <returns>true: sync data is successfully | false: is failed.</returns>
        public async Task<bool> SyncData(MySqlConnection sqlConnection)
        {
            try
            {
                var listBrands = new List<Brand>();
                var command = new MySqlCommand(BrandQuery.List(), sqlConnection);
                using (var sqlReader = command.ExecuteReader())
                {
                    while (sqlReader.Read())
                    {
                        listBrands.Add(new Brand()
                        {
                            ref_brand_id = sqlReader.GetInt32((int)BrandQuery.ColumnIndex.refBrandId),
                            name = sqlReader.GetString((int)BrandQuery.ColumnIndex.name),
                        });
                    }
                }
                MappingData<Brand>(listBrands);
                return true;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return false;
            }
        }

        /// <summary>
        /// Insert or update data from mysql to sql server
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="dataSource"></param>
        protected override void MappingData<T>(List<T> dataSource)
        {
            var mySqlData = dataSource.Cast<Brand>().ToList();
            // Get exists data
            var listIds = mySqlData.Select(model => model.ref_brand_id).ToArray();
            brands = _brandRepository.Query(model => listIds.Contains(model.ref_brand_id)).ToList();
            // Insert root categories
            foreach (var rootCat in mySqlData)
            {
                //mySqlData.Remove(rootCat);
                var dbId = AddOrUpdateModel(rootCat);
                //if (dbId != -1)
                //{
                //    rootCat.id = dbId;
                //    MappingSublist(rootCat, mySqlData);
                //}
            }
        }
        private int AddOrUpdateModel(Brand rootModel)
        {
            try
            {
                //rootCat.updated_by = "CrawlingTools";
                //rootCat.updated_date = DateTime.Now;
                var sqlServerItem = brands.FirstOrDefault(cat => cat.ref_brand_id == rootModel.ref_brand_id);
                if (null == sqlServerItem)
                {
                    _brandRepository.Add(rootModel);
                }
                else
                {
                    rootModel.id = sqlServerItem.id;
                    rootModel.created_by = sqlServerItem.created_by;
                    rootModel.created_date = sqlServerItem.created_date;
                    _brandRepository.Update(rootModel);
                }
                SaveChanges();
                return rootModel.id;
            }
            catch (Exception ex)
            {
                System.Diagnostics.Trace.WriteLine(ex.Message);
                return -1;
            }
        }
    }
}

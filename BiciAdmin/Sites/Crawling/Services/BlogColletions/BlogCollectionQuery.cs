﻿using System;
using System.Text;

namespace Crawling.Services
{
    public class BlogCollectionQuery
    {
        public static string List()
        {
            var sqlBuilder = new StringBuilder();
            sqlBuilder.AppendLine("SELECT distinct");
            sqlBuilder.AppendLine("       T.term_id AS ref_colletion_id,");
            sqlBuilder.AppendLine("       X.parent AS parent_collection_id,");
            sqlBuilder.AppendLine("       T.name AS name");
            sqlBuilder.AppendLine("FROM wp_term_taxonomy AS X");
            // JOIN TERMS
            sqlBuilder.AppendLine("INNER JOIN wp_terms AS T");
            sqlBuilder.AppendLine("ON T.term_id = X.term_id");
            // JOIN RELATIONSHIPS
            sqlBuilder.AppendLine("INNER JOIN wp_term_relationships AS R");
            sqlBuilder.AppendLine("ON X.term_id = R.term_taxonomy_id");
            // JOIN POSTS
            sqlBuilder.AppendLine("INNER JOIN wp_posts AS P");
            sqlBuilder.AppendLine("ON P.ID = R.object_id");
            // QUERY CONDITIONS
            sqlBuilder.AppendLine("WHERE X.taxonomy = 'category'");
            sqlBuilder.AppendLine("AND P.post_type = 'post'");
            sqlBuilder.AppendLine("AND P.post_status = 'publish'");

            var sql = sqlBuilder.ToString();
            return sql;
        }
        public enum ColumnIndex
        {
            refColletionId = 0,
            parentCollectionId = 1,
            name = 2,
        }
    }
}

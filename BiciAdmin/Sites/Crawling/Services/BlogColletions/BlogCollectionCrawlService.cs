﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DataCore.Data.Repositories;
using DataCore.Models;
using MySql.Data.MySqlClient;

namespace Crawling.Services
{
    public class BlogCollectionCrawlService : BaseService, IService
    {
        private readonly IBlogCollectionRepository _blogCollectionRepository;
        private List<BlogCollection> blogCollections;

        public BlogCollectionCrawlService()
        {
            _blogCollectionRepository = new BlogCollectionRepository(this.databaseFactory);
            blogCollections = new List<BlogCollection>();
        }

        /// <summary>
        /// Prepare data from mysql server and sync all data to sql server.
        /// </summary>
        /// <param name="sqlConnection">Connection for mysql</param>
        /// <returns>true: sync data is successfully | false: is failed.</returns>
        public async Task<bool> SyncData(MySqlConnection sqlConnection)
        {
            try
            {
                var listBlogCollection = new List<BlogCollection>();
                var command = new MySqlCommand(BlogCollectionQuery.List(), sqlConnection);
                using (var sqlReader = command.ExecuteReader())
                {
                    while (sqlReader.Read())
                    {
                        listBlogCollection.Add(new BlogCollection()
                        {
                            ref_web_id = sqlReader.GetInt32((int)BlogCollectionQuery.ColumnIndex.refColletionId),
                            name = sqlReader.GetString((int)BlogCollectionQuery.ColumnIndex.name),
                            parent_collection_id = sqlReader.GetInt32((int)BlogCollectionQuery.ColumnIndex.parentCollectionId),
                        });
                    }
                }
                MappingData<BlogCollection>(listBlogCollection);
                return true;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return false;
            }
        }

        /// <summary>
        /// Insert or update data from mysql to sql server
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="dataSource"></param>
        protected override void MappingData<T>(List<T> dataSource)
        {
            var mySqlData = dataSource.Cast<BlogCollection>().ToList();
            // Get exists data
            var listIds = mySqlData.Select(model => model.ref_web_id).ToArray();
            blogCollections = _blogCollectionRepository.Query(model => listIds.Contains(model.ref_web_id)).ToList();
            // Insert root categories
            var rootCats = mySqlData.Where(cat => cat.parent_collection_id == 0);
            foreach (var rootCat in rootCats)
            {
                //mySqlData.Remove(rootCat);
                var dbId = AddOrUpdateCategory(rootCat);
                if (dbId != -1)
                {
                    rootCat.id = dbId;
                    MappingSublist(rootCat, mySqlData);
                }
            }
        }

        private void MappingSublist(BlogCollection rootCat, List<BlogCollection> categories)
        {
            var list = categories.Where(cat => cat.parent_collection_id == rootCat.ref_web_id);
            foreach (var item in list)
            {
                //categories.Remove(item);
                item.parent_collection_id = rootCat.id;
                var dbId = AddOrUpdateCategory(item);
                if (dbId != -1)
                {
                    //item.id = -1;
                    MappingSublist(item, categories);
                }
            }
        }

        private int AddOrUpdateCategory(BlogCollection rootCat)
        {
            try
            {
                //rootCat.updated_by = "CrawlingTools";
                //rootCat.updated_date = DateTime.Now;
                var sqlServerItem = blogCollections.FirstOrDefault(cat => cat.ref_web_id == rootCat.ref_web_id);
                if (null == sqlServerItem)
                {
                    //rootCat.created_by = "CrawlingTools";
                    //rootCat.created_date = DateTime.Now;
                    //rootCat.status = 1;
                    _blogCollectionRepository.Add(rootCat);
                }
                else
                {
                    rootCat.id = sqlServerItem.id;
                    //rootCat.created_by = sqlServerItem.created_by;
                    //rootCat.created_date = sqlServerItem.created_date;
                    _blogCollectionRepository.Update(rootCat);
                }
                SaveChanges();
                return rootCat.id;
            }
            catch (Exception ex)
            {
                System.Diagnostics.Trace.WriteLine(ex.Message);
                return -1;
            }
        }
    }
}

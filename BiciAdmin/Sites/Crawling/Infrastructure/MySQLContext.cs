﻿using System;
using Microsoft.Extensions.Configuration;
using MySql.Data.MySqlClient;

namespace Crawling.Infrastructure
{
    public class MySQLContext : IMySQLContext
    {
        private readonly IConfiguration configuration;
        private MySqlConnection _connection;

        public MySQLContext(IConfiguration configuration)
        {
            this.configuration = configuration;
        }

        public MySqlConnection Get()
        {
            var mySQLconnectionString = configuration.GetConnectionString("BiciWebContext");
            return new MySqlConnection(mySQLconnectionString);
        }
    }
}

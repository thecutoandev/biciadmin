﻿using System;
using MySql.Data.MySqlClient;

namespace Crawling.Infrastructure
{
    public interface IMySQLContext
    {
        MySqlConnection Get();
    }
}

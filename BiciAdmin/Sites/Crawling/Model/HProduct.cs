﻿using System;
using System.Collections.Generic;

namespace Crawling.Model
{
    public class HProduct
    {
        public string body_html { set; get; }
        public DateTime created_at { set; get; }
        public string handle { set; get; }
        public int id { set; get; }
        public string product_type { set; get; }
        public DateTime? published_at { set; get; }
        public string published_scope { set; get; }
        public string template_suffix { set; get; }
        public string title { set; get; }
        public DateTime? updated_at { set; get; }
        public string vendor { set; get; }
        public string tags { set; get; }
        public List<HVariant> variants { set; get; }
        public List<HOption> options { set; get; }
        public List<HImage> images { set; get; }
    }

    public class HProductCount
    {
        public int count { get; set; }
    }

    public class HProductResp
    {
        public HProduct product { get; set; }
    }
}

﻿using System;
namespace Crawling.Model
{
    public class HVariant
    {
        public string barcode {set;get;} // public string 1234_pinkpublic string ,
        public decimal compare_at_price {set;get;} // null,
        public DateTime created_at {set;get;} // public string 2015-03-28T13:31:19-04:00public string ,
        public string fulfillment_service {set;get;} // public string manualpublic string ,
        public decimal grams {set;get;} // 200,
        public int id {set;get;} // 808950810,
        public string inventory_management {set;get;} // public string haravanpublic string ,
        public string inventory_policy {set;get;} // public string continuepublic string ,
        public string option1 {set;get;} // public string Pinkpublic string ,
        public string option2 {set;get;} // null,
        public string option3 {set;get;} // null,
        public int position {set;get;} // 1,
        public decimal price {set;get;} // public string 199.00public string ,
        public int product_id {set;get;} // 632910392,
        public bool requires_shipping {set;get;} // true,
        public string sku {set;get;} // public string IPOD2008PINKpublic string ,
        public bool taxable {set;get;} // true,
        public string title {set;get;} // public string Pinkpublic string ,
        public DateTime updated_at {set;get;} // public string 2015-03-28T13:31:19-04:00public string ,
        public int inventory_quantity {set;get;} // 10,
        public int old_inventory_quantity {set;get;} // 10,
        public int? image_id {set;get;} // 562641783,
        public decimal weight {set;get;} // 0.2,
        public string weight_unit {set;get;} // public string kgpublic string

        public object this[string propertyName]
        {
            get { return this.GetType().GetProperty(propertyName).GetValue(this, null); }
        }
    }
}

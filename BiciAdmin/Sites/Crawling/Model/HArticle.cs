﻿using System;
namespace Crawling.Model
{
    public class HArticle
    {
        public int id { get; set; } //989034056,
        public string title { get; set; } //public string Some crazy article I'm coming up withpublic string ,
        public DateTime? created_at { get; set; } //public string 2008-12-31T19:00:00-05:00public string ,
        public string body_html { get; set; } //public string I have no idea what to write about, but it's going to rock!public string ,
        public int blog_id { get; set; } //241253187,
        public string author { get; set; } //public string Johnpublic string ,
        public long user_id { get; set; } //null,
        public DateTime? published_at { get; set; } //null,
        public DateTime? updated_at { get; set; } //public string 2009-01-31T19:00:00-05:00public string ,
        public string summary_html { get; set; } //null,
        public string template_suffix { get; set; } //null,
        public string handle { get; set; }
        public string tags { get; set; }
    }
}

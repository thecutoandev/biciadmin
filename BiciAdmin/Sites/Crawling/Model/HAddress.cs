﻿using System;
namespace Crawling.Model
{
    public class HAddress
    {
        public string address1 { get; set; } //2B đường 2B
        public string address2 { get; set; } //null,
        public string city { get; set; } //null,
        public string country { get; set; } //Vietnam
        public string first_name { get; set; } //Quang Toan
        public int id { get; set; } //1036069899,
        public string last_name { get; set; } //Phan
        public string phone { get; set; } //0946636691
        public string province { get; set; } //Hồ Chí Minh
        public string name { get; set; } //Phan Quang Toan
        public string province_code { get; set; } //HC
        public string country_code { get; set; } //vn
        public string district { get; set; } //Quận Bình Tân
        public string ward { get; set; } //Phường Bình Hưng Hoà A
    }
}

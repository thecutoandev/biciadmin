﻿using System;
namespace Crawling.Model
{
    public class HCollect
    {
        public int collection_id { get; set; } 
        public DateTime? created_at { get; set; } 
        public bool featured { get; set; } 
        public int id { get; set; } 
        public int position { get; set; } 
        public int product_id { get; set; } 
        public string sort_value { get; set; } 
        public DateTime updated_at { get; set; }
    }
}

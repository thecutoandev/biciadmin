﻿using System;
using System.Collections.Generic;

namespace Crawling.Model
{
    public class ResponseProduct
    {
        public  List<HProduct> products { get; set; }
    }
}

﻿using System;
namespace Crawling.Model
{
    public class ResponseFullModel<T> where T : class
    {
        public ResponseModel<T> Data { get; set; }
        public bool IsError { get; set; }
    }
}

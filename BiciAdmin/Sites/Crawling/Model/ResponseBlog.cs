﻿using System;
using System.Collections.Generic;

namespace Crawling.Model
{
    public class ResponseBlog
    {
        public List<HBlog> blogs { get; set; }
    }
}

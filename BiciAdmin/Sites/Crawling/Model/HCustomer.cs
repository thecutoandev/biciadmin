﻿using System;
using System.Collections.Generic;

namespace Crawling.Model
{
    public class HCustomer
    {
        public int id { get; set; }
        public string email { get; set; }
        public string first_name { get; set; }
        public string last_name { get; set; }
        public string phone { get; set; }
        public List<HAddress> addresses { get; set; }
    }
}

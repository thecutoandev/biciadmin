﻿using System;
namespace Crawling.Model
{
    public class HCollection
    {
        public int Id { get; set; }//1001805110,
        public string Title { get; set; }//public string Nhom SP Test 04public string ,
        public string UrlHandle { get; set; }//public string nhom-sp-test-04public string ,
        public bool IsManualSelect { get; set; }//true,
        public bool Visibility { get; set; }//true,
        public string ConditionDisplay { get; set; }//null,
        public string ListConditionDisplay { get; set; }//null,
        public int ImageId { get; set; }//1125475070,
        public string ImageUrl { get; set; }
        public bool IsDeleted { get; set; }//false,
        public string VersionNo { get; set; }//null
    }
}

﻿using System;
using System.Collections.Generic;

namespace Crawling.Model
{
    public class ResponseOrder
    {
        public List<HOrder> orders { get; set; }
    }
}

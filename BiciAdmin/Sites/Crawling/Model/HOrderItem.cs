﻿using System;
namespace Crawling.Model
{
    public class HOrderItem
    {
        public int product_id { get; set; }
        public int variant_id { get; set; }
        public int quantity { get; set; }
        public decimal price { get; set; }
    }
}

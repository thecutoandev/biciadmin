﻿using System;
using System.Collections.Generic;

namespace Crawling.Model
{
    public class ResponseModel<T> where T : class
    {
        public List<HCollection> Data { get; set; }
        public int TotalRecord { get; set; } 
    }
}

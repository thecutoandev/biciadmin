﻿using System;
using System.Collections.Generic;

namespace Crawling.Model
{
    public class HImage
    {
        public DateTime created_at { set; get; } //public string 2015-03-28T13:31:19-04:00public string ,
        public int id { set; get; } //850703190,
        public int position { set; get; } //1,
        public int product_id { set; get; } //632910392,
        public DateTime updated_at { set; get; } //public string 2015-03-28T13:31:19-04:00public string ,
        public string src { set; get; } //public string https:\/\/cdn.haravan.com\/s\/files\/1\/0006\/9093\/3842\/products\/ipod-nano.png?v=1427563879public string ,
        public List<int> variant_ids { set; get; } //[
    }
}

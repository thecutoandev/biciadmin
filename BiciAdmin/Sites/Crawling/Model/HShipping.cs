﻿using System;
namespace Crawling.Model
{
    public class HShipping
    {
        public string code { get; set; }
        public decimal price { get; set; }
        public string source { get; set; }
        public string title { get; set; }
    }
}

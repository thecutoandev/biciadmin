﻿using System;
using System.Collections.Generic;

namespace Crawling.Model
{
    public class HOrder
    {
        public HAddress billing_address { get; set; }
        public HAddress shipping_address { get; set; }
        public List<HShipping> shipping_lines { get; set; }
        public List<HOrderItem> line_items { get; set; }
        public HCustomer customer { get; set; }
        public string note { get; set; }
        public long user_id { get; set; }
        public decimal subtotal_price { get; set; }
        public string order_number { get; set; }
        public int id { get; set; }
        public DateTime created_at { get; set; }
        public DateTime updated_at { get; set; }
        public DateTime? cancelled_at { get; set; }
        public DateTime? confirmed_at { get; set; }
        public DateTime? closed_at { get; set; }
    }
}

﻿using System;
namespace Crawling.Model
{
    public class RequestModel
    {
        public Page Page { get; set; }
    }

    public class Page
    {
        public int CurrentPage { get; set; }
        public int PageSize { get; set; }
    }
}

﻿using System;
using System.Text;
using Autofac;
using Autofac.Extensions.DependencyInjection;
using AutoMapper;
using DataCore.Data;
using DataCore.Data.Infrastructure;
using DataCore.Data.Repositories;
using DataCore.Models;
using KiotService.Services;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.IdentityModel.Tokens;
using Services.APIServices;
using Services.Mappers;

namespace WebAPI.Configs
{
    public class BootStrapper
    {
        /// <summary>
        /// Setting up for DI container (repository, services)
        /// </summary>
        /// <param name="services"></param>
        /// <param name="configuration"></param>
        /// <returns></returns>
        public static IContainer SetupAutofacContainer(IServiceCollection services, IConfiguration configuration)
        {
            var builder = new ContainerBuilder();

            builder.RegisterType<UnitOfWork>().As<IUnitOfWork>().InstancePerDependency();
            builder.RegisterType<DatabaseFactory>().As<IDatabaseFactory>().InstancePerLifetimeScope();
            // Scan repositories for components
            builder.RegisterAssemblyTypes(typeof(CollectionRepository).Assembly)
                   .Where(instance => instance.Name.EndsWith("Repository", StringComparison.Ordinal))
                   .AsImplementedInterfaces();

            // Scan services for components
            builder.RegisterAssemblyTypes(typeof(CollectionService).Assembly)
                   .Where(instance => instance.Name.EndsWith("Service", StringComparison.Ordinal))
                   .AsImplementedInterfaces();

            builder.RegisterAssemblyTypes(typeof(KProductService).Assembly)
                   .Where(instance => instance.Name.EndsWith("Service", StringComparison.Ordinal))
                   .AsImplementedInterfaces();

            // Init required components.
            if (null != services)
            {
                var connectionString = configuration.GetConnectionString("MainDBContext");
                //var assembly = typeof(MainDBContext).Namespace;
                services.AddDbContext<MainDBContext>(options => options.UseSqlServer(connectionString));
                services.AddIdentity<User, IdentityRole>()
                        .AddEntityFrameworkStores<MainDBContext>()
                        .AddDefaultTokenProviders();

                // Set automaper
                IMapper mapper = AutoMapperConfiguration.AutoMapperConfigInstance().CreateMapper();
                services.AddSingleton(mapper);

                SetupAuthentication(services, configuration);

                builder.Populate(services);
            }
            var container = builder.Build();
            return container;
        }


        /// <summary>
        /// Setting for authorize.
        /// </summary>
        /// <param name="services"></param>
        /// <param name="configuration"></param>
        public static void SetupAuthentication(IServiceCollection services, IConfiguration configuration)
        {
            // Get app settings
            var appSettingsSection = configuration.GetSection("AppSettings");
            services.Configure<AppSettings>(appSettingsSection);

            // Configure jwt authentication
            var appSettings = appSettingsSection.Get<AppSettings>();
            var keyJwt = Encoding.ASCII.GetBytes(appSettings.JwtKey);

            services.AddAuthentication(app =>
            {
                app.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                app.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
            })
            .AddJwtBearer(app =>
            {
                app.RequireHttpsMetadata = false;
                app.SaveToken = true;
                app.TokenValidationParameters = new TokenValidationParameters
                {
                    ValidateIssuerSigningKey = true,
                    IssuerSigningKey = new SymmetricSecurityKey(keyJwt),
                    ValidateIssuer = false,
                    ValidateAudience = false
                };
            });

        }

        /// <summary>
        /// Migrate database.
        /// </summary>
        /// <param name="app"></param>
        public static void UpdateDatabase(IApplicationBuilder app)
        {
            using (var scope = app.ApplicationServices.GetRequiredService<IServiceScopeFactory>().CreateScope())
            {
                scope.ServiceProvider.GetService<MainDBContext>().Database.Migrate();
                scope.ServiceProvider.GetService<MainDBContext>().Database.ExecuteSqlRaw("SET IDENTITY_INSERT dbo.District ON");
            }
        }
    }
}

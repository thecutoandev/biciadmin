﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Services.Interfaces;
using Website.ViewModels;

namespace WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    //[Authorize]
    public class OrdersController : WaoControllerBase
    {
        private readonly IOrderService orderService;
        public OrdersController(IOrderService orderService)
        {
            this.orderService = orderService;
        }

        // GET: api/values
        [HttpPost("list")]
        [ProducesResponseType(typeof(ResponseListModel<OrderAdminViewModel>), 200)]
        [ProducesResponseType(typeof(ResponseStatus), 400)]
        public IActionResult Get(PagingOrder pagingOrder)
        {
            try
            {
                var data = this.orderService.GetAllOrders(pagingOrder);
                return Ok(data);
            }
            catch (Exception ex)
            {
                return WaoBadRequest(ex.Message);
            }
        }

        // GET: api/values
        [HttpGet("{id}")]
        [ProducesResponseType(typeof(ResponseSignleModel<OrderAdminViewModel>), 200)]
        [ProducesResponseType(typeof(ResponseStatus), 400)]
        public IActionResult GetInfo(int id)
        {
            try
            {
                var data = this.orderService.GetInfoByAdmin(id);
                return Ok(data);
            }
            catch (Exception ex)
            {
                return WaoBadRequest(ex.Message);
            }
        }

        [HttpPost("listinfo")]
        [ProducesResponseType(typeof(ResponseListModel<OrderAdminViewModel>), 200)]
        [ProducesResponseType(typeof(ResponseStatus), 400)]
        public IActionResult GetInfos(KiotOrderViewModel kiotOrderViewModel)
        {
            try
            {
                var listOrder = new List<OrderAdminViewModel>();
                foreach (var id in kiotOrderViewModel.order_ids)
                {
                    var data = this.orderService.GetInfoByAdmin(id);
                    listOrder.Add(data.dataset);
                }
                var respData = new ResponseListModel<OrderAdminViewModel>()
                {
                    dataset = listOrder,
                    successfull = true
                };
                return Ok(respData);
            }
            catch (Exception ex)
            {
                return WaoBadRequest(ex.Message);
            }
        }
    }
}

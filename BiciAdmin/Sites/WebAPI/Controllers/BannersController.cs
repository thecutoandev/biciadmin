﻿using System;
using DataCore.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using Services.Interfaces;
using WebAPI.Ultils;
using Website.ViewModels;

namespace WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class BannersController : WaoControllerBase
    {
        private readonly IBannerService bannerService;
        private readonly AppSettings appSettings;
        public BannersController(IBannerService bannerService,
                                 IOptions<AppSettings> appSettings)
        {
            this.bannerService = bannerService;
            this.appSettings = appSettings.Value;
        }

        /// <summary>
        /// Get all actived banner.
        /// </summary>
        /// <returns></returns>
        [HttpGet("{id}")]
        [ProducesResponseType(typeof(ResponseListModel<BannerViewModel>), 200)]
        [ProducesResponseType(typeof(ResponseStatus), 400)]
        public IActionResult Get(int id)
        {
            try
            {
                var data = bannerService.GetInfo(id);
                return Ok(data);
            }
            catch (Exception ex)
            {
                return WaoBadRequest(ex.Message);
            }
        }

        [HttpPost("list")]
        [ProducesResponseType(typeof(ResponseListModel<BannerViewModel>), 200)]
        [ProducesResponseType(typeof(ResponseStatus), 400)]
        public IActionResult Gets(PaggingBase paggingBase)
        {
            try
            {
                var data = bannerService.GetAll(true);
                return Ok(data);
            }
            catch (Exception ex)
            {
                return WaoBadRequest(ex.Message);
            }
        }

        /// <summary>
        /// Get all actived banner.
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [ProducesResponseType(typeof(ResponseStatus), 200)]
        [ProducesResponseType(typeof(ResponseStatus), 400)]
        public IActionResult Post(BannerViewModel bannerViewModel)
        {
            try
            {
                var data = bannerService.Create(bannerViewModel);
                return Ok(data);
            }
            catch (Exception ex)
            {
                return WaoBadRequest(ex.Message);
            }
        }

        /// <summary>
        /// Get all actived banner.
        /// </summary>
        /// <returns></returns>
        [HttpPatch]
        [ProducesResponseType(typeof(ResponseStatus), 200)]
        [ProducesResponseType(typeof(ResponseStatus), 400)]
        public IActionResult Update(BannerViewModel bannerViewModel)
        {
            try
            {
                var data = bannerService.Update(bannerViewModel);
                return Ok(data);
            }
            catch (Exception ex)
            {
                return WaoBadRequest(ex.Message);
            }
        }


        [HttpPost("upload")]
        [ProducesResponseType(typeof(ResponseStatus), 200)]
        [ProducesResponseType(typeof(ResponseStatus), 400)]
        public IActionResult UploadFile(IFormFile file)
        {
            try
            {
                var imageHelper = new ImageHelper(appSettings, "banners");
                var pathUploaded = imageHelper.SaveToDisk(file);

                return Ok(new ResponseStatus()
                {
                    successfull = true,
                    dataset = pathUploaded
                });

            }
            catch (Exception ex)
            {
                return WaoBadRequest(ex.Message);
            }
        }

        /// <summary>
        /// Deleting data.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete("{id}")]
        [ProducesResponseType(typeof(ResponseStatus), 200)]
        [ProducesResponseType(typeof(ResponseStatus), 400)]
        public IActionResult Delete(int id)
        {
            try
            {
                var respdata = bannerService.DeleteById(id);
                return Ok(respdata);
            }
            catch (Exception ex)
            {
                return WaoBadRequest(ex.Message);
            }
        }
    }
}

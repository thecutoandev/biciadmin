﻿using System;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using Services.Interfaces;
using DataCore.Models;
using System.Threading.Tasks;
using Website.ViewModels;

namespace WebAPI.Controllers
{
    [ApiController]
    public class AuthenticationController : WaoControllerBase
    {
        private readonly IUserService _userService;

        public AuthenticationController(IUserService userService)
        {
            this._userService = userService;
        }

        [AllowAnonymous]
        [HttpPost]
        [Route("token")]
        public async Task<ActionResult> Token(AuthReq authReq)
        {
            var userLogin = await _userService.Authenticate(authReq.user_name, authReq.password);
            if (userLogin == null)
            {
                return BadRequest(new ResponseStatus { message = "Mật khẩu hoặc password không đúng", successfull = false });
            }
            if (!userLogin.is_authenticated)
            {
                return Unauthorized(new ResponseStatus()
                {
                    dataset = null,
                    message = "Tài khoản đã bị khóa hoặc bị xóa.",
                    successfull = false,
                });
            }
            return Ok(userLogin);
        }

        [HttpPost]
        [Route("api/users")]
        public async Task<ActionResult> Register(UserAdminReq userViewModel)
        {
            try
            {
                var modelData = new UserViewModel()
                {
                    UserName = userViewModel.user_name,
                    FullName = userViewModel.full_name,
                    PassWord = userViewModel.password,
                    Email = userViewModel.email,
                    PhoneNumber = userViewModel.phone,
                    BirthDay = userViewModel.birth_day,
                    Gender = userViewModel.gender,
                    BranchId = userViewModel.branch_id,
                    RoleId = userViewModel.role_id
                };
                var userLogin = await _userService.Register(modelData);
                if (userLogin == null)
                {
                    return WaoBadRequest("Something when wrong!!!");
                }
                return Ok(new ResponseStatus()
                {
                    dataset = userLogin,
                    message = "Tạo tài khoản đăng nhập thành công.",
                    successfull = true
                });
            }
            catch (Exception ex)
            {
                 return WaoBadRequest(ex.Message);
            }
            
        }

        [HttpPatch]
        [Route("api/reset-password")]
        public async Task<ActionResult> ResetPassword(UserAdminReq userViewModel)
        {
            try
            {
                var userLogin = await _userService.ResetPassword(userViewModel.user_name);
                if (userLogin == null)
                {
                    return WaoBadRequest("Xảy ra lỗi không xác định!!!");
                }
                return Ok(new ResponseStatus()
                {
                    dataset = userLogin,
                    message = "Đặt lại mật khẩu thành công.",
                    successfull = true
                });
            }
            catch (Exception ex)
            {
                return WaoBadRequest(ex.Message);
            }

        }

        [HttpPatch]
        [Route("api/users/deactive")]
        public IActionResult DeactiveUser(UserAdminReq userViewModel)
        {
            try
            {
                var userLogin = _userService.DeactiveUser(userViewModel.user_name);
                return Ok(new ResponseStatus()
                {
                    dataset = userLogin,
                    message = "Khóa người dùng thành công.",
                    successfull = true
                });
            }
            catch (Exception ex)
            {
                return WaoBadRequest(ex.Message);
            }

        }
        [HttpPatch]
        [Route("api/users/active")]
        public IActionResult ActiveUser(UserAdminReq userViewModel)
        {
            try
            {
                var userLogin = _userService.ActiveUser(userViewModel.user_name);
                return Ok(new ResponseStatus()
                {
                    dataset = userLogin,
                    message = "Kích hoạt người dùng thành công.",
                    successfull = true
                });
            }
            catch (Exception ex)
            {
                return WaoBadRequest(ex.Message);
            }

        }

        [HttpPatch]
        [Route("api/users")]
        public async Task<ActionResult> Update(UserAdminReq userViewModel)
        {
            try
            {
                var modelData = new UserViewModel()
                {
                    UserName = userViewModel.user_name,
                    FullName = userViewModel.full_name,
                    PassWord = userViewModel.password,
                    Email = userViewModel.email,
                    PhoneNumber = userViewModel.phone,
                    BirthDay = userViewModel.birth_day,
                    Gender = userViewModel.gender,
                    BranchId = userViewModel.branch_id,
                    Id = userViewModel.user_name,
                    RoleId = userViewModel.role_id
                };
                var userLogin = await _userService.Update(modelData);
                if (userLogin == null)
                {
                    return WaoBadRequest("Something when wrong!!!");
                }
                return Ok(new ResponseStatus()
                {
                    dataset = userLogin,
                    message = "Cập nhật thông tin thành công.",
                    successfull = true
                });
            }
            catch (Exception ex)
            {
                return WaoBadRequest(ex.Message);
            }

        }

        /// <summary>
        /// Get list data.
        /// </summary>
        /// <param name="rContext"></param>
        /// <returns></returns>
        [HttpPost]
        [AllowAnonymous]
        [Route("api/users/findUser")]
        [ProducesResponseType(typeof(ResponseListModel<UserViewModel>), 200)]
        [ProducesResponseType(typeof(ResponseStatus), 400)]
        public IActionResult Gets(PaggingBase rContext)
        {
            try
            {
                var respData = _userService.GetDatatables(rContext);
                return Ok(respData);
            }
            catch (Exception ex)
            {
                return WaoBadRequest(ex.Message);
            }
        }

        [HttpGet("api/users/{userName}")]
        [ProducesResponseType(typeof(ResponseListModel<UserInfoViewModel>), 200)]
        [ProducesResponseType(typeof(ResponseStatus), 400)]
        public IActionResult Get(string userName)
        {
            try
            {
                var data = _userService.GetUser(userName);
                return Ok(data);
            }
            catch (Exception ex)
            {
                return WaoBadRequest(ex.Message);
            }
        }
    }
}

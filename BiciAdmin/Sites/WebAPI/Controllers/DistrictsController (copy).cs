using System;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using Services.Interfaces;
using DataCore.Models;
using System.Threading.Tasks;
using Website.ViewModels;

namespace WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class DistrictsController : WaoControllerBase
    {
        private readonly IDistrictService _districtService;
        public DistrictsController(IDistrictService districtService)
        {
            this._districtService = districtService;
        }

        /// <summary>
        /// Get list districts of city.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("{id}")]
        [AllowAnonymous]
        [ProducesResponseType(typeof(ResponseListModel<DistrictViewModel>), 200)]
        [ProducesResponseType(typeof(ResponseStatus), 400)]
        public IActionResult Get(int id)
        {
            try
            {
                var respData = _districtService.GetDataByCity(id);
                return Ok(respData);
            }
            catch (Exception ex)
            {
                return WaoBadRequest(ex.Message);
            }
        }
    }
}

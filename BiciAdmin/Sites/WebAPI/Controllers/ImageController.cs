﻿using System;
using DataCore.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using WebAPI.Ultils;

namespace WebAPI.Controllers
{

    [Route("files/[controller]")]
    [ApiController]
    [AllowAnonymous]
    public class ImagesController : WaoControllerBase
    {
        private readonly AppSettings appSettings;
        public ImagesController(IOptions<AppSettings> appSettings)
        {
            this.appSettings = appSettings.Value;
        }

        [HttpGet("{folder}/{fileName}")]
        public ActionResult GetImage(string folder, string fileName)
        {
            try
            {
                var imgHelper = new ImageHelper(appSettings, folder);
                var streamFile = imgHelper.GetFromDisk(fileName);
                return base.File(streamFile, "image/jpeg");
            }
            catch (Exception ex)
            {
                return WaoBadRequest(ex.Message);
            }
        }
    }
}

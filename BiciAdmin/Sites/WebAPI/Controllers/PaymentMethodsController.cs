using System;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using Services.Interfaces;
using DataCore.Models;
using System.Threading.Tasks;
using Website.ViewModels;

namespace WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class PaymentMethodsController : WaoControllerBase
    {
        private readonly IPaymentMethodService _paymentMethodService;
        public PaymentMethodsController(IPaymentMethodService paymentMethodService)
        {
            this._paymentMethodService = paymentMethodService;
        }

        /// <summary>
        /// Get list data.
        /// </summary>
        /// <param name="rContext"></param>
        /// <returns></returns>
        [HttpPost]
        [AllowAnonymous]
        [Route("list")]
        [ProducesResponseType(typeof(ResponseListModel<PaymentMethodViewModel>), 200)]
        [ProducesResponseType(typeof(ResponseStatus), 400)]
        public IActionResult Gets(PaggingBase rContext)
        {
            try
            {
                var respData = _paymentMethodService.GetDatatables(rContext);
                return Ok(respData);
            }
            catch (Exception ex)
            {
                return WaoBadRequest(ex.Message);
            }
        }

        /// <summary>
        /// Create new data.
        /// </summary>
        /// <param name="viewModel"></param>
        /// <returns></returns>
        [HttpPost]
        [ProducesResponseType(typeof(ResponseStatus), 200)]
        [ProducesResponseType(typeof(ResponseStatus), 400)]
        public IActionResult Create(PaymentMethodViewModel viewModel)
        {
            try
            {
                var data = _paymentMethodService.Create(viewModel);
                return Ok(data);
            }
            catch (Exception ex)
            {
                return WaoBadRequest(ex.Message);
            }
        }

        /// <summary>
        /// Updating data.
        /// </summary>
        /// <param name="viewModel"></param>
        /// <returns></returns>
        [HttpPatch]
        [ProducesResponseType(typeof(ResponseStatus), 200)]
        [ProducesResponseType(typeof(ResponseStatus), 400)]
        public IActionResult Update(PaymentMethodViewModel viewModel)
        {
            try
            {
                var data = _paymentMethodService.Update(viewModel);
                return Ok(data);
            }
            catch (Exception ex)
            {
                return WaoBadRequest(ex.Message);
            }
        }

        /// <summary>
        /// Deleting data.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete("{id}")]
        [ProducesResponseType(typeof(ResponseStatus), 200)]
        [ProducesResponseType(typeof(ResponseStatus), 400)]
        public IActionResult Delete(int id)
        {
            try
            {
                var respdata = _paymentMethodService.DeleteById(id);
                return Ok(respdata);
            }
            catch (Exception ex)
            {
                return WaoBadRequest(ex.Message);
            }
        }

        /// <summary>
        /// Get infomation by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("{id}")]
        [AllowAnonymous]
        [ProducesResponseType(typeof(ResponseSignleModel<PaymentMethodViewModel>), 200)]
        [ProducesResponseType(typeof(ResponseStatus), 400)]
        public IActionResult Get(int id)
        {
            try
            {
                var respData = _paymentMethodService.GetInfo(id);
                return Ok(respData);
            }
            catch (Exception ex)
            {
                return WaoBadRequest(ex.Message);
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Services.Interfaces;
using Website.ViewModels;


namespace WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class OrdersPickController : WaoControllerBase
    {
        private IOrderPickingService orderPickingService;
        public OrdersPickController(IOrderPickingService orderPickingService)
        {
            this.orderPickingService = orderPickingService;
        }

        // GET: api/values
        [HttpPatch]
        [ProducesResponseType(typeof(ResponseSignleModel<List<int>>), 200)]
        [ProducesResponseType(typeof(ResponseStatus), 400)]
        public IActionResult Get(PagingOrder pagingOrder)
        {
            try
            {
                var userName = this.User.Identity.Name;
                var data = this.orderPickingService.AutoPick(userName);
                return Ok(data);
            }
            catch (Exception ex)
            {
                return WaoBadRequest(ex.Message);
            }
        }

        // GET: api/values
        [HttpGet("check")]
        [AllowAnonymous]
        [ProducesResponseType(typeof(ResponseListModel<OrderAdminPrepareViewModel>), 200)]
        [ProducesResponseType(typeof(ResponseStatus), 400)]
        public IActionResult CheckProduct(string ids)
        {
            try
            {
                var userName = this.User.Identity.Name;
                var data = this.orderPickingService.GetProductForPrepare(ids, userName);
                return Ok(data);
            }
            catch (Exception ex)
            {
                return WaoBadRequest(ex.Message);
            }
        }

        // GET: api/values
        [HttpPatch("undo")]
        [ProducesResponseType(typeof(ResponseStatus), 200)]
        [ProducesResponseType(typeof(ResponseStatus), 400)]
        public IActionResult UndoOrder(OrderViewModel orderViewModel)
        {
            try
            {
                var data = this.orderPickingService.UndoOrder(orderViewModel.id);
                return Ok(data);
            }
            catch (Exception ex)
            {
                return WaoBadRequest(ex.Message);
            }
        }

        // GET: api/values
        [HttpPatch("confirm")]
        [AllowAnonymous]
        [ProducesResponseType(typeof(ResponseStatus), 200)]
        [ProducesResponseType(typeof(ResponseStatus), 400)]
        public IActionResult ConfirmOrder(OrderConfirmation orderConfirmation)
        {
            try
            {
                var userName = this.User.Identity.Name;
                orderConfirmation.approver = userName;
                var data = this.orderPickingService.ConfirmOrder(orderConfirmation);
                return Ok(data);
            }
            catch (Exception ex)
            {
                return WaoBadRequest(ex.Message);
            }
        }

        // GET: api/values
        [HttpPost("create")]
        [AllowAnonymous]
        [ProducesResponseType(typeof(ResponseStatus), 200)]
        [ProducesResponseType(typeof(ResponseStatus), 400)]
        public IActionResult UndoOrder(KiotOrderViewModel orderViewModel)
        {
            try
            {
                var userName = this.User.Identity.Name;
                orderViewModel.user_name = userName;
                var data = this.orderPickingService.CreateKiotOrder(orderViewModel);
                return Ok(data);
            }
            catch (Exception ex)
            {
                return WaoBadRequest(ex.Message);
            }
        }
    }
}

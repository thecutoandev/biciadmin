using System;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using Services.Interfaces;
using DataCore.Models;
using System.Threading.Tasks;
using Website.ViewModels;

namespace WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class BlogsController : WaoControllerBase
    {
        private readonly IBlogService _blogService;
        public BlogsController(IBlogService blogService)
        {
            this._blogService = blogService;
        }

        /// <summary>
        /// Get list data.
        /// </summary>
        /// <param name="rContext"></param>
        /// <returns></returns>
        [HttpPost]
        [AllowAnonymous]
        [Route("list")]
        [ProducesResponseType(typeof(ResponseListModel<BlogViewModel>), 200)]
        [ProducesResponseType(typeof(ResponseStatus), 400)]
        public IActionResult Gets(BlogRequest rContext)
        {
            try
            {
                var respData = _blogService.GetDatatables(rContext);
                return Ok(respData);
            }
            catch (Exception ex)
            {
                return WaoBadRequest(ex.Message);
            }
        }

        /// <summary>
        /// Create new data.
        /// </summary>
        /// <param name="viewModel"></param>
        /// <returns></returns>
        [HttpPost]
        [ProducesResponseType(typeof(ResponseStatus), 200)]
        [ProducesResponseType(typeof(ResponseStatus), 400)]
        public IActionResult Create(BlogViewModel viewModel)
        {
            try
            {
                var currentUser = this.User.Identity.Name;
                viewModel.created_by = currentUser;
                var data = _blogService.Create(viewModel);
                return Ok(data);
            }
            catch (Exception ex)
            {
                return WaoBadRequest(ex.Message);
            }
        }

        /// <summary>
        /// Updating data.
        /// </summary>
        /// <param name="viewModel"></param>
        /// <returns></returns>
        [HttpPatch]
        [ProducesResponseType(typeof(ResponseStatus), 200)]
        [ProducesResponseType(typeof(ResponseStatus), 400)]
        public IActionResult Update(BlogViewModel viewModel)
        {
            try
            {
                var data = _blogService.Update(viewModel);
                return Ok(data);
            }
            catch (Exception ex)
            {
                return WaoBadRequest(ex.Message);
            }
        }

        /// <summary>
        /// Deleting data.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete("{id}")]
        [ProducesResponseType(typeof(ResponseStatus), 200)]
        [ProducesResponseType(typeof(ResponseStatus), 400)]
        public IActionResult Delete(int id)
        {
            try
            {
                var respdata = _blogService.DeleteById(id);
                return Ok(respdata);
            }
            catch (Exception ex)
            {
                return WaoBadRequest(ex.Message);
            }
        }

        /// <summary>
        /// Get infomation by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("{id}")]
        [AllowAnonymous]
        [ProducesResponseType(typeof(ResponseSignleModel<BlogViewModel>), 200)]
        [ProducesResponseType(typeof(ResponseStatus), 400)]
        public IActionResult Get(int id)
        {
            try
            {
                var respData = _blogService.GetInfo(id);
                return Ok(respData);
            }
            catch (Exception ex)
            {
                return WaoBadRequest(ex.Message);
            }
        }
    }
}

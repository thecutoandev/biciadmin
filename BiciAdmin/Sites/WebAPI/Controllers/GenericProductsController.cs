using System;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using Services.Interfaces;
using DataCore.Models;
using System.Threading.Tasks;
using Website.ViewModels;

namespace WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class GenericProductsController : WaoControllerBase
    {
        private readonly IGenericProductService _genericProductService;
        public GenericProductsController(IGenericProductService genericProductService)
        {
            this._genericProductService = genericProductService;
        }

        /// <summary>
        /// Get list data.
        /// </summary>
        /// <param name="rContext"></param>
        /// <returns></returns>
        [HttpPost]
        [AllowAnonymous]
        [Route("list")]
        [ProducesResponseType(typeof(ResponseListModel<GenericProductViewModel>), 200)]
        [ProducesResponseType(typeof(ResponseStatus), 400)]
        public IActionResult Gets(GenericProductRequestBase rContext)
        {
            try
            {
                var requestBody = new GenericProductRequest(rContext);
                requestBody.is_get_hidden = true;
                var respData = _genericProductService.GetDatatables(requestBody);
                return Ok(respData);
            }
            catch (Exception ex)
            {
                return WaoBadRequest(ex.Message);
            }
        }

        /// <summary>
        /// Create new data.
        /// </summary>
        /// <param name="viewModel"></param>
        /// <returns></returns>
        [HttpPost]
        [ProducesResponseType(typeof(ResponseStatus), 200)]
        [ProducesResponseType(typeof(ResponseStatus), 400)]
        public IActionResult Create(GenericProductViewModel viewModel)
        {
            try
            {
                var data = _genericProductService.Create(viewModel);
                return Ok(data);
            }
            catch (Exception ex)
            {
                return WaoBadRequest(ex.Message);
            }
        }

        /// <summary>
        /// Updating data.
        /// </summary>
        /// <param name="viewModel"></param>
        /// <returns></returns>
        [HttpPatch]
        [ProducesResponseType(typeof(ResponseStatus), 200)]
        [ProducesResponseType(typeof(ResponseStatus), 400)]
        public IActionResult Update(GenericProductViewModel viewModel)
        {
            try
            {
                var data = _genericProductService.Update(viewModel);
                return Ok(data);
            }
            catch (Exception ex)
            {
                return WaoBadRequest(ex.Message);
            }
        }

        /// <summary>
        /// Deleting data.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete("{id}")]
        [ProducesResponseType(typeof(ResponseStatus), 200)]
        [ProducesResponseType(typeof(ResponseStatus), 400)]
        public IActionResult Delete(int id)
        {
            try
            {
                var respdata = _genericProductService.DeleteById(id);
                return Ok(respdata);
            }
            catch (Exception ex)
            {
                return WaoBadRequest(ex.Message);
            }
        }

        /// <summary>
        /// Get infomation by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("{id}")]
        [AllowAnonymous]
        [ProducesResponseType(typeof(ResponseSignleModel<GenericProductViewModel>), 200)]
        [ProducesResponseType(typeof(ResponseStatus), 400)]
        public IActionResult Get(int id)
        {
            try
            {
                var respData = _genericProductService.GetInfo(id);
                return Ok(respData);
            }
            catch (Exception ex)
            {
                return WaoBadRequest(ex.Message);
            }
        }
    }
}

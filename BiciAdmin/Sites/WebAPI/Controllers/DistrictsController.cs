using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Services.Interfaces;
using WebAPI.Controllers;
using Website.ViewModels;

namespace MobileAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class DistrictsController : WaoControllerBase
    {
        // Variables
        private readonly IDistrictService districtService;

        public DistrictsController(IDistrictService districtService)
        {
            this.districtService = districtService;
        }

        /// <summary>
        /// Get all list districts by city id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("{id}")]
        [ProducesResponseType(typeof(ResponseListModel<DistrictViewModel>), 200)]
        [ProducesResponseType(typeof(ResponseStatus), 400)]
        public IActionResult Gets(int id)
        {
            try
            {
                var respData = districtService.GetDataByCity(id);
                return Ok(respData);
            }
            catch (System.Exception ex)
            {
                return WaoBadRequest(ex.Message);
            }
        }
    }
}

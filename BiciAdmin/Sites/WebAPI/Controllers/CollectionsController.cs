﻿using System;
using DataCore.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using Services.Interfaces;
using WebAPI.Ultils;
using Website.ViewModels;

namespace WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CollectionsController : WaoControllerBase
    {
        private readonly ICollectionService _collectionService;
        private readonly AppSettings appSettings;

        public CollectionsController(ICollectionService collectionService,
                                     IOptions<AppSettings> appSettings)
        {
            _collectionService = collectionService;
            this.appSettings = appSettings.Value;
        }

        /// <summary>
        /// Get infomation by Id.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("{id}/{mode}")]
        public ActionResult<ResponseStatus> Get(int id, string mode = "")
        {

            try
            {
                var response = new ResponseSignleModel<CollectionViewModel>();
                if (mode != "full")
                {
                    response = _collectionService.GetInfo(id);
                }
                else
                {
                    response = _collectionService.GetInfoFullProduct(id);
                }
                return Ok(response);
            }
            catch (Exception ex)
            {
                return WaoBadRequest(ex.Message);
            }
        }


        [HttpPost]
        [ProducesResponseType(typeof(ResponseStatus), 200)]
        [ProducesResponseType(typeof(ResponseStatus), 400)]
        public ActionResult<ResponseStatus> Create(CollectionViewModel viewModel)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest();
                }
                // Get user infomation
                var currentUser = null == this.User ? string.Empty : this.User.Identity.Name;
                viewModel.created_by = currentUser;
                // Call service to inserting data to database
                var response = _collectionService.Create(viewModel);
                return Ok(response);
            }
            catch (Exception ex)
            {
                return WaoBadRequest(ex.Message);
            }
        }

        [HttpPatch]
        [ProducesResponseType(typeof(ResponseStatus), 200)]
        [ProducesResponseType(typeof(ResponseStatus), 400)]
        public ActionResult<ResponseStatus> Update(CollectionViewModel viewModel)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest();
                }
                // Get user infomation
                var currentUser = null == this.User ? string.Empty : this.User.Identity.Name;
                viewModel.created_by = currentUser;
                // Call service to inserting data to database
                var response = _collectionService.Update(viewModel);
                return Ok(response);
            }
            catch (Exception ex)
            {
                return WaoBadRequest(ex.Message);
            }
        }

        /// <summary>
        /// Get list data.
        /// </summary>
        /// <param name="rContext"></param>
        /// <returns></returns>
        [HttpPost]
        [AllowAnonymous]
        [Route("list")]
        [ProducesResponseType(typeof(ResponseListModel<CollectionViewModel>), 200)]
        [ProducesResponseType(typeof(ResponseStatus), 400)]
        public IActionResult Gets(CollectionRequest rContext)
        {
            try
            {
                var respData = _collectionService.GetDatatables(rContext);
                return Ok(respData);
            }
            catch (Exception ex)
            {
                return WaoBadRequest(ex.Message);
            }
        }

        [HttpPost("upload")]
        [ProducesResponseType(typeof(ResponseStatus), 200)]
        [ProducesResponseType(typeof(ResponseStatus), 400)]
        public IActionResult UploadFile(IFormFile file)
        {
            try
            {
                var imageHelper = new ImageHelper(appSettings, "collections");
                var pathUploaded = imageHelper.SaveToDisk(file);

                return Ok(new ResponseStatus()
                {
                    successfull = true,
                    dataset = pathUploaded
                });
                
            }
            catch (Exception ex)
            {
                return WaoBadRequest(ex.Message);
            }
        }

        [HttpDelete("{id}")]
        [ProducesResponseType(typeof(ResponseStatus), 200)]
        [ProducesResponseType(typeof(ResponseStatus), 400)]
        public IActionResult Delete(int id)
        {
            try
            {
                var respdata = _collectionService.DeleteById(id);
                return Ok(respdata);
            }
            catch (Exception ex)
            {
                return WaoBadRequest(ex.Message);
            }
        }
    }
}

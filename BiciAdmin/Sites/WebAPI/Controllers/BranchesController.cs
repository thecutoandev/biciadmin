﻿using System;
using KiotService.Interfaces;
using KiotService.Models.Branches;
using Microsoft.AspNetCore.Mvc;
using Website.ViewModels;
using Services.Interfaces;


namespace WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class BranchesController : WaoControllerBase
    {
        private readonly IKBranchesService kBranchesService;
        private readonly IBranchPickService branchPickService;
        public BranchesController(IKBranchesService kBranchesService,
                                  IBranchPickService branchPickService)
        {
            this.kBranchesService = kBranchesService;
            this.branchPickService = branchPickService;
        }

        /// <summary>
        /// Get all actived banner.
        /// </summary>
        /// <returns></returns>
        [HttpPost("list")]
        [ProducesResponseType(typeof(ResponseListModel<Branch>), 200)]
        [ProducesResponseType(typeof(ResponseStatus), 400)]
        public IActionResult Get()
        {
            try
            {
                var data = this.kBranchesService.GetAllBranches();
                return Ok(data);
            }
            catch (Exception ex)
            {
                return WaoBadRequest(ex.Message);
            }
        }

        [HttpGet]
        [ProducesResponseType(typeof(ResponseListModel<BranchPickViewModel>), 200)]
        [ProducesResponseType(typeof(ResponseStatus), 400)]
        public IActionResult GetForUpdate()
        {
            try
            {
                var data = branchPickService.GetListBranches();
                return Ok(data);
            }
            catch (Exception ex)
            {
                return WaoBadRequest(ex.Message);
            }
        }

        [HttpPatch]
        [ProducesResponseType(typeof(ResponseStatus), 200)]
        [ProducesResponseType(typeof(ResponseStatus), 400)]
        public IActionResult Update(BranchPickUpdate viewModel)
        {
            try
            {
                var data = branchPickService.UpdatePriority(viewModel);
                return Ok(data);
            }
            catch (Exception ex)
            {
                return WaoBadRequest(ex.Message);
            }
        }
    }
}

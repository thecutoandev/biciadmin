﻿using System;
using System.IO;
using System.Text;
using DataCore.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.FileProviders;

namespace WebAPI.Ultils
{
    public class ImageHelper
    {
        private readonly AppSettings appSettings;
        private readonly string folder;
        public ImageHelper(AppSettings appSettings, string folder)
        {
            this.appSettings = appSettings;
            this.folder = folder;
        }

        public string SaveToDisk(IFormFile file)
        {
            try
            {
                long size = file.Length;
                var uploads = string.Empty;
                var webRoothPath = appSettings.FolderFiles;
                uploads = Path.Combine(webRoothPath, folder);
                if (!Directory.Exists(uploads))
                {
                    Directory.CreateDirectory(uploads);
                }

                //generate file name
                var uniqueName = GetUniqueFileName(file.FileName);
                var fullPath = Path.Combine(uploads, uniqueName);
                using (var stream = System.IO.File.Create(fullPath))
                {
                    file.CopyTo(stream);
                }
                var endPoint = string.Format(appSettings.BaseFileEndPoint, folder, uniqueName);
                return endPoint;
            }
            catch
            {
                return string.Empty;
            }
        }

        public Stream GetFromDisk(string fileName)
        {
            var pathName = Path.Combine(appSettings.FolderFiles, folder);
            IFileProvider provider = new PhysicalFileProvider(pathName);
            IFileInfo fileInfo = provider.GetFileInfo(fileName);
            var readStream = fileInfo.CreateReadStream();
            return readStream;
            //pathName = Path.Combine(pathName, fileName);
            //return pathName;
        }

        private string GetUniqueFileName(string fileName)
        {
            fileName = Path.GetFileName(fileName);
            return Path.GetFileNameWithoutExtension(fileName)
                      + "_"
                      + Guid.NewGuid().ToString().Substring(0, 4)
                      + Path.GetExtension(fileName);
        }
    }
}

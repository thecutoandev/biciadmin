﻿using System.IO;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;

namespace MobileAPI.Ultils
{
    public sealed class RequestHandlerMiddleware
    {
        private readonly RequestDelegate next;
        private readonly Bugsnag.IClient _bugsnag;

        public RequestHandlerMiddleware(Bugsnag.IClient client, RequestDelegate next)
        {
            this.next = next;
            this._bugsnag = client;
        }

        public async Task Invoke(HttpContext context)
        {
            //switch (context.Response.StatusCode)
            //{
            //    case 400:
            //        _bugsnag.Notify(new Exception(JsonConvert.SerializeObject(context.Response.Body, Formatting.Indented)));
            //        break;
            //    default:
            //        break;
            //}
            await next(context);
            //Create a new memory stream...
            using (var responseBody = new MemoryStream())
            {
                //...and use that for the temporary response body
                context.Response.Body = responseBody;

                //Continue down the Middleware pipeline, eventually returning to this class
                await next(context);

                //Format the response from the server
                var response = await FormatResponse(context.Response);

                //TODO: Save log to chosen datastor
            }
        }

        private async Task<string> FormatResponse(HttpResponse response)
        {
            //We need to read the response stream from the beginning...
            response.Body.Seek(0, SeekOrigin.Begin);

            //...and copy it into a string
            string text = await new StreamReader(response.Body).ReadToEndAsync();

            //We need to reset the reader for the response so that the client can read it.
            response.Body.Seek(0, SeekOrigin.Begin);

            //Return the string for the response, including the status code (e.g. 200, 404, 401, etc.)
            return $"{response.StatusCode}: {text}";
        }
    }
}

﻿using System;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Services.Interfaces;
using Website.ViewModels;

namespace MobileAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CategoriesController : WaoControllerBase
    {
        private readonly ICategoryService _categoryService;
        public CategoriesController(ICategoryService categoryService)
        {
            this._categoryService = categoryService;
        }

        // GET: api/categories
        /// <summary>
        /// Get list categories.
        /// </summary>
        /// <param name="requestContext"></param>
        /// <returns></returns>
        [HttpPost]
        [ProducesResponseType(typeof(ResponseListModel<CategoryViewModel>), 200)]
        [ProducesResponseType(typeof(ResponseStatus), 400)]
        public ActionResult Categories(CategoriesRequest requestContext)
        {
            try
            {
                var categories = _categoryService.GetChildsList(requestContext);
                return Ok(categories);
            }
            catch (Exception ex)
            {
                return WaoBadRequest(ex.Message);
            }
        } 

        /// <summary>
        /// Get category infomation by Id.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("{id}")]
        [ProducesResponseType(typeof(ResponseSignleModel<CategoryViewModel>), 200)]
        [ProducesResponseType(typeof(ResponseStatus), 400)]
        public ActionResult Get(int id)
        {
            try
            {
                var category = _categoryService.GetInfo(id);
                return Ok(category);
            }
            catch (Exception ex)
            {
                return WaoBadRequest(ex.Message);
            }
        }
    }
}

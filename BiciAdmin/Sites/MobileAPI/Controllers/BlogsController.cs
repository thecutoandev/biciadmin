﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Services.Interfaces;
using Website.ViewModels;
using System;

namespace MobileAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class BlogsController : WaoControllerBase
    {
        // Variables
        private readonly IBlogService _blogService;

        public BlogsController(IBlogService blogService)
        {
            this._blogService = blogService;
        }

        /// <summary>
        /// Get list blog with pagination
        /// </summary>
        /// <param name="blogRequest">Pagging infomation.</param>
        /// <returns></returns>
        [HttpPost]
        [ProducesResponseType(typeof(ResponseListModel<BlogViewModel>), 200)]
        [ProducesResponseType(typeof(ResponseStatus), 400)]
        public IActionResult Get(BlogRequest blogRequest)
        {
            try
            {
                var data = _blogService.GetDatatables(blogRequest);
                return Ok(data);
            }
            catch (Exception ex)
            {
                return WaoBadRequest(ex.Message);
            }
        }

        //[HttpPost]
        //public IActionResult Hook(object obj)
        //{
        //    try
        //    {
        //        var data = new BlogViewModel()
        //        {
        //            content = obj.ToString(),
        //            name = "haravan title",
        //            link = "http://haravan.com",
        //            ref_web_id = -1,
        //            created_by = "haravan",
        //            created_date = DateTime.Now,
        //            updated_by = "haravan",
        //            updated_date = DateTime.Now
        //        };
        //        var resp = _blogService.Create(data);
        //        return Ok(resp);
        //    }
        //    catch (Exception ex)
        //    {
        //        return WaoBadRequest(ex.Message);
        //    }
        //}
    }
}

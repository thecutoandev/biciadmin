﻿using System;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Services.Interfaces;
using Website.ViewModels;

namespace MobileAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class UserAddressController : ControllerBase
    {
        private readonly IUserAddressService _userAddressService;
        public UserAddressController(IUserAddressService userAddressService)
        {
            this._userAddressService = userAddressService;
        }

        [HttpGet]
        public IActionResult Get()
        {
            var currentUser = this.User.Identity.Name;
            var respData = _userAddressService.GetDatatablesWithUser(currentUser);
            if (respData == null)
            {
                return BadRequest(new ResponseStatus());
            }
            return Ok(respData);
        }

        [HttpPut]
        [ProducesResponseType(typeof(ResponseStatus), 200)]
        [ProducesResponseType(typeof(ResponseStatus), 400)]
        public IActionResult Update(UserAddressViewModel dataModel)
        {
            
            try
            {
                var currentUser = this.User.Identity.Name;
                dataModel.username = currentUser;
                //if (currentUser == dataModel.username)
                //{
                //    return BadRequest("You have not permission to update other user.");
                //}
                var respInfo = _userAddressService.Update(dataModel);
                return Ok(respInfo);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpDelete("{id}")]
        [ProducesResponseType(typeof(ResponseStatus), 200)]
        [ProducesResponseType(typeof(ResponseStatus), 400)]
        public IActionResult Delete(int id)
        {

            try
            {
                var currentUser = this.User.Identity.Name;
                var viewModel = new UserAddressViewModel()
                {
                    username = currentUser,
                    id = id
                };
                
                var respInfo = _userAddressService.Delete(viewModel);
                return Ok(respInfo);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

    }
}

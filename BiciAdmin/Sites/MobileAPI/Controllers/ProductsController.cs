﻿using Microsoft.AspNetCore.Mvc;
using Services.Interfaces;
using Website.ViewModels;
using System;
using KiotService.Models.Products;
using KiotService.Interfaces;

namespace MobileAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProductsController : WaoControllerBase
    {
        private readonly IGenericProductService genericProductService;
        private readonly IKProductService kProductService;
        //private readonly IProductService productService;

        public ProductsController(IGenericProductService genericProductService,
                                  IKProductService kProductService)
        //IProductService productService)
        {
            this.genericProductService = genericProductService;
            this.kProductService = kProductService;
            //this.productService = productService;
        }

        /// <summary>
        /// Get list generic products.
        /// </summary>
        /// <param name="rContext"></param>
        /// <returns></returns>
        [HttpPost]
        [ProducesResponseType(typeof(ResponseListModel<GenericProductViewModel>), 200)]
        [ProducesResponseType(typeof(ResponseStatus), 400)]
        public IActionResult GenericProducts(GenericProductRequestBase rContext)
        {
            try
            {
                var requestBody = new GenericProductRequest(rContext);
                requestBody.is_get_hidden = false;
                var respData = genericProductService.GetDatatables(requestBody);
                return Ok(respData);
            }
            catch (Exception ex)
            {
                return WaoBadRequest(ex.Message);
            }
        }

        [HttpGet("{id}")]
        [ProducesResponseType(typeof(ResponseSignleModel<GenericProductViewModel>), 200)]
        [ProducesResponseType(typeof(ResponseStatus), 400)]
        public IActionResult GetVariants(int id)
        {
            try
            {
                var genericInfo = genericProductService.GetInfo(id);
                return Ok(genericInfo);
            }
            catch (Exception ex)
            {
                return WaoBadRequest(ex.Message);
            }
        }

        [HttpGet]
        [Route("Stocking/{kiotCode}")]
        [ProducesResponseType(typeof(ResponseSignleModel<KiotStocking>), 200)]
        [ProducesResponseType(typeof(ResponseStatus), 400)]
        public IActionResult CheckStock(string kiotCode)
        {
            try
            {
                var genericInfo = kProductService.GetDetail(kiotCode);
                return Ok(genericInfo);
            }
            catch (Exception ex)
            {
                return WaoBadRequest(ex.Message);
            }
        }

    }
}

﻿using System;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Services.Interfaces;
using Website.ViewModels;

namespace MobileAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class OrdersController : WaoControllerBase
    {
        private readonly IOrderService _orderService;
        private readonly IDistrictService _districtService;
        public OrdersController(IOrderService orderService,
                               IDistrictService districtService)
        {
            this._orderService = orderService;
            this._districtService = districtService;
        }

        /// <summary>
        /// Get order detail.
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("/api/Order/{id}")]
        [Authorize]
        [ProducesResponseType(typeof(ResponseSignleModel<OrderViewModel>), 200)]
        [ProducesResponseType(typeof(ResponseStatus), 400)]
        public IActionResult Get(int id)
        {
            //var userName = this.User.Identity.Name;
            var respData = _orderService.GetInfo(id);
            if (respData.successfull)
            {
                return Ok(respData);
            }
            return BadRequest(respData);
        }

        /// <summary>
        /// Get histories of user.
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Authorize]
        [ProducesResponseType(typeof(ResponseListModel<OrderViewModel>), 200)]
        [ProducesResponseType(typeof(ResponseStatus), 400)]
        public IActionResult Get(int page, int itemPerPage)
        {
            var userName = this.User.Identity.Name;
            var paging = new PagingOrderHistories()
            {
                itemPerPage = itemPerPage,
                page = page
            };
            var respData = _orderService.GetHistories(userName, paging);
            if (respData.successfull)
            {
                return Ok(respData);
            }
            return BadRequest(respData);
        }

        /// <summary>
        /// Create order.
        /// </summary>
        /// <param name="orderViewModel"></param>
        /// <returns></returns>
        [HttpPost]
        [Authorize]
        [Route("/api/Order")]
        public ActionResult Post(OrderViewModel orderViewModel)
        {
            orderViewModel.source = "Mobile";
            orderViewModel.user_id = this.User.Identity.Name;
            var respData = _orderService.Create(orderViewModel);
            if (respData.successfull)
            {
                return Ok(respData);
            }
            return BadRequest(respData);
        }

        [HttpGet("/api/ShippingFee/{id}")]
        public IActionResult GetShippingFee(int id)
        {
            try
            {
                var respData = _districtService.GetShippingFee(districtId: id);
                return Ok(respData);
            }
            catch (Exception ex)
            {
                return WaoBadRequest(ex.Message);
            }
        }
    }
}

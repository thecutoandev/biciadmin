﻿using System;
using Microsoft.AspNetCore.Mvc;
using Website.ViewModels;
namespace MobileAPI.Controllers
{
    public class WaoControllerBase : ControllerBase
    {
        public override BadRequestObjectResult BadRequest(object error)
        {
            return base.BadRequest(error);
        }

        protected BadRequestObjectResult WaoBadRequest(string errorMessage)
        {
            var responseModel = new ResponseStatus()
            {
                message = errorMessage,
                successfull = false
            };
            return new BadRequestObjectResult(responseModel);
        }
    }
}

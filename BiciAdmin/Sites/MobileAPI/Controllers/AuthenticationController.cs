﻿using System;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using Services.Interfaces;
using DataCore.Models;
using System.Threading.Tasks;
using Website.ViewModels;
using Swashbuckle.AspNetCore;
using Website.ViewModels.Auth;

namespace MobileAPI.Controllers
{
    [ApiController]
    public class AuthenticationController : ControllerBase
    {
        private readonly IUserService _userService;

        public AuthenticationController(IUserService userService)
        {
            this._userService = userService;
        }

        /// <summary>
        /// Get server token from facebook token.
        /// </summary>
        /// <param name="userViewModel"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("tokenByFaceBook")]
        [ProducesResponseType(typeof(ResponseSignleModel<AuthModel>), 200)]
        [ProducesResponseType(typeof(ResponseStatus), 400)]
        public async Task<ActionResult> TokenByFaceBook(FaceBookAuthViewModel userViewModel)
        {
            var userLogin = await _userService.FaceBookAuthenticate(userViewModel.access_token);
            if (userLogin == null)
            {
                return BadRequest(new ResponseStatus()
                {
                    successfull = false,
                    message = "Login is failed. Please re-check facebook token."
                });
            }
            return Ok(new ResponseSignleModel<AuthModel>()
            {
                dataset = userLogin
            });
        }

        /// <summary>
        /// Get server token by access token that got from account kit.
        /// </summary>
        /// <param name="userViewModel"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("tokenByPhone")]
        [ProducesResponseType(typeof(ResponseSignleModel<AuthModel>), 200)]
        [ProducesResponseType(typeof(ResponseStatus),400)]
        public async Task<ActionResult> TokenByPhone(FaceBookAuthViewModel userViewModel)
        {
            var userLogin = await _userService.PhoneAuthenticate(userViewModel.access_token);
            if (userLogin == null)
            {
                return BadRequest(new ResponseStatus()
                {
                    successfull =false,
                    message = "Login is failed. Please re-check account-kit token."
                });
            }
            return Ok(new ResponseSignleModel<AuthModel>()
            {
                successfull = true,
                dataset = userLogin
            });
        }

        /// <summary>
        /// Get token by id token from firebase
        /// </summary>
        /// <param name="tokenInfo"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("tokenByFireBase")]
        [ProducesResponseType(typeof(ResponseSignleModel<AuthModel>), 200)]
        [ProducesResponseType(typeof(ResponseStatus), 400)]
        public async Task<ActionResult> tokenByFireBase(FireBaseAuth tokenInfo)
        {
            var userLogin = await _userService.FireBaseAuthenticate(tokenInfo.id_token);
            if (userLogin == null)
            {
                return BadRequest(new ResponseStatus()
                {
                    successfull = false,
                    message = "Login is failed. Please re-check facebook token."
                });
            }
            return Ok(new ResponseSignleModel<AuthModel>()
            {
                dataset = userLogin
            });
        }

        //[HttpPost]
        //[Route("api/users")]
        //public async Task<ActionResult> Register(UserViewModel userViewModel)
        //{
        //    try
        //    {
        //        var userLogin = await _userService.Register(userViewModel);
        //        if (userLogin == null)
        //        {
        //            return BadRequest(new { message = "Something when wrong!!!" });
        //        }
        //        return Ok(userLogin);
        //    }
        //    catch (Exception ex)
        //    {
        //        return BadRequest(ex.Message);
        //    }

        //}
    }
}

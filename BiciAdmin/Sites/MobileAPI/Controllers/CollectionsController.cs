﻿using System;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Services.Interfaces;
using Website.ViewModels;

namespace MobileAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CollectionsController : WaoControllerBase
    {
        private readonly ICollectionService _collectionService;
        public CollectionsController(ICollectionService collectionService)
        {
            this._collectionService = collectionService;
        }

        /// <summary>
        /// Get list collection
        /// </summary>
        /// <param name="collectionRequest"></param>
        /// <returns></returns>
        [HttpPost]
        [ProducesResponseType(typeof(ResponseListModel<CollectionViewModel>), 200)]
        [ProducesResponseType(typeof(ResponseStatus), 400)]
        public IActionResult GetList(CollectionRequest collectionRequest)
        {
            try
            {
                var dataResp = _collectionService.GetDatatables(collectionRequest);
                return Ok(dataResp);
            }
            catch (Exception ex)
            {
                return WaoBadRequest(ex.Message);
            }
        }


        /// <summary>
        /// Get category infomation by Id.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("{id}")]
        [ProducesResponseType(typeof(ResponseSignleModel<CollectionViewModel>), 200)]
        [ProducesResponseType(typeof(ResponseStatus), 400)]
        public ActionResult Get(int id)
        {
            try
            {
                var collection = _collectionService.GetInfo(id);
                return Ok(collection);
            }
            catch (Exception ex)
            {
                return WaoBadRequest(ex.Message);
            }
        }
    }
}

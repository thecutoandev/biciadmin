﻿using System;
using Microsoft.AspNetCore.Mvc;
using Services.Interfaces;
using Website.ViewModels;

namespace MobileAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class BannersController : WaoControllerBase
    {
        private readonly IBannerService bannerService;
        public BannersController(IBannerService bannerService)
        {
            this.bannerService = bannerService;
        }

        /// <summary>
        /// Get all actived banner.
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [ProducesResponseType(typeof(ResponseListModel<BannerViewModel>), 200)]
        [ProducesResponseType(typeof(ResponseStatus), 400)]
        public IActionResult Get()
        {
            try
            {
                var data = bannerService.GetAll(false);
                return Ok(data);
            }
            catch (Exception ex)
            {
                return WaoBadRequest(ex.Message);
            }
        }
    }
}

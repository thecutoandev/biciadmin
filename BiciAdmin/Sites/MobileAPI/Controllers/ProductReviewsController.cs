﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Services.Interfaces;
using Website.ViewModels;
using System;

namespace MobileAPI.Controllers
{
    [Route("api/[controller]")]
    [Authorize]
    [ApiController]
    public class ProductReviewsController : WaoControllerBase
    {
        private readonly IProductRatingService _productRatingService;

        public ProductReviewsController(IProductRatingService productRatingService)
        {
            this._productRatingService = productRatingService;
        }


        /// <summary>
        /// Get list reviews of the product.
        /// </summary>
        /// <param name="ratingRequest"></param>
        /// <returns></returns>
        [HttpPost]
        [AllowAnonymous]
        [ProducesResponseType(typeof(ResponseListModel<ProductRatingViewModel>), 200)]
        [ProducesResponseType(typeof(ResponseStatus), 400)]
        public IActionResult GetReviewOfProduct(ProductRatingRequest ratingRequest)
        {
            try
            {
                var ratings = _productRatingService.GetDatatables(ratingRequest);
                return Ok(ratings);
            }
            catch (Exception ex)
            {
                return WaoBadRequest(ex.Message);
            }
        }

        /// <summary>
        /// Create new data.
        /// </summary>
        /// <param name="viewModel"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("/api/ProductReview")]
        [ProducesResponseType(typeof(ResponseStatus), 200)]
        [ProducesResponseType(typeof(ResponseStatus), 400)]
        public IActionResult Create(ProductRatingViewModel viewModel)
        {
            try
            {
                var data = _productRatingService.Create(viewModel);
                return Ok(data);
            }
            catch (Exception ex)
            {
                return WaoBadRequest(ex.Message);
            }
        }
    }
}

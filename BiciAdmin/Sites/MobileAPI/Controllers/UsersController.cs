﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using Services.Interfaces;
using DataCore.Models;
using Website.ViewModels;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Hosting;
using System.IO;
using Microsoft.Extensions.Options;
using MobileAPI.Ultils;

namespace MobileAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class UsersController : WaoControllerBase
    {
        private readonly IUserService _userService;
        private readonly AppSettings appSettings;
        public UsersController(IUserService userService,
                               IOptions<AppSettings> appSettings)
        {
            _userService = userService;
            this.appSettings = appSettings.Value;
        }

        /// <summary>
        /// Update user infomation
        /// </summary>
        /// <param name="userViewModel"></param>
        /// <returns></returns>
        [HttpPatch]
        [ProducesResponseType(typeof(ResponseSignleModel<UserRes>), 200)]
        [ProducesResponseType(typeof(ResponseStatus), 400)]
        public async Task<ActionResult> Update(UserRes userViewModel)
        {
            var currentUser = this.User.Identity.Name;
            var modelToupdate = new UserViewModel()
            {
                FullName = userViewModel.fullName,
                PhoneNumber = userViewModel.phoneNumber,
                Gender = userViewModel.gender,
                BirthDay = userViewModel.birthday,
                Email = userViewModel.email,
                UserName = currentUser
            };
            var userInfo = await _userService.Update(modelToupdate);
            var resp = new ResponseSignleModel<UserRes>()
            {
                dataset = userViewModel
            };
            if (userInfo == null)
            {
                return BadRequest(new ResponseStatus()
                {
                    successfull = false,
                    message = "Update user is not success, please try again"
                });
            }
            return Ok(resp);
        }

#if DEBUG
        [HttpDelete("{id}")]
        [AllowAnonymous]
        public IActionResult Delete(string id)
        {
            var resp = new ResponseStatus();
            var del = _userService.Delete(id);
            if (del)
            {
                resp.successfull = true;
            }
            else
            {
                resp.successfull = false;
            }
            return Ok(resp);
        }
#endif

        [HttpPut("UploadAvatar")]
        public IActionResult UploadAvatar(IFormFile file)
        {
            try
            {
                var imageHelper = new ImageHelper(appSettings, "avatars");
                var currentUser = this.User.Identity.Name;
                var modelToupdate = new UserViewModel()
                {
                    Avatar = imageHelper.SaveToDisk(file),
                    UserName = currentUser
                };
                var userInfo =  _userService.UpdateAvatar(modelToupdate).Result;
                return Ok(new ResponseStatus()
                {
                    successfull = true,
                    dataset = modelToupdate.Avatar
                });
            }
            catch (Exception ex)
            {
                return WaoBadRequest(ex.Message);
            }
        }

        [HttpGet("GetInfo")]
        [ProducesResponseType(typeof(ResponseSignleModel<UserInfoViewModel>), 200)]
        [ProducesResponseType(typeof(ResponseStatus), 400)]
        public IActionResult GetInfo()
        {
            try
            {
                var currentUser = this.User.Identity.Name;
                var userInfo = _userService.GetUser(currentUser);
                return Ok(userInfo);
            }
            catch (Exception ex)
            {
                return WaoBadRequest(ex.Message);
            }
        }


    }
}

﻿using System;
using System.IO;
using System.Reflection;
using Autofac.Extensions.DependencyInjection;
using FirebaseAdmin;
using Google.Apis.Auth.OAuth2;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.OpenApi.Models;
using MobileAPI.Configs;

[assembly: ApiController]
namespace MobileAPI
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public IServiceProvider ConfigureServices(IServiceCollection services)
        {
            services.AddControllers();

            // Register the Swagger generator, defining 1 or more Swagger documents
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new Microsoft.OpenApi.Models.OpenApiInfo
                {
                    Title = "API for aceessing from BiciCosmetic mobile app",
                    Version = "v1",
                    Description = "Tool for testing api",
                    License = new Microsoft.OpenApi.Models.OpenApiLicense
                    {
                        Name = "API power by Wao Solutions",
                        Url = new Uri("http://waosolutions.com")
                    },
                    Contact = new Microsoft.OpenApi.Models.OpenApiContact
                    {
                        Name = "Wao Solutions",
                        Email = "support@waosolutions.com",
                        Url = new Uri("http://waosolutions.com")
                    }
                });
                
                c.OrderActionsBy(p => p.GroupName);
                //Set the comments path for the Swagger JSON and UI.

                var xmlFile = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
                var xmlPath = Path.Combine(AppContext.BaseDirectory, xmlFile);

                c.IncludeXmlComments(xmlPath);
                c.AddSecurityDefinition("oauth2", new Microsoft.OpenApi.Models.OpenApiSecurityScheme
                {
                    Description = "Standard Authorization header using the Bearer scheme. Example: \"bearer {token}\"",
                    In = ParameterLocation.Header,
                    Name = "Authorization",
                    Type = SecuritySchemeType.ApiKey
                });
            });

            // Create the IServiceProvider based on the container.
            return new AutofacServiceProvider(BootStrapper.SetupAutofacContainer(services, this.Configuration));
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, Bugsnag.IClient client)
        {
            //if (env.IsDevelopment())
            //{
            //    //app.UseStaticFiles();
            //    app.UseDeveloperExceptionPage();
            //    //app.UseMiddleware<MobileAPI.Ultils.RequestHandlerMiddleware>();
            //}
            //else
            //{
            //    app.UseDeveloperExceptionPage();
            //    // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
            //    app.UseHsts();
            //}
            // Enable swagger.
            app.UseSwagger();
            app.UseSwaggerUI(c => {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "API V1 Collections");
            });
            //BootStrapper.UpdateDatabase(app);
            // Setting global cors policy
            app.UseCors(x => x.AllowAnyOrigin().AllowAnyMethod().AllowAnyHeader());
            app.UseHttpsRedirection();
            app.UseAuthentication();
            app.UseRouting();
            app.UseAuthorization();
            
            app.UseEndpoints(e =>
            {
                e.MapControllers();
            });
            BootStrapper.UpdateDatabase(app, client);

            FirebaseApp.Create(new AppOptions()
            {
                Credential = GoogleCredential.FromFile("firebase.json")
            }) ;
        }
    }
}

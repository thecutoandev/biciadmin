﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using DataCore.Data.Infrastructure;
using DataCore.Data.Repositories;
using Services.Handlers;
using Website.ViewModels;
using Models = DataCore.Models;
using WaoInterfaces = Services.Interfaces;

namespace Services.APIServices
{
    public class GenericProductService : BaseService, WaoInterfaces.IGenericProductService
    {
        // Define singleton instances.
        private readonly IUnitOfWork unitOfWork;
        private readonly IGenericProductRepository _genericProductRepository;
        private readonly IGenericProductMediaRepository _genericProductMediaRepository;
        private readonly IProductCollectionRepository _productCollectionRepository;
        private readonly IProductRepository _productRepository;
        private readonly IProductMediaRepository _productMediaRepository;
        private readonly IProductAttributeTypeRepository _productAttributeTypeRepository;
        private readonly IProductAttributeRepository _productAttributeRepository;
        private readonly IBrandRepository _brandRepository;
        private readonly ICategoryRepository _categoryRepository;
        private readonly ICategoryCollectionRepository _categoryCollectionRepository;
        private readonly IMapper mapper;
        private readonly IProductTypeRepository _productTypeRepository;

        // Constructor
        public GenericProductService(IUnitOfWork unitOfWork,
                                 IGenericProductRepository GenericProductRepository,
                                 IProductCollectionRepository productCollectionRepository,
                                 IGenericProductMediaRepository genericProductMediaRepository,
                                 IProductRepository productRepository,
                                 IProductMediaRepository productMediaRepository,
                                 IProductAttributeTypeRepository productAttributeTypeRepository,
                                 IProductAttributeRepository productAttributeRepository,
                                 IBrandRepository brandRepository,
                                 ICategoryRepository categoryRepository,
                                 ICategoryCollectionRepository categoryCollectionRepository,
                                 IProductTypeRepository productTypeRepository,
                                 IMapper mapper) : base(unitOfWork)
        {
            this.unitOfWork = unitOfWork;
            _genericProductRepository = GenericProductRepository;
            _productCollectionRepository = productCollectionRepository;
            _genericProductMediaRepository = genericProductMediaRepository;
            _productRepository = productRepository;
            _productMediaRepository = productMediaRepository;
            _productAttributeRepository = productAttributeRepository;
            _productAttributeTypeRepository = productAttributeTypeRepository;
            _brandRepository = brandRepository;
            _categoryRepository = categoryRepository;
            _categoryCollectionRepository = categoryCollectionRepository;
            _productTypeRepository = productTypeRepository;
            this.mapper = mapper;
        }

        /// <summary>
        /// Insert data to database.
        /// </summary>
        /// <param name="viewModel"></param>
        /// <returns></returns>
        public ResponseStatus Create(GenericProductViewModel viewModel)
        {
            try
            {
                // Mapping field value from view model to model entity
                var genericProductEntity = mapper.Map<Models.GenericProduct>(viewModel);
                genericProductEntity.created_date = DateTime.Now;
                genericProductEntity.updated_date = DateTime.Now;
                // Save data to database
                _genericProductRepository.Add(genericProductEntity);
                SaveChanges();
                // Create response data
                var resp = new ResponseStatus()
                {
                    successfull = true,
                    message = string.Format("Create data is successfull with id => {0}", genericProductEntity.id),
                    dataset = genericProductEntity
                };
                return resp;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                throw;
            }
        }

        /// <summary>
        /// Delete data from database.
        /// </summary>
        /// <param name="viewModel"></param>
        /// <returns></returns>
        public ResponseStatus Delete(GenericProductViewModel viewModel)
        {
            try
            {
                // Checking exist data.
                var entity = _genericProductRepository.Query(model => model.id == viewModel.id).FirstOrDefault();
                if (null == entity)
                {
                    throw new NotFoundDataException(viewModel.id.ToString(), typeof(Models.Collection));
                }
                _genericProductRepository.Delete(entity);
                SaveChanges();
                return new ResponseStatus()
                {
                    successfull = true,
                    message = string.Format("Delete data is successfull with id => {0}", entity.id)
                };
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                throw;
            }
        }

        /// <summary>
        /// Delete data by primary key.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ResponseStatus DeleteById(int id)
        {
            try
            {
                _genericProductRepository.Delete(model => model.id == id);
                SaveChanges();
                return new ResponseStatus()
                {
                    successfull = true,
                    message = string.Format("Delete data is successfull with id => {0}", id)
                };
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                throw;
            }
        }

        /// <summary>
        /// Get datatable with pagination info.
        /// </summary>
        /// <param name="rContext"></param>
        /// <returns></returns>
        public ResponseListModel<GenericProductViewModel> GetDatatables(GenericProductRequest rContext)
        {
            try
            {
                // Variables
                //var sortingString = rContext.sortBy + (rContext.reverse ? " descending" : "");
                var isSearching = !string.IsNullOrEmpty(rContext.searchValue);
                var genericId = -1;
                if (isSearching)
                {
                    rContext.searchValue = rContext.searchValue.ToLower();
                    var product = _productRepository.Query(model => model.kiot_product_code == rContext.searchValue).FirstOrDefault();
                    if (product != null)
                    {
                        genericId = product.generic_product_id;
                    }
                }
                var responseData = new GenericProductDatatable();

                // Fetching data from database.
                var genericProducts = from model in _genericProductRepository.GetAllQueryable()
                                      //join cat in _productTypeRepository.GetAllQueryable()
                                      //on model.category_id equals cat.id into cModel
                                      //from cat in cModel.DefaultIfEmpty()
                                      join brand in _brandRepository.GetAllQueryable()
                                      on model.brand_id equals brand.id into bModel
                                      from brand in bModel.DefaultIfEmpty()
                                      
                                      //orderby sortingString
                                      select new GenericProductViewModel()
                                      {
                                          id = model.id,
                                          name = model.name,
                                          category_id = model.category_id,
                                          brand_id = model.brand_id,
                                          product_name = model.product_name,
                                          product_content = model.product_content,
                                          product_price = model.product_price,
                                          link = model.link,
                                          is_free_ship = model.is_free_ship,
                                          category_name = "",
                                          brand_name = brand.name,
                                          short_description = model.short_description,
                                          hidden = model.hidden,
                                          product_type = model.product_type,
                                          ref_web_id = model.ref_web_id
                                      };
                //var param = rContext.sortBy;
                // var pi = typeof(GenericProductViewModel).GetProperty(param);
                //genericProducts = rContext.reverse ? genericProducts.OrderByDescending(x => pi.GetValue(x, null)) : genericProducts.OrderBy(x => pi.GetValue(x, null));

                // Get hidden
                if (!rContext.is_get_hidden)
                {
                    genericProducts = genericProducts.Where(model => model.hidden == false);
                }
                if (!string.IsNullOrEmpty(rContext.searchValue))
                {
                    genericProducts = genericProducts.Where(model => ((genericId == -1 && model.name.ToLower().Contains(rContext.searchValue)) ||
                                                                     //model.category_name.ToLower().Contains(rContext.searchValue) ||
                                                                     //model.brand_name.ToLower().Contains(rContext.searchValue) ||
                                                                     (genericId != -1 && model.id == genericId))
                                                            );
                }
                // Filter with collectin.
                if (rContext.brand_id != 0)
                {
                    genericProducts = genericProducts.Where(model => model.brand_id == rContext.brand_id);
                }

                if (rContext.category_id != 0)
                {
                    genericProducts = from g in genericProducts
                                      join coll in _categoryCollectionRepository.GetAllQueryable()
                                      on g.ref_web_id equals coll.product_id into c
                                      from coll in c.DefaultIfEmpty()
                                      join cate in _categoryRepository.GetAllQueryable()
                                      on coll.category_id equals cate.ref_category_id into c2
                                      from cate in c2.DefaultIfEmpty()
                                      where cate.id == rContext.category_id
                                      orderby coll.position descending
                                      select g;
                    //genericProducts = genericProducts.Where(model => collect.Contains(model.ref_web_id));
                }

                // Filter with collection
                if (rContext.collection_id != 0)
                {
                    var collections = _productCollectionRepository.Query(model => model.collection_id == rContext.collection_id)
                                                                  .Select(model => model.product_id);
                    genericProducts = genericProducts.Where(model => collections.Contains(model.id));
                }

                //Sorting

                responseData.total = genericProducts.Count();

                // Paging data
                genericProducts = genericProducts.Skip((rContext.page - 1) * rContext.itemPerPage)
                                                 .Take(rContext.itemPerPage);


                // Fetching data from database
                responseData.data = genericProducts.ToList();

                // Get images
                if (rContext.isGetImages)
                {
                    var medias = GetGenericMedias(responseData.data.Select(model => model.id).ToArray());
                    responseData.data.ForEach(model =>
                    {
                        model.medias = medias.Where(img => img.generic_product_id == model.id)
                                             .OrderByDescending(img => img.position)
                                             .Select(img => img.image_link).ToList();
                    });
                }
                return new ResponseListModel<GenericProductViewModel>()
                {
                    successfull = true,
                    dataset = responseData.data,
                    total = responseData.total
                };
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                throw;
            }
        }

        /// <summary>
        /// Get medias for generic product
        /// </summary>
        /// <param name="productIds"></param>
        /// <returns></returns>
        public List<GenericProductMediaViewModel> GetGenericMedias(int[] productIds)
        {
            try
            {
                var medias = (from media in _genericProductMediaRepository.GetAllQueryable()
                              where productIds.Contains(media.generic_product_id)
                              orderby media.generic_product_id, media.position 
                              select new GenericProductMediaViewModel()
                              {
                                  id = media.id,
                                  generic_product_id = media.generic_product_id,
                                  image_link = media.image_link,
                                  position = media.position
                              }).ToList();
                return medias;
            }
            catch
            {
                return new List<GenericProductMediaViewModel>();
            }
        }

        /// <summary>
        /// Get medias for generic product
        /// </summary>
        /// <param name="productIds"></param>
        /// <returns></returns>
        public List<ProductMediaViewModel> GetProductMedias(int[] productIds)
        {
            try
            {
                var medias = (from media in _productMediaRepository.GetAllQueryable()
                              where productIds.Contains(media.product_id)
                              orderby media.product_id, media.position
                              select new ProductMediaViewModel()
                              {
                                  id = media.id,
                                  product_id = media.product_id,
                                  image_link = media.image_link
                              }).ToList();
                return medias;
            }
            catch
            {
                return new List<ProductMediaViewModel>();
            }
        }

        /// <summary>
        /// Get detail info by id.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ResponseSignleModel<GenericProductViewModel> GetInfo(int id)
        {
            try
            {
                var collection = _genericProductRepository.Query(model => model.id == id).FirstOrDefault();
                if (null == collection)
                {
                    throw new NotFoundDataException(id.ToString(), typeof(GenericProductViewModel));
                }
                var respData = mapper.Map<GenericProductViewModel>(collection);

                var medias = GetGenericMedias(new int[] { respData.id });
                respData.medias = medias.Select(img => img.image_link).ToList();

                // Get products list
                var listProducts = (from product in _productRepository.Query(model => model.generic_product_id == id)
                                    join pTypeValue in _productAttributeRepository.GetAllQueryable()
                                               on product.id equals pTypeValue.product_id into valueList
                                    from pTypeValue in valueList.DefaultIfEmpty()
                                    join pType in _productAttributeTypeRepository.GetAllQueryable()
                                               on pTypeValue.attribute_id equals pType.id into typelist
                                    from pType in typelist.DefaultIfEmpty()
                                    select new ProductViewModel()
                                    {
                                        id = product.id,
                                        link = product.link,
                                        generic_product_id = product.generic_product_id,
                                        kiot_product_code = product.kiot_product_code,
                                        product_code = product.product_code,
                                        product_name = product.product_name,
                                        product_price = product.product_price,
                                        selling_price = product.selling_price,
                                        created_by = product.created_by,
                                        created_date = product.created_date,
                                        updated_by = product.updated_by,
                                        updated_date = product.updated_date,
                                        varirant_type = pType.name,
                                        variant_value = pTypeValue.value,
                                    }).ToList();
                respData.products = listProducts;
                var productIds = listProducts.Select(m => m.id).ToArray();
                var pMedias = GetProductMedias(productIds);
                listProducts.ForEach(model =>
                {
                    model.medias = pMedias.Where(img => img.product_id == model.id)
                                          .Select(img => img.image_link).ToList();
                });

                return new ResponseSignleModel<GenericProductViewModel>()
                {
                    successfull = true,
                    dataset = respData
                };
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                throw;
            }
        }

        /// <summary>
        /// Update data.
        /// </summary>
        /// <param name="viewModel"></param>
        /// <returns></returns>
        public ResponseStatus Update(GenericProductViewModel viewModel)
        {
            try
            {
                // Mapping field value from view model to model entity
                var entity = mapper.Map<Models.GenericProduct>(viewModel);
                // Save data to database
                _genericProductRepository.Update(entity);
                SaveChanges();
                // Create response data
                var resp = new ResponseStatus()
                {
                    successfull = true,
                    message = string.Format("Update data is successfull with id => {0}", entity.id),
                    dataset = entity
                };
                return resp;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                throw;
            }
        }

        private List<int> GetAllType(int catId)
        {
            var types = new List<int>();
            var respTypes = _categoryCollectionRepository.Query(model => model.category_id == catId)
                                                         .Select(model => model.product_type_id)
                                                         .ToList();
            types.AddRange(respTypes);
            // Get child cats
            var childs = _categoryRepository.Query(model => model.parent_category_id == catId)
                                            .Select(model => model.id)
                                            .ToList();
            foreach (var item in childs)
            {
                var childTypes = GetAllType(item);
                if (childTypes.Count > 0)
                {
                    types.AddRange(childTypes);
                }
            }

            return types;
        }

    }
}
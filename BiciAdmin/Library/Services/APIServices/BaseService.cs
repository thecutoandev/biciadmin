﻿using System;
using DataCore.Data.Infrastructure;

namespace Services.APIServices
{
    public abstract class BaseService
    {
        private readonly IUnitOfWork unitOfWork;
        public BaseService(IUnitOfWork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }

        protected void SaveChanges()
        {
            unitOfWork.Commit();
        }
    }
}

﻿using System;
using AutoMapper;
using DataCore.Data.Infrastructure;
using DataCore.Data.Repositories;
using Services.Interfaces;
using Website.ViewModels;
using System.Linq;

namespace Services.APIServices
{
    public class ProductTypeService : BaseService, IProductTypeService
    {
        // Variables
        private readonly IBannerRepository _bannerRepository;
        private readonly IProductTypeRepository _productTypeRepository;
        private readonly IMapper mapper;

        public ProductTypeService(IUnitOfWork unitOfWork,
                                  IBannerRepository bannerRepository,
                                  IProductTypeRepository productTypeRepository,
                                  IMapper mapper) : base(unitOfWork)
        {
            _bannerRepository = bannerRepository;
            _productTypeRepository = productTypeRepository;
            this.mapper = mapper;
        }

        /// <summary>
        /// Search product type
        /// </summary>
        /// <param name="searchValue"></param>
        /// <returns></returns>
        public ResponseListModel<ProductTypeViewModel> SearchProductType(string searchValue)
        {
            try
            {
                var dataList = from pType in _productTypeRepository.Query(model => model.name.Contains(searchValue))
                               select new ProductTypeViewModel()
                               {
                                   id = pType.id,
                                   name = pType.name,
                                   created_by = pType.created_by,
                                   created_date = pType.created_date,
                                   updated_by = pType.updated_by,
                                   updated_date = pType.updated_date
                               };

                var respData = dataList.ToList();
                return new ResponseListModel<ProductTypeViewModel>()
                {
                    dataset = respData,
                    total = respData.Count
                };
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                throw;
            }
        }
    }
}

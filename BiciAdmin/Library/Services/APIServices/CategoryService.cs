﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using DataCore.Data.Infrastructure;
using DataCore.Data.Repositories;
using Services.Handlers;
using Website.ViewModels;
using Models = DataCore.Models;
using WaoInterfaces = Services.Interfaces;

namespace Services.APIServices
{
    public class CategoryService : BaseService, WaoInterfaces.ICategoryService
    {
        // Define singleton instances.
        //private readonly IUnitOfWork unitOfWork;
        private readonly ICategoryRepository _categoryRepository;
        private readonly ICategoryMediaRepository _categoryMediaRepository;
        private readonly IGenericProductRepository _genericProductRepository;
        private readonly IGenericProductMediaRepository _genericProductMediaRepository;
        private readonly ICategoryCollectionRepository _categoryCollectionRepository;
        private readonly IProductTypeRepository _productTypeRepository;
        private readonly WaoInterfaces.IGenericProductService _genericProductService;
        private readonly IMapper mapper;

        // Constructor
        public CategoryService(IUnitOfWork unitOfWork,
                               ICategoryRepository CategoryRepository,
                               ICategoryMediaRepository categoryMediaRepository,
                               IGenericProductRepository genericProductRepository,
                               IGenericProductMediaRepository genericProductMediaRepository,
                               ICategoryCollectionRepository categoryCollectionRepository,
                               IProductTypeRepository productTypeRepository,
                               WaoInterfaces.IGenericProductService genericProductService,
                               IMapper mapper) : base(unitOfWork)
        {
            //this.unitOfWork = unitOfWork;
            _categoryRepository = CategoryRepository;
            _categoryMediaRepository = categoryMediaRepository;
            _genericProductRepository = genericProductRepository;
            _genericProductMediaRepository = genericProductMediaRepository;
            _categoryCollectionRepository = categoryCollectionRepository;
            _productTypeRepository = productTypeRepository;
            _genericProductService = genericProductService;
            this.mapper = mapper;
        }

        /// <summary>
        /// Insert data to database, and response for client.
        /// </summary>
        /// <param name="viewModel">Model for inserting.</param>
        /// <returns>Response infomation.</returns>
        public ResponseStatus Create(CategoryViewModel viewModel)
        {
            try
            {
                // Mapping field value from view model to model entity
                var categoryEntity = mapper.Map<Models.Category>(viewModel);
                categoryEntity.created_date = DateTime.Now;
                categoryEntity.updated_date = DateTime.Now;
                // check exists category name
                var isExists = _categoryRepository.Query(model => model.name == viewModel.name).FirstOrDefault();
                if (isExists != null)
                {
                    throw new Exception("Tên phân loại đã tồn , vùi lòng nhập tên khác");
                }
                // Save data to database
                _categoryRepository.Add(categoryEntity);
                SaveChanges();
                // Create response data
                var resp = new ResponseStatus()
                {
                    successfull = true,
                    message = string.Format("Create data is successfull with id => {0}", categoryEntity.id),
                    dataset = categoryEntity
                };
                return resp;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                throw;
            }
        }

        public ResponseStatus CreateCategoryCollection(CategoryCollectionViewModel viewModel)
        {
            try
            {
                // Mapping field value from view model to model entity
                // check exists category name
                var entityUpdate = _categoryRepository.Query(model => model.id == viewModel.id).FirstOrDefault();
                if (entityUpdate == null)
                {
                    throw new Exception("Không tồn tại phân loại này.");
                }
                entityUpdate.name = viewModel.name;
                entityUpdate.parent_category_id = viewModel.parent_category_id;
                entityUpdate.created_date = DateTime.Now;
                entityUpdate.updated_date = DateTime.Now;
                // Save data to database
                _categoryRepository.Update(entityUpdate);
                SaveChanges();


                _categoryMediaRepository.Delete(model => model.category_id == entityUpdate.id);
                SaveChanges();
                foreach (var img in viewModel.medias)
                {
                    var media = new Models.CategoryMedia()
                    {
                        category_id = entityUpdate.id,
                        image_link = img
                    };
                    _categoryMediaRepository.Add(media);
                }
                SaveChanges();

                _categoryCollectionRepository.Delete(model => model.category_id == entityUpdate.id);
                SaveChanges();

                foreach (var item in viewModel.product_types)
                {
                    var entity = new Models.CategoryCollection()
                    {
                        category_id = entityUpdate.id,
                        product_type_id = item.id
                    };
                    _categoryCollectionRepository.Add(entity);
                }
                SaveChanges();
                // Create response data
                var resp = new ResponseStatus()
                {
                    successfull = true,
                    message = string.Format("Updating data is successfull with id => {0}", entityUpdate.id),
                    dataset = entityUpdate
                };
                return resp;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                throw;
            }
        }

        /// <summary>
        /// Delete data from database.
        /// </summary>
        /// <param name="viewModel">Model for deleting.</param>
        /// <returns>Response infomation</returns>
        public ResponseStatus Delete(CategoryViewModel viewModel)
        {
            try
            {
                var entity = _categoryRepository.Query(model => model.id == viewModel.id).FirstOrDefault();
                if (null == entity)
                {
                    throw new NotFoundDataException(viewModel.id.ToString(), typeof(Models.Collection));
                }
                _categoryRepository.Delete(entity);
                SaveChanges();
                return new ResponseStatus()
                {
                    successfull = true,
                    message = string.Format("Delete data is successfull with id => {0}", entity.id)
                };
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                throw;
            }
        }

        /// <summary>
        /// Delete database by id.
        /// </summary>
        /// <param name="id">Id of model.</param>
        /// <returns>Response infomation.</returns>
        public ResponseStatus DeleteById(int id)
        {
            try
            {
                _categoryRepository.Delete(model => model.id == id);
                SaveChanges();
                return new ResponseStatus()
                {
                    successfull = true,
                    message = string.Format("Delete data is successfull with id => {0}", id)
                };
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                throw;
            }
        }

        /// <summary>
        /// Get list categories
        /// </summary>
        /// <param name="rContext"></param>
        /// <returns></returns>
        public ResponseListModel<CategoryViewModel> GetChildsList(CategoriesRequest rContext)
        {
            try
            {
                rContext.page = rContext.page == 0 ? 1 : rContext.page;
                // Variables
                var sortingString = rContext.sortBy + (rContext.reverse ? " descending" : "");
                var isSearching = !string.IsNullOrEmpty(rContext.searchValue);
                if (isSearching)
                {
                    rContext.searchValue = rContext.searchValue.ToLower();
                }
                var responseData = new CategoryDatatable();

                // Fetching data from database.
                var collections = from model in _categoryRepository.GetAllQueryable()
                                  join parent in _categoryRepository.GetAllQueryable()
                                  on model.parent_category_id equals parent.id into pr
                                  from parent in pr.DefaultIfEmpty()
                                  where (!isSearching || (isSearching && model.name.Contains(rContext.searchValue))) &&
                                        (model.parent_category_id == rContext.parrentId || rContext.parrentId == -1) &&
                                        model.status == 1
                                  orderby sortingString
                                  select new CategoryViewModel()
                                  {
                                      id = model.id,
                                      name = model.name,
                                      parent_category_id = model.parent_category_id,
                                      status = model.status,
                                      ref_category_id = model.ref_category_id,
                                      parent_name = model.parent_category_id == 0 ? "Phân loại gốc" : parent.name
                                  };
                responseData.total = collections.Count();

                if (rContext.page != 0 && rContext.itemPerPage != -1)
                {
                    collections = collections.Skip((rContext.page - 1) * rContext.itemPerPage)
                                             .Take(rContext.itemPerPage);
                }
                // Fetching data from database
                responseData.data = collections.ToList();

                var listIds = responseData.data.Select(m => m.id).ToArray();

                var dataCountChilds = _categoryRepository.Query(m => listIds.Contains(m.parent_category_id))
                                                     .GroupBy(model => model.parent_category_id)
                                                     .Select(model => new CountViewModel
                                                     {
                                                         count_id = model.Key,
                                                         count = model.Count()
                                                     }).ToList();

                dataCountChilds.ForEach(model =>
                {
                    var dataEntity = responseData.data.FirstOrDefault(m => m.id == model.count_id);
                    if (dataEntity != null)
                    {
                        dataEntity.has_childs = model.count;
                    }

                });

                // Get medias
                if (rContext.isGetImages)
                {
                    var categoryIds = responseData.data.Select(model => model.id).ToArray();
                    var listImages = _categoryMediaRepository.Query(model => categoryIds.Contains(model.category_id)).ToArray();
                    responseData.data.ForEach(model =>
                    {
                        model.medias = listImages.Where(img => img.category_id == model.id)
                                                 .Select(img => img.image_link).ToList();
                    });
                }

                return new ResponseListModel<CategoryViewModel>()
                {
                    successfull = true,
                    dataset = responseData.data,
                    total = responseData.total
                };
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                throw;
            }
        }

        public ResponseListModel<CategoryViewModel> GetDatatables(CategoriesRequest rContext)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Get data detail info by id
        /// </summary>
        /// <param name="id">Id of data.</param>
        /// <returns>Response infomation.</returns>
        public ResponseSignleModel<CategoryViewModel> GetInfo(int id)
        {
            try
            {
                var category = _categoryRepository.Query(model => model.id == id).FirstOrDefault();
                if (null == category)
                {
                    throw new NotFoundDataException(id.ToString(), typeof(CategoryViewModel));
                }
                // Get list media links
                var linkMedias = _categoryMediaRepository.Query(model => model.category_id == category.id)
                                                         .Select(model => model.image_link).ToList();

                // Convert data to viewmodel
                var responseData = mapper.Map<CategoryViewModel>(category);

                // Check has childs
                var isHasChilds = _categoryRepository.Query(model => model.parent_category_id == category.id).Count();
                responseData.has_childs = isHasChilds;

                responseData.medias = linkMedias;

                var dataTypes = GetAllType(id).ToArray();

                // List limited generic product.
                var listProducts = from product in _genericProductRepository.GetAllQueryable()
                                   where dataTypes.Contains(product.category_id)
                                   select new GenericProductViewModel()
                                   {
                                       brand_id = product.brand_id,
                                       category_id = product.category_id,
                                       id = product.id,
                                       name = product.name,
                                       product_name = product.name,
                                       product_price = product.product_price,
                                       is_free_ship = product.is_free_ship
                                   };
                listProducts = listProducts.OrderBy(x => x.created_date).Take(10);

                responseData.products = listProducts.ToList();

                var listProductIds = responseData.products.Select(product => product.id).ToArray();
                var listProductImages = _genericProductService.GetGenericMedias(listProductIds);

                // Set images for products
                responseData.products.ForEach(product =>
                {
                    product.medias = listProductImages.Where(img => img.generic_product_id == product.id)
                                                      .Select(img => img.image_link).ToList();
                });
                // Create response status for responsing to client.
                return new ResponseSignleModel<CategoryViewModel>()
                {
                    successfull = true,
                    dataset = responseData
                };
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                throw;
            }
        }

        /// <summary>
        /// Get category info by admin.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ResponseSignleModel<CategoryCollectionViewModel> GetInfoByAdmin(int id)
        {
            try
            {
                var category = _categoryRepository.Query(model => model.id == id).FirstOrDefault();
                if (null == category)
                {
                    throw new NotFoundDataException(id.ToString(), typeof(CategoryCollectionViewModel));
                }
                // Get list media links
                var linkMedias = _categoryMediaRepository.Query(model => model.category_id == category.id)
                                                         .Select(model => model.image_link).ToList();

                // Convert data to viewmodel
                var responseData = mapper.Map<CategoryCollectionViewModel>(category);
                responseData.medias = linkMedias;

                var dataTypes = from catCollection in _categoryCollectionRepository.Query(model => model.category_id == id)
                                join pType in _productTypeRepository.GetAllQueryable()
                                on catCollection.product_type_id equals pType.id into pc
                                from pType in pc.DefaultIfEmpty()
                                select new ProductTypeViewModel()
                                {
                                    id = pType.id,
                                    name = pType.name,
                                    created_by = pType.created_by,
                                    created_date = pType.created_date,
                                    updated_by = pType.updated_by,
                                    updated_date = pType.updated_date
                                };

                responseData.product_types = dataTypes.ToList();

                return new ResponseSignleModel<CategoryCollectionViewModel>()
                {
                    dataset = responseData,
                    successfull = true
                };
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                throw;
            }
        }

        /// <summary>
        /// Update data to database
        /// </summary>
        /// <param name="viewModel">Model for updating.</param>
        /// <returns>Response infomation.</returns>
        public ResponseStatus Update(CategoryViewModel viewModel)
        {
            try
            {
                // Mapping field value from view model to model entity
                var categoryEntity = mapper.Map<Models.Category>(viewModel);
                // check exists category name
                var isExists = _categoryRepository.Query(model => model.id != viewModel.id &&
                                                                  model.name == categoryEntity.name).FirstOrDefault();
                if (isExists != null)
                {
                    throw new Exception("Tên phân loại đã tồn , vùi lòng nhập tên khác");
                }
                // Save data to database
                _categoryRepository.Update(categoryEntity);
                SaveChanges();
                // Create response data
                var resp = new ResponseStatus()
                {
                    successfull = true,
                    message = string.Format("Update data is successfull with id => {0}", categoryEntity.id),
                    dataset = categoryEntity
                };
                return resp;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                throw;
            }
        }

        private List<int> GetAllType(int catId)
        {
            var types = new List<int>();
            var respTypes = _categoryCollectionRepository.Query(model => model.category_id == catId)
                                                         .Select(model => model.product_type_id)
                                                         .ToList();
            types.AddRange(respTypes);
            // Get child cats
            var childs = _categoryRepository.Query(model => model.parent_category_id == catId)
                                            .Select(model => model.id)
                                            .ToList();
            foreach (var item in childs)
            {
                var childTypes = GetAllType(item);
                if (childTypes.Count > 0)
                {
                    types.AddRange(childTypes);
                }
            }

            return types;
        }
    }
}

using System;
using System.Linq;
using AutoMapper;
using DataCore.Data.Infrastructure;
using DataCore.Data.Repositories;
using Services.Handlers;
using Website.ViewModels;
using Models = DataCore.Models;
using WaoInterfaces = Services.Interfaces;

namespace Services.APIServices
{
    public class BrandService : BaseService, WaoInterfaces.IBrandService
    {
        // Define singleton instances.
        private readonly IUnitOfWork unitOfWork;
        private readonly IBrandRepository _brandRepository;
        private readonly IMapper mapper;

        // Constructor
        public BrandService(IUnitOfWork unitOfWork,
                                 IBrandRepository BrandRepository,
                                 IMapper mapper) : base(unitOfWork)
        {
            this.unitOfWork = unitOfWork;
            _brandRepository = BrandRepository;
            this.mapper = mapper;
        }

        /// <summary>
        /// Insert data to database.
        /// </summary>
        /// <param name="viewModel"></param>
        /// <returns></returns>
        public ResponseStatus Create(BrandViewModel viewModel)
        {
            try
            {
                // Mapping field value from view model to model entity
                var brandEntity = mapper.Map<Models.Brand>(viewModel);
                brandEntity.created_date = DateTime.Now;
                brandEntity.updated_date = DateTime.Now;
                var isExists = _brandRepository.Query(model => model.name == viewModel.name).FirstOrDefault();
                if (isExists != null)
                {
                    throw new Exception("Tên nhãn hiệu đã tồn tại, vùi lòng nhập tên khác");
                }
                // Save data to database
                _brandRepository.Add(brandEntity);
                SaveChanges();
                // Create response data
                var resp = new ResponseStatus()
                {
                    successfull = true,
                    message = string.Format("Create data is successfull with id => {0}", brandEntity.id),
                    dataset = brandEntity
                };
                return resp;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                throw;
            }
        }

        /// <summary>
        /// Delete data from database.
        /// </summary>
        /// <param name="viewModel"></param>
        /// <returns></returns>
        public ResponseStatus Delete(BrandViewModel viewModel)
        {
            try
            {
                // Checking exist data.
                var entity = _brandRepository.Query(model => model.id == viewModel.id).FirstOrDefault();
                if (null == entity)
                {
                    throw new NotFoundDataException(viewModel.id.ToString(), typeof(Models.Collection));
                }
                _brandRepository.Delete(entity);
                SaveChanges();
                return new ResponseStatus()
                {
                    successfull = true,
                    message = string.Format("Delete data is successfull with id => {0}", entity.id)
                };
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                throw;
            }
        }

        /// <summary>
        /// Delete data by primary key.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ResponseStatus DeleteById(int id)
        {
            try
            {
                _brandRepository.Delete(model => model.id == id);
                SaveChanges();
                return new ResponseStatus()
                {
                    successfull = true,
                    message = string.Format("Delete data is successfull with id => {0}", id)
                };
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                throw;
            }
        }

        /// <summary>
        /// Get datatable with pagination info.
        /// </summary>
        /// <param name="rContext"></param>
        /// <returns></returns>
        public ResponseListModel<BrandViewModel> GetDatatables(PaggingBase rContext)
        {
            try
            {
                // Variables
                var sortingString = rContext.sortBy + (rContext.reverse ? " descending" : "");
                var isSearching = !string.IsNullOrEmpty(rContext.searchValue);
                if (isSearching)
                {
                    rContext.searchValue = rContext.searchValue.ToLower();
                }
                var responseData = new BrandDatatable();

                // Fetching data from database.
                var collections = from model in _brandRepository.GetAllQueryable()
                                  where (!isSearching || (isSearching && model.name.Contains(rContext.searchValue)))
                                  orderby sortingString
                                  select new BrandViewModel()
                                  {
                                      id = model.id,
                                      name = model.name,
                                      ref_brand_id = model.ref_brand_id,
                                  };
                responseData.total = collections.Count();

                // Paging data
                collections = collections.Where(model => _brandRepository.GetAllQueryable()
                                                                        .OrderBy(x => x.id)
                                                                        .Select(x => x.id)
                                                                        .Skip((rContext.page - 1) * rContext.itemPerPage)
                                                                        .Take(rContext.itemPerPage).Contains(model.id)
                                               );

                // Fetching data from database
                responseData.data = collections.ToList();
                return new ResponseListModel<BrandViewModel>()
                {
                    successfull = true,
                    dataset = responseData.data,
                    total = responseData.total
                };
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                throw;
            }
        }


        /// <summary>
        /// Get detail info by id.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ResponseSignleModel<BrandViewModel> GetInfo(int id)
        {
            try
            {
                var collection = _brandRepository.Query(model => model.id == id).FirstOrDefault();
                if (null == collection)
                {
                    throw new NotFoundDataException(id.ToString(), typeof(BrandViewModel));
                }
                var respData = mapper.Map<BrandViewModel>(collection);
                return new ResponseSignleModel<BrandViewModel>()
                {
                    successfull = true,
                    dataset = respData
                };
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                throw;
            }
        }

        /// <summary>
        /// Update data.
        /// </summary>
        /// <param name="viewModel"></param>
        /// <returns></returns>
        public ResponseStatus Update(BrandViewModel viewModel)
        {
            try
            {
                // Mapping field value from view model to model entity
                var entity = mapper.Map<Models.Brand>(viewModel);
                var isExists = _brandRepository.Query(model => model.id != viewModel.id && model.name == viewModel.name).FirstOrDefault();
                if (isExists != null)
                {
                    throw new Exception("Tên nhãn hiệu đã tồn tại, vùi lòng nhập tên khác");
                }
                // Save data to database
                _brandRepository.Update(entity);
                SaveChanges();
                // Create response data
                var resp = new ResponseStatus()
                {
                    successfull = true,
                    message = string.Format("Update data is successfull with id => {0}", entity.id),
                    dataset = entity
                };
                return resp;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                throw;
            }
        }

    }
}
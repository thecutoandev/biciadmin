﻿using System;
using System.Linq;
using AutoMapper;
using DataCore.Data.Infrastructure;
using DataCore.Data.Repositories;
using Services.Handlers;
using Website.ViewModels;
using Models = DataCore.Models;
using WaoInterfaces = Services.Interfaces;

namespace Services.APIServices 
{
    public class BannerService : BaseService, WaoInterfaces.IBannerService
    {
        // Variables
        private readonly IBannerRepository _bannerRepository;
        private readonly IMapper mapper;

        public BannerService(IUnitOfWork unitOfWork,
                            IBannerRepository bannerRepository,
                            IMapper mapper) : base(unitOfWork)
        {
            _bannerRepository = bannerRepository;
            this.mapper = mapper;
        }
        public ResponseStatus Create(BannerViewModel viewModel)
        {
            try
            {
                // Mapping field value from view model to model entity
                var bannerEntity = mapper.Map<Models.Banner>(viewModel);
                bannerEntity.created_date = DateTime.Now;
                bannerEntity.updated_date = DateTime.Now;
                // Save data to database
                _bannerRepository.Add(bannerEntity);
                SaveChanges();
                // Create response data
                var resp = new ResponseStatus()
                {
                    successfull = true,
                    message = string.Format("Create data is successfull with id => {0}", bannerEntity.id),
                    dataset = bannerEntity
                };
                return resp;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                throw;
            }
        }

        public ResponseStatus Delete(BannerViewModel viewModel)
        {
            throw new NotImplementedException();
        }

        public ResponseStatus DeleteById(int id)
        {
            try
            {
                _bannerRepository.Delete(model => model.id == id);
                SaveChanges();
                return new ResponseStatus()
                {
                    successfull = true,
                    message = string.Format("Delete data is successfull with id => {0}", id)
                };
            }
            catch (Exception ex)
            {
                return new ResponseStatus()
                {
                    successfull = false,
                    message = ex.Message
                };
            }
        }

        public ResponseListModel<BannerViewModel> GetAll(bool isGetAll)
        {
            try
            {
                var datalist = (from banner in _bannerRepository.GetAllQueryable()
                                where (isGetAll || ( !isGetAll && banner.isActive == true))
                                orderby banner.created_date
                                select new BannerViewModel()
                                {
                                    id = banner.id,
                                    image = banner.image,
                                    type = banner.type,
                                    url = banner.url,
                                    isActive = banner.isActive,
                                    created_by = banner.created_by,
                                    created_date = banner.created_date,
                                    updated_by = banner.updated_by,
                                    updated_date = banner.updated_date
                                }).ToList();
                return new ResponseListModel<BannerViewModel>()
                {
                    successfull = true,
                    dataset = datalist,
                    total = datalist.Count
                };
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                throw;
            }
        }

        public ResponseListModel<BannerViewModel> GetDatatables(PaggingBase rContext)
        {
            throw new NotImplementedException();
        }

        public ResponseSignleModel<BannerViewModel> GetInfo(int id)
        {
            try
            {
                var collection = _bannerRepository.Query(model => model.id == id).FirstOrDefault();
                var responseData = mapper.Map<BannerViewModel>(collection);
                if (null == collection)
                {
                    throw new NotFoundDataException(id.ToString(), typeof(BlogViewModel));
                }
                return new ResponseSignleModel<BannerViewModel>()
                {
                    successfull = true,
                    dataset = responseData
                };
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                throw;
            }
        }

        public ResponseStatus Update(BannerViewModel viewModel)
        {
            try
            {
                // Mapping field value from view model to model entity
                var bannerEntity = mapper.Map<Models.Banner>(viewModel);
                bannerEntity.created_date = DateTime.Now;
                bannerEntity.updated_date = DateTime.Now;
                // Save data to database
                _bannerRepository.Update(bannerEntity);
                SaveChanges();
                // Create response data
                var resp = new ResponseStatus()
                {
                    successfull = true,
                    message = string.Format("Create data is successfull with id => {0}", bannerEntity.id),
                    dataset = bannerEntity
                };
                return resp;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                throw;
            }
        }
    }
}

﻿using System;
using Services.Interfaces;
using Website.ViewModels;
using KiotService.Interfaces;
using DataCore.Data.Repositories;
using DataCore.Data.Infrastructure;
using System.Linq;
using AutoMapper;
using DataCore.Models;
using System.Collections.Generic;

namespace Services.APIServices
{
    public class BranchPickService : BaseService, IBranchPickService
    {
        private readonly IKBranchesService kBranchesService;
        private readonly IBranchPickPriorityRepository branchPickPriorityRepository;
        private readonly IMapper mapper;
        public BranchPickService(IKBranchesService kBranchesService,
                                 IBranchPickPriorityRepository branchPickPriorityRepository,
                                 IMapper mapper,
                                 IUnitOfWork unitOfWork) : base(unitOfWork)
        {
            this.kBranchesService = kBranchesService;
            this.branchPickPriorityRepository = branchPickPriorityRepository;
            this.mapper = mapper;
        }

        /// <summary>
        /// Get all list priority
        /// </summary>
        /// <returns></returns>
        public ResponseListModel<BranchPickViewModel> GetListBranches()
        {
            try
            {
                var branches = kBranchesService.GetAllBranches().dataset;
                var prorities = (from branch in branchPickPriorityRepository.GetAllQueryable()
                                 select new BranchPickViewModel()
                                 {
                                     branch_id = branch.branch_id,
                                     priority = branch.priority,
                                     max_auto_pick = branch.max_auto_pick,
                                     id = branch.id
                                 }).ToList();
                var listData = new List<BranchPickViewModel>();
                // Mapping name
                branches.ForEach(model =>
                {
                    var pBranch = prorities.FirstOrDefault(b => b.branch_id == model.id);
                    listData.Add(new BranchPickViewModel()
                    {
                        branch_id = model.id,
                        branch_name = model.branchName,
                        priority = pBranch == null ? 999 : pBranch.priority,
                        max_auto_pick = pBranch == null ? 0 : pBranch.max_auto_pick,
                        //id = pBranch.id
                    });
                });

                return new ResponseListModel<BranchPickViewModel>()
                {
                    dataset = listData,
                    total = listData.Count,
                    successfull = true
                };

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                throw;
            }
        }

        /// <summary>
        /// Update priority
        /// </summary>
        /// <param name="viewModel"></param>
        /// <returns></returns>
        public ResponseStatus UpdatePriority(BranchPickUpdate viewModel)
        {
            try
            {
                branchPickPriorityRepository.Delete(model => true);
                SaveChanges();

                foreach (var item in viewModel.branches)
                {
                    var entity = mapper.Map<BranchPickPriority>(item);
                    entity.id = 0;
                    branchPickPriorityRepository.Add(item);
                }
                SaveChanges();

                return new ResponseStatus()
                {
                    dataset = viewModel.branches,
                    successfull = true
                };
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                throw;
            }
        }
    }
}

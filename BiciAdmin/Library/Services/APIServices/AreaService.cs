using System;
using System.Linq;
using AutoMapper;
using DataCore.Data.Infrastructure;
using DataCore.Data.Repositories;
using Services.Handlers;
using Website.ViewModels;
using Models = DataCore.Models;
using WaoInterfaces = Services.Interfaces;

namespace Services.APIServices
{
    public class AreaService : BaseService, WaoInterfaces.IAreaService
    {
        // Define singleton instances.
        private readonly IUnitOfWork unitOfWork;
        private readonly IAreaRepository _areaRepository;
        private readonly IDistrictRepository _districtRepository;
        private readonly IAreaDistrictRepository _areaDistrictRepository;
        private readonly ICityRepository _cityRepository;
        private readonly IMapper mapper;

        // Constructor
        public AreaService(IUnitOfWork unitOfWork,
                                 IAreaRepository AreaRepository,
                                 IDistrictRepository districtRepository,
                                 IAreaDistrictRepository areaDistrictRepository,
                                 ICityRepository cityRepository,
                                 IMapper mapper) : base(unitOfWork)
        {
            this.unitOfWork = unitOfWork;
            _areaRepository = AreaRepository;
            this._districtRepository = districtRepository;
            this._areaDistrictRepository = areaDistrictRepository;
            this._cityRepository = cityRepository;
            this.mapper = mapper;
        }

        /// <summary>
        /// Insert data to database.
        /// </summary>
        /// <param name="viewModel"></param>
        /// <returns></returns>
        public ResponseStatus Create(AreaViewModel viewModel)
        {
            try
            {
                // Mapping field value from view model to model entity
                var areaEntity = mapper.Map<Models.Area>(viewModel);
                areaEntity.created_date = DateTime.Now;
                areaEntity.updated_date = DateTime.Now;
                // Save data to database
                _areaRepository.Add(areaEntity);
                SaveChanges();
                // Create response data
                var resp = new ResponseStatus()
                {
                    successfull = true,
                    message = string.Format("Create data is successfull with id => {0}", areaEntity.id),
                    dataset = areaEntity
                };
                return resp;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                throw;
            }
        }

        /// <summary>
        /// Delete data from database.
        /// </summary>
        /// <param name="viewModel"></param>
        /// <returns></returns>
        public ResponseStatus Delete(AreaViewModel viewModel)
        {
            try
            {
                // Checking exist data.
                var entity = _areaRepository.Query(model => model.id == viewModel.id).FirstOrDefault();
                if (null == entity)
                {
                    throw new NotFoundDataException(viewModel.id.ToString(), typeof(Models.Collection));
                }
                _areaRepository.Delete(entity);
                SaveChanges();
                return new ResponseStatus()
                {
                    successfull = true,
                    message = string.Format("Delete data is successfull with id => {0}", entity.id)
                };
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                throw;
            }
        }

        /// <summary>
        /// Delete data by primary key.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ResponseStatus DeleteById(int id)
        {
            try
            {
                _areaRepository.Delete(model => model.id == id);
                SaveChanges();
                _areaDistrictRepository.Delete(model => model.area_id == id);
                SaveChanges();
                return new ResponseStatus()
                {
                    successfull = true,
                    message = string.Format("Delete data is successfull with id => {0}", id)
                };
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                throw;
            }
        }

        /// <summary>
        /// Get datatable with pagination info.
        /// </summary>
        /// <param name="rContext"></param>
        /// <returns></returns>
        public ResponseListModel<AreaViewModel> GetDatatables(PaggingBase rContext)
        {
            try
            {
                // Variables
                var sortingString = rContext.sortBy + (rContext.reverse ? " descending" : "");
                var isSearching = !string.IsNullOrEmpty(rContext.searchValue);
                if (isSearching)
                {
                    rContext.searchValue = rContext.searchValue.ToLower();
                }
                var responseData = new AreaDatatable();

                // Fetching data from database.
                var collections = from model in _areaRepository.GetAllQueryable()
                                  orderby sortingString
                                  select new AreaViewModel()
                                  {
                                      id = model.id,
                                      name = model.name,
                                      limit_transaction_amount = model.limit_transaction_amount,
                                      fee_amount = model.fee_amount,
                                  };
                responseData.total = collections.Count();

                // Paging data
                collections = collections.Where(model => _areaRepository.GetAllQueryable()
                                                                              .OrderBy(x => x.id)
                                                                              .Select(x => x.id)
                                                                              .Skip((rContext.page - 1) * rContext.itemPerPage)
                                                                              .Take(rContext.itemPerPage).Contains(model.id)
                                               );

                // Fetching data from database
                responseData.data = collections.ToList();
                return new ResponseListModel<AreaViewModel>()
                {
                    successfull = true,
                    dataset = responseData.data,
                    total = responseData.total
                };
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                throw;
            }
        }


        /// <summary>
        /// Get detail info by id.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ResponseSignleModel<AreaViewModel> GetInfo(int id)
        {
            try
            {
                var collection = _areaRepository.Query(model => model.id == id).FirstOrDefault();
                if (null == collection)
                {
                    throw new NotFoundDataException(id.ToString(), typeof(AreaViewModel));
                }
                var respData = mapper.Map<AreaViewModel>(collection);
                var districts = from ad in _areaDistrictRepository.GetAllQueryable()
                                join d in _districtRepository.GetAllQueryable()
                                on ad.district_id equals d.id into dlist
                                from district in dlist.DefaultIfEmpty()
                                join c in _cityRepository.GetAllQueryable()
                                on district.city_id equals c.id into cd
                                from city in cd.DefaultIfEmpty()
                                where ad.area_id == respData.id
                                select new DistrictViewModel()
                                {
                                    city_id = district.city_id,
                                    id = district.id,
                                    name = district.name,
                                    city_name = city.name
                                };
                respData.districts = districts.ToList();
                return new ResponseSignleModel<AreaViewModel>()
                {
                    successfull = true,
                    dataset = respData
                };
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                throw;
            }
        }

        /// <summary>
        /// Update data.
        /// </summary>
        /// <param name="viewModel"></param>
        /// <returns></returns>
        public ResponseStatus Update(AreaViewModel viewModel)
        {
            try
            {
                // Mapping field value from view model to model entity
                var entity = mapper.Map<Models.Area>(viewModel);
                // Save data to database
                _areaRepository.Update(entity);
                SaveChanges();

                _areaDistrictRepository.Delete(model => model.area_id == entity.id);
                SaveChanges();

                foreach (var district in viewModel.districts)
                {
                    var dEntity = new Models.AreaDistrict()
                    {
                        area_id = entity.id,
                        district_id = district.id,
                    };
                    _areaDistrictRepository.Add(dEntity);
                }
                SaveChanges();
                // Create response data
                var resp = new ResponseStatus()
                {
                    successfull = true,
                    message = string.Format("Update data is successfull with id => {0}", entity.id),
                    dataset = entity
                };
                return resp;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                throw;
            }
        }

    }
}

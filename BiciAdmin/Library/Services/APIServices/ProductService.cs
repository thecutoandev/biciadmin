﻿using System;
using System.Linq;
using AutoMapper;
using DataCore.Data.Infrastructure;
using DataCore.Data.Repositories;
using Services.Handlers;
using Website.ViewModels;
using Models = DataCore.Models;
using WaoInterfaces = Services.Interfaces;

namespace Services.APIServices
{
    public class ProductService : BaseService, WaoInterfaces.IProductService
    {
        // Define singleton instances.
        private readonly IUnitOfWork unitOfWork;
        private readonly IProductRepository _productRepository;
        private readonly IGenericProductRepository _genericProductRepository;
        private readonly IMapper mapper;

        // Constructor
        public ProductService(IUnitOfWork unitOfWork,
                                 IProductRepository ProductRepository,
                                 IGenericProductRepository genericProductRepository,
                                 IMapper mapper) : base(unitOfWork)
        {
            this.unitOfWork = unitOfWork;
            _productRepository = ProductRepository;
            _genericProductRepository = genericProductRepository;
            this.mapper = mapper;
        }

        /// <summary>
        /// Insert data to database.
        /// </summary>
        /// <param name="viewModel"></param>
        /// <returns></returns>
        public ResponseStatus Create(ProductViewModel viewModel)
        {
            try
            {
                // Mapping field value from view model to model entity
                var productEntity = mapper.Map<Models.Product>(viewModel);
                productEntity.created_date = DateTime.Now;
                productEntity.updated_date = DateTime.Now;
                // Save data to database
                _productRepository.Add(productEntity);
                SaveChanges();
                // Create response data
                var resp = new ResponseStatus()
                {
                    successfull = true,
                    message = string.Format("Create data is successfull with id => {0}", productEntity.id),
                    dataset = productEntity
                };
                return resp;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                throw;
            }
        }

        /// <summary>
        /// Delete data from database.
        /// </summary>
        /// <param name="viewModel"></param>
        /// <returns></returns>
        public ResponseStatus Delete(ProductViewModel viewModel)
        {
            try
            {
                // Checking exist data.
                var entity = _productRepository.Query(model => model.id == viewModel.id).FirstOrDefault();
                if (null == entity)
                {
                    throw new NotFoundDataException(viewModel.id.ToString(), typeof(Models.Collection));
                }
                _productRepository.Delete(entity);
                SaveChanges();
                return new ResponseStatus()
                {
                    successfull = true,
                    message = string.Format("Delete data is successfull with id => {0}", entity.id)
                };
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                throw;
            }
        }

        /// <summary>
        /// Delete data by primary key.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ResponseStatus DeleteById(int id)
        {
            try
            {
                _productRepository.Delete(model => model.id == id);
                SaveChanges();
                return new ResponseStatus()
                {
                    successfull = true,
                    message = string.Format("Delete data is successfull with id => {0}", id)
                };
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                throw;
            }
        }

        /// <summary>
        /// Get datatable with pagination info.
        /// </summary>
        /// <param name="rContext"></param>
        /// <returns></returns>
        public ResponseListModel<ProductViewModel> GetDatatables(ProductRequest rContext)
        {
            try
            {
                // Variables
                //var sortingString = rContext.sortBy + (rContext.reverse ? " descending" : "");
                var isSearching = !string.IsNullOrEmpty(rContext.searchValue);
                if (isSearching)
                {
                    rContext.searchValue = rContext.searchValue.ToLower();
                }
                var responseData = new ProductDatatable();

                // Fetching data from database.
                var collections = from model in _productRepository.GetAllQueryable()
                                  where model.generic_product_id == rContext.generic_product_id &&
                                        (!isSearching ||
                                        (isSearching &&
                                        (model.product_name.Contains(rContext.searchValue) || model.product_code == rContext.searchValue )))
                                  //orderby sortingString
                                  select new ProductViewModel()
                                  {
                                      id = model.id,
                                      product_code = model.product_code,
                                      product_name = model.product_name,
                                      product_price = model.product_price,
                                      generic_product_id = model.generic_product_id,
                                      link = model.link,
                                      kiot_product_code = model.kiot_product_code,

                                  };

                // Sorting database.
                var param = rContext.sortBy;
                var pi = typeof(ProductViewModel).GetProperty(param);
                collections = rContext.reverse ? collections.OrderByDescending(x => pi.GetValue(x, null)) : collections.OrderBy(x => pi.GetValue(x, null));

                responseData.total = collections.Count();

                // Paging data
                collections = collections.Skip((rContext.page - 1) * rContext.itemPerPage)
                                         .Take(rContext.itemPerPage);
                //.Where(model => _productRepository.Query(x => x.generic_product_id == rContext.generic_protuct_id)
                //                                                          .OrderBy(x => x.id)
                //                                                          .Select(x => x.id)
                //                                                          .Skip((rContext.page - 1) * rContext.itemPerPage)
                //                                                          .Take(rContext.itemPerPage).Contains(model.id)
                //                           );

                // Fetching data from database
                responseData.data = collections.ToList();
                return new ResponseListModel<ProductViewModel>()
                {
                    successfull = true,
                    dataset = responseData.data,
                    total = responseData.total
                };
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                throw;
            }
        }

        /// <summary>
        /// Get detail info by id.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ResponseSignleModel<ProductViewModel> GetInfo(int id)
        {
            try
            {
                var collection = _productRepository.Query(model => model.id == id).FirstOrDefault();
                if (null == collection)
                {
                    throw new NotFoundDataException(id.ToString(), typeof(ProductViewModel));
                }
                var respData = mapper.Map<ProductViewModel>(collection);
                return new ResponseSignleModel<ProductViewModel>()
                {
                    successfull = true,
                    dataset = respData
                };
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                throw;
            }
        }

        /// <summary>
        /// Update data.
        /// </summary>
        /// <param name="viewModel"></param>
        /// <returns></returns>
        public ResponseStatus Update(ProductViewModel viewModel)
        {
            try
            {
                // Mapping field value from view model to model entity
                var entity = mapper.Map<Models.Product>(viewModel);
                // Save data to database
                _productRepository.Update(entity);
                SaveChanges();
                // Create response data
                var resp = new ResponseStatus()
                {
                    successfull = true,
                    message = string.Format("Update data is successfull with id => {0}", entity.id),
                    dataset = entity
                };
                return resp;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                throw;
            }
        }

    }
}
﻿using System;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using DataCore.Data.Repositories;
using DataCore.Models;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Logging;
using Microsoft.IdentityModel.Tokens;
using Services.Interfaces;
using System.Net.Http;
using Newtonsoft.Json;
using DataCore.Models.Auth;
using System.Net;
using Website.ViewModels;
using Services.Handlers;
using DataCore.Data.Infrastructure;
using AutoMapper;
using FirebaseAdmin.Auth;
using KiotService.Interfaces;

namespace Services.APIServices
{
    public class UserService : IUserService
    {
        private readonly IUserRepository _userRepository;
        private readonly AppSettings _appSettings;
        private readonly FaceBookAuthSettings _faceBookAuthSettings;
        private readonly AccountKitAuthSettings _accountKitAuthSettings;
        private readonly IKBranchesService kBranchesService;
        private readonly IUnitOfWork unitOfWork;
        private readonly IMapper mapper;
        private readonly HttpClient _client;

        public UserService(IUserRepository userRepository,
                           IUnitOfWork unitOfWork,
                           IOptions<AppSettings> appSettings,
                           IMapper mapper,
                           IKBranchesService kBranchesService)
        {
            this._userRepository = userRepository;
            this._appSettings = appSettings.Value;
            this._faceBookAuthSettings = this._appSettings.FacebookAuthSettings;
            this._accountKitAuthSettings = this._appSettings.AccountKitAuthSettings;
            this.unitOfWork = unitOfWork;
            this._client = new HttpClient();
            this.mapper = mapper;
            this.kBranchesService = kBranchesService;
        }

        /// <summary>
        /// Validate user name and password. (authorization method).
        /// </summary>
        /// <param name="userName"></param>
        /// <param name="passWord"></param>
        /// <returns></returns>
        public async Task<AdminAuthModel> Authenticate(string userName, string passWord)
        {
            var userLogin = await _userRepository.FindUser(userName, passWord);

            // If user is not found, return null;
            if (userLogin == null)
            {
                return null;
            }

            switch (userLogin.Status)
            {
                case 1:
                    return (AdminAuthModel)GenerateAccessToken(userLogin, -1, true);
                case 2:
                case 3:
                    return new AdminAuthModel()
                    {
                        token = string.Empty,
                        is_authenticated = false
                    };
                default:
                    return new AdminAuthModel()
                    {
                        token = string.Empty,
                        is_authenticated = false
                    };
            }

            // Authencate is success, so generate jwt token for client.
            
        }

        /// <summary>
        /// Authentication by firebase.
        /// </summary>
        /// <param name="idToken"></param>
        /// <returns></returns>
        public async Task<AuthModel> FireBaseAuthenticate(string idToken)
        {
            try
            {
                //FirebaseToken decodedToken = await FirebaseAuth.DefaultInstance
                //                                           .VerifyIdTokenAsync(idToken);

                // If user is not found, return null;
                string uid = idToken;

                UserRecord userRecord = await FirebaseAuth.DefaultInstance.GetUserAsync(uid);

                var userDB = _userRepository.FindUserByPhone(userRecord.PhoneNumber);

                if (userDB == null)
                {
                    var appUser = new UserViewModel
                    {
                        FBEmail = string.Empty,
                        Email = string.Empty,
                        UserName = userRecord.PhoneNumber,
                        Avatar = string.Empty,
                        Id = userRecord.PhoneNumber,
                        EmailConfirmed = false,
                        PhoneNumberConfirmed = true,
                        BirthDay = null,
                        PhoneNumber = userRecord.PhoneNumber,
                        BranchId = 0,
                    };

                    var result = await _userRepository.RegisterUser(appUser);

                    if (!result.Succeeded) return null;
                }

                var localUser = _userRepository.Query(model => model.UserName == userRecord.PhoneNumber || model.PhoneNumber == userRecord.PhoneNumber).SingleOrDefault();
                return (AuthModel)GenerateAccessToken(localUser, -1);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return null;
            }
            
        }

        private object GenerateAccessToken(User userLogin, int numberOfDayForExpireToken = 7, bool isAdminApi = false)
        {
            try
            {
                // Time expire token
                var day = numberOfDayForExpireToken == -1 ? 365 : numberOfDayForExpireToken;
                // Authencate is success, so generate jwt token for client.
                var tokenHandler = new JwtSecurityTokenHandler();
                var keyJwt = Encoding.ASCII.GetBytes(_appSettings.JwtKey);

                var tokenDescriptor = new SecurityTokenDescriptor()
                {
                    Subject = new ClaimsIdentity(new Claim[]
                    {
                        new Claim(ClaimTypes.Name, userLogin.UserName)
                    }),
                    Expires = DateTime.Now.AddDays(day),
                    SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(keyJwt), SecurityAlgorithms.HmacSha256Signature)
                };
                IdentityModelEventSource.ShowPII = true;
                var token = tokenHandler.CreateToken(tokenDescriptor);
                if (!isAdminApi)
                {
                    return new AuthModel()
                    {
                        name = userLogin.FullName,
                        gender = userLogin.Gender,
                        birthDay = userLogin.BirthDay,
                        token = tokenHandler.WriteToken(token),
                        avatar = userLogin.Avatar,
                        fbEmail = userLogin.FBEmail,
                        phone = userLogin.PhoneNumber,
                        role = userLogin.RoleId
                    };
                }
                else
                {
                    return new AdminAuthModel()
                    {
                        name = userLogin.FullName,
                        gender = userLogin.Gender,
                        birthDay = userLogin.BirthDay,
                        token = tokenHandler.WriteToken(token),
                        avatar = userLogin.Avatar,
                        fbEmail = userLogin.FBEmail,
                        phone = userLogin.PhoneNumber,
                        branch_id = userLogin.BranchId,
                        role = userLogin.RoleId,
                        is_authenticated = true
                    };
                }
            }
            catch
            {
                throw new Exception("Authorize is failed");
            }

        }

        public async Task<AuthModel> FaceBookAuthenticate(string accessToken)
        {
            // 1.generate an app access token
            var appAccessTokenResponse = await _client.GetStringAsync(string.Format(_faceBookAuthSettings.AppTokenEndPoint, _faceBookAuthSettings.AppId, _faceBookAuthSettings.AppSecret));
            var appAccessToken = JsonConvert.DeserializeObject<FacebookAppAccessToken>(appAccessTokenResponse);

            // 2. validate the user access token
            var userAccessTokenValidationResponse = await _client.GetStringAsync(string.Format(_faceBookAuthSettings.ValidateTokenEndPoint, accessToken, appAccessToken.AccessToken));
            var userAccessTokenValidation = JsonConvert.DeserializeObject<FacebookUserAccessTokenValidation>(userAccessTokenValidationResponse);

            if (!userAccessTokenValidation.Data.IsValid)
            {
                return null;
            }

            // 3. we've got a valid token so we can request user data from fb
            var userInfoResponse = await _client.GetStringAsync(string.Format(_faceBookAuthSettings.UserInfoEndPoint, accessToken));
            var userInfo = JsonConvert.DeserializeObject<FacebookUserData>(userInfoResponse);

            var userDB = await _userRepository.FindUserByMail(userInfo.Email);

            if (userDB == null)
            {
                var appUser = new UserViewModel
                {
                    FBEmail = userInfo.Email,
                    Email = userInfo.Email,
                    UserName = userInfo.Email,
                    Avatar = userInfo.Picture.Data.Url,
                    Id = userInfo.Email,
                    EmailConfirmed = true,
                    BirthDay = userInfo.Birthday
                };

                var result = await _userRepository.RegisterUser(appUser);

                if (!result.Succeeded) return null;
            }

            var localUser = _userRepository.Query(model => model.Email == userInfo.Email).SingleOrDefault();
            return (AuthModel)GenerateAccessToken(localUser, -1);
        }

        /// <summary>
        /// Register new user.
        /// </summary>
        /// <param name="userViewModel"></param>
        /// <returns></returns>
        public async Task<User> Register(UserViewModel userViewModel)
        {
            if (_userRepository.Query(model => model.NormalizedUserName == userViewModel.UserName.ToUpper()).Any())
            {
                throw new Exception("Tên đăng nhập đã tồn tại. Vui lòng chọn tên đăng nhập khác.");
            }
            var user = await _userRepository.RegisterUser(userViewModel);
            if (user.Succeeded)
            {
                var userDB = _userRepository.Query(model => model.UserName == userViewModel.UserName).SingleOrDefault();
                return userDB;
            }
            else
            {
                var error = user.Errors.FirstOrDefault().Description;
                throw new Exception(error);
            }
        }

        /// <summary>
        /// Get user info by user name
        /// </summary>
        /// <param name="userName"></param>
        /// <returns></returns>
        public ResponseSignleModel<UserInfoViewModel> GetUser(string userName)
        {
            try
            {
                var user = _userRepository.Query(model => model.UserName == userName).SingleOrDefault();
                if (user != null)
                {
                    var resp = new UserInfoViewModel()
                    {
                        user_name = user.UserName,
                        avatar = user.Avatar,
                        birth_day = user.BirthDay,
                        email = user.Email,
                        full_name = user.FullName,
                        gender = user.Gender,
                        phone = user.PhoneNumber,
                        role_id = user.RoleId,
                        branch_id = user.BranchId
                    };
                    return new ResponseSignleModel<UserInfoViewModel>()
                    {
                        dataset = resp
                    };
                }
                throw new Exception("Not found user");
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                throw;
            }
            
        }

        /// <summary>
        /// Update user info
        /// </summary>
        /// <param name="userViewModel"></param>
        /// <returns></returns>
        public async Task<User> Update(UserViewModel userViewModel)
        {
            try
            {
                var user = await _userRepository.UpdateUser(userViewModel);
                if (user.Succeeded)
                {
                    var userInfo = _userRepository.Query(model => model.UserName == userViewModel.UserName).SingleOrDefault();
                    return userInfo;
                }
                return null;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                throw;
            }
            
        }

        /// <summary>
        /// Update user info
        /// </summary>
        /// <param name="userViewModel"></param>
        /// <returns></returns>
        public async Task<User> UpdateAvatar(UserViewModel userViewModel)
        {
            try
            {
                var userData = _userRepository.Query(model => model.UserName == userViewModel.UserName).FirstOrDefault();
                if (userData == null)
                {
                    throw new Exception("Not found user data.");
                }
                var dataUpdate = mapper.Map<UserViewModel>(userData);
                dataUpdate.Avatar = userViewModel.Avatar;
                var user = await _userRepository.UpdateUser(dataUpdate);
                if (user.Succeeded)
                {
                    var userInfo = _userRepository.Query(model => model.UserName == userViewModel.UserName).SingleOrDefault();
                    return userInfo;
                }
                return null;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                throw;
            }

        }

        public async Task<AuthModel> PhoneAuthenticate(string accessToken)
        {
            var tokenSecret = Utilities.GetHash(accessToken, _faceBookAuthSettings.AppSecret);
            // 1. Authorize
            var serverResponse = await _client.GetAsync(string.Format(_accountKitAuthSettings.AuthorizeTokenEndPoint, accessToken, tokenSecret));
            if (serverResponse.StatusCode != HttpStatusCode.OK)
            {
                return null;
            }
            string jsonResponse = await serverResponse.Content.ReadAsStringAsync();
            var dataValidated = JsonConvert.DeserializeObject<PhoneAuth>(jsonResponse);

            var userDB = _userRepository.FindUserByPhone(dataValidated.Phone.Number);

            if (userDB == null)
            {
                var appUser = new UserViewModel
                {
                    FBEmail = string.Empty,
                    Email = string.Empty,
                    UserName = dataValidated.Phone.Number,
                    Avatar = string.Empty,
                    Id = dataValidated.Phone.Number,
                    EmailConfirmed = false,
                    PhoneNumberConfirmed = true,
                    BirthDay = null,
                    PhoneNumber = dataValidated.Phone.Number,
                    Status = 1
                };

                var result = await _userRepository.RegisterUser(appUser);

                if (!result.Succeeded) return null;
            }

            var localUser = _userRepository.Query(model => model.UserName == dataValidated.Phone.Number || model.PhoneNumber == dataValidated.Phone.Number).SingleOrDefault();
            return (AdminAuthModel)GenerateAccessToken(localUser, -1);
        }

        // TODO: delete before relase.
#if DEBUG
        public bool Delete(string phoneNumber)
        {
            try
            {
                _userRepository.Delete(model => model.UserName == phoneNumber || model.PhoneNumber == phoneNumber);
                unitOfWork.Commit();
                return true;
            }
            catch
            {
                return false;
            }
        }
#endif

        public ResponseListModel<UserViewModel> GetDatatables(PaggingBase rContext)
        {
            try
            {
                // Variables
                var sortingString = rContext.sortBy + (rContext.reverse ? " descending" : "");
                var isSearching = !string.IsNullOrEmpty(rContext.searchValue);
                if (isSearching)
                {
                    rContext.searchValue = rContext.searchValue.ToLower();
                }
                var responseData = new UserViewDatatable();

                // Fetching data from database.
                var collections = from model in _userRepository.GetAllQueryable()
                                  where model.UserName.Contains(rContext.searchValue) && model.BranchId != 0
                                       
                                  orderby sortingString
                                  select new UserViewModel()
                                  {
                                      Id = model.Id,
                                      UserName = model.UserName,
                                      FullName = model.FullName,
                                      PhoneNumber = model.PhoneNumber,
                                      Email = model.Email,
                                      BranchId = model.BranchId,
                                      Avatar = model.Avatar,
                                      RoleId = model.RoleId,
                                      Status = model.Status
                                  };
                responseData.total = collections.Count();

                // Paging data
                collections = collections.Skip((rContext.page - 1) * rContext.itemPerPage)
                                         .Take(rContext.itemPerPage);

                // Fetching data from database
                responseData.data = collections.ToList();

                // Get list branch
                var branches = kBranchesService.GetAllBranches();
                if (branches.successfull)
                {
                    responseData.data.ForEach(model =>
                    {
                        var branch = branches.dataset.FirstOrDefault(b => b.id == model.BranchId);
                        if (branch != null)
                        {
                            model.BranchName = branch.branchName;
                        }
                    });
                }
                return new ResponseListModel<UserViewModel>()
                {
                    successfull = true,
                    dataset = responseData.data,
                    total = responseData.total
                };
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                throw;
            }
        }

        public async Task<User> ResetPassword(string userName)
        {
            try
            {
                var userData = _userRepository.Query(model => model.UserName == userName).FirstOrDefault();
                if (userData == null)
                {
                    throw new Exception("Không tìm thấy dữ liệu của người dùng.");
                }
                var user = await _userRepository.ResetPassWord(userData.UserName, _appSettings.DefaultPassWord);
                if (user.Succeeded)
                {
                    var userInfo = _userRepository.Query(model => model.UserName == userName).SingleOrDefault();
                    return userInfo;
                }
                return null;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                throw;
            }

        }

        public bool DeactiveUser(string userName)
        {
            try
            {
                var userData = _userRepository.Query(model => model.UserName == userName).FirstOrDefault();
                if (userData == null)
                {
                    throw new Exception("Không tìm thấy dữ liệu của người dùng.");
                }
                userData.Status = 2;
                _userRepository.Update(userData);
                unitOfWork.Commit();
                return true;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                throw;
            }

        }

        public bool ActiveUser(string userName)
        {
            try
            {
                var userData = _userRepository.Query(model => model.UserName == userName).FirstOrDefault();
                if (userData == null)
                {
                    throw new Exception("Không tìm thấy dữ liệu của người dùng.");
                }
                userData.Status = 1;
                _userRepository.Update(userData);
                unitOfWork.Commit();
                return true;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                throw;
            }

        }
    }
}

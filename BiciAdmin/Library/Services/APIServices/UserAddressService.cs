﻿using System;
using System.Linq;
using AutoMapper;
using DataCore.Data.Infrastructure;
using DataCore.Data.Repositories;
using Services.Handlers;
using Website.ViewModels;
using Models = DataCore.Models;
using WaoInterfaces = Services.Interfaces;

namespace Services.APIServices
{
    public class UserAddressService : BaseService, WaoInterfaces.IUserAddressService
    {
        // Define singleton instances.
        private readonly IUnitOfWork unitOfWork;
        private readonly IUserAddressRepository _userAddressRepository;
        private readonly ICityRepository _cityRepository;
        private readonly IDistrictRepository _districtRepository;
        private readonly IMapper mapper;

        // Constructor
        public UserAddressService(IUnitOfWork unitOfWork,
                                 IUserAddressRepository UserAddressRepository,
                                 ICityRepository cityRepository,
                                 IDistrictRepository districtRepository,
                                 IMapper mapper) : base(unitOfWork)
        {
            this.unitOfWork = unitOfWork;
            _userAddressRepository = UserAddressRepository;
            this._cityRepository = cityRepository;
            this._districtRepository = districtRepository;
            this.mapper = mapper;
        }

        /// <summary>
        /// Insert data to database.
        /// </summary>
        /// <param name="viewModel"></param>
        /// <returns></returns>
        public ResponseStatus Create(UserAddressViewModel viewModel)
        {
            try
            {
                ClearDefault(viewModel);
                // Mapping field value from view model to model entity
                var userAddressEntity = mapper.Map<Models.UserAddress>(viewModel);
                userAddressEntity.created_date = DateTime.Now;
                userAddressEntity.updated_date = DateTime.Now;
                // Save data to database
                _userAddressRepository.Add(userAddressEntity);
                SaveChanges();
                // Create response data
                var resp = new ResponseStatus()
                {
                    successfull = true,
                    message = string.Format("Create data is successfull with id => {0}", userAddressEntity.id),
                    dataset = userAddressEntity
                };
                return resp;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                throw;
            }
        }

        private void ClearDefault(UserAddressViewModel viewModel)
        {
            if (viewModel.isdefault)
            {
                var data = _userAddressRepository.Query(model => model.username == viewModel.username).ToList();
                data.ForEach(model =>
                {
                    model.isdefault = false;
                });
                SaveChanges();
            }
        } 

        /// <summary>
        /// Delete data from database.
        /// </summary>
        /// <param name="viewModel"></param>
        /// <returns></returns>
        public ResponseStatus Delete(UserAddressViewModel viewModel)
        {
            try
            {
                // Checking exist data.
                var entity = _userAddressRepository.Query(model => model.id == viewModel.id).FirstOrDefault();
                if (null == entity)
                {
                    throw new NotFoundDataException(viewModel.id.ToString(), typeof(Models.Collection));
                }

                if (entity.username != viewModel.username)
                {
                    throw new Exception("You not granted permission to delete this address.");
                }
                _userAddressRepository.Delete(entity);
                SaveChanges();
                return new ResponseStatus()
                {
                    successfull = true,
                    message = string.Format("Delete data is successfull with id => {0}", entity.id)
                };
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                throw;
            }
        }

        /// <summary>
        /// Delete data by primary key.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ResponseStatus DeleteById(int id)
        {
            try
            {
                _userAddressRepository.Delete(model => model.id == id);
                SaveChanges();
                return new ResponseStatus()
                {
                    successfull = true,
                    message = string.Format("Delete data is successfull with id => {0}", id)
                };
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                throw;
            }
        }

        /// <summary>
        /// Get datatable with pagination info.
        /// </summary>
        /// <param name="rContext"></param>
        /// <returns></returns>
        public ResponseListModel<UserAddressViewModel> GetDatatables(PaggingBase rContext)
        {
            try
            {
                // Variables
                var sortingString = rContext.sortBy + (rContext.reverse ? " descending" : "");
                var isSearching = !string.IsNullOrEmpty(rContext.searchValue);
                if (isSearching)
                {
                    rContext.searchValue = rContext.searchValue.ToLower();
                }
                var responseData = new UserAddressDatatable();

                // Fetching data from database.
                var collections = from model in _userAddressRepository.GetAllQueryable()
                                  orderby sortingString
                                  select new UserAddressViewModel()
                                  {
                                      id = model.id,
                                      username = model.username,
                                      location_type = model.location_type,
                                      address = model.address,
                                      receiver_name = model.receiver_name,
                                      receiver_phoneno = model.receiver_phoneno,
                                      district_id = model.district_id,
                                      city_id = model.city_id,
                                      isdefault = model.isdefault,

                                  };
                responseData.total = collections.Count();

                // Paging data
                collections = collections.Where(model => _userAddressRepository.GetAllQueryable()
                                                                              .OrderBy(x => x.id)
                                                                              .Select(x => x.id)
                                                                              .Skip((rContext.page - 1) * rContext.itemPerPage)
                                                                              .Take(rContext.itemPerPage).Contains(model.id)
                                               );

                // Fetching data from database
                responseData.data = collections.ToList();
                return new ResponseListModel<UserAddressViewModel>()
                {
                    successfull = true,
                    dataset = responseData.data,
                    total = responseData.total
                };
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                throw;
            }
        }

        /// <summary>
        /// Get list address of user.
        /// </summary>
        /// <param name="currentUser"></param>
        /// <returns></returns>
        public ResponseListModel<UserAddressViewModel> GetDatatablesWithUser(string currentUser)
        {
            try
            {
                // Variables
                var responseData = new UserAddressDatatable();

                // Fetching data from database.
                var collections = from model in _userAddressRepository.Query(q => q.username == currentUser)
                                  join city in _cityRepository.GetAllQueryable() on model.city_id equals city.id into cModel
                                  from city in cModel.DefaultIfEmpty()
                                  join district in _districtRepository.GetAllQueryable() on model.district_id equals district.id into dModel
                                  from district in dModel.DefaultIfEmpty()
                                  select new UserAddressViewModel()
                                  {
                                      id = model.id,
                                      username = model.username,
                                      location_type = model.location_type,
                                      address = model.address,
                                      receiver_name = model.receiver_name,
                                      receiver_phoneno = model.receiver_phoneno,
                                      district_id = model.district_id,
                                      city_id = model.city_id,
                                      isdefault = model.isdefault,
                                      city_name = city.name,
                                      district_name = district.name
                                  };
                //responseData.total = collections.Count();

                // Paging data
                //collections = collections.Where(model => _userAddressRepository.GetAllQueryable()
                //                                                              .OrderBy(x => x.id)
                //                                                              .Select(x => x.id)
                //                                                              .Skip(0)
                //                                                              .Take(3).Contains(model.id)
                //                               );

                // Fetching data from database
                responseData.data = collections.ToList();
                return new ResponseListModel<UserAddressViewModel>()
                {
                    successfull = true,
                    dataset = responseData.data,
                    total = responseData.data.Count
                };
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                throw;
            }
        }


        /// <summary>
        /// Get detail info by id.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ResponseSignleModel<UserAddressViewModel> GetInfo(int id)
        {
            try
            {
                var collection = _userAddressRepository.Query(model => model.id == id).FirstOrDefault();
                if (null == collection)
                {
                    throw new NotFoundDataException(id.ToString(), typeof(UserAddressViewModel));
                }
                var respData = mapper.Map<UserAddressViewModel>(collection);
                return new ResponseSignleModel<UserAddressViewModel>()
                {
                    successfull = true,
                    dataset = respData
                };
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                throw;
            }
        }

        /// <summary>
        /// Update data.
        /// </summary>
        /// <param name="viewModel"></param>
        /// <returns></returns>
        public ResponseStatus Update(UserAddressViewModel viewModel)
        {
            try
            {
                ClearDefault(viewModel);
                // Mapping field value from view model to model entity
                var entity = mapper.Map<Models.UserAddress>(viewModel);
                var isExists = _userAddressRepository.Query(model => model.username == entity.username && entity.id == model.id).Any();
                if (isExists)
                {
                    // Save data to database
                    _userAddressRepository.Update(entity);
                }
                else
                {
                    entity.id = 0;
                    _userAddressRepository.Add(entity);
                }
                
                SaveChanges();
                viewModel.id = entity.id;
                // Create response data
                var resp = new ResponseStatus()
                {
                    successfull = true,
                    message = string.Format("Update data is successfull with id => {0}", entity.id),
                    dataset = viewModel
                };
                return resp;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                throw;
            }
        }

    }
}
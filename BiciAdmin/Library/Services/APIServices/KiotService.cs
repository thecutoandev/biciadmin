﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using DataCore.Models;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using Services.Interfaces;
using Website.ViewModels;

namespace Services.APIServices
{
    public class KiotService : IKiotService
    {
        private readonly AppSettings _appSettings;
        private readonly HttpClient _client;
        private readonly string dataForGetToken;
        public KiotService(IOptions<AppSettings> appSettings)
        {
            this._appSettings = appSettings.Value;
            this._client = new HttpClient();
            this.dataForGetToken = "scopes=PublicApi.Access&grant_type=client_credentials&client_id={0}&client_secret={1}";
            this.dataForGetToken = string.Format(dataForGetToken,
                                                 _appSettings.KiotVietSettings.ClientId,
                                                 _appSettings.KiotVietSettings.ClientSecret);
        }

        /// <summary>
        /// Get quantity avaiable product in stock.
        /// </summary>
        /// <param name="kiotCodes">list kitot code.</param>
        /// <returns></returns>
        public List<KiotStocking> GetStockings(string[] kiotCodes)
        {
            try
            {
                var tokenInfo = GetKiotToken();
                if (tokenInfo == null)
                {
                    throw new Exception();
                }
                return new List<KiotStocking>();
            }
            catch
            {
                return null;
            }
        }

        private KiotAuth GetKiotToken()
        {
            try
            {
                _client.DefaultRequestHeaders.Add("Content-Type", "application/x-www-form-urlencoded");
                // 1.generate an app access token
                var content = new StringContent(dataForGetToken);
                var postResponse = _client.PostAsync(_appSettings.KiotVietSettings.TokenEndPoint, content);
                var dataResponse = postResponse.Result.Content.ReadAsStringAsync().Result;
                var kiotAuth = JsonConvert.DeserializeObject<KiotAuth>(dataResponse);
                return kiotAuth;
            }
            catch
            {
                return null;
            }
        }
    }
}

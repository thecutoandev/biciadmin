﻿using System;
using System.Linq;
using AutoMapper;
using DataCore.Data.Infrastructure;
using DataCore.Data.Repositories;
using Services.Handlers;
using Website.ViewModels;
using Models = DataCore.Models;
using WaoInterfaces = Services.Interfaces;

namespace Services.APIServices
{
    public class CityService : BaseService, WaoInterfaces.ICityService
    {
        // Define singleton instances.
        private readonly IUnitOfWork unitOfWork;
        private readonly ICityRepository _cityRepository;
        private readonly IMapper mapper;

        // Constructor
        public CityService(IUnitOfWork unitOfWork,
                                 ICityRepository CityRepository,
                                 IMapper mapper) : base(unitOfWork)
        {
            this.unitOfWork = unitOfWork;
            _cityRepository = CityRepository;
            this.mapper = mapper;
        }

        /// <summary>
        /// Insert data to database, and response for client.
        /// </summary>
        /// <param name="viewModel">Model for inserting.</param>
        /// <returns>Response infomation.</returns>
        public ResponseStatus Create(CityViewModel viewModel)
        {
            try
            {
                // Mapping field value from view model to model entity
                var cityEntity = mapper.Map<Models.City>(viewModel);
                // Save data to database
                _cityRepository.Add(cityEntity);
                SaveChanges();
                // Create response data
                var resp = new ResponseStatus()
                {
                    successfull = true,
                    message = string.Format("Create data is successfull with id => {0}", cityEntity.id),
                    dataset = cityEntity
                };
                return resp;
            }
            catch (Exception ex)
            {
                return new ResponseStatus()
                {
                    successfull = false,
                    message = ex.Message,
                    dataset = null
                };
            }
        }

        /// <summary>
        /// Delete data from database.
        /// </summary>
        /// <param name="viewModel">Model for deleting.</param>
        /// <returns>Response infomation</returns>
        public ResponseStatus Delete(CityViewModel viewModel)
        {
            try
            {
                var entity = _cityRepository.Query(model => model.id == viewModel.id).FirstOrDefault();
                if (null == entity)
                {
                    throw new NotFoundDataException(viewModel.id.ToString(), typeof(Models.Collection));
                }
                _cityRepository.Delete(entity);
                SaveChanges();
                return new ResponseStatus()
                {
                    successfull = true,
                    message = string.Format("Delete data is successfull with id => {0}", entity.id)
                };
            }
            catch (Exception ex)
            {
                return new ResponseStatus()
                {
                    successfull = false,
                    message = ex.Message
                };
            }
        }

        /// <summary>
        /// Delete database by id.
        /// </summary>
        /// <param name="id">Id of model.</param>
        /// <returns>Response infomation.</returns>
        public ResponseStatus DeleteById(int id)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Get list of data from database with condition
        /// </summary>
        /// <param name="page">Current page</param>
        /// <param name="itemPerpage">Item per page</param>
        /// <param name="sortBy">Field name for sorting.</param>
        /// <param name="reverse">Sorting with asc or desc</param>
        /// <param name="searchValue">Value for searching.</param>
        /// <returns></returns>
        public ResponseStatus GetDatatables(int page, int itemPerpage, string sortBy, bool reverse, string searchValue)
        {
            try
            {
                // Variables
                var sortingString = sortBy + (reverse ? " descending" : "");
                var isSearching = !string.IsNullOrEmpty(searchValue);
                if (isSearching)
                {
                    searchValue = searchValue.ToLower();
                }
                var responseData = new CityDatatable();

                // Fetching data from database.
                var collections = from model in _cityRepository.GetAllQueryable()
                                  where (!isSearching || (isSearching && model.name.Contains(searchValue)))
                                  orderby sortingString
                                  select new CityViewModel()
                                  {
                                      id = model.id,
                                      name = model.name,

                                  };
                responseData.total = collections.Count();

                // Paging data
                collections = collections.Where(model => _cityRepository.GetAllQueryable()
                                                                              .OrderBy(x => x.id)
                                                                              .Select(x => x.id)
                                                                              .Skip((page - 1) * itemPerpage)
                                                                              .Take(itemPerpage).Contains(model.id)
                                               );

                // Fetching data from database
                responseData.data = collections.ToList();
                return new ResponseStatus()
                {
                    successfull = true,
                    dataset = responseData
                };
            }
            catch (Exception ex)
            {
                return new ResponseStatus()
                {
                    successfull = false,
                    message = ex.Message,
                    dataset = null
                };
            }
        }

        public ResponseListModel<CityViewModel> GetDatatables(PaggingBase rContext)
        {
            try
            {
                var cities = (from city in _cityRepository.GetAllQueryable()
                              orderby city.name ascending
                             select new CityViewModel()
                             {
                                 id = city.id,
                                 name = city.name,
                             }).ToList();
                return new ResponseListModel<CityViewModel>()
                {
                    successfull = true,
                    dataset = cities,
                    total = cities.Count
                };
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                throw;
            }
        }

        ///// <summary>
        ///// Get data detail info by id
        ///// </summary>
        ///// <param name="id">Id of data.</param>
        ///// <returns>Response infomation.</returns>
        //public ResponseStatus GetInfo(int id)
        //{
        //    try
        //    {
        //        var collection = _cityRepository.Query(model => model.id == id).FirstOrDefault();
        //        if (null == collection)
        //        {
        //            throw new NotFoundDataException(id.ToString(), typeof(CityViewModel));
        //        }
        //        return new ResponseStatus()
        //        {
        //            successfull = true,
        //            dataset = collection
        //        };
        //    }
        //    catch (Exception ex)
        //    {
        //        return new ResponseStatus()
        //        {
        //            successfull = false,
        //            message = ex.Message,
        //            dataset = null
        //        };
        //    }
        //    throw new NotImplementedException();
        //}

        /// <summary>
        /// Update data to database
        /// </summary>
        /// <param name="viewModel">Model for updating.</param>
        /// <returns>Response infomation.</returns>
        public ResponseStatus Update(CityViewModel viewModel)
        {
            try
            {
                // Mapping field value from view model to model entity
                var cityEntity = mapper.Map<Models.City>(viewModel);
                // Save data to database
                _cityRepository.Update(cityEntity);
                SaveChanges();
                // Create response data
                var resp = new ResponseStatus()
                {
                    successfull = true,
                    message = string.Format("Update data is successfull with id => {0}", cityEntity.id),
                    dataset = cityEntity
                };
                return resp;
            }
            catch (Exception ex)
            {
                return new ResponseStatus()
                {
                    successfull = false,
                    message = ex.Message,
                    dataset = null
                };
            }
        }

        public ResponseSignleModel<CityViewModel> GetInfo(int id)
        {
            throw new NotImplementedException();
        }
    }
}

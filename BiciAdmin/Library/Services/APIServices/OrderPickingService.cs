﻿using System;
using System.Collections.Generic;
using DataCore.Data.Infrastructure;
using DataCore.Data.Repositories;
using Services.Interfaces;
using System.Linq;
using DataCore.Models;
using Website.ViewModels;
using KiotService.Models.Order;
using Microsoft.Extensions.Options;
//using KiotService = KiotService.Interfaces;

namespace Services.APIServices
{
    public class OrderPickingService : BaseService, IOrderPickingService
    {
        private readonly IUserRepository userRepository;
        private readonly IUserAddressRepository userAddressRepository;
        private readonly IDistrictRepository districtRepository;
        private readonly ICityRepository cityRepository;
        private readonly IBranchPickPriorityRepository branchPick;
        private readonly IOrderRepository orderRepository;
        private readonly IOrderItemRepository orderItemRepository;
        private readonly IProductRepository productRepository;
        private readonly IGenericProductRepository genericProductRepository;
        private readonly IOrderSummaryRepository orderSummaryRepository;
        private readonly KiotService.Interfaces.IKProductService kProductService;
        private readonly KiotService.Interfaces.IOrderService orderService;
        private const int ItemPerPage = 50;
        public OrderPickingService(IUnitOfWork unitOfWork,
                                   IUserRepository userRepository,
                                   IBranchPickPriorityRepository branchPick,
                                   IOrderRepository orderRepository,
                                   KiotService.Interfaces.IKProductService kProductService,
                                   IOrderItemRepository orderItemRepository,
                                   IProductRepository productRepository,
                                   IGenericProductRepository genericProductRepository,
                                   IOrderSummaryRepository orderSummaryRepository,
                                   KiotService.Interfaces.IOrderService orderService,
                                   IUserAddressRepository userAddressRepository,
                                   IDistrictRepository districtRepository,
                                   ICityRepository cityRepository)
            :base(unitOfWork)
        {
            this.userRepository = userRepository;
            this.branchPick = branchPick;
            this.orderRepository = orderRepository;
            this.kProductService = kProductService;
            this.orderItemRepository = orderItemRepository;
            this.productRepository = productRepository;
            this.genericProductRepository = genericProductRepository;
            this.orderSummaryRepository = orderSummaryRepository;
            this.orderService = orderService;
            this.userAddressRepository = userAddressRepository;
            this.districtRepository = districtRepository;
            this.cityRepository = cityRepository;
        }

        public ResponseSignleModel<List<string>> AutoPick(string userName)
        {
            try
            {
                // Get user information
                var userInfo = userRepository.Query(model => model.UserName == userName).FirstOrDefault();
                if (userInfo == null)
                {
                    throw new Exception("Người dùng không tồn tại");
                }

                // Get priority information
                var priority = branchPick.Query(model => model.branch_id == userInfo.BranchId).FirstOrDefault();
                if (priority == null)
                {
                    throw new Exception("Không tìm thấy thông tin chi nhánh");
                }

                // Check processing order
                var orderPending = orderRepository.Query(model => model.branch_id == priority.branch_id && model.status == 2).Count();
                if (orderPending  > 0)
                {
                    throw new Exception($"Chi nhánh của bạn còn {orderPending} đơn hàng chưa xử lý\nVui lòng xử lý hết các đơn hàng.");
                }
                // Get list order and pick
                var listPicked = new List<string>();
                var beginPage = 1;
                int countPages = ( orderRepository.Query(model => model.status == 1).Count() / ItemPerPage) + 1;
                for (int page = beginPage; page <= countPages; page++)
                {
                    var orders = orderRepository.Query(model => model.status == 1)
                                                .Skip((page - 1) * ItemPerPage)
                                                .Take(ItemPerPage).ToList();

                    foreach (var order in orders)
                    {
                        var resultPicking = AutoPicking(order, userInfo.BranchId);
                        if (resultPicking)
                        {
                            order.status = 2;
                            order.employee = userInfo.UserName;
                            order.branch_id = userInfo.BranchId;
                            orderRepository.Update(order);
                            SaveChanges();
                            listPicked.Add(order.code);
                            if (listPicked.Count == priority.max_auto_pick)
                            {
                                return new ResponseSignleModel<List<string>>()
                                {
                                    dataset = listPicked,
                                    successfull = true
                                };
                            }
                        }
                    }
                }
                if (listPicked.Count <= 0)
                {
                    throw new Exception("Không tìm thấy đơn hàng phù hợp.");
                }
                return new ResponseSignleModel<List<string>>()
                {
                    dataset = listPicked,
                    successfull = true
                };
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                throw;
            }
        }

        private bool AutoPicking(Order order, long branchId)
        {
            try
            {
                var result = true;

                var items = (from item in orderItemRepository.Query(model => model.order_id == order.id)
                             join product in productRepository.GetAllQueryable()
                             on item.product_id equals product.id into pd
                             from product in pd.DefaultIfEmpty()
                             select new OrderAdminItemViewModel()
                             {
                                 id = item.id,
                                 product_id = item.product_id,
                                 sku = product.kiot_product_code,
                                 qty = item.quantity
                             }).ToList();

                foreach (var item in items)
                {
                    var stocking = kProductService.GetStockingOfBranch(item.sku, branchId).dataset;
                    if (stocking == null)
                    {
                        return result = false;
                    }

                    if (stocking.quantity < item.qty)
                    {
                        return result = false;
                    }
                }
                return result;
            }
            catch
            {
                return false;
            }
            
        }

        public ResponseListModel<OrderAdminPrepareViewModel> GetProductForPrepare(string orderIds, string userName)
        {
            try
            {
                // Get user information
                var userInfo = userRepository.Query(model => model.UserName == userName).FirstOrDefault();
                if (userInfo == null)
                {
                    throw new Exception("Not found data");
                }

                var ids = orderIds.Split(',').ToArray();
                var data = (from item in orderItemRepository.Query(model => ids.Contains(model.order_id.ToString()))
                           join product in productRepository.GetAllQueryable()
                           on item.product_id equals product.id into pd
                           from product in pd.DefaultIfEmpty()
                           join generic in genericProductRepository.GetAllQueryable()
                           on product.generic_product_id equals generic.id into gp
                           from generic in gp.DefaultIfEmpty()
                           select new OrderAdminPrepareViewModel()
                           {
                               product_name = generic.product_name,
                               qty = item.quantity,
                               sku = product.kiot_product_code,
                               stocking = 0,
                               variant = product.product_name,
                               product_id = item.product_id,
                               generic_id = generic.id
                           }).ToList();

                var sumData = (from item in data
                              group item by item.product_id into itemG
                              select new OrderAdminPrepareViewModel()
                              {
                                  product_id = itemG.Key,
                                  qty = itemG.Sum(x => x.qty)
                              }).ToList();

                sumData.ForEach(model =>
                {
                    var product = data.FirstOrDefault(m => m.product_id == model.product_id);
                    model.product_name = product.product_name;
                    model.sku = product.sku;
                    model.variant = product.variant;
                    var stocking = kProductService.GetStockingOfBranch(model.sku, userInfo.BranchId).dataset;
                    if (stocking != null)
                    {
                        model.stocking = stocking.quantity;
                    }
                    model.generic_id = product.generic_id;
                });

                return new ResponseListModel<OrderAdminPrepareViewModel>()
                {
                    dataset = sumData,
                    successfull = true,
                    total = sumData.Count
                };
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                throw;
            }
        }

        public ResponseStatus UndoOrder(int id)
        {
            try
            {
                var order = orderRepository.Query(model => model.id == id).FirstOrDefault();
                if (order == null)
                {
                    throw new Exception("Not found data");
                }
                order.employee = string.Empty;
                order.status = 1;
                order.branch_id = 0;
                orderRepository.Update(order);
                SaveChanges();
                return new ResponseStatus()
                {
                    successfull = true,
                    dataset = order
                };
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                throw;
            }
        }

        public ResponseStatus ConfirmOrder(OrderConfirmation orderConfirmation)
        {
            try
            {
                var order = orderRepository.Query(model => model.id == orderConfirmation.order_id).FirstOrDefault();
                if (order == null)
                {
                    throw new Exception("Not found data");
                }
                var orderSummary = orderSummaryRepository.Query(model => model.order_id == orderConfirmation.order_id).FirstOrDefault();
                if (orderSummary == null)
                {
                    throw new Exception("Not found data");
                }
                order.status = orderConfirmation.status;
                orderSummary.note = orderConfirmation.note;
                orderSummary.approver = orderConfirmation.approver;
                orderRepository.Update(order);
                orderSummaryRepository.Update(orderSummary);
                SaveChanges();
                return new ResponseStatus()
                {
                    successfull = true,
                    message = "Update is successfull",
                    dataset = orderConfirmation
                };
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                throw;
            }
        }

        public ResponseSignleModel<List<string>> AutoMultiPick()
        {
            try
            {

                var branches = branchPick.GetAllQueryable().OrderByDescending(model => model.priority).ToArray();
                var listPicked = new List<string>();
                foreach (var priority in branches)
                {
                    // Get list order and pick
                    //var listPicked = new List<string>();
                    var beginPage = 1;
                    int countPages = (orderRepository.Query(model => model.status <= 1).Count() / ItemPerPage) + 1;
                    for (int page = beginPage; page <= countPages; page++)
                    {
                        var orders = orderRepository.Query(model => model.status == 1)
                                                    .Skip((page - 1) * ItemPerPage)
                                                    .Take(ItemPerPage).ToList();

                        foreach (var order in orders)
                        {
                            var resultPicking = AutoPicking(order, priority.branch_id);
                            if (resultPicking)
                            {
                                order.status = 2;
                                order.employee = "AutoPicking";
                                order.branch_id = priority.branch_id;
                                orderRepository.Update(order);
                                SaveChanges();
                                listPicked.Add(order.code);
                                if (listPicked.Count == priority.max_auto_pick)
                                {
                                    break;
                                }
                            }
                        }
                    }
                }

                return new ResponseSignleModel<List<string>>()
                {
                    dataset = listPicked,
                    successfull = true
                };
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                throw;
            }
        }

        public ResponseSignleModel<List<string>> CreateKiotOrder(KiotOrderViewModel kiotOrderViewModel)
        {
            try
            {
                // Get user information
                var userInfo = userRepository.Query(model => model.UserName == kiotOrderViewModel.user_name).FirstOrDefault();
                if (userInfo == null)
                {
                    throw new Exception("Not found data");
                }
                var listIds = new List<string>();
                var orders = (from order in orderRepository.Query(model => kiotOrderViewModel.order_ids.Contains(model.id))
                              join summary in orderSummaryRepository.GetAllQueryable()
                              on order.id equals summary.order_id into sm
                              from summary in sm.DefaultIfEmpty()
                              select new OrderAdminViewModel()
                              {
                                  amount = summary.sub_total,
                                  total_amount = summary.total_amount,
                                  created_date = order.created_date,
                                  branch_id = order.branch_id,
                                  user_id = order.user_id,
                                  user_address_id = order.user_address_id,
                                  id = order.id,
                              }).ToList();
                foreach (var order in orders)
                {
                    var customer = userRepository.Query(model => model.Id == order.user_id).FirstOrDefault();
                    var address = (from addr in userAddressRepository.Query(model => model.id == order.user_address_id)
                                  join district in districtRepository.GetAllQueryable()
                                  on addr.district_id equals district.id into dt
                                  from district in dt.DefaultIfEmpty()
                                  join city in cityRepository.GetAllQueryable()
                                  on addr.city_id equals city.id into ct
                                  from city in ct.DefaultIfEmpty()
                                  select new UserAddressViewModel()
                                  {
                                      address = addr.address,
                                      city_name = city.name,
                                      district_name = district.name,
                                      receiver_phoneno = addr.receiver_phoneno,
                                      receiver_name = addr.receiver_name,
                                      id = addr.id
                                  }).FirstOrDefault();
                    var kOrder = new KOrder()
                    {
                        branchId = userInfo.BranchId,
                        purchaseDate = order.created_date.Value,
                        totalPayment = 0,
                        discount = 0,
                        description = "DD",
                        customer = new Customer()
                        {
                            name = customer.FullName,
                            birthDate = customer.BirthDay == null ? DateTime.Today : customer.BirthDay.Value,
                            gender = true,
                            email = customer.Email,
                            address = address.address + ", " + address.city_name + ", " + address.district_name,
                            contactNumber = address.receiver_phoneno,
                        }
                    };

                    var details = (from item in orderItemRepository.Query(model => model.order_id == order.id)
                                   join product in productRepository.GetAllQueryable()
                                   on item.product_id equals product.id into pd
                                   from product in pd.DefaultIfEmpty()
                                   select new OrderItemViewModel()
                                   {
                                       kiot_code = product.kiot_product_code,
                                       id = item.id,
                                       amount = item.amount,
                                       order_id = item.order_id,
                                       product_id = item.product_id,
                                       product_name = product.product_name,
                                       quantity = item.quantity,
                                       unit_price = item.unit_price
                                   }).ToList();

                    var kiotDetails = new List<OrderDetail>();

                    foreach (var item in details)
                    {
                        var kproduct = kProductService.GetDetail(item.kiot_code);
                        if (kproduct.successfull)
                        {
                            var tmpProduct = kproduct.dataset;
                            var data = new OrderDetail()
                            {
                                productId = tmpProduct.kiot_id,
                                productCode = tmpProduct.product_code,
                                price = item.unit_price,
                                quantity = item.quantity
                            };
                            kiotDetails.Add(data);
                        }
                    }
                     kOrder.orderDetails = kiotDetails;

                    var result = orderService.CreateOrder(kOrder);
                    if (result.successfull)
                    {
                        listIds.Add(order.code);
                        var orderToUpdate = orderRepository.Query(model => model.id == order.id).FirstOrDefault();

                        orderToUpdate.status = 3;
                        orderRepository.Update(orderToUpdate);
                        SaveChanges();
                    }
                }

                return new ResponseSignleModel<List<string>>()
                {
                    dataset = listIds,
                    successfull = kiotOrderViewModel.order_ids.Count == listIds.Count ? true : false
                };
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                throw;
            }
        }
    }
}

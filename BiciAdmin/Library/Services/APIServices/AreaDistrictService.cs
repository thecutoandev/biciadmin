using System;
using System.Linq;
using AutoMapper;
using DataCore.Data.Infrastructure;
using DataCore.Data.Repositories;
using Services.Handlers;
using Website.ViewModels;
using Models = DataCore.Models;
using WaoInterfaces = Services.Interfaces;

namespace Services.APIServices
{
    public class AreaDistrictService : BaseService, WaoInterfaces.IAreaDistrictService
    {
        // Define singleton instances.
        private readonly IUnitOfWork unitOfWork;
        private readonly IAreaDistrictRepository _areaDistrictRepository;
        private readonly IMapper mapper;

        // Constructor
        public AreaDistrictService(IUnitOfWork unitOfWork,
                                 IAreaDistrictRepository AreaDistrictRepository,
                                 IMapper mapper) : base(unitOfWork)
        {
            this.unitOfWork = unitOfWork;
            _areaDistrictRepository = AreaDistrictRepository;
            this.mapper = mapper;
        }

        /// <summary>
        /// Insert data to database.
        /// </summary>
        /// <param name="viewModel"></param>
        /// <returns></returns>
        public ResponseStatus Create(AreaDistrictViewModel viewModel)
        {
            try
            {
                // Mapping field value from view model to model entity
                var areaDistrictEntity = mapper.Map<Models.AreaDistrict>(viewModel);
                // Save data to database
                _areaDistrictRepository.Add(areaDistrictEntity);
                SaveChanges();
                // Create response data
                var resp = new ResponseStatus()
                {
                    successfull = true,
                    message = string.Format("Create data is successfull with id => {0}", areaDistrictEntity.id),
                    dataset = areaDistrictEntity
                };
                const string a = "";
                return resp;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                throw;
            }
        }

        /// <summary>
        /// Delete data from database.
        /// </summary>
        /// <param name="viewModel"></param>
        /// <returns></returns>
        public ResponseStatus Delete(AreaDistrictViewModel viewModel)
        {
            try
            {
                // Checking exist data.
                var entity = _areaDistrictRepository.Query(model => model.id == viewModel.id).FirstOrDefault();
                if (null == entity)
                {
                    throw new NotFoundDataException(viewModel.id.ToString(), typeof(Models.Collection));
                }
                // If exists data, then delete from database.
                _areaDistrictRepository.Delete(entity);
                SaveChanges();
                return new ResponseStatus()
                {
                    successfull = true,
                    message = string.Format("Delete data is successfull with id => {0}", entity.id)
                };
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                throw;
            }
        }

        /// <summary>
        /// Delete data by primary key.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ResponseStatus DeleteById(int id)
        {
            try
            {
                _areaDistrictRepository.Delete(model => model.id == id);
                SaveChanges();
                return new ResponseStatus()
                {
                    successfull = true,
                    message = string.Format("Delete data is successfull with id => {0}", id)
                };
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                throw;
            }
        }

        /// <summary>
        /// Get datatable with pagination info.
        /// </summary>
        /// <param name="rContext"></param>
        /// <returns></returns>
        public ResponseListModel<AreaDistrictViewModel> GetDatatables(PaggingBase rContext)
        {
            try
            {
                // Variables
                var sortingString = rContext.sortBy + (rContext.reverse ? " descending" : "");
                var isSearching = !string.IsNullOrEmpty(rContext.searchValue);
                if (isSearching)
                {
                    rContext.searchValue = rContext.searchValue.ToLower();
                }
                var responseData = new AreaDistrictDatatable();

                // Fetching data from database.
                var collections = from model in _areaDistrictRepository.GetAllQueryable()
                                  orderby sortingString
                                  select new AreaDistrictViewModel()
                                  {
                                      id = model.id,
                                      area_id = model.area_id,
                                      district_id = model.district_id
                                  };
                responseData.total = collections.Count();

                // Paging data
                collections = collections.Where(model => _areaDistrictRepository.GetAllQueryable()
                                                                              .OrderBy(x => x.id)
                                                                              .Select(x => x.id)
                                                                              .Skip((rContext.page - 1) * rContext.itemPerPage)
                                                                              .Take(rContext.itemPerPage).Contains(model.id)
                                               );

                // Fetching data from database
                responseData.data = collections.ToList();
                return new ResponseListModel<AreaDistrictViewModel>()
                {
                    successfull = true,
                    dataset = responseData.data,
                    total = responseData.total
                };
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                throw;
            }
        }


        /// <summary>
        /// Get detail info by id.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ResponseSignleModel<AreaDistrictViewModel> GetInfo(int id)
        {
            try
            {
                var collection = _areaDistrictRepository.Query(model => model.id == id).FirstOrDefault();
                if (null == collection)
                {
                    throw new NotFoundDataException(id.ToString(), typeof(AreaDistrictViewModel));
                }
                var respData = mapper.Map<AreaDistrictViewModel>(collection);
                return new ResponseSignleModel<AreaDistrictViewModel>()
                {
                    successfull = true,
                    dataset = respData
                };
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                throw;
            }
        }

        /// <summary>
        /// Update data.
        /// </summary>
        /// <param name="viewModel"></param>
        /// <returns></returns>
        public ResponseStatus Update(AreaDistrictViewModel viewModel)
        {
            try
            {
                // Mapping field value from view model to model entity
                var areaDistrictEntity = mapper.Map<Models.AreaDistrict>(viewModel);
                // Save data to database
                _areaDistrictRepository.Update(areaDistrictEntity);
                SaveChanges();
                // Create response data
                var resp = new ResponseStatus()
                {
                    successfull = true,
                    message = string.Format("Update data is successfull with id => {0}", areaDistrictEntity.id),
                    dataset = areaDistrictEntity
                };
                return resp;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                throw;
            }
        }

    }
}

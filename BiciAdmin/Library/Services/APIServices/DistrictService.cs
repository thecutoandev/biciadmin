using System;
using System.Linq;
using AutoMapper;
using DataCore.Data.Infrastructure;
using DataCore.Data.Repositories;
using Services.Handlers;
using Website.ViewModels;
using Models = DataCore.Models;
using WaoInterfaces = Services.Interfaces;

namespace Services.APIServices
{
    public class DistrictService : BaseService, WaoInterfaces.IDistrictService
    {
        // Define singleton instances.
        private readonly IUnitOfWork unitOfWork;
        private readonly IDistrictRepository _districtRepository;
        private readonly IAreaRepository _areaRepository;
        private readonly IAreaDistrictRepository _areaDistrictRepository;
        private readonly IMapper mapper;

        // Constructor
        public DistrictService(IUnitOfWork unitOfWork,
                                 IDistrictRepository DistrictRepository,
                                 IAreaRepository areaRepository,
                                 IAreaDistrictRepository areaDistrictRepository,
                                 IMapper mapper) : base(unitOfWork)
        {
            this.unitOfWork = unitOfWork;
            _districtRepository = DistrictRepository;
            _areaRepository = areaRepository;
            _areaDistrictRepository = areaDistrictRepository;
            this.mapper = mapper;
        }

        #region NO IMPLEMENT FUNCTION
        public ResponseStatus Create(DistrictViewModel viewModel)
        {
            throw new NotImplementedException();
        }

        public ResponseStatus Delete(DistrictViewModel viewModel)
        {
            throw new NotImplementedException();
        }

        public ResponseStatus DeleteById(int id)
        {
            throw new NotImplementedException();
        }

        public ResponseListModel<DistrictViewModel> GetDatatables(PaggingBase rContext)
        {
            throw new NotImplementedException();
        }

        public ResponseSignleModel<DistrictViewModel> GetInfo(int id)
        {
            throw new NotImplementedException();
        }

        public ResponseStatus GetShippingFee(int districtId)
        {
            try
            {
                var dInfo = (from district in _districtRepository.Query(model => model.id == districtId)
                             join areaDistrict in _areaDistrictRepository.GetAllQueryable()
                             on district.id equals areaDistrict.district_id into aDistrict
                             from areaDitstrict in aDistrict.DefaultIfEmpty()
                             join area in _areaRepository.GetAllQueryable()
                             on areaDitstrict.area_id equals area.id into areaInfo
                             from area in areaInfo.DefaultIfEmpty()
                             select new AreaViewModel
                             {
                                 id = area.id,
                                 fee_amount = area.fee_amount,
                                 limit_transaction_amount = area.limit_transaction_amount,
                                 name = area.name
                             }).FirstOrDefault();
                if (dInfo == null)
                {
                    dInfo = new AreaViewModel()
                    {
                        name = "Default",
                        fee_amount = 40000,
                        limit_transaction_amount = 1000000
                    };
                    //throw new NotFoundDataException(districtId.ToString());
                }
                return new ResponseStatus
                {
                    dataset = dInfo,
                    successfull = true
                };
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return new ResponseStatus()
                {
                    dataset = new AreaViewModel()
                    {
                        name = "Default",
                        fee_amount = 40000,
                        limit_transaction_amount = 1000000
                    },
                    successfull = true,
                    message = "this is default value"
                };
            }
        }

        public ResponseStatus Update(DistrictViewModel viewModel)
        {
            throw new NotImplementedException();
        }
        #endregion NO IMPLEMENT FUNCTION

        public ResponseListModel<DistrictViewModel> GetDataByCity(int cityId)
        {
            try
            {
                var data = (from district in _districtRepository.Query(model => model.city_id == cityId)
                            orderby district.name descending
                            select new DistrictViewModel()
                            {
                                city_id = district.city_id,
                                id = district.id,
                                name = district.name
                            }
                            ).ToList();
                return new ResponseListModel<DistrictViewModel>()
                {
                    successfull = true,
                    dataset = data,
                    total = data.Count
                };
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                throw;
            }
        }
    }
}

﻿using System;
using System.Linq;
using AutoMapper;
using DataCore.Data.Infrastructure;
using DataCore.Data.Repositories;
using Services.Handlers;
using Website.ViewModels;
using Models = DataCore.Models;
using WaoInterfaces = Services.Interfaces;

namespace Services.APIServices
{
    public class InventoryService : BaseService, WaoInterfaces.IInventoryService
    {
        // Define singleton instances.
        private readonly IUnitOfWork unitOfWork;
        private readonly IInventoryRepository _inventoryRepository;
        private readonly IMapper mapper;

        // Constructor
        public InventoryService(IUnitOfWork unitOfWork,
                                 IInventoryRepository InventoryRepository,
                                 IMapper mapper) : base(unitOfWork)
        {
            this.unitOfWork = unitOfWork;
            _inventoryRepository = InventoryRepository;
            this.mapper = mapper;
        }

        /// <summary>
        /// Insert data to database.
        /// </summary>
        /// <param name="viewModel"></param>
        /// <returns></returns>
        public ResponseStatus Create(InventoryViewModel viewModel)
        {
            try
            {
                // Mapping field value from view model to model entity
                var inventoryEntity = mapper.Map<Models.Inventory>(viewModel);
                // Save data to database
                _inventoryRepository.Add(inventoryEntity);
                SaveChanges();
                // Create response data
                var resp = new ResponseStatus()
                {
                    successfull = true,
                    message = string.Format("Create data is successfull with id => {0}", inventoryEntity.id),
                    dataset = inventoryEntity
                };
                return resp;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                throw;
            }
        }

        /// <summary>
        /// Delete data from database.
        /// </summary>
        /// <param name="viewModel"></param>
        /// <returns></returns>
        public ResponseStatus Delete(InventoryViewModel viewModel)
        {
            try
            {
                // Checking exist data.
                var entity = _inventoryRepository.Query(model => model.id == viewModel.id).FirstOrDefault();
                if (null == entity)
                {
                    throw new NotFoundDataException(viewModel.id.ToString(), typeof(Models.Collection));
                }
                _inventoryRepository.Delete(entity);
                SaveChanges();
                return new ResponseStatus()
                {
                    successfull = true,
                    message = string.Format("Delete data is successfull with id => {0}", entity.id)
                };
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                throw;
            }
        }

        /// <summary>
        /// Delete data by primary key.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ResponseStatus DeleteById(int id)
        {
            try
            {
                _inventoryRepository.Delete(model => model.id == id);
                SaveChanges();
                return new ResponseStatus()
                {
                    successfull = true,
                    message = string.Format("Delete data is successfull with id => {0}", id)
                };
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                throw;
            }
        }

        /// <summary>
        /// Get datatable with pagination info.
        /// </summary>
        /// <param name="rContext"></param>
        /// <returns></returns>
        public ResponseListModel<InventoryViewModel> GetDatatables(PaggingBase rContext)
        {
            try
            {
                // Variables
                var sortingString = rContext.sortBy + (rContext.reverse ? " descending" : "");
                var isSearching = !string.IsNullOrEmpty(rContext.searchValue);
                if (isSearching)
                {
                    rContext.searchValue = rContext.searchValue.ToLower();
                }
                var responseData = new InventoryDatatable();

                // Fetching data from database.
                var collections = from model in _inventoryRepository.GetAllQueryable()
                                  orderby sortingString
                                  select new InventoryViewModel()
                                  {
                                      id = model.id,
                                      name = model.name,
                                      address = model.address,

                                  };
                responseData.total = collections.Count();

                // Paging data
                collections = collections.Where(model => _inventoryRepository.GetAllQueryable()
                                                                              .OrderBy(x => x.id)
                                                                              .Select(x => x.id)
                                                                              .Skip((rContext.page - 1) * rContext.itemPerPage)
                                                                              .Take(rContext.itemPerPage).Contains(model.id)
                                               );

                // Fetching data from database
                responseData.data = collections.ToList();
                return new ResponseListModel<InventoryViewModel>()
                {
                    successfull = true,
                    dataset = responseData.data,
                    total = responseData.total
                };
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                throw;
            }
        }


        /// <summary>
        /// Get detail info by id.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ResponseSignleModel<InventoryViewModel> GetInfo(int id)
        {
            try
            {
                var collection = _inventoryRepository.Query(model => model.id == id).FirstOrDefault();
                if (null == collection)
                {
                    throw new NotFoundDataException(id.ToString(), typeof(InventoryViewModel));
                }
                var respData = mapper.Map<InventoryViewModel>(collection);
                return new ResponseSignleModel<InventoryViewModel>()
                {
                    successfull = true,
                    dataset = respData
                };
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                throw;
            }
        }

        /// <summary>
        /// Update data.
        /// </summary>
        /// <param name="viewModel"></param>
        /// <returns></returns>
        public ResponseStatus Update(InventoryViewModel viewModel)
        {
            try
            {
                // Mapping field value from view model to model entity
                var entity = mapper.Map<Models.Inventory>(viewModel);
                // Save data to database
                _inventoryRepository.Update(entity);
                SaveChanges();
                // Create response data
                var resp = new ResponseStatus()
                {
                    successfull = true,
                    message = string.Format("Update data is successfull with id => {0}", entity.id),
                    dataset = entity
                };
                return resp;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                throw;
            }
        }

    }
}
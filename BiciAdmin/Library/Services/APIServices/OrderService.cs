﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using DataCore.Data.Infrastructure;
using DataCore.Data.Repositories;
using DataCore.Models;
using Services.Handlers;
using Website.ViewModels;
using Models = DataCore.Models;
using WaoInterfaces = Services.Interfaces;

namespace Services.APIServices
{
    public class OrderService : BaseService, WaoInterfaces.IOrderService
    {
        // Define singleton instances.
        private const string TITLE_FORMAT = "{0}";
        private const string TITLE_FORMAT_MORE = "{0}...và {1} sản phẩm khác.";
        private readonly IUnitOfWork unitOfWork;
        private readonly IOrderRepository _orderRepository;
        private readonly IOrderItemRepository _orderItemRepository;
        private readonly IProductRepository _productRepository;
        private readonly IOrderSummaryRepository _orderSummaryRepository;
        private readonly IProductAttributeRepository _productAttributeRepository;
        private readonly IProductAttributeTypeRepository _productAttributeTypeRepository;
        private readonly IGenericProductRepository _genericProductRepository;
        private readonly IGenericProductMediaRepository _genericProductMediaRepository;
        private readonly ICityRepository _cityRepository;
        private readonly IDistrictRepository _districtRepository;
        private readonly IUserAddressRepository _userAddressRepository;
        private readonly IPaymentMethodRepository _paymentMethodRepository;
        private readonly WaoInterfaces.IDistrictService _districtService;
        private readonly IProductMediaRepository _productMediaRepository;
        private readonly WaoInterfaces.IGenericProductService _genericProductService;
        private readonly IMapper mapper;

        // Constructor
        public OrderService(IUnitOfWork unitOfWork,
                                 IOrderRepository OrderRepository,
                                 IOrderItemRepository orderItemRepository,
                                 IProductRepository productRepository,
                                 IOrderSummaryRepository orderSummaryRepository,
                                 IProductAttributeRepository productAttributeRepository,
                                 IProductAttributeTypeRepository productAttributeTypeRepository,
                                 IGenericProductRepository genericProductRepository,
                                 IGenericProductMediaRepository genericProductMediaRepository,
                                 ICityRepository cityRepository,
                                 IDistrictRepository districtRepository,
                                 IUserAddressRepository userAddressRepository,
                                 IPaymentMethodRepository paymentMethodRepository,
                                 WaoInterfaces.IDistrictService districtService,
                                 IProductMediaRepository productMediaRepository,
                                 WaoInterfaces.IGenericProductService genericProductService,
                                 IMapper mapper) : base(unitOfWork)
        {
            this.unitOfWork = unitOfWork;
            _orderRepository = OrderRepository;
            this._orderItemRepository = orderItemRepository;
            this._productRepository = productRepository;
            this._orderSummaryRepository = orderSummaryRepository;
            this._productAttributeRepository = productAttributeRepository;
            this._productAttributeTypeRepository = productAttributeTypeRepository;
            this._genericProductRepository = genericProductRepository;
            this._genericProductMediaRepository = genericProductMediaRepository;
            this._cityRepository = cityRepository;
            this._districtRepository = districtRepository;
            this._userAddressRepository = userAddressRepository;
            this._paymentMethodRepository = paymentMethodRepository;
            this._districtService = districtService;
            this._productMediaRepository = productMediaRepository;
            this._genericProductService = genericProductService;
            this.mapper = mapper;
        }

        /// <summary>
        /// Insert data to database.
        /// </summary>
        /// <param name="viewModel"></param>
        /// <returns></returns>
        public ResponseStatus Create(OrderViewModel viewModel)
        {
            try
            {
                if (viewModel.details ==  null || viewModel.details.Count <= 0)
                {
                    throw new Exception("Detail must have one or more items.");
                }
                GetTitleAndImageOrder(viewModel);
                // Mapping field value from view model to model entity
                var orderEntity = mapper.Map<Models.Order>(viewModel);
                orderEntity.created_date = DateTime.Now;
                orderEntity.updated_date = DateTime.Now;
                orderEntity.status = 0;
                // Save data to database
                _orderRepository.Add(orderEntity);
                SaveChanges();
                orderEntity.code = GenOrderCode(orderEntity);
                // Create summary.
                var summary = new OrderSummary();
                summary.order_id = orderEntity.id;
                // get service fee
                var paymentMethod = _paymentMethodRepository.Query(model => model.id == orderEntity.payment_method_id).FirstOrDefault();
                if (paymentMethod.is_cod)
                {
                    summary.service_fee_amount = paymentMethod.cod_fee_amount;
                }

                var productIds = viewModel.details.Select(model => model.product_id).ToArray();
                var listProducts = _productRepository.Query(model => productIds.Contains(model.id)).ToList();
                foreach (var item in viewModel.details)
                {
                    var product = listProducts.FirstOrDefault(model => model.id == item.product_id);
                    var itemEntity = new OrderItem()
                    {
                        product_id = product.id,
                        quantity = item.quantity,
                        amount = product.selling_price * item.quantity,
                        order_id = orderEntity.id,
                        unit_price = product.selling_price
                    };
                    _orderItemRepository.Add(itemEntity);
                    summary.sub_total += itemEntity.amount;
                }
                SaveChanges();

                // Get shipping fee
                var district = _userAddressRepository.Query(model => model.id == orderEntity.user_address_id).FirstOrDefault();
                var shippingFee = (AreaViewModel)_districtService.GetShippingFee(district != null ? district.district_id : -1).dataset;

                if (summary.sub_total < shippingFee.limit_transaction_amount)
                {
                    summary.shipping_fee_amount = shippingFee.fee_amount;
                }
                summary.total_amount = summary.sub_total + summary.shipping_fee_amount + summary.service_fee_amount;
                _orderSummaryRepository.Add(summary);
                SaveChanges();
                // Create response data
                var resp = new ResponseStatus()
                {
                    successfull = true,
                    message = string.Format("Create data is successfull with id => {0}", orderEntity.id),
                    dataset = orderEntity
                };
                return resp;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                throw;
            }
        }

        /// <summary>
        /// Update code
        /// </summary>
        /// <param name="order"></param>
        /// <returns></returns>
        private string GenOrderCode(Models.Order order)
        {
            
            try
            {
                order.code = GenerateCode();
                while(_orderRepository.Query(model => model.code == order.code).FirstOrDefault() != null)
                {
                    order.code = GenerateCode();
                }
                _orderRepository.Update(order);
                SaveChanges();
                return order.code;
            }
            catch
            {
                return string.Empty;
            }
        }

        private string GenerateCode()
        {
            var random = new Random((int)DateTime.Now.Ticks);
            var prefix = random.Next(100, 999).ToString("000");
            random = new Random((int)DateTime.Now.Ticks);
            var middle = random.Next(111, 888).ToString("000");
            random = new Random((int)DateTime.Now.Ticks);
            var end = random.Next(222, 777).ToString("000");


            return "M" + prefix + middle + end;
        }

        /// <summary>
        /// Get title for order.
        /// </summary>
        /// <param name="viewModel"></param>
        /// <returns></returns>
        private void GetTitleAndImageOrder(OrderViewModel viewModel)
        {
            try
            {
                var product = _productRepository.Query(model => model.id == viewModel.details[0].product_id).FirstOrDefault();
                if (product == null)
                {
                    throw new Exception();
                }

                var genericProduct = _genericProductRepository.Query(model => model.id == product.generic_product_id).FirstOrDefault();
                if (genericProduct == null)
                {
                    throw new Exception();
                }

                var productImage = _genericProductService.GetProductMedias(new int[] { product.id }).FirstOrDefault();
                if (productImage != null)
                {
                    viewModel.image = productImage.image_link;
                }
                else
                {
                    // Get image
                    var image = _genericProductService.GetGenericMedias(new int[] { genericProduct.id }).FirstOrDefault();
                    if (image != null)
                    {
                        viewModel.image = image.image_link;
                    }

                }

                if (viewModel.details.Count == 1)
                {
                    viewModel.title = string.Format(TITLE_FORMAT, genericProduct.product_name);
                }
                viewModel.title = string.Format(TITLE_FORMAT_MORE, genericProduct.product_name, viewModel.details.Count - 1);
            }
            catch
            {
                viewModel.title = $"Đơn hàng ngày {DateTime.Now.ToString("dd/MM/yyyy")}";
            }
        }

        /// <summary>
        /// Delete data from database.
        /// </summary>
        /// <param name="viewModel"></param>
        /// <returns></returns>
        public ResponseStatus Delete(OrderViewModel viewModel)
        {
            try
            {
                // Checking exist data.
                var entity = _orderRepository.Query(model => model.id == viewModel.id).FirstOrDefault();
                if (null == entity)
                {
                    throw new NotFoundDataException(viewModel.id.ToString(), typeof(Models.Collection));
                }
                _orderRepository.Delete(entity);
                SaveChanges();
                return new ResponseStatus()
                {
                    successfull = true,
                    message = string.Format("Delete data is successfull with id => {0}", entity.id)
                };
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                throw;
            }
        }

        /// <summary>
        /// Delete data by primary key.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ResponseStatus DeleteById(int id)
        {
            try
            {
                _orderRepository.Delete(model => model.id == id);
                SaveChanges();
                return new ResponseStatus()
                {
                    successfull = true,
                    message = string.Format("Delete data is successfull with id => {0}", id)
                };
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                throw;
            }
        }

        /// <summary>
        /// Get datatable with pagination info.
        /// </summary>
        /// <param name="rContext"></param>
        /// <returns></returns>
        public ResponseListModel<OrderViewModel> GetDatatables(PaggingBase rContext)
        {
            try
            {
                throw new NotImplementedException();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                throw;
            }
        }

        /// <summary>
        /// Get histories of user.
        /// </summary>
        /// <param name="username"></param>
        /// <returns></returns>
        public ResponseListModel<OrderViewModel> GetHistories(string username, PagingOrderHistories rContext)
        {
            try
            {
                // Variables
                var responseData = new OrderDatatable();

                // Fetching data from database.
                var collections = from model in _orderRepository.GetAllQueryable()
                                  where model.user_id == username
                                  orderby model.created_date descending
                                  select new OrderViewModel()
                                  {
                                      id = model.id,
                                      code = model.code,
                                      bank_transfer_code = model.bank_transfer_code,
                                      user_id = model.user_id,
                                      user_address_id = model.user_address_id,
                                      payment_method_id = model.payment_method_id,
                                      note = model.note,
                                      source = model.source,
                                      title = model.title,
                                      image = model.image,
                                      created_date = model.created_date
                                  };
                responseData.total = collections.Count();
                if (rContext.page != 0 && rContext.itemPerPage != -1)
                {
                    collections = collections.Skip((rContext.page - 1) * rContext.itemPerPage)
                                             .Take(rContext.itemPerPage);
                }
                // Fetching data from database
                responseData.data = collections.ToList();
                

                return new ResponseListModel<OrderViewModel>()
                {
                    successfull = true,
                    dataset = responseData.data,
                    total = responseData.total
                };
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                throw;
            }
        }


        /// <summary>
        /// Get detail info by id.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ResponseSignleModel<OrderViewModel> GetInfo(int id)
        {
            try
            {
                var collection = _orderRepository.Query(model => model.id == id).FirstOrDefault();
                if (null == collection)
                {
                    throw new NotFoundDataException(id.ToString(), typeof(OrderViewModel));
                }
                var respData = mapper.Map<OrderViewModel>(collection);
                // Get summary
                var orderSummary = _orderSummaryRepository.Query(model => model.order_id == respData.id).FirstOrDefault();
                respData.summary = mapper.Map<OrderSummaryViewModel>(orderSummary);
                var details = from detail in _orderItemRepository.Query(model => model.order_id == respData.id)
                              join product in _productRepository.GetAllQueryable()
                              on detail.product_id equals product.id into dProduct
                              from product in dProduct.DefaultIfEmpty()
                              join pTypeValue in _productAttributeRepository.GetAllQueryable()
                                               on product.id equals pTypeValue.product_id into valueList
                              from pTypeValue in valueList.DefaultIfEmpty()
                              join pType in _productAttributeTypeRepository.GetAllQueryable()
                                         on pTypeValue.attribute_id equals pType.id into typelist
                              from pType in typelist.DefaultIfEmpty()
                              join gProduct in _genericProductRepository.GetAllQueryable()
                              on product.generic_product_id equals gProduct.id into gp
                              from gproduct in gp.DefaultIfEmpty()
                              select new OrderItemViewModel()
                              {
                                  id = detail.id,
                                  amount = detail.amount,
                                  order_id = detail.order_id,
                                  product_id = detail.product_id,
                                  quantity = detail.quantity,
                                  unit_price = detail.unit_price,
                                  product_name = gproduct.product_name + "-" + product.product_name,
                                  variant_type = pType.name,
                                  variant_value = pTypeValue.value,
                                  generic_product_id = gproduct.id,
                              };
                respData.details = details.ToList();

                // Get medias
                var productIds = respData.details.Select(p => p.product_id).ToArray();
                var medias = _genericProductService.GetProductMedias(productIds);

                var genericIds = respData.details.Select(p => p.generic_product_id).ToArray();
                var genericMedia = _genericProductService.GetGenericMedias(genericIds);

                respData.details.ForEach(model =>
                {
                    var media = medias.FirstOrDefault(img => img.product_id == model.product_id);
                    if (media == null)
                    {
                        var mediaGeneric = genericMedia.FirstOrDefault(img => img.generic_product_id == model.generic_product_id);
                        model.product_image = mediaGeneric == null ? null : mediaGeneric.image_link;
                    }
                    else
                    {
                        model.product_image = media.image_link;
                    }
                });


                // Get address
                var address = (from uAddress in _userAddressRepository.Query(model => model.id == respData.user_address_id)
                              join ct in _cityRepository.GetAllQueryable()
                              on uAddress.city_id equals ct.id into uct
                              from city in uct.DefaultIfEmpty()
                              join dt in _districtRepository.GetAllQueryable()
                              on uAddress.district_id equals dt.id into udt
                              from district in udt.DefaultIfEmpty()
                              select new UserAddressViewModel()
                              {
                                  address = uAddress.address,
                                  city_id = uAddress.city_id,
                                  city_name = city.name,
                                  district_id = uAddress.district_id,
                                  district_name = district.name,
                                  isdefault = uAddress.isdefault,
                                  id = uAddress.id,
                                  location_type = uAddress.location_type,
                                  username = uAddress.username,
                                  receiver_name = uAddress.receiver_name,
                                  receiver_phoneno = uAddress.receiver_phoneno,
                                  created_by = uAddress.created_by,
                                  created_date = uAddress.created_date,
                                  updated_by = uAddress.updated_by,
                                  updated_date = uAddress.updated_date
                              }).FirstOrDefault();
                respData.delivery_address = address;
                return new ResponseSignleModel<OrderViewModel>()
                {
                    successfull = true,
                    dataset = respData
                };
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                throw;
            }
        }

        /// <summary>
        /// Update data.
        /// </summary>
        /// <param name="viewModel"></param>
        /// <returns></returns>
        public ResponseStatus Update(OrderViewModel viewModel)
        {
            try
            {
                // Mapping field value from view model to model entity
                var entity = mapper.Map<Models.Order>(viewModel);
                // Save data to database
                _orderRepository.Update(entity);
                SaveChanges();
                // Create response data
                var resp = new ResponseStatus()
                {
                    successfull = true,
                    message = string.Format("Update data is successfull with id => {0}", entity.id),
                    dataset = entity
                };
                return resp;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                throw;
            }
        }

        /// <summary>
        /// Get allorders
        /// </summary>
        /// <param name="rContext"></param>
        /// <returns></returns>
        public ResponseListModel<OrderAdminViewModel> GetAllOrders(PagingOrder rContext)
        {
            try
            {
                // Variables
                //var sortingString = rContext.sortBy + (rContext.reverse ? " descending" : "");
                var isSearching = !string.IsNullOrEmpty(rContext.searchValue);
                if (isSearching)
                {
                    rContext.searchValue = rContext.searchValue.ToLower();
                }
                var responseData = new ResponseListModel<OrderAdminViewModel>();

                // Fetching data from database.
                var collections = from model in _orderRepository.GetAllQueryable()
                                  join addr in _userAddressRepository.GetAllQueryable()
                                  on model.user_address_id equals addr.id into uAddr
                                  from addr in uAddr.DefaultIfEmpty()
                                  join city in _cityRepository.GetAllQueryable()
                                  on addr.city_id equals city.id into uCity
                                  from city in uCity.DefaultIfEmpty()
                                  join district in _districtRepository.GetAllQueryable()
                                  on addr.district_id equals district.id into uDistrict
                                  from district in uDistrict.DefaultIfEmpty()
                                  join summary in _orderSummaryRepository.GetAllQueryable()
                                  on model.id equals summary.order_id into uOrder
                                  from summary in uOrder.DefaultIfEmpty()
                                  join payment in _paymentMethodRepository.GetAllQueryable()
                                  on model.payment_method_id equals payment.id into uPayment
                                  from payment in uPayment.DefaultIfEmpty()
                                  //orderby sortingString
                                  where (rContext.branch_id == 0 || (rContext.branch_id != 0 && model.branch_id == rContext.branch_id))
                                        & (rContext.status == -1 || (rContext.status != -1 && model.status == rContext.status))
                                        & ( string.IsNullOrEmpty(rContext.searchValue) ||
                                            (!string.IsNullOrEmpty(rContext.searchValue) &
                                            (model.code.Contains(rContext.searchValue) || addr.receiver_phoneno.Contains(rContext.searchValue)))
                                          )
                                        & ( string.IsNullOrEmpty(rContext.source) ||
                                            (!string.IsNullOrEmpty(rContext.source) & model.source.Contains(rContext.source))
                                          )
                                        & (rContext.ids == null || (rContext.ids.Contains(model.id)))
                                  select new OrderAdminViewModel()
                                  {
                                      id = model.id,
                                      code = model.code,
                                      bank_transfer_code = model.bank_transfer_code,
                                      user_id = model.user_id,
                                      user_address_id = model.user_address_id,
                                      payment_method_id = model.payment_method_id,
                                      note = model.note,
                                      source = model.source,
                                      customer_name = addr == null ? "Chưa rõ" : addr.receiver_name,
                                      area = city.name + "(" + district.name + ")",
                                      total_amount = summary == null ? 0 : summary.total_amount,
                                      status = model.status,
                                      created_date = model.created_date,
                                      payment_method_name = payment == null ? "Không rõ" : payment.name,
                                      cod_fee_amount = payment == null ? 0 : payment.cod_fee_amount,
                                      approve_note = summary.note
                                  };

                // Sorting
                var param = rContext.sortBy;
                var pi = typeof(OrderAdminViewModel).GetProperty(param);

                collections = rContext.reverse ? collections.OrderByDescending(x => pi.GetValue(x, null)) : collections.OrderBy(x => pi.GetValue(x, null));
                responseData.total = collections.Count();

                // Paging data
                collections = collections.Skip((rContext.page - 1) * rContext.itemPerPage)
                                         .Take(rContext.itemPerPage);

                // Fetching data from database
                responseData.dataset = collections.ToList();
                responseData.successfull = true;
                return responseData;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                throw;
            }
        }

        public ResponseSignleModel<OrderAdminViewModel> GetInfoByAdmin(int id)
        {
            try
            {
                var data = (from model in _orderRepository.GetAllQueryable()
                           join addr in _userAddressRepository.GetAllQueryable()
                           on model.user_address_id equals addr.id into uAddr
                           from addr in uAddr.DefaultIfEmpty()
                           join city in _cityRepository.GetAllQueryable()
                           on addr.city_id equals city.id into uCity
                           from city in uCity.DefaultIfEmpty()
                           join district in _districtRepository.GetAllQueryable()
                           on addr.district_id equals district.id into uDistrict
                           from district in uDistrict.DefaultIfEmpty()
                           join summary in _orderSummaryRepository.GetAllQueryable()
                           on model.id equals summary.order_id into uOrder
                           from summary in uOrder.DefaultIfEmpty()
                           join payment in _paymentMethodRepository.GetAllQueryable()
                           on model.payment_method_id equals payment.id into uPayment
                           from payment in uPayment.DefaultIfEmpty()
                            where model.id == id
                           select new OrderAdminViewModel()
                           {
                               id = model.id,
                               code = model.code,
                               bank_transfer_code = model.bank_transfer_code,
                               user_id = model.user_id,
                               user_address_id = model.user_address_id,
                               payment_method_id = model.payment_method_id,
                               note = model.note,
                               source = model.source,
                               customer_name = addr.receiver_name,
                               area = city.name + "(" + district.name + ")",
                               total_amount = summary == null ? 0 : summary.total_amount,
                               amount = summary == null ? 0 : summary.sub_total,
                               ship = summary == null ? 0 : summary.shipping_fee_amount,
                               status = model.status,
                               created_date = model.created_date,
                               full_address = addr.address + ", " + district.name + ", " + city.name,
                               phone = addr.receiver_phoneno,
                               payment_method_name = payment == null ? "Không rõ" : payment.name,
                               cod_fee_amount = summary ==  null ? 0 : summary.service_fee_amount
                           }).FirstOrDefault();
                if (data == null)
                {
                    throw new Exception("Not found data");
                }

                var items = (from orderD in _orderItemRepository.GetAllQueryable()
                            join product in _productRepository.GetAllQueryable()
                            on orderD.product_id equals product.id into pd
                            from product in pd.DefaultIfEmpty()
                            join gProduct in _genericProductRepository.GetAllQueryable()
                            on product.generic_product_id equals gProduct.id into gp
                            from gProduct in gp.DefaultIfEmpty()
                            where orderD.order_id == data.id
                            select new OrderAdminItemViewModel()
                            {
                                id = orderD.id,
                                product_id = orderD.product_id,
                                price = orderD.unit_price,
                                qty = orderD.quantity,
                                amount = orderD.amount,
                                product_name = gProduct.product_name,
                                sku = product.kiot_product_code,
                                variant_name = product.product_name,
                                generic_id = gProduct.id
                            }).ToList();
                data.items = items;
                return new ResponseSignleModel<OrderAdminViewModel>()
                {
                    dataset = data,
                    successfull = true
                };
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                throw;
            }
        }
    }
}
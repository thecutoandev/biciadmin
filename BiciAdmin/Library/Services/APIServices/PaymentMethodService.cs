﻿using System;
using System.Linq;
using AutoMapper;
using DataCore.Data.Infrastructure;
using DataCore.Data.Repositories;
using Services.Handlers;
using Website.ViewModels;
using Models = DataCore.Models;
using WaoInterfaces = Services.Interfaces;

namespace Services.APIServices
{
    public class PaymentMethodService : BaseService, WaoInterfaces.IPaymentMethodService
    {
        // Define singleton instances.
        private readonly IUnitOfWork unitOfWork;
        private readonly IPaymentMethodRepository _paymentMethodRepository;
        private readonly IMapper mapper;

        // Constructor
        public PaymentMethodService(IUnitOfWork unitOfWork,
                                 IPaymentMethodRepository PaymentMethodRepository,
                                 IMapper mapper) : base(unitOfWork)
        {
            this.unitOfWork = unitOfWork;
            _paymentMethodRepository = PaymentMethodRepository;
            this.mapper = mapper;
        }

        /// <summary>
        /// Insert data to database.
        /// </summary>
        /// <param name="viewModel"></param>
        /// <returns></returns>
        public ResponseStatus Create(PaymentMethodViewModel viewModel)
        {
            try
            {
                // Mapping field value from view model to model entity
                var paymentMethodEntity = mapper.Map<Models.PaymentMethod>(viewModel);
                paymentMethodEntity.created_date = DateTime.Now;
                paymentMethodEntity.updated_date = DateTime.Now;
                // Save data to database
                _paymentMethodRepository.Add(paymentMethodEntity);
                SaveChanges();
                // Create response data
                var resp = new ResponseStatus()
                {
                    successfull = true,
                    message = string.Format("Create data is successfull with id => {0}", paymentMethodEntity.id),
                    dataset = paymentMethodEntity
                };
                return resp;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                throw;
            }
        }

        /// <summary>
        /// Delete data from database.
        /// </summary>
        /// <param name="viewModel"></param>
        /// <returns></returns>
        public ResponseStatus Delete(PaymentMethodViewModel viewModel)
        {
            try
            {
                // Checking exist data.
                var entity = _paymentMethodRepository.Query(model => model.id == viewModel.id).FirstOrDefault();
                if (null == entity)
                {
                    throw new NotFoundDataException(viewModel.id.ToString(), typeof(Models.Collection));
                }
                _paymentMethodRepository.Delete(entity);
                SaveChanges();
                return new ResponseStatus()
                {
                    successfull = true,
                    message = string.Format("Delete data is successfull with id => {0}", entity.id)
                };
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                throw;
            }
        }

        /// <summary>
        /// Delete data by primary key.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ResponseStatus DeleteById(int id)
        {
            try
            {
                _paymentMethodRepository.Delete(model => model.id == id);
                SaveChanges();
                return new ResponseStatus()
                {
                    successfull = true,
                    message = string.Format("Delete data is successfull with id => {0}", id)
                };
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                throw;
            }
        }

        /// <summary>
        /// Get datatable with pagination info.
        /// </summary>
        /// <param name="rContext"></param>
        /// <returns></returns>
        public ResponseListModel<PaymentMethodViewModel> GetDatatables(PaggingBase rContext)
        {
            try
            {
                // Variables
                //var sortingString = rContext.sortBy + (rContext.reverse ? " descending" : "");
                //var isSearching = !string.IsNullOrEmpty(rContext.searchValue);
                //if (isSearching)
                //{
                //    rContext.searchValue = rContext.searchValue.ToLower();
                //}
                var responseData = new PaymentMethodDatatable();

                // Fetching data from database.
                var collections = from model in _paymentMethodRepository.GetAllQueryable()
                                  orderby model.name
                                  select new PaymentMethodViewModel()
                                  {
                                      id = model.id,
                                      name = model.name,
                                      description = model.description,
                                      is_cod = model.is_cod,
                                      cod_fee_amount = model.cod_fee_amount,

                                  };
                responseData.total = collections.Count();

                // Paging data
                //collections = collections.Where(model => _paymentMethodRepository.GetAllQueryable()
                //                                                              .OrderBy(x => x.id)
                //                                                              .Select(x => x.id)
                //                                                              .Skip((rContext.page - 1) * rContext.itemPerPage)
                //                                                              .Take(rContext.itemPerPage).Contains(model.id)
                //                               );

                // Fetching data from database
                responseData.data = collections.ToList();
                return new ResponseListModel<PaymentMethodViewModel>()
                {
                    successfull = true,
                    dataset = responseData.data,
                    total = responseData.total
                };
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                throw;
            }
        }


        /// <summary>
        /// Get detail info by id.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ResponseSignleModel<PaymentMethodViewModel> GetInfo(int id)
        {
            try
            {
                var collection = _paymentMethodRepository.Query(model => model.id == id).FirstOrDefault();
                if (null == collection)
                {
                    throw new NotFoundDataException(id.ToString(), typeof(PaymentMethodViewModel));
                }
                var respData = mapper.Map<PaymentMethodViewModel>(collection);
                return new ResponseSignleModel<PaymentMethodViewModel>()
                {
                    successfull = true,
                    dataset = respData
                };
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                throw;
            }
        }

        /// <summary>
        /// Update data.
        /// </summary>
        /// <param name="viewModel"></param>
        /// <returns></returns>
        public ResponseStatus Update(PaymentMethodViewModel viewModel)
        {
            try
            {
                // Mapping field value from view model to model entity
                var entity = mapper.Map<Models.PaymentMethod>(viewModel);
                // Save data to database
                _paymentMethodRepository.Update(entity);
                SaveChanges();
                // Create response data
                var resp = new ResponseStatus()
                {
                    successfull = true,
                    message = string.Format("Update data is successfull with id => {0}", entity.id),
                    dataset = entity
                };
                return resp;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                throw;
            }
        }

    }
}
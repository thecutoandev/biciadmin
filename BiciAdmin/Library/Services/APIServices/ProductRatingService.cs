﻿using System;
using System.Linq;
using AutoMapper;
using DataCore.Data.Infrastructure;
using DataCore.Data.Repositories;
using Services.Handlers;
using Website.ViewModels;
using Models = DataCore.Models;
using WaoInterfaces = Services.Interfaces;

namespace Services.APIServices
{
    public class ProductRatingService : BaseService, WaoInterfaces.IProductRatingService
    {
        // Define singleton instances.
        private readonly IUnitOfWork unitOfWork;
        private readonly IProductRatingRepository _productRatingRepository;
        private readonly IMapper mapper;

        // Constructor
        public ProductRatingService(IUnitOfWork unitOfWork,
                                 IProductRatingRepository ProductRatingRepository,
                                 IMapper mapper) : base(unitOfWork)
        {
            this.unitOfWork = unitOfWork;
            _productRatingRepository = ProductRatingRepository;
            this.mapper = mapper;
        }

        /// <summary>
        /// Insert data to database, and response for client.
        /// </summary>
        /// <param name="viewModel">Model for inserting.</param>
        /// <returns>Response infomation.</returns>
        public ResponseStatus Create(ProductRatingViewModel viewModel)
        {
            try
            {
                // Mapping field value from view model to model entity
                var productRatingEntity = mapper.Map<Models.ProductRating>(viewModel);
                // Save data to database
                _productRatingRepository.Add(productRatingEntity);
                SaveChanges();
                // Create response data
                var resp = new ResponseStatus()
                {
                    successfull = true,
                    message = string.Format("Create data is successfull with id => {0}", productRatingEntity.id),
                    dataset = productRatingEntity
                };
                return resp;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                throw;
            }
        }

        /// <summary>
        /// Delete data from database.
        /// </summary>
        /// <param name="viewModel">Model for deleting.</param>
        /// <returns>Response infomation</returns>
        public ResponseStatus Delete(ProductRatingViewModel viewModel)
        {
            try
            {
                var entity = _productRatingRepository.Query(model => model.id == viewModel.id).FirstOrDefault();
                if (null == entity)
                {
                    throw new NotFoundDataException(viewModel.id.ToString(), typeof(Models.Collection));
                }
                _productRatingRepository.Delete(entity);
                SaveChanges();
                return new ResponseStatus()
                {
                    successfull = true,
                    message = string.Format("Delete data is successfull with id => {0}", entity.id)
                };
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                throw;
            }
        }

        /// <summary>
        /// Delete database by id.
        /// </summary>
        /// <param name="id">Id of model.</param>
        /// <returns>Response infomation.</returns>
        public ResponseStatus DeleteById(int id)
        {
            try
            {
                _productRatingRepository.Delete(model => model.id == id);
                SaveChanges();
                return new ResponseStatus()
                {
                    successfull = true,
                    message = string.Format("Delete data is successfull with id => {0}", id)
                };
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                throw;
            }
        }

        /// <summary>
        /// Get list product rating of the product.
        /// </summary>
        /// <param name="rContext">Pagination context</param>
        /// <returns></returns>
        public ResponseListModel<ProductRatingViewModel> GetDatatables(ProductRatingRequest rContext)
        {
            try
            {
                // Variables
                var sortingString = rContext.sortBy + (rContext.reverse ? " descending" : "");
                var responseData = new ProductRatingDatatable();

                // Fetching data from database.
                var collections = from model in _productRatingRepository.GetAllQueryable()
                                  where model.product_id == rContext.productId
                                  orderby sortingString
                                  select new ProductRatingViewModel()
                                  {
                                      id = model.id,
                                      product_id = model.product_id,
                                      order_id = model.order_id,
                                      rating_product_star = model.rating_product_star,
                                      rating_content = model.rating_content,

                                  };
                responseData.total = collections.Count();

                // Paging data
                collections = collections.Where(model => _productRatingRepository.GetAllQueryable()
                                                                              .OrderBy(x => x.id)
                                                                              .Select(x => x.id)
                                                                              .Skip((rContext.page - 1) * rContext.itemPerPage)
                                                                              .Take(rContext.itemPerPage).Contains(model.id)
                                               );

                // Fetching data from database
                responseData.data = collections.ToList();
                return new ResponseListModel<ProductRatingViewModel>()
                {
                    successfull = true,
                    dataset = responseData.data,
                    total = responseData.total
                };
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                throw;
            }
        }

        /// <summary>
        /// Get product rating info
        /// </summary>
        /// <param name="id">Id of rating</param>
        /// <returns></returns>
        public ResponseSignleModel<ProductRatingViewModel> GetInfo(int id)
        {
            try
            {
                var modelData = _productRatingRepository.Query(model => model.product_id == id).FirstOrDefault();
                if (null == modelData)
                {
                    throw new NotFoundDataException(id.ToString(), typeof(ProductRatingViewModel));
                }
                var respData = mapper.Map<ProductRatingViewModel>(modelData);
                return new ResponseSignleModel<ProductRatingViewModel>()
                {
                    successfull = true,
                    dataset = respData
                };
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                throw;
            }
        }

        /// <summary>
        /// Update data to database
        /// </summary>
        /// <param name="viewModel">Model for updating.</param>
        /// <returns>Response infomation.</returns>
        public ResponseStatus Update(ProductRatingViewModel viewModel)
        {
            try
            {
                // Mapping field value from view model to model entity
                var productRatingEntity = mapper.Map<Models.ProductRating>(viewModel);
                // Save data to database
                _productRatingRepository.Update(productRatingEntity);
                SaveChanges();
                // Create response data
                var resp = new ResponseStatus()
                {
                    successfull = true,
                    message = string.Format("Update data is successfull with id => {0}", productRatingEntity.id),
                    dataset = productRatingEntity
                };
                return resp;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                throw;
            }
        }
    }
}

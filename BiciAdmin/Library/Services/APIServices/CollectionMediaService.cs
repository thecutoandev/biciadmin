﻿using System;
using System.Linq;
using AutoMapper;
using DataCore.Data.Infrastructure;
using DataCore.Data.Repositories;
using Services.Handlers;
using Website.ViewModels;
using Models = DataCore.Models;
using WaoInterfaces = Services.Interfaces;

namespace Services.APIServices
{
    public class CollectionMediaService : BaseService, WaoInterfaces.ICollectionMediaService
    {
        // Define singleton instances.
        private readonly IUnitOfWork unitOfWork;
        private readonly ICollectionMediaRepository _collectionMediaRepository;
        private readonly IMapper mapper;

        // Constructor
        public CollectionMediaService(IUnitOfWork unitOfWork,
                                 ICollectionMediaRepository CollectionMediaRepository,
                                 IMapper mapper) : base(unitOfWork)
        {
            this.unitOfWork = unitOfWork;
            _collectionMediaRepository = CollectionMediaRepository;
            this.mapper = mapper;
        }

        /// <summary>
        /// Insert data to database.
        /// </summary>
        /// <param name="viewModel"></param>
        /// <returns></returns>
        public ResponseStatus Create(CollectionMediaViewModel viewModel)
        {
            try
            {
                // Mapping field value from view model to model entity
                var collectionMediaEntity = mapper.Map<Models.CollectionMedia>(viewModel);
                // Save data to database
                _collectionMediaRepository.Add(collectionMediaEntity);
                SaveChanges();
                // Create response data
                var resp = new ResponseStatus()
                {
                    successfull = true,
                    message = string.Format("Create data is successfull with id => {0}", collectionMediaEntity.id),
                    dataset = collectionMediaEntity
                };
                return resp;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                throw;
            }
        }

        /// <summary>
        /// Delete data from database.
        /// </summary>
        /// <param name="viewModel"></param>
        /// <returns></returns>
        public ResponseStatus Delete(CollectionMediaViewModel viewModel)
        {
            try
            {
                // Checking exist data.
                var entity = _collectionMediaRepository.Query(model => model.id == viewModel.id).FirstOrDefault();
                if (null == entity)
                {
                    throw new NotFoundDataException(viewModel.id.ToString(), typeof(Models.Collection));
                }
                _collectionMediaRepository.Delete(entity);
                SaveChanges();
                return new ResponseStatus()
                {
                    successfull = true,
                    message = string.Format("Delete data is successfull with id => {0}", entity.id)
                };
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                throw;
            }
        }

        /// <summary>
        /// Delete data by primary key.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ResponseStatus DeleteById(int id)
        {
            try
            {
                _collectionMediaRepository.Delete(model => model.id == id);
                SaveChanges();
                return new ResponseStatus()
                {
                    successfull = true,
                    message = string.Format("Delete data is successfull with id => {0}", id)
                };
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                throw;
            }
        }

        /// <summary>
        /// Get datatable with pagination info.
        /// </summary>
        /// <param name="rContext"></param>
        /// <returns></returns>
        public ResponseListModel<CollectionMediaViewModel> GetDatatables(PaggingBase rContext)
        {
            try
            {
                // Variables
                var sortingString = rContext.sortBy + (rContext.reverse ? " descending" : "");
                var isSearching = !string.IsNullOrEmpty(rContext.searchValue);
                if (isSearching)
                {
                    rContext.searchValue = rContext.searchValue.ToLower();
                }
                var responseData = new CollectionMediaDatatable();

                // Fetching data from database.
                var collections = from model in _collectionMediaRepository.GetAllQueryable()
                                  orderby sortingString
                                  select new CollectionMediaViewModel()
                                  {
                                      id = model.id,
                                      collection_id = model.collection_id,
                                      image_link = model.image_link,

                                  };
                responseData.total = collections.Count();

                // Paging data
                collections = collections.Where(model => _collectionMediaRepository.GetAllQueryable()
                                                                              .OrderBy(x => x.id)
                                                                              .Select(x => x.id)
                                                                              .Skip((rContext.page - 1) * rContext.itemPerPage)
                                                                              .Take(rContext.itemPerPage).Contains(model.id)
                                               );

                // Fetching data from database
                responseData.data = collections.ToList();
                return new ResponseListModel<CollectionMediaViewModel>()
                {
                    successfull = true,
                    dataset = responseData.data,
                    total = responseData.total
                };
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                throw;
            }
        }


        /// <summary>
        /// Get detail info by id.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ResponseSignleModel<CollectionMediaViewModel> GetInfo(int id)
        {
            try
            {
                var collection = _collectionMediaRepository.Query(model => model.id == id).FirstOrDefault();
                if (null == collection)
                {
                    throw new NotFoundDataException(id.ToString(), typeof(CollectionMediaViewModel));
                }
                var respData = mapper.Map<CollectionMediaViewModel>(collection);
                return new ResponseSignleModel<CollectionMediaViewModel>()
                {
                    successfull = true,
                    dataset = respData
                };
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                throw;
            }
        }

        /// <summary>
        /// Update data.
        /// </summary>
        /// <param name="viewModel"></param>
        /// <returns></returns>
        public ResponseStatus Update(CollectionMediaViewModel viewModel)
        {
            try
            {
                // Mapping field value from view model to model entity
                var entity = mapper.Map<Models.CollectionMedia>(viewModel);
                // Save data to database
                _collectionMediaRepository.Update(entity);
                SaveChanges();
                // Create response data
                var resp = new ResponseStatus()
                {
                    successfull = true,
                    message = string.Format("Update data is successfull with id => {0}", entity.id),
                    dataset = entity
                };
                return resp;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                throw;
            }
        }

    }
}
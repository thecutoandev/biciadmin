﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using DataCore.Data.Infrastructure;
using DataCore.Data.Repositories;
using Services.Handlers;
using Website.ViewModels;
using Models = DataCore.Models;
using WaoInterfaces = Services.Interfaces;

namespace Services.APIServices
{
    public class CollectionService : BaseService, WaoInterfaces.ICollectionService
    {
        // Define singleton instances.
        private readonly IUnitOfWork unitOfWork;
        private readonly ICollectionRepository _collectionRepository;
        private readonly ICollectionMediaRepository _collectionMediaRepository;
        private readonly IGenericProductRepository _genericProductRepository;
        private readonly IGenericProductMediaRepository _genericProductMediaRepository;
        private readonly IProductRepository _productRepository;
        private readonly IProductCollectionRepository _productCollectionRepository;
        private readonly WaoInterfaces.IGenericProductService _genericProductService;
        private readonly IMapper mapper;

        // Constructor
        public CollectionService(IUnitOfWork unitOfWork,
                                 ICollectionRepository collectionRepository,
                                 IGenericProductRepository genericProductRepository,
                                 IProductRepository productRepository,
                                 IProductCollectionRepository productCollectionRepository,
                                 ICollectionMediaRepository collectionMediaRepository,
                                 IGenericProductMediaRepository genericProductMediaRepository,
                                 WaoInterfaces.IGenericProductService genericProductService,
                                 IMapper mapper) : base(unitOfWork)
        {
            this.unitOfWork = unitOfWork;
            _collectionRepository = collectionRepository;
            this._genericProductRepository = genericProductRepository;
            this._productRepository = productRepository;
            this._productCollectionRepository = productCollectionRepository;
            this._collectionMediaRepository = collectionMediaRepository;
            this._genericProductMediaRepository = genericProductMediaRepository;
            this._genericProductService = genericProductService;
            this.mapper = mapper;
        }

        /// <summary>
        /// Insert data to database, and response for client.
        /// </summary>
        /// <param name="viewModel">Model for inserting.</param>
        /// <returns>Response infomation.</returns>
        public ResponseStatus Create(CollectionViewModel viewModel)
        {
            try
            {
                // Mapping field value from view model to model entity
                var collectionEntity = mapper.Map<Models.Collection>(viewModel);
                collectionEntity.created_date = DateTime.Now;
                collectionEntity.updated_date = DateTime.Now;
                // Save data to database
                _collectionRepository.Add(collectionEntity);
                SaveChanges();

                _collectionMediaRepository.Delete(model => model.collection_id == collectionEntity.id);
                SaveChanges();

                foreach (var img in viewModel.medias)
                {
                    var imgEntity = new Models.CollectionMedia()
                    {
                        collection_id = collectionEntity.id,
                        image_link = img
                    };
                    _collectionMediaRepository.Add(imgEntity);
                }
                SaveChanges();

                foreach (var item in viewModel.generic_products)
                {
                    var entity = new Models.ProductCollection()
                    {
                        collection_id = collectionEntity.id,
                        product_id = item.id
                    };
                    _productCollectionRepository.Add(entity);
                }
                SaveChanges();
                // Create response data
                var resp = new ResponseStatus()
                {
                    successfull = true,
                    message = string.Format("Create data is successfull with id => {0}", collectionEntity.id),
                    dataset = collectionEntity
                };
                return resp;
            }
            catch (Exception ex)
            {
                return new ResponseStatus()
                {
                    successfull = false,
                    message = ex.Message,
                    dataset = null
                };
            }
        }

        /// <summary>
        /// Delete data from database.
        /// </summary>
        /// <param name="viewModel">Model for deleting.</param>
        /// <returns>Response infomation</returns>
        public ResponseStatus Delete(CollectionViewModel viewModel)
        {
            try
            {
                var entity = _collectionRepository.Query(model => model.id == viewModel.id).FirstOrDefault();
                if (null == entity)
                {
                    throw new NotFoundDataException(viewModel.id.ToString(), typeof(Models.Collection));
                }
                _collectionRepository.Delete(entity);
                SaveChanges();
                return new ResponseStatus()
                {
                    successfull = true,
                    message = string.Format("Delete data is successfull with id => {0}", entity.id)
                };
            }
            catch (Exception ex)
            {
                return new ResponseStatus()
                {
                    successfull = false,
                    message = ex.Message
                };
            }
        }

        /// <summary>
        /// Delete database by id.
        /// </summary>
        /// <param name="id">Id of model.</param>
        /// <returns>Response infomation.</returns>
        public ResponseStatus DeleteById(int id)
        {
            try
            {
                _collectionRepository.Delete(model => model.id == id);
                SaveChanges();
                return new ResponseStatus()
                {
                    successfull = true,
                    message = string.Format("Delete data is successfull with id => {0}", id)
                };
            }
            catch (Exception ex)
            {
                return new ResponseStatus()
                {
                    successfull = false,
                    message = ex.Message
                };
            }
        }

        /// <summary>
        /// Get list collection
        /// </summary>
        /// <param name="rContext"></param>
        /// <returns></returns>
        public ResponseListModel<CollectionViewModel> GetDatatables(CollectionRequest rContext)
        {
            try
            {
                // Variables
                var sortingString = rContext.sortBy + (rContext.reverse ? " descending" : "");
                var isSearching = !string.IsNullOrEmpty(rContext.searchValue);
                if (isSearching)
                {
                    rContext.searchValue = rContext.searchValue.ToLower();
                }
                var responseData = new CollectionDatatable();

                // Fetching data from database.
                var collections = from collect in _collectionRepository.GetAllQueryable()
                                  where (!isSearching || (isSearching && collect.name.Contains(rContext.searchValue))) &&
                                        (rContext.type == 99 || (rContext.type != 99 && collect.type == rContext.type))
                                  orderby sortingString
                                  select new CollectionViewModel()
                                  {
                                      id = collect.id,
                                      name = collect.name,
                                      priority = collect.priority,
                                      status = collect.status,
                                      display_column = collect.display_column,
                                      limit_product = collect.limit_product,
                                      created_by = collect.created_by,
                                      created_date = collect.created_date,
                                      updated_by = collect.updated_by,
                                      updated_date = collect.updated_date,
                                      type = collect.type
                                  };
                responseData.total = collections.Count();

                if (rContext.page != 0 && rContext.itemPerPage != -1)
                {
                    // Paging data
                    //collections = collections.Where(model => _collectionRepository.GetAllQueryable()
                    //                                                              .OrderBy(x => x.id)
                    //                                                              .Select(x => x.id)
                    //                                                              .Skip((rContext.page - 1) * rContext.itemPerPage)
                    //                                                              .Take(rContext.itemPerPage).Contains(model.id)
                    //                               );
                    collections = collections.Skip((rContext.page - 1) * rContext.itemPerPage)
                                             .Take(rContext.itemPerPage);
                }
                

                // Fetching data from database
                responseData.data = collections.ToList();
                var listImages = new List<Models.CollectionMedia>();
                if (rContext.isGetImages)
                {
                    var collectionIds = collections.Select(model => model.id).ToArray();
                    listImages = _collectionMediaRepository.Query(model => collectionIds.Contains(model.collection_id)).ToList();
                }
                responseData.data.ForEach(model =>
                {
                    model.generic_products = GetGenericProducts(model, rContext.isGetImages);
                    model.medias = listImages.Where(img => img.collection_id == model.id)
                                             .Select(img => img.image_link)
                                             .ToList();
                });
                return new ResponseListModel<CollectionViewModel>()
                {
                    successfull = true,
                    dataset = responseData.data,
                    total = responseData.total
                };
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                throw;
            }
        }

        /// <summary>
        /// Get data detail info by id
        /// </summary>
        /// <param name="id">Id of data.</param>
        /// <returns>Response infomation.</returns>
        public ResponseSignleModel<CollectionViewModel> GetInfo(int id)
        {
            try
            {
                var collection = _collectionRepository.Query(model => model.id == id).FirstOrDefault();
                if (null == collection)
                {
                    throw new NotFoundDataException(id.ToString(), typeof(CollectionViewModel));
                }
                // Mapping data
                var mapData = mapper.Map<CollectionViewModel>(collection);
                mapData.generic_products = GetGenericProducts(mapData, true);
                mapData.medias = GetMedias(mapData.id);

                return new ResponseSignleModel<CollectionViewModel>()
                {
                    successfull = true,
                    dataset = mapData
                };
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                throw;
            }
        }

        /// <summary>
        /// Get data detail info by id
        /// </summary>
        /// <param name="id">Id of data.</param>
        /// <returns>Response infomation.</returns>
        public ResponseSignleModel<CollectionViewModel> GetInfoFullProduct(int id)
        {
            try
            {
                var collection = _collectionRepository.Query(model => model.id == id).FirstOrDefault();
                if (null == collection)
                {
                    throw new NotFoundDataException(id.ToString(), typeof(CollectionViewModel));
                }
                // Mapping data
                var mapData = mapper.Map<CollectionViewModel>(collection);
                mapData.generic_products = GetGenericProducts(mapData, true, true);
                mapData.medias = GetMedias(mapData.id);

                return new ResponseSignleModel<CollectionViewModel>()
                {
                    successfull = true,
                    dataset = mapData
                };
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                throw;
            }
            throw new NotImplementedException();
        }

        private List<GenericProductViewModel> GetGenericProducts(CollectionViewModel collectionView, bool isGetImages = false, bool isFullproduct = false)
        {
            try
            {
                var genericProducts = from gProduct in _genericProductRepository.Query(model => model.hidden == false)
                                      select new GenericProductViewModel()
                                      {
                                          id = gProduct.id,
                                          brand_id = gProduct.brand_id,
                                          category_id = gProduct.category_id,
                                          name = gProduct.name,
                                          product_name = gProduct.product_name,
                                          is_free_ship = gProduct.is_free_ship,
                                          link = gProduct.link,
                                          product_price = gProduct.product_price
                                      };
                genericProducts = genericProducts.Where(model => (from collection in _productCollectionRepository.GetAllQueryable()
                                                                  where collection.collection_id == collectionView.id
                                                                  select collection.product_id
                                                                  ).Contains(model.id));
                var respData = new List<GenericProductViewModel>();
                if (!isFullproduct)
                {
                    respData = genericProducts.Take(collectionView.limit_product).ToList();
                }
                {
                    respData = genericProducts.ToList();
                }
                if (isGetImages)
                {
                    var productIds = respData.Select(model => model.id).ToArray();
                    var medias = _genericProductService.GetGenericMedias(productIds);
                    if (medias.Count > 0)
                    {
                        respData.ForEach(model =>
                        {
                            model.medias = medias.Where(img => img.generic_product_id == model.id)
                                                 .Select(img => img.image_link)
                                                 .ToList();
                        });
                    }
                }
                return respData;
            }
            catch(Exception ex)
            {
                Console.WriteLine(ex.Message);
                return new List<GenericProductViewModel>();
            }
        }

        /// <summary>
        /// Get medias for collections.
        /// </summary>
        /// <param name="collectionId"></param>
        /// <returns></returns>
        private List<string> GetMedias(int collectionId)
        {
            try
            {
                var medias = from media in _collectionMediaRepository.GetAllQueryable()
                             where media.collection_id == collectionId
                             select new CollectionMediaViewModel()
                             {
                                 id = media.id,
                                 collection_id = media.collection_id,
                                 image_link = media.image_link
                             };
                return medias.Select(img => img.image_link).ToList();
            }
            catch
            {
                return new List<string>();
            }
        }

        /// <summary>
        /// Update data to database
        /// </summary>
        /// <param name="viewModel">Model for updating.</param>
        /// <returns>Response infomation.</returns>
        public ResponseStatus Update(CollectionViewModel viewModel)
        {
            try
            {
                // Mapping field value from view model to model entity
                var collectionEntity = mapper.Map<Models.Collection>(viewModel);
                // Save data to database
                _collectionRepository.Update(collectionEntity);
                SaveChanges();

                _collectionMediaRepository.Delete(model => model.collection_id == collectionEntity.id);
                SaveChanges();

                foreach (var img in viewModel.medias)
                {
                    var imgEntity = new Models.CollectionMedia()
                    {
                        collection_id = collectionEntity.id,
                        image_link = img
                    };
                    _collectionMediaRepository.Add(imgEntity);
                }
                SaveChanges();

                _productCollectionRepository.Delete(model => model.collection_id == collectionEntity.id);
                SaveChanges();

                foreach (var item in viewModel.generic_products)
                {
                    var entity = new Models.ProductCollection()
                    {
                        collection_id = collectionEntity.id,
                        product_id = item.id
                    };
                    _productCollectionRepository.Add(entity);
                }
                SaveChanges();
                // Create response data
                var resp = new ResponseStatus()
                {
                    successfull = true,
                    message = string.Format("Update data is successfull with id => {0}", collectionEntity.id),
                    dataset = collectionEntity
                };
                return resp;
            }
            catch (Exception ex)
            {
                return new ResponseStatus()
                {
                    successfull = false,
                    message = ex.Message,
                    dataset = null
                };
            }
        }
    }
}

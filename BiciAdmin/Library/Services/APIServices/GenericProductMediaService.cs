﻿using System;
using System.Linq;
using AutoMapper;
using DataCore.Data.Infrastructure;
using DataCore.Data.Repositories;
using Services.Handlers;
using Website.ViewModels;
using Models = DataCore.Models;
using WaoInterfaces = Services.Interfaces;

namespace Services.APIServices
{
    public class GenericProductMediaService : BaseService, WaoInterfaces.IGenericProductMediaService
    {
        // Define singleton instances.
        private readonly IUnitOfWork unitOfWork;
        private readonly IGenericProductMediaRepository _genericProductMediaRepository;
        private readonly IMapper mapper;

        // Constructor
        public GenericProductMediaService(IUnitOfWork unitOfWork,
                                 IGenericProductMediaRepository GenericProductMediaRepository,
                                 IMapper mapper) : base(unitOfWork)
        {
            this.unitOfWork = unitOfWork;
            _genericProductMediaRepository = GenericProductMediaRepository;
            this.mapper = mapper;
        }

        /// <summary>
        /// Insert data to database.
        /// </summary>
        /// <param name="viewModel"></param>
        /// <returns></returns>
        public ResponseStatus Create(GenericProductMediaViewModel viewModel)
        {
            try
            {
                // Mapping field value from view model to model entity
                var genericProductMediaEntity = mapper.Map<Models.GenericProductMedia>(viewModel);
                // Save data to database
                _genericProductMediaRepository.Add(genericProductMediaEntity);
                SaveChanges();
                // Create response data
                var resp = new ResponseStatus()
                {
                    successfull = true,
                    message = string.Format("Create data is successfull with id => {0}", genericProductMediaEntity.id),
                    dataset = genericProductMediaEntity
                };
                return resp;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                throw;
            }
        }

        /// <summary>
        /// Delete data from database.
        /// </summary>
        /// <param name="viewModel"></param>
        /// <returns></returns>
        public ResponseStatus Delete(GenericProductMediaViewModel viewModel)
        {
            try
            {
                // Checking exist data.
                var entity = _genericProductMediaRepository.Query(model => model.id == viewModel.id).FirstOrDefault();
                if (null == entity)
                {
                    throw new NotFoundDataException(viewModel.id.ToString(), typeof(Models.Collection));
                }
                _genericProductMediaRepository.Delete(entity);
                SaveChanges();
                return new ResponseStatus()
                {
                    successfull = true,
                    message = string.Format("Delete data is successfull with id => {0}", entity.id)
                };
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                throw;
            }
        }

        /// <summary>
        /// Delete data by primary key.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ResponseStatus DeleteById(int id)
        {
            try
            {
                _genericProductMediaRepository.Delete(model => model.id == id);
                SaveChanges();
                return new ResponseStatus()
                {
                    successfull = true,
                    message = string.Format("Delete data is successfull with id => {0}", id)
                };
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                throw;
            }
        }

        /// <summary>
        /// Get datatable with pagination info.
        /// </summary>
        /// <param name="rContext"></param>
        /// <returns></returns>
        public ResponseListModel<GenericProductMediaViewModel> GetDatatables(PaggingBase rContext)
        {
            try
            {
                // Variables
                var sortingString = rContext.sortBy + (rContext.reverse ? " descending" : "");
                var isSearching = !string.IsNullOrEmpty(rContext.searchValue);
                if (isSearching)
                {
                    rContext.searchValue = rContext.searchValue.ToLower();
                }
                var responseData = new GenericProductMediaDatatable();

                // Fetching data from database.
                var collections = from model in _genericProductMediaRepository.GetAllQueryable()
                                  orderby sortingString
                                  select new GenericProductMediaViewModel()
                                  {
                                      id = model.id,
                                      generic_product_id = model.generic_product_id,
                                      image_link = model.image_link,

                                  };
                responseData.total = collections.Count();

                // Paging data
                collections = collections.Where(model => _genericProductMediaRepository.GetAllQueryable()
                                                                              .OrderBy(x => x.id)
                                                                              .Select(x => x.id)
                                                                              .Skip((rContext.page - 1) * rContext.itemPerPage)
                                                                              .Take(rContext.itemPerPage).Contains(model.id)
                                               );

                // Fetching data from database
                responseData.data = collections.ToList();
                return new ResponseListModel<GenericProductMediaViewModel>()
                {
                    successfull = true,
                    dataset = responseData.data,
                    total = responseData.total
                };
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                throw;
            }
        }


        /// <summary>
        /// Get detail info by id.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ResponseSignleModel<GenericProductMediaViewModel> GetInfo(int id)
        {
            try
            {
                var collection = _genericProductMediaRepository.Query(model => model.id == id).FirstOrDefault();
                if (null == collection)
                {
                    throw new NotFoundDataException(id.ToString(), typeof(GenericProductMediaViewModel));
                }
                var respData = mapper.Map<GenericProductMediaViewModel>(collection);
                return new ResponseSignleModel<GenericProductMediaViewModel>()
                {
                    successfull = true,
                    dataset = respData
                };
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                throw;
            }
        }

        /// <summary>
        /// Update data.
        /// </summary>
        /// <param name="viewModel"></param>
        /// <returns></returns>
        public ResponseStatus Update(GenericProductMediaViewModel viewModel)
        {
            try
            {
                // Mapping field value from view model to model entity
                var entity = mapper.Map<Models.GenericProductMedia>(viewModel);
                // Save data to database
                _genericProductMediaRepository.Update(entity);
                SaveChanges();
                // Create response data
                var resp = new ResponseStatus()
                {
                    successfull = true,
                    message = string.Format("Update data is successfull with id => {0}", entity.id),
                    dataset = entity
                };
                return resp;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                throw;
            }
        }

    }
}
﻿using System;
using System.Linq;
using AutoMapper;
using DataCore.Data.Infrastructure;
using DataCore.Data.Repositories;
using Services.Handlers;
using Website.ViewModels;
using Models = DataCore.Models;
using WaoInterfaces = Services.Interfaces;

namespace Services.APIServices
{
    public class ProductCollectionService : BaseService, WaoInterfaces.IProductCollectionService
    {
        // Define singleton instances.
        private readonly IUnitOfWork unitOfWork;
        private readonly IProductCollectionRepository _productCollectionRepository;
        private readonly IMapper mapper;

        // Constructor
        public ProductCollectionService(IUnitOfWork unitOfWork,
                                 IProductCollectionRepository ProductCollectionRepository,
                                 IMapper mapper) : base(unitOfWork)
        {
            this.unitOfWork = unitOfWork;
            _productCollectionRepository = ProductCollectionRepository;
            this.mapper = mapper;
        }

        /// <summary>
        /// Insert data to database, and response for client.
        /// </summary>
        /// <param name="viewModel">Model for inserting.</param>
        /// <returns>Response infomation.</returns>
        public ResponseStatus Create(ProductCollectionViewModel viewModel)
        {
            try
            {
                // Mapping field value from view model to model entity
                var productCollectionEntity = mapper.Map<Models.ProductCollection>(viewModel);
                productCollectionEntity.created_date = DateTime.Now;
                productCollectionEntity.updated_date = DateTime.Now;
                // Save data to database
                _productCollectionRepository.Add(productCollectionEntity);
                SaveChanges();
                // Create response data
                var resp = new ResponseStatus()
                {
                    successfull = true,
                    message = string.Format("Create data is successfull with id => {0}", productCollectionEntity.id),
                    dataset = productCollectionEntity
                };
                return resp;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                throw;
            }
        }

        /// <summary>
        /// Delete data from database.
        /// </summary>
        /// <param name="viewModel">Model for deleting.</param>
        /// <returns>Response infomation</returns>
        public ResponseStatus Delete(ProductCollectionViewModel viewModel)
        {
            try
            {
                var entity = _productCollectionRepository.Query(model => model.id == viewModel.id).FirstOrDefault();
                if (null == entity)
                {
                    throw new NotFoundDataException(viewModel.id.ToString(), typeof(Models.Collection));
                }
                _productCollectionRepository.Delete(entity);
                SaveChanges();
                return new ResponseStatus()
                {
                    successfull = true,
                    message = string.Format("Delete data is successfull with id => {0}", entity.id)
                };
            }
            catch (Exception ex)
            {
                return new ResponseStatus()
                {
                    successfull = false,
                    message = ex.Message
                };
            }
        }

        /// <summary>
        /// Delete database by id.
        /// </summary>
        /// <param name="id">Id of model.</param>
        /// <returns>Response infomation.</returns>
        public ResponseStatus DeleteById(int id)
        {
            try
            {
                _productCollectionRepository.Delete(model => model.id == id);
                SaveChanges();
                return new ResponseStatus()
                {
                    successfull = true,
                    message = string.Format("Delete data is successfull with id => {0}", id)
                };
            }
            catch (Exception ex)
            {
                return new ResponseStatus()
                {
                    successfull = false,
                    message = ex.Message
                };
            }
        }

        /// <summary>
        /// Get list of data from database with condition
        /// </summary>
        /// <param name="page">Current page</param>
        /// <param name="itemPerpage">Item per page</param>
        /// <param name="sortBy">Field name for sorting.</param>
        /// <param name="reverse">Sorting with asc or desc</param>
        /// <param name="searchValue">Value for searching.</param>
        /// <returns></returns>
        public ResponseStatus GetDatatables(int page, int itemPerpage, string sortBy, bool reverse, string searchValue)
        {
            try
            {
                // Variables
                var sortingString = sortBy + (reverse ? " descending" : "");
                var isSearching = !string.IsNullOrEmpty(searchValue);
                if (isSearching)
                {
                    searchValue = searchValue.ToLower();
                }
                var responseData = new ProductCollectionDatatable();

                // Fetching data from database.
                var collections = from model in _productCollectionRepository.GetAllQueryable()
                                  orderby sortingString
                                  select new ProductCollectionViewModel()
                                  {
                                      id = model.id,
                                      collection_id = model.collection_id,
                                      product_id = model.product_id,

                                  };
                responseData.total = collections.Count();

                // Paging data
                collections = collections.Where(model => _productCollectionRepository.GetAllQueryable()
                                                                              .OrderBy(x => x.id)
                                                                              .Select(x => x.id)
                                                                              .Skip((page - 1) * itemPerpage)
                                                                              .Take(itemPerpage).Contains(model.id)
                                               );

                // Fetching data from database
                responseData.data = collections.ToList();
                return new ResponseStatus()
                {
                    successfull = true,
                    dataset = responseData
                };
            }
            catch (Exception ex)
            {
                return new ResponseStatus()
                {
                    successfull = false,
                    message = ex.Message,
                    dataset = null
                };
            }
        }

        public ResponseListModel<ProductCollectionViewModel> GetDatatables(PaggingBase rContext)
        {
            throw new NotImplementedException();
        }

        public ResponseSignleModel<ProductCollectionViewModel> GetInfo(int id)
        {
            throw new NotImplementedException();
        }

        ///// <summary>
        ///// Get data detail info by id
        ///// </summary>
        ///// <param name="id">Id of data.</param>
        ///// <returns>Response infomation.</returns>
        //public ResponseStatus GetInfo(int id)
        //{
        //    try
        //    {
        //        var collection = _productCollectionRepository.Query(model => model.id == id).FirstOrDefault();
        //        if (null == collection)
        //        {
        //            throw new NotFoundDataException(id.ToString(), typeof(ProductCollectionViewModel));
        //        }
        //        return new ResponseStatus()
        //        {
        //            successfull = true,
        //            dataset = collection
        //        };
        //    }
        //    catch (Exception ex)
        //    {
        //        return new ResponseStatus()
        //        {
        //            successfull = false,
        //            message = ex.Message,
        //            dataset = null
        //        };
        //    }
        //    throw new NotImplementedException();
        //}

        /// <summary>
        /// Update data to database
        /// </summary>
        /// <param name="viewModel">Model for updating.</param>
        /// <returns>Response infomation.</returns>
        public ResponseStatus Update(ProductCollectionViewModel viewModel)
        {
            try
            {
                // Mapping field value from view model to model entity
                var productCollectionEntity = mapper.Map<Models.ProductCollection>(viewModel);
                // Save data to database
                _productCollectionRepository.Update(productCollectionEntity);
                SaveChanges();
                // Create response data
                var resp = new ResponseStatus()
                {
                    successfull = true,
                    message = string.Format("Update data is successfull with id => {0}", productCollectionEntity.id),
                    dataset = productCollectionEntity
                };
                return resp;
            }
            catch (Exception ex)
            {
                return new ResponseStatus()
                {
                    successfull = false,
                    message = ex.Message,
                    dataset = null
                };
            }
        }
    }
}

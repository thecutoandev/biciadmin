using System;
using System.Linq;
using AutoMapper;
using DataCore.Data.Infrastructure;
using DataCore.Data.Repositories;
using Services.Handlers;
using Website.ViewModels;
using Models = DataCore.Models;
using WaoInterfaces = Services.Interfaces;

namespace Services.APIServices
{
    public class BlogService : BaseService, WaoInterfaces.IBlogService
    {
        // Define singleton instances.
        private readonly IUnitOfWork unitOfWork;
        private readonly IBlogRepository _blogRepository;
        private readonly ICollectionBlogRepository _collectionBlogRepository;
        private readonly IBlogCollectionRepository _blogCollectionRepository;
        private readonly IMapper mapper;

        // Constructor
        public BlogService(IUnitOfWork unitOfWork,
                                 IBlogRepository BlogRepository,
                                 ICollectionBlogRepository collectionBlogRepository,
                                 IBlogCollectionRepository blogCollectionRepository,
                                 IMapper mapper) : base(unitOfWork)
        {
            this.unitOfWork = unitOfWork;
            _blogRepository = BlogRepository;
            this._collectionBlogRepository = collectionBlogRepository;
            this._blogCollectionRepository = blogCollectionRepository;
            this.mapper = mapper;
        }

        /// <summary>
        /// Insert data to database, and response for client.
        /// </summary>
        /// <param name="viewModel">Model for inserting.</param>
        /// <returns>Response infomation.</returns>
        public ResponseStatus Create(BlogViewModel viewModel)
        {
            try
            {
                // Mapping field value from view model to model entity
                var blogEntity = mapper.Map<Models.Blog>(viewModel);
                blogEntity.created_date = DateTime.Now;
                blogEntity.updated_date = DateTime.Now;
                // Save data to database
                _blogRepository.Add(blogEntity);
                SaveChanges();

                foreach (var cat in viewModel.collections)
                {
                    _collectionBlogRepository.Add(new Models.CollectionBlog()
                    {
                        blog_id = blogEntity.id,
                        collection_id = cat.id
                    });
                }
                SaveChanges();
                // Create response data
                var resp = new ResponseStatus()
                {
                    successfull = true,
                    message = string.Format("Create data is successfull with id => {0}", blogEntity.id),
                    dataset = blogEntity
                };
                return resp;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                throw;
            }
        }

        /// <summary>
        /// Delete data from database.
        /// </summary>
        /// <param name="viewModel">Model for deleting.</param>
        /// <returns>Response infomation</returns>
        public ResponseStatus Delete(BlogViewModel viewModel)
        {
            try
            {
                var entity = _blogRepository.Query(model => model.id == viewModel.id).FirstOrDefault();
                if (null == entity)
                {
                    throw new NotFoundDataException(viewModel.id.ToString(), typeof(Models.Collection));
                }
                _blogRepository.Delete(entity);
                SaveChanges();
                return new ResponseStatus()
                {
                    successfull = true,
                    message = string.Format("Delete data is successfull with id => {0}", entity.id)
                };
            }
            catch (Exception ex)
            {
                return new ResponseStatus()
                {
                    successfull = false,
                    message = ex.Message
                };
            }
        }

        /// <summary>
        /// Delete database by id.
        /// </summary>
        /// <param name="id">Id of model.</param>
        /// <returns>Response infomation.</returns>
        public ResponseStatus DeleteById(int id)
        {
            try
            {
                _blogRepository.Delete(model => model.id == id);
                SaveChanges();
                return new ResponseStatus()
                {
                    successfull = true,
                    message = string.Format("Delete data is successfull with id => {0}", id)
                };
            }
            catch (Exception ex)
            {
                return new ResponseStatus()
                {
                    successfull = false,
                    message = ex.Message
                };
            }
        }

        /// <summary>
        /// Update data to database
        /// </summary>
        /// <param name="viewModel">Model for updating.</param>
        /// <returns>Response infomation.</returns>
        public ResponseStatus Update(BlogViewModel viewModel)
        {
            try
            {
                // Mapping field value from view model to model entity
                var blogEntity = mapper.Map<Models.Blog>(viewModel);
                // Save data to database
                _blogRepository.Update(blogEntity);
                SaveChanges();
                _collectionBlogRepository.Delete(model => model.blog_id == blogEntity.id);
                foreach (var cat in viewModel.collections)
                {
                    _collectionBlogRepository.Add(new Models.CollectionBlog()
                    {
                        blog_id = blogEntity.id,
                        collection_id = cat.id
                    });
                }
                SaveChanges();
                // Create response data
                var resp = new ResponseStatus()
                {
                    successfull = true,
                    message = string.Format("Update data is successfull with id => {0}", blogEntity.id),
                    dataset = blogEntity
                };
                return resp;
            }
            catch (Exception ex)
            {
                return new ResponseStatus()
                {
                    successfull = false,
                    message = ex.Message,
                    dataset = null
                };
            }
        }

        /// <summary>
        /// Return 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ResponseSignleModel<BlogViewModel> GetInfo(int id)
        {
            try
            {
                var collection = _blogRepository.Query(model => model.id == id).FirstOrDefault();
                var responseData = mapper.Map<BlogViewModel>(collection);
                if (null == collection)
                {
                    throw new NotFoundDataException(id.ToString(), typeof(BlogViewModel));
                }
                responseData.collections = (from cat in _collectionBlogRepository.Query(model => model.blog_id == responseData.id)
                                            join c in _blogCollectionRepository.GetAllQueryable()
                                            on cat.collection_id equals c.id into bc
                                            from blogCat in bc.DefaultIfEmpty()
                                            select new BlogCollectionViewModel()
                                            {
                                                id = blogCat.id,
                                                image_link = blogCat.image_link,
                                                name = blogCat.name
                                            }).ToList();

                return new ResponseSignleModel<BlogViewModel>()
                {
                    successfull = true,
                    dataset = responseData
                };
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                throw;
            }
        }

        public ResponseListModel<BlogViewModel> GetDatatables(BlogRequest rContext)
        {
            try
            {
                // Variables
                var sortingString = rContext.sortBy + (rContext.reverse ? " descending" : "");
                var isSearching = !string.IsNullOrEmpty(rContext.searchValue);
                if (isSearching)
                {
                    rContext.searchValue = rContext.searchValue.ToLower();
                }
                var responseData = new BlogDatatable();

                // Fetching data from database.
                var collections = from model in _blogRepository.GetAllQueryable()
                                  where (!isSearching || (isSearching && model.name.Contains(rContext.searchValue)))
                                  orderby sortingString
                                  select new BlogViewModel()
                                  {
                                      id = model.id,
                                      name = model.name,
                                      content = model.content,
                                      link = model.link,

                                  };
                responseData.total = collections.Count();

                // Paging data
                collections = collections.Where(model => _blogRepository.GetAllQueryable()
                                                                        .OrderBy(x => x.id)
                                                                        .Select(x => x.id)
                                                                        .Skip((rContext.page - 1) * rContext.itemPerPage)
                                                                        .Take(rContext.itemPerPage).Contains(model.id)
                                               );

                // Fetching data from database
                responseData.data = collections.ToList();
                return new ResponseListModel<BlogViewModel>()
                {
                    successfull = true,
                    dataset = responseData.data,
                    total = responseData.total
                };
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                throw;
            }
        }
    }
}

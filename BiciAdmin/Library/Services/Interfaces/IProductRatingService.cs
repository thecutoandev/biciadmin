using Website.ViewModels;

namespace Services.Interfaces
{
    public interface IProductRatingService : IBaseService<ProductRatingViewModel, ProductRatingRequest>
    {
    }
}
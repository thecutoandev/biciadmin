using Website.ViewModels;

namespace Services.Interfaces
{
    public interface IAreaService : IBaseService<AreaViewModel, PaggingBase>
    {
    }
}
﻿using System;
using Models = DataCore.Models;
using ViewModels = Website.ViewModels;

namespace Services.Interfaces
{
    public interface IBaseService<TModel, TRequest>
        where TModel : class
        where TRequest : class
    {
        //ViewModels.BaseDatatable<T> GetDatatables();
        ViewModels.ResponseSignleModel<TModel> GetInfo(int id);
        ViewModels.ResponseStatus Create(TModel viewModel);
        ViewModels.ResponseStatus Update(TModel viewModel);
        ViewModels.ResponseStatus Delete(TModel viewModel);
        ViewModels.ResponseStatus DeleteById(int id);
        ViewModels.ResponseListModel<TModel> GetDatatables(TRequest rContext);
    }
}

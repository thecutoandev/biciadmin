using Website.ViewModels;

namespace Services.Interfaces
{
    public interface IPaymentMethodService : IBaseService<PaymentMethodViewModel, PaggingBase>
    {
    }
}
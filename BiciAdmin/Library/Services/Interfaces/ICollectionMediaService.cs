using Website.ViewModels;

namespace Services.Interfaces
{
    public interface ICollectionMediaService : IBaseService<CollectionMediaViewModel, PaggingBase>
    {
    }
}
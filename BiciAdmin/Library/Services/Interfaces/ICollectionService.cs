﻿using Website.ViewModels;

namespace Services.Interfaces
{
    public interface ICollectionService : IBaseService<CollectionViewModel, CollectionRequest>
    {
       ResponseSignleModel<CollectionViewModel> GetInfoFullProduct(int id);
    }
}

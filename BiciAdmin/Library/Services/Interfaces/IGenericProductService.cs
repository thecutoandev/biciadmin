using System.Collections.Generic;
using Website.ViewModels;

namespace Services.Interfaces
{
    public interface IGenericProductService : IBaseService<GenericProductViewModel, GenericProductRequest>
    {
        List<GenericProductMediaViewModel> GetGenericMedias(int[] productIds);
        List<ProductMediaViewModel> GetProductMedias(int[] productIds);
        //ResponseStatus GetDataWithFilters(int page, int itemPerpage, string sortBy, bool reverse, string name, string categories, string brands);
    }
}
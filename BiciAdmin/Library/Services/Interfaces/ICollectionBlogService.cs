using Website.ViewModels;

namespace Services.Interfaces
{
    public interface ICollectionBlogService : IBaseService<CollectionBlogViewModel, PaggingBase>
    {
    }
}
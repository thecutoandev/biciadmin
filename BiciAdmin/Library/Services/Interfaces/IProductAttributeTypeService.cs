using Website.ViewModels;

namespace Services.Interfaces
{
    public interface IProductAttributeTypeService : IBaseService<ProductAttributeTypeViewModel, PaggingBase>
    {
    }
}
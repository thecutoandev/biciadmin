﻿using System;
using Website.ViewModels;

namespace Services.Interfaces
{
    public interface IBranchPickService
    {
        ResponseListModel<BranchPickViewModel> GetListBranches();
        ResponseStatus UpdatePriority(BranchPickUpdate viewModel);
    }
}

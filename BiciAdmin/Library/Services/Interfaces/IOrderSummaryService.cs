using Website.ViewModels;

namespace Services.Interfaces
{
    public interface IOrderSummaryService : IBaseService<OrderSummaryViewModel, PaggingBase>
    {
    }
}
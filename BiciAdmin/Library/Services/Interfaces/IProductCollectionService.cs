using Website.ViewModels;

namespace Services.Interfaces
{
    public interface IProductCollectionService : IBaseService<ProductCollectionViewModel, PaggingBase>
    {
    }
}
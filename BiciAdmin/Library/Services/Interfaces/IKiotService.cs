﻿using System;
using System.Collections.Generic;
using Website.ViewModels;

namespace Services.Interfaces
{
    public interface IKiotService
    {
        List<KiotStocking> GetStockings(string[] kiotCodes);
    }
}

﻿using Website.ViewModels;

namespace Services.Interfaces
{
    public interface IBannerService : IBaseService<BannerViewModel, PaggingBase>
    {
        ResponseListModel<BannerViewModel> GetAll(bool isGetall); 
    }
}

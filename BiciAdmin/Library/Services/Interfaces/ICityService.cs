using Website.ViewModels;

namespace Services.Interfaces
{
    public interface ICityService : IBaseService<CityViewModel, PaggingBase>
    {
    }
}
﻿using System;
using Website.ViewModels;

namespace Services.Interfaces
{
    public interface IProductTypeService
    {
        ResponseListModel<ProductTypeViewModel> SearchProductType(string searchValue);
    }
}

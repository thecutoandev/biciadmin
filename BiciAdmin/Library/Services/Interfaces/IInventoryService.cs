using Website.ViewModels;

namespace Services.Interfaces
{
    public interface IInventoryService : IBaseService<InventoryViewModel, PaggingBase>
    {
    }
}
using Website.ViewModels;

namespace Services.Interfaces
{
    public interface IAreaDistrictService : IBaseService<AreaDistrictViewModel, PaggingBase>
    {

    }
}
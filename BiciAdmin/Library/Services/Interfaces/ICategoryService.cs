using Website.ViewModels;

namespace Services.Interfaces
{
    public interface ICategoryService : IBaseService<CategoryViewModel, CategoriesRequest>
    {
        ResponseListModel<CategoryViewModel> GetChildsList(CategoriesRequest requestContext);
        ResponseStatus CreateCategoryCollection(CategoryCollectionViewModel viewModel);
        ResponseSignleModel<CategoryCollectionViewModel> GetInfoByAdmin(int id);
    }
}
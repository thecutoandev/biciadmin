using Website.ViewModels;

namespace Services.Interfaces
{
    public interface IProductService : IBaseService<ProductViewModel, ProductRequest>
    {
        //ResponseStatus GetDataByGeneric(int page, int itemPerpage, string sortBy, bool reverse, int genericId);
        //ResponseStatus GetInfoByKiotCode(string kiotCode);
    }
}
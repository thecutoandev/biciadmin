using Website.ViewModels;

namespace Services.Interfaces
{
    public interface IBrandService : IBaseService<BrandViewModel, PaggingBase>
    {
    }
}
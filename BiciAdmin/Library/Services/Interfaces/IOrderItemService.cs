using Website.ViewModels;

namespace Services.Interfaces
{
    public interface IOrderItemService : IBaseService<OrderItemViewModel, PaggingBase>
    {
    }
}
using Website.ViewModels;

namespace Services.Interfaces
{
    public interface IUserAddressService : IBaseService<UserAddressViewModel, PaggingBase>
    {
        ResponseListModel<UserAddressViewModel> GetDatatablesWithUser(string currentUser);
    }
}
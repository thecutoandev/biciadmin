using Website.ViewModels;

namespace Services.Interfaces
{
    public interface IOrderService : IBaseService<OrderViewModel, PaggingBase>
    {
        ResponseListModel<OrderViewModel> GetHistories(string username, PagingOrderHistories pagingOrder);
        ResponseListModel<OrderAdminViewModel> GetAllOrders(PagingOrder pagingOrder);
        ResponseSignleModel<OrderAdminViewModel> GetInfoByAdmin(int id);
    }
}
using Website.ViewModels;

namespace Services.Interfaces
{
    public interface IProductMediaService : IBaseService<ProductMediaViewModel, PaggingBase>
    {
    }
}
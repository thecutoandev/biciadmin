using Website.ViewModels;

namespace Services.Interfaces
{
    public interface IBlogCollectionService : IBaseService<BlogCollectionViewModel, PaggingBase>
    {
    }
}
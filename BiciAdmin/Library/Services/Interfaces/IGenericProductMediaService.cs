using Website.ViewModels;

namespace Services.Interfaces
{
    public interface IGenericProductMediaService : IBaseService<GenericProductMediaViewModel, PaggingBase>
    {
    }
}
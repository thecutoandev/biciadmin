using Website.ViewModels;
using ViewModels = Website.ViewModels;

namespace Services.Interfaces
{
    public interface IDistrictService : IBaseService<DistrictViewModel, PaggingBase>
    {
        ViewModels.ResponseListModel<DistrictViewModel> GetDataByCity(int cityId);
        ViewModels.ResponseStatus GetShippingFee(int districtId);
    }
}
﻿using System;
using System.Collections.Generic;
using Website.ViewModels;

namespace Services.Interfaces
{
    public interface IOrderPickingService
    {
        ResponseSignleModel<List<string>> AutoPick(string userName);
        ResponseListModel<OrderAdminPrepareViewModel> GetProductForPrepare(string orderIds, string userName);
        ResponseStatus UndoOrder(int id);
        ResponseSignleModel<List<string>> AutoMultiPick();
        ResponseSignleModel<List<string>> CreateKiotOrder(KiotOrderViewModel kiotOrderViewModel);
        ResponseStatus ConfirmOrder(OrderConfirmation orderConfirmation);
    }
}

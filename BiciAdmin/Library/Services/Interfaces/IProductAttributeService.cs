using Website.ViewModels;

namespace Services.Interfaces
{
    public interface IProductAttributeService : IBaseService<ProductAttributeViewModel, PaggingBase>
    {
    }
}
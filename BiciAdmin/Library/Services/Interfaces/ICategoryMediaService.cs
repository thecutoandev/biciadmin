using Website.ViewModels;

namespace Services.Interfaces
{
    public interface ICategoryMediaService : IBaseService<CategoryMediaViewModel, PaggingBase>
    {
    }
}
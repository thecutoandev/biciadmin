﻿using System;
using System.Threading.Tasks;
using DataCore.Models;
using Website.ViewModels;

namespace Services.Interfaces
{
    public interface IUserService
    {
        Task<AdminAuthModel> Authenticate(string userName, string passWord);
        Task<AuthModel> FaceBookAuthenticate(string accessToken);
        Task<AuthModel> PhoneAuthenticate(string accessToken);
        Task<AuthModel> FireBaseAuthenticate(string idToken);
        Task<User> Register(UserViewModel userViewModel);
        ResponseSignleModel<UserInfoViewModel> GetUser(string userName);
        Task<User> Update(UserViewModel userViewModel);
        Task<User> UpdateAvatar(UserViewModel userViewModel);
        ResponseListModel<UserViewModel> GetDatatables(PaggingBase rContext);
        Task<User> ResetPassword(string userName);
        bool ActiveUser(string userName);
        bool DeactiveUser(string userName);
        // TODO: Delete before release.
#if DEBUG
        bool Delete(string phoneNumber);
#endif
    }
}

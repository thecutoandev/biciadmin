using Website.ViewModels;

namespace Services.Interfaces
{
    public interface IBlogService : IBaseService<BlogViewModel, BlogRequest>
    {
    }
}
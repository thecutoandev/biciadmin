﻿using AutoMapper;
using Models = DataCore.Models;
using ViewModels = Website.ViewModels;

namespace Services.Mappers
{
    public class ViewModelToDomainProfile : Profile
    {
        public override string ProfileName => "ViewModelToDomainProfile";
        public ViewModelToDomainProfile()
        {
            CreateMap<ViewModels.CollectionViewModel, Models.Collection>();
            CreateMap<Models.UserViewModel, Models.User>();
            CreateMap<ViewModels.BannerViewModel, Models.Banner>();
            CreateMap<ViewModels.UserAddressViewModel, Models.UserAddress>();
            CreateMap<ViewModels.CityViewModel, Models.City>();
            CreateMap<ViewModels.CollectionMediaViewModel, Models.CollectionMedia>();
            CreateMap<ViewModels.CategoryViewModel, Models.Category>();
            CreateMap<ViewModels.CategoryMediaViewModel, Models.CategoryMedia>();
            CreateMap<ViewModels.BrandViewModel, Models.Brand>();
            CreateMap<ViewModels.GenericProductViewModel, Models.GenericProduct>();
            CreateMap<ViewModels.GenericProductMediaViewModel, Models.GenericProductMedia>();
            CreateMap<ViewModels.ProductCollectionViewModel, Models.ProductCollection>();
            CreateMap<ViewModels.ProductViewModel, Models.Product>();
            CreateMap<ViewModels.ProductMediaViewModel, Models.ProductMedia>();
            CreateMap<ViewModels.ProductAttributeTypeViewModel, Models.ProductAttributeType>();
            CreateMap<ViewModels.ProductAttributeViewModel, Models.ProductAttribute>();
            CreateMap<ViewModels.DistrictViewModel, Models.District>();
            CreateMap<ViewModels.AreaViewModel, Models.Area>();
            CreateMap<ViewModels.AreaDistrictViewModel, Models.AreaDistrict>();
            CreateMap<ViewModels.PaymentMethodViewModel, Models.PaymentMethod>();
            CreateMap<ViewModels.OrderViewModel, Models.Order>();
            CreateMap<ViewModels.OrderItemViewModel, Models.OrderItem>();
            CreateMap<ViewModels.OrderSummaryViewModel, Models.OrderSummary>();
            CreateMap<ViewModels.ProductRatingViewModel, Models.ProductRating>();
            CreateMap<ViewModels.BlogViewModel, Models.Blog>();
            CreateMap<ViewModels.BlogCollectionViewModel, Models.BlogCollection>();
            CreateMap<ViewModels.CollectionBlogViewModel, Models.CollectionBlog>();
            CreateMap<ViewModels.InventoryViewModel, Models.Inventory>();
            CreateMap<ViewModels.BranchPickViewModel, Models.BranchPickPriority>();
            CreateMap<ViewModels.CategoryCollectionViewModel, Models.Category>();
        }
    }
}

﻿using AutoMapper;

namespace Services.Mappers
{
    public class AutoMapperConfiguration
    {
        public static MapperConfiguration AutoMapperConfigInstance()
        {
            return new MapperConfiguration(mc =>
            {
                mc.AddProfile(new DomainToViewModelProfile());
                mc.AddProfile(new ViewModelToDomainProfile());
            });
        }
    }
}

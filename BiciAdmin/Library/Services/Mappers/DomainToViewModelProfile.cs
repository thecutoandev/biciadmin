﻿using AutoMapper;
using Models = DataCore.Models;
using ViewModels = Website.ViewModels;

namespace Services.Mappers
{
    public class DomainToViewModelProfile : Profile
    {
        public override string ProfileName => "DomainToViewModelProfile";

        public DomainToViewModelProfile()
        {
            CreateMap<Models.Collection, ViewModels.CollectionViewModel>();
            CreateMap<Models.Blog, ViewModels.BlogViewModel>();
            CreateMap<Models.User, Models.UserViewModel>();
            CreateMap<Models.Banner, ViewModels.BannerViewModel>();
            CreateMap<Models.UserAddress, ViewModels.UserAddressViewModel>();
            CreateMap<Models.City, ViewModels.CityViewModel>();
            CreateMap<Models.CollectionMedia, ViewModels.CollectionMediaViewModel>();
            CreateMap<Models.Category, ViewModels.CategoryViewModel>();
            CreateMap<Models.CategoryMedia, ViewModels.CategoryMediaViewModel>();
            CreateMap<Models.Brand, ViewModels.BrandViewModel>();
            CreateMap<Models.GenericProduct, ViewModels.GenericProductViewModel>();
            CreateMap<Models.GenericProductMedia, ViewModels.GenericProductMediaViewModel>();
            CreateMap<Models.ProductCollection, ViewModels.ProductCollectionViewModel>();
            CreateMap<Models.Product, ViewModels.ProductViewModel>();
            CreateMap<Models.ProductMedia, ViewModels.ProductMediaViewModel>();
            CreateMap<Models.ProductAttributeType, ViewModels.ProductAttributeTypeViewModel>();
            CreateMap<Models.ProductAttribute, ViewModels.ProductAttributeViewModel>();
            CreateMap<Models.District, ViewModels.DistrictViewModel>();
            CreateMap<Models.Area, ViewModels.AreaViewModel>();
            CreateMap<Models.AreaDistrict, ViewModels.AreaDistrictViewModel>();
            CreateMap<Models.PaymentMethod, ViewModels.PaymentMethodViewModel>();
            CreateMap<Models.Order, ViewModels.OrderViewModel>();
            CreateMap<Models.OrderItem, ViewModels.OrderItemViewModel>();
            CreateMap<Models.OrderSummary, ViewModels.OrderSummaryViewModel>();
            CreateMap<Models.ProductRating, ViewModels.ProductRatingViewModel>();
            CreateMap<Models.Blog, ViewModels.BlogViewModel>();
            CreateMap<Models.BlogCollection, ViewModels.BlogCollectionViewModel>();
            CreateMap<Models.CollectionBlog, ViewModels.CollectionBlogViewModel>();
            CreateMap<Models.Inventory, ViewModels.InventoryViewModel>();
            CreateMap<Models.BranchPickPriority, ViewModels.BranchPickViewModel>();
            CreateMap<Models.Category, ViewModels.CategoryCollectionViewModel>();
        }
    }
}

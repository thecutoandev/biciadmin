﻿using System;
namespace Services.Handlers
{
    public class NotFoundDataException : Exception
    {
        public NotFoundDataException(string idData, Type type = null)
            :base(string.Format("Not found data ({0}) with id => {1}", type == null ? string.Empty : type.FullName, idData))
        {
        }
    }
}

﻿using System;
namespace KiotService.Models.Branches
{
    public class Branch
    {
        public int id { set; get; } // Id chi nhánh        public string branchName { set; get; }        public string branchCode { set; get; }        public string contactNumber { set; get; }
        public int retailerId { set; get; }
        public string email { set; get; }
    }
}

﻿using System;
using System.Collections.Generic;

namespace KiotService.Models.Branches
{
    public class Branches
    {
        public List<int> removedIds { get; set; }
        public int total { set; get; }
        public int pageSize { set; get; }
        public List<Branch> data { set; get; }
        public DateTime timestamp { set; get; }
    }
}

﻿using System;
namespace KiotService.Models.Products
{
    // Ton kho theo chi nhanh
    public class Inventory
    {
        public long productId { set; get; }        public string productCode { get; set; }        public double onHand { get; set; }
        public double reserved { get; set; }
        public string productName { get; set; }
        public long branchId { get; set; }
        public string branchName { get; set; }
    }
}
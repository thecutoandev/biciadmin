﻿using System;
using System.Collections.Generic;

namespace KiotService.Models.Products
{
    public class KProduct
    {
        public long id { set; get; }
        public string code { set; get; }
        public bool allowsSale { set; get; }
        public int categoryId { set; get; }        public string categoryName { set; get; }
        public string fullName { set; get; }
        public List<Inventory> inventories { get; set; }
    }
}

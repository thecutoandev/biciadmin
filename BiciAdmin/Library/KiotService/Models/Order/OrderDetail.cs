﻿using System;
namespace KiotService.Models.Order
{
    public class OrderDetail
    {
        public long productId;        public string productCode;
        public string productName;
        public double quantity;
        public decimal price;
        public decimal discount;
        public double discountRatio;
    }
}

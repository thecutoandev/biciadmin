﻿using System;
namespace KiotService.Models.Order
{
    public class Customer
    {
        public long id;
        public string code;
        public string name;
        public Boolean gender;
        public DateTime birthDate;
        public string contactNumber;
        public string address;
        public string email;
        public string comment;
    }
}

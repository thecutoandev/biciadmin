﻿using System;
using System.Collections.Generic;

namespace KiotService.Models.Order
{
    public class KOrder
    {
        public long id { get; set; }
        public DateTime purchaseDate { get; set; }
        public long branchId { get; set; }
        public long cashierId { get; set; }
        public long soldById { get; set; }
        public decimal discount { get; set; }
        public string description { get; set; }
        public string method { get; set; }
        public decimal totalPayment { get; set; }
        public bool makeInvoice { get; set; }
        public List<OrderDetail> orderDetails { set; get; }
        public Customer customer { get; set; }
        public long haravan_id { get; set; }

        public KOrder()
        {
            //cashierId = 34400;
            //soldById = 34400;
            discount = 0;
            description = "Đơn hàng tạo tự động khi soạn hàng";
            method = "Khac";
            makeInvoice = false;
        }
    }
}

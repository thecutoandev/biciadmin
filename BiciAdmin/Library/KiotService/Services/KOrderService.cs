﻿using System;
using System.Net.Http;
using DataCore.Models;
using KiotService.Interfaces;
using KiotService.Models.Order;
using Microsoft.Extensions.Options;
using Website.ViewModels;
using Newtonsoft.Json;
using System.Text;
using System.Net.Http.Headers;
using System.Threading.Tasks;

namespace KiotService.Services
{
    public class KOrderService : BaseService, IOrderService
    {
        private readonly string mainUrl;
        public KOrderService(IOptions<AppSettings> appSettings)
            : base(appSettings)
        {
            this.mainUrl = $"{this.appSettings.KiotVietSettings.PublicAPIEndPoint}/orders";
        }


        public ResponseStatus CreateOrder(KOrder order, bool cancelFromHaravan = false)
        {
            try
            {
                order.cashierId = this.appSettings.KiotVietSettings.CashierId;
                order.soldById = this.appSettings.KiotVietSettings.CashierId;
                var contentData = new StringContent(JsonConvert.SerializeObject(order), Encoding.UTF8, "application/json");
                HttpResponseMessage response = this.http.PostAsync(this.mainUrl, contentData).Result;
                var data = response.Content.ReadAsAsync<KOrder>().Result;
                if (response.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    if (cancelFromHaravan)
                    {
                        Task.Run(() => CancelOrderFromHaravan(order.haravan_id));
                    }
                    return new ResponseStatus()
                    {
                        dataset = data,
                        successfull = true
                    };
                }
                return new ResponseStatus()
                {
                    dataset = null,
                    successfull = false
                };
            }
            catch (Exception ex)
            {
                return new ResponseStatus()
                {
                    dataset = null,
                    message = ex.Message,
                    successfull = false
                };
            }
        }


        // Cancel order from 
        private bool CancelOrderFromHaravan(long orderId)
        {
            try
            {
                var http = new HttpClient();
                var urlService = this.appSettings.HaravanSettings.UrlBase + "admin/orders/{0}/cancel.json";
                urlService = string.Format(urlService, orderId);
                this.http.BaseAddress = new Uri(urlService);
                this.http.DefaultRequestHeaders.Authorization =
                    new AuthenticationHeaderValue("Basic", appSettings.HaravanSettings.TokenPass);
                HttpResponseMessage response = this.http.PostAsync(this.mainUrl, null).Result;
                if (response.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    return true;
                }
                return false;
            }
            catch
            {
                return false;
            }
        }
    }
}

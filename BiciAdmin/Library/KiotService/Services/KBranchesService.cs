﻿using System;
using System.Net.Http;
using DataCore.Models;
using KiotService.Interfaces;
using KiotService.Models.Branches;
using Microsoft.Extensions.Options;
using Website.ViewModels;

namespace KiotService.Services
{
    public class KBranchesService : BaseService, IKBranchesService
    {
        private readonly string mainUrl;
        public KBranchesService(IOptions<AppSettings> appSettings)
            : base(appSettings)
        {
            this.mainUrl = $"{this.appSettings.KiotVietSettings.PublicAPIEndPoint}/branches";
        }

        /// <summary>
        /// Get all branches
        /// </summary>
        /// <returns></returns>
        public ResponseListModel<Branch> GetAllBranches()
        {
            try
            {
                var despUrl = $"{this.mainUrl}?pageSize=100";
                HttpResponseMessage response = this.http.GetAsync(despUrl).Result;
                var data = response.Content.ReadAsAsync<Branches>().Result;
                return new ResponseListModel<Branch>()
                {
                    dataset = data.data,
                    successfull = true
                };
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                throw;
            }
        }
    }
}

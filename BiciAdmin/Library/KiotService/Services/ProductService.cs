﻿using System;
using System.Net.Http;
using DataCore.Models;
using KiotService.Interfaces;
using KiotService.Models.Products;
using Microsoft.Extensions.Options;
using Website.ViewModels;

namespace KiotService.Services
{
    public class KProductService : BaseService, IKProductService
    {
        private readonly string mainUrl;
        public KProductService(IOptions<AppSettings> appSettings)
            :base(appSettings)
        {
            this.mainUrl = $"{this.appSettings.KiotVietSettings.PublicAPIEndPoint}/products";
        }

        /// <summary>
        /// Get kiot product infomation.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ResponseSignleModel<KiotStocking> GetDetail(long id)
        {
            try
            {
                var stocking = new KiotStocking();
                var despUrl = $"{this.mainUrl}/{id}";
                stocking = GetStocking(despUrl);
                return new ResponseSignleModel<KiotStocking>()
                {
                    dataset = stocking,
                    successfull = true
                };
            }
            catch(Exception ex)
            {
                Console.WriteLine(ex.Message);
                throw;
            }
        }

        /// <summary>
        /// Get kiot product infomation by code.
        /// </summary>
        /// <param name="code"></param>
        /// <returns></returns>
        public ResponseSignleModel<KiotStocking> GetDetail(string code)
        {
            try
            {
                var stocking = new KiotStocking();
                var despUrl = $"{this.mainUrl}/code/{code}";
                stocking = GetStocking(despUrl);
                return new ResponseSignleModel<KiotStocking>()
                {
                    dataset = stocking,
                    successfull = true
                };
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                throw;
            }
        }

        /// <summary>
        /// Get stocking of branch.
        /// </summary>
        /// <param name="code"></param>
        /// <param name="branchId"></param>
        /// <returns></returns>
        public ResponseSignleModel<KiotStocking> GetStockingOfBranch(string code, long branchId)
        {
            try
            {
                var stocking = new KiotStocking();
                var despUrl = $"{this.mainUrl}/code/{code}";
                stocking = GetStocking(despUrl, branchId);
                return new ResponseSignleModel<KiotStocking>()
                {
                    dataset = stocking,
                    successfull = true
                };
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                throw;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="url"></param>
        /// <returns></returns>
        private KiotStocking GetStocking(string url, long branchId = 0)
        {
            HttpResponseMessage response = this.http.GetAsync(url).Result;
            var data = response.Content.ReadAsAsync<KProduct>().Result;
            var stocking = new KiotStocking()
            {
                product_name = data.fullName,
                kiot_id = data.id,
                product_code = data.code
            };
            foreach (var inventory in data.inventories)
            {
                if (branchId == 0)
                {
                    stocking.quantity += inventory.onHand;
                }
                else
                {
                    if (inventory.branchId == branchId)
                    {
                        stocking.quantity += inventory.onHand;
                        break;
                    }
                }
            }
            return stocking;
        }
    }
}

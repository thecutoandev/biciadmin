﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using DataCore.Models;
using DataCore.Models.Auth;
using KiotService.Interfaces;
using Microsoft.Extensions.Options;
using System.Net.Http.Headers;

namespace KiotService.Services
{
    public class BaseService : IToken
    {
        protected AppSettings appSettings;
        protected readonly HttpClient http;
        public BaseService(IOptions<AppSettings> appSettings)
        {
            this.appSettings = appSettings.Value;
            this.http = new HttpClient();
            if (RefreshToken())
            {
                this.http.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", this.appSettings.KiotVietSettings.TokenGlobal.access_token);
                this.http.DefaultRequestHeaders.Add("Retailer", this.appSettings.KiotVietSettings.Retail);
            }
        }

        /// <summary>
        /// Refresh token infomation.
        /// </summary>
        /// <returns></returns>
        public bool RefreshToken()
        {
            try
            {
                if (CheckValidToken())
                {
                    return true;
                }
                var kiotSettings = this.appSettings.KiotVietSettings;
                var postData = new Dictionary<string, string>();
                postData.Add("scopes", "PublicApi.Access");
                postData.Add("grant_type", "client_credentials");
                postData.Add("client_id", this.appSettings.KiotVietSettings.ClientId);
                postData.Add("client_secret", this.appSettings.KiotVietSettings.ClientSecret);
                var req = new HttpRequestMessage(HttpMethod.Post, kiotSettings.TokenEndPoint) { Content = new FormUrlEncodedContent(postData) };
                var tokenResp = this.http.SendAsync(req).Result;
                var respData = tokenResp.Content.ReadAsAsync<TokenInfo>().Result;
                respData.token_time = DateTime.Now;
                this.appSettings.KiotVietSettings.TokenGlobal = respData;
                return true;
            }
            catch
            {
                return false;
            }
        }

        /// <summary>
        /// Check valid current token.
        /// </summary>
        /// <returns></returns>
        private bool CheckValidToken()
        {
            try
            {
                var tokenGlobal = this.appSettings.KiotVietSettings.TokenGlobal;
                if (tokenGlobal == null)
                {
                    return false;
                }
                var secondToken = (DateTime.Now - tokenGlobal.token_time).TotalSeconds;
                if (secondToken >= tokenGlobal.expires_in)
                {
                    return false;
                }
                return true;
            }
            catch
            {
                return false;
            }
        }
    }
}

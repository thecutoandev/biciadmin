﻿using System;
using KiotService.Models.Products;
using Website.ViewModels;

namespace KiotService.Interfaces
{
    public interface IKProductService
    {
        ResponseSignleModel<KiotStocking> GetDetail(long id);
        ResponseSignleModel<KiotStocking> GetDetail(string code);
        ResponseSignleModel<KiotStocking> GetStockingOfBranch(string code, long branchId);
    }

}

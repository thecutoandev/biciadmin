﻿using System;
using KiotService.Models.Branches;
using Website.ViewModels;
namespace KiotService.Interfaces
{
    public interface IKBranchesService
    {
        ResponseListModel<Branch> GetAllBranches();
    }
}

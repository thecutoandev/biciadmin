﻿using System;
using KiotService.Models.Order;
using Website.ViewModels;

namespace KiotService.Interfaces
{
    public interface IOrderService
    {
        ResponseStatus CreateOrder(KOrder order, bool cancelFromHaravan = false);
    }
}

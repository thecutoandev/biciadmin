﻿using System;
using DataCore.Models;
using DataCore.Models.Auth;

namespace KiotService.Interfaces
{
    public interface IToken
    {
        bool RefreshToken();
    }
}

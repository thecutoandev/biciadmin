using DataCore.Data.Infrastructure;
using DataCore.Models;

namespace DataCore.Data.Repositories
{

    public interface IProductAttributeRepository : IRepository<ProductAttribute>
    { }

    public class ProductAttributeRepository : RepositoryBase<ProductAttribute>, IProductAttributeRepository
    {
        public ProductAttributeRepository(IDatabaseFactory databaseFactory)
            : base(databaseFactory)
        {

        }
    }
}
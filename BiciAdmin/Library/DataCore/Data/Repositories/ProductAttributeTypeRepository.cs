using DataCore.Data.Infrastructure;
using DataCore.Models;

namespace DataCore.Data.Repositories
{

    public interface IProductAttributeTypeRepository : IRepository<ProductAttributeType>
    { }

    public class ProductAttributeTypeRepository : RepositoryBase<ProductAttributeType>, IProductAttributeTypeRepository
    {
        public ProductAttributeTypeRepository(IDatabaseFactory databaseFactory)
            : base(databaseFactory)
        {

        }
    }
}
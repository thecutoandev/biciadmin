﻿using DataCore.Data.Infrastructure;
using DataCore.Models;

namespace DataCore.Data.Repositories
{

    public interface ICollectionRepository : IRepository<Collection>
    { }

    public class CollectionRepository : RepositoryBase<Collection>, ICollectionRepository
    {
        public CollectionRepository(IDatabaseFactory databaseFactory)
            : base(databaseFactory)
        {

        }
    }
}

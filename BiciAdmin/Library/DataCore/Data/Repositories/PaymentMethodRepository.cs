using DataCore.Data.Infrastructure;
using DataCore.Models;

namespace DataCore.Data.Repositories
{

    public interface IPaymentMethodRepository : IRepository<PaymentMethod>
    { }

    public class PaymentMethodRepository : RepositoryBase<PaymentMethod>, IPaymentMethodRepository
    {
        public PaymentMethodRepository(IDatabaseFactory databaseFactory)
            : base(databaseFactory)
        {

        }
    }
}
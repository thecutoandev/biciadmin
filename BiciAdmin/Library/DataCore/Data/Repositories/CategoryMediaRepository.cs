using DataCore.Data.Infrastructure;
using DataCore.Models;

namespace DataCore.Data.Repositories
{

    public interface ICategoryMediaRepository : IRepository<CategoryMedia>
    { }

    public class CategoryMediaRepository : RepositoryBase<CategoryMedia>, ICategoryMediaRepository
    {
        public CategoryMediaRepository(IDatabaseFactory databaseFactory)
            : base(databaseFactory)
        {

        }
    }
}
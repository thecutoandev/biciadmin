using DataCore.Data.Infrastructure;
using DataCore.Models;

namespace DataCore.Data.Repositories
{

    public interface IOrderItemRepository : IRepository<OrderItem>
    { }

    public class OrderItemRepository : RepositoryBase<OrderItem>, IOrderItemRepository
    {
        public OrderItemRepository(IDatabaseFactory databaseFactory)
            : base(databaseFactory)
        {

        }
    }
}
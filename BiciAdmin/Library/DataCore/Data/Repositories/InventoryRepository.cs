using DataCore.Data.Infrastructure;
using DataCore.Models;

namespace DataCore.Data.Repositories
{

    public interface IInventoryRepository : IRepository<Inventory>
    { }

    public class InventoryRepository : RepositoryBase<Inventory>, IInventoryRepository
    {
        public InventoryRepository(IDatabaseFactory databaseFactory)
            : base(databaseFactory)
        {

        }
    }
}
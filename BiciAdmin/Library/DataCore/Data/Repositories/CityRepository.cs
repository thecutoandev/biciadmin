using DataCore.Data.Infrastructure;
using DataCore.Models;

namespace DataCore.Data.Repositories
{

    public interface ICityRepository : IRepository<City>
    { }

    public class CityRepository : RepositoryBase<City>, ICityRepository
    {
        public CityRepository(IDatabaseFactory databaseFactory)
            : base(databaseFactory)
        {

        }
    }
}
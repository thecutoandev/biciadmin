using DataCore.Data.Infrastructure;
using DataCore.Models;

namespace DataCore.Data.Repositories
{

    public interface ICategoryRepository : IRepository<Category>
    { }

    public class CategoryRepository : RepositoryBase<Category>, ICategoryRepository
    {
        public CategoryRepository(IDatabaseFactory databaseFactory)
            : base(databaseFactory)
        {

        }
    }
}
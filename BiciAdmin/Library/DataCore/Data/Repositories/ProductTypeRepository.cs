﻿using DataCore.Data.Infrastructure;
using DataCore.Models;

namespace DataCore.Data.Repositories
{
    public interface IProductTypeRepository : IRepository<ProductType>
    { }

    public class ProductTypeRepository : RepositoryBase<ProductType>, IProductTypeRepository
    {
        public ProductTypeRepository(IDatabaseFactory databaseFactory)
            : base(databaseFactory)
        {

        }
    }
}

using DataCore.Data.Infrastructure;
using DataCore.Models;

namespace DataCore.Data.Repositories
{

    public interface ICollectionMediaRepository : IRepository<CollectionMedia>
    { }

    public class CollectionMediaRepository : RepositoryBase<CollectionMedia>, ICollectionMediaRepository
    {
        public CollectionMediaRepository(IDatabaseFactory databaseFactory)
            : base(databaseFactory)
        {

        }
    }
}
using DataCore.Data.Infrastructure;
using DataCore.Models;

namespace DataCore.Data.Repositories
{

    public interface IProductCollectionRepository : IRepository<ProductCollection>
    { }

    public class ProductCollectionRepository : RepositoryBase<ProductCollection>, IProductCollectionRepository
    {
        public ProductCollectionRepository(IDatabaseFactory databaseFactory)
            : base(databaseFactory)
        {

        }
    }
}
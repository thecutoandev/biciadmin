using DataCore.Data.Infrastructure;
using DataCore.Models;

namespace DataCore.Data.Repositories
{

    public interface IOrderRepository : IRepository<Order>
    { }

    public class OrderRepository : RepositoryBase<Order>, IOrderRepository
    {
        public OrderRepository(IDatabaseFactory databaseFactory)
            : base(databaseFactory)
        {

        }
    }
}
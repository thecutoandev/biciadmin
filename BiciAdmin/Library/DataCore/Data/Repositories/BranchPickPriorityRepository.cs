﻿using DataCore.Data.Infrastructure;
using DataCore.Models;

namespace DataCore.Data.Repositories
{
    public interface IBranchPickPriorityRepository : IRepository<BranchPickPriority>
    { }

    public class BranchPickPriorityRepository : RepositoryBase<BranchPickPriority>, IBranchPickPriorityRepository
    {
        public BranchPickPriorityRepository(IDatabaseFactory databaseFactory)
            : base(databaseFactory)
        {

        }
    }
}

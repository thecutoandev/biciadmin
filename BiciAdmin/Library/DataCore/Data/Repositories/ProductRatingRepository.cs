using DataCore.Data.Infrastructure;
using DataCore.Models;

namespace DataCore.Data.Repositories
{

    public interface IProductRatingRepository : IRepository<ProductRating>
    { }

    public class ProductRatingRepository : RepositoryBase<ProductRating>, IProductRatingRepository
    {
        public ProductRatingRepository(IDatabaseFactory databaseFactory)
            : base(databaseFactory)
        {

        }
    }
}
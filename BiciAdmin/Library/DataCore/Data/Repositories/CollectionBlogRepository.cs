using DataCore.Data.Infrastructure;
using DataCore.Models;

namespace DataCore.Data.Repositories
{

    public interface ICollectionBlogRepository : IRepository<CollectionBlog>
    { }

    public class CollectionBlogRepository : RepositoryBase<CollectionBlog>, ICollectionBlogRepository
    {
        public CollectionBlogRepository(IDatabaseFactory databaseFactory)
            : base(databaseFactory)
        {

        }
    }
}
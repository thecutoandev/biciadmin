using DataCore.Data.Infrastructure;
using DataCore.Models;

namespace DataCore.Data.Repositories
{

    public interface IOrderSummaryRepository : IRepository<OrderSummary>
    { }

    public class OrderSummaryRepository : RepositoryBase<OrderSummary>, IOrderSummaryRepository
    {
        public OrderSummaryRepository(IDatabaseFactory databaseFactory)
            : base(databaseFactory)
        {

        }
    }
}
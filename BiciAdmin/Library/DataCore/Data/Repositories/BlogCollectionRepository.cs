using DataCore.Data.Infrastructure;
using DataCore.Models;

namespace DataCore.Data.Repositories
{

    public interface IBlogCollectionRepository : IRepository<BlogCollection>
    { }

    public class BlogCollectionRepository : RepositoryBase<BlogCollection>, IBlogCollectionRepository
    {
        public BlogCollectionRepository(IDatabaseFactory databaseFactory)
            : base(databaseFactory)
        {

        }
    }
}
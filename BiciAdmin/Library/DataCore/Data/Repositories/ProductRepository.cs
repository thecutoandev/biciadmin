using DataCore.Data.Infrastructure;
using DataCore.Models;

namespace DataCore.Data.Repositories
{

    public interface IProductRepository : IRepository<Product>
    { }

    public class ProductRepository : RepositoryBase<Product>, IProductRepository
    {
        public ProductRepository(IDatabaseFactory databaseFactory)
            : base(databaseFactory)
        {

        }
    }
}
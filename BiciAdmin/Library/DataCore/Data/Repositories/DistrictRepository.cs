using DataCore.Data.Infrastructure;
using DataCore.Models;

namespace DataCore.Data.Repositories
{

    public interface IDistrictRepository : IRepository<District>
    { }

    public class DistrictRepository : RepositoryBase<District>, IDistrictRepository
    {
        public DistrictRepository(IDatabaseFactory databaseFactory)
            : base(databaseFactory)
        {

        }
    }
}
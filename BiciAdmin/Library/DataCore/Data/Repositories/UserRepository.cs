﻿using System.Threading.Tasks;
using DataCore.Data.Infrastructure;
using DataCore.Models;
using Microsoft.AspNetCore.Identity;
using System.Linq;
using System;

namespace DataCore.Data.Repositories
{
    public interface IUserRepository : IRepository<User>
    {
        Task<User> FindUser(string userName, string passWord);
        Task<IdentityResult> RegisterUser(UserViewModel user);
        Task<IdentityResult> ChangePassword(string userName, string password, string newPassword);
        Task<IdentityResult> UpdateUser(UserViewModel userViewModel);
        Task<User> FindUserByMail(string email);
        User FindUserByPhone(string phoneNumber);
        Task<IdentityResult> ResetPassWord(string userName, string newPassword);
    }
    public class UserRepository : RepositoryBase<User>, IUserRepository
    {
        // Variables
        private MainDBContext _ctx;
        private SignInManager<User> _signInManager;
        private UserManager<User> _userManager;

        public UserRepository(IDatabaseFactory databaseFactory,
                              UserManager<User> userManager,
                              SignInManager<User> signInManager) : base(databaseFactory)
        {
            _ctx = DataContext;
            _userManager = userManager;
            _signInManager = signInManager;
        }

        /// <summary>
        /// Change password
        /// </summary>
        /// <param name="userName"></param>
        /// <param name="password"></param>
        /// <param name="newPassword"></param>
        /// <returns></returns>
        public async Task<IdentityResult> ChangePassword(string userName, string password, string newPassword)
        {
            var user = _userManager.FindByNameAsync(userName).Result;
            var result = await _userManager.ChangePasswordAsync(user, password, newPassword);
            return result;
        }

        public async Task<IdentityResult> ResetPassWord(string userName, string newPassword)
        {
            var user = _userManager.FindByNameAsync(userName).Result;
            var token = await _userManager.GeneratePasswordResetTokenAsync(user);
            var result = await _userManager.ResetPasswordAsync(user, token, newPassword);
            return result;
        }

        /// <summary>
        /// Find user from database.
        /// </summary>
        /// <param name="userName"></param>
        /// <param name="passWord"></param>
        /// <returns></returns>
        public async Task<User> FindUser(string userName, string passWord)
        {
            var login = await _signInManager.PasswordSignInAsync(userName, passWord, true, true);
            if (login.Succeeded)
            {
                var user = await _userManager.FindByNameAsync(userName);
                return user;
            }
            return null;
        }

        public Task<User> FindUserByMail(string email)
        {
            try
            {
                var userInfo = _userManager.FindByEmailAsync(email);
                return userInfo;
            }
            catch
            {
                return null;
            }
        }

        public User FindUserByPhone(string phoneNumber)
        {
            try
            {
                var userInfo = Query(model => model.PhoneNumber == phoneNumber || model.UserName == phoneNumber || model.Id == phoneNumber).SingleOrDefault();
                if (userInfo != null)
                {
                    userInfo = _userManager.FindByNameAsync(phoneNumber).Result;
                }
                return userInfo;
            }
            catch
            {
                return null;
            }
        }

        /// <summary>
        /// Register user to database.
        /// </summary>
        /// <param name="vmUser"></param>
        /// <returns></returns>
        public async Task<IdentityResult> RegisterUser(UserViewModel vmUser)
        {
            try
            {
                var user = new User()
                {
                    Id = vmUser.UserName,
                    UserName = vmUser.UserName,
                    BirthDay = vmUser.BirthDay,
                    Email = vmUser.Email,
                    EmailConfirmed = false,
                    Gender = 1,
                    PhoneNumber = vmUser.PhoneNumber,
                    PhoneNumberConfirmed = false,
                    TwoFactorEnabled = false,
                    LockoutEnabled = false,
                    AccessFailedCount = 0,
                    FullName = vmUser.FullName,
                    BranchId = vmUser.BranchId,
                    RoleId = vmUser.RoleId,
                    Status = 1
                };
                if (string.IsNullOrEmpty(vmUser.PassWord))
                {
                    vmUser.PassWord = "Bici123)(*&";
                }
                var result = await _userManager.CreateAsync(user, vmUser.PassWord);
                return result;
            }
            catch (System.Exception ex)
            {
                Console.WriteLine(ex.Message);
                throw;
            }
            
        }

        /// <summary>
        /// Update user info.
        /// </summary>
        /// <param name="userViewModel"></param>
        /// <returns></returns>
        public async Task<IdentityResult> UpdateUser(UserViewModel userViewModel)
        {
            IdentityResult result = null;
            var user = _userManager.FindByNameAsync(userViewModel.UserName).Result;
            if (user != null)
            {
                user.PhoneNumber = userViewModel.PhoneNumber;
                user.BirthDay = userViewModel.BirthDay;
                user.Email = userViewModel.Email;
                user.Gender = userViewModel.Gender;
                user.FullName = userViewModel.FullName;
                user.BranchId = userViewModel.BranchId;
                user.RoleId = userViewModel.RoleId;
                user.Avatar = userViewModel.Avatar;
                result = await _userManager.UpdateAsync(user);
            }
            return result;
        }
    }
}

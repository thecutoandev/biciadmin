using DataCore.Data.Infrastructure;
using DataCore.Models;

namespace DataCore.Data.Repositories
{

    public interface IGenericProductMediaRepository : IRepository<GenericProductMedia>
    { }

    public class GenericProductMediaRepository : RepositoryBase<GenericProductMedia>, IGenericProductMediaRepository
    {
        public GenericProductMediaRepository(IDatabaseFactory databaseFactory)
            : base(databaseFactory)
        {

        }
    }
}
using DataCore.Data.Infrastructure;
using DataCore.Models;

namespace DataCore.Data.Repositories
{

    public interface IBlogRepository : IRepository<Blog>
    { }

    public class BlogRepository : RepositoryBase<Blog>, IBlogRepository
    {
        public BlogRepository(IDatabaseFactory databaseFactory)
            : base(databaseFactory)
        {

        }
    }
}
using DataCore.Data.Infrastructure;
using DataCore.Models;

namespace DataCore.Data.Repositories
{

    public interface IUserAddressRepository : IRepository<UserAddress>
    { }

    public class UserAddressRepository : RepositoryBase<UserAddress>, IUserAddressRepository
    {
        public UserAddressRepository(IDatabaseFactory databaseFactory)
            : base(databaseFactory)
        {

        }
    }
}
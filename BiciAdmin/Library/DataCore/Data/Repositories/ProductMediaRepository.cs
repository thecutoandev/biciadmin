using DataCore.Data.Infrastructure;
using DataCore.Models;

namespace DataCore.Data.Repositories
{

    public interface IProductMediaRepository : IRepository<ProductMedia>
    { }

    public class ProductMediaRepository : RepositoryBase<ProductMedia>, IProductMediaRepository
    {
        public ProductMediaRepository(IDatabaseFactory databaseFactory)
            : base(databaseFactory)
        {

        }
    }
}
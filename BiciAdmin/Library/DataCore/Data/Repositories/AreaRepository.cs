using DataCore.Data.Infrastructure;
using DataCore.Models;

namespace DataCore.Data.Repositories
{

    public interface IAreaRepository : IRepository<Area>
    { }

    public class AreaRepository : RepositoryBase<Area>, IAreaRepository
    {
        public AreaRepository(IDatabaseFactory databaseFactory)
            : base(databaseFactory)
        {

        }
    }
}
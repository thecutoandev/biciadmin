﻿using System;
using DataCore.Data.Infrastructure;
using DataCore.Models;

namespace DataCore.Data.Repositories
{
    public interface IBatchJobsRepository : IRepository<BatchJobs>
    { }

    public class BatchJobsRepository : RepositoryBase<BatchJobs>, IBatchJobsRepository
    {
        public BatchJobsRepository(IDatabaseFactory databaseFactory)
            : base(databaseFactory)
        {

        }
    }
}

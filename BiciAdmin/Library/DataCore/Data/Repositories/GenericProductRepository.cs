using DataCore.Data.Infrastructure;
using DataCore.Models;

namespace DataCore.Data.Repositories
{

    public interface IGenericProductRepository : IRepository<GenericProduct>
    { }

    public class GenericProductRepository : RepositoryBase<GenericProduct>, IGenericProductRepository
    {
        public GenericProductRepository(IDatabaseFactory databaseFactory)
            : base(databaseFactory)
        {

        }

        //public override void Update(GenericProduct entity)
        //{
        //    DataContext.Entry(entity).State = Microsoft.EntityFrameworkCore.EntityState.Modified;
        //    DataContext.GenericProduct.Update(entity);
        //    //DataContext.SaveChanges();
        //}
    }
}
using DataCore.Data.Infrastructure;
using DataCore.Models;

namespace DataCore.Data.Repositories
{

    public interface IAreaDistrictRepository : IRepository<AreaDistrict>
    { }

    public class AreaDistrictRepository : RepositoryBase<AreaDistrict>, IAreaDistrictRepository
    {
        public AreaDistrictRepository(IDatabaseFactory databaseFactory)
            : base(databaseFactory)
        {

        }
    }
}
﻿using DataCore.Data.Infrastructure;
using DataCore.Models;

namespace DataCore.Data.Repositories
{
    public interface IBannerRepository : IRepository<Banner>
    { }

    public class BannerRepository : RepositoryBase<Banner>, IBannerRepository
    {
        public BannerRepository(IDatabaseFactory databaseFactory)
            : base(databaseFactory)
        {

        }
    }
}

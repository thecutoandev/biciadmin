﻿using DataCore.Data.Infrastructure;
using DataCore.Models;

namespace DataCore.Data.Repositories
{
    public interface ICategoryCollectionRepository : IRepository<CategoryCollection>
    { }

    public class CategoryCollectionRepository : RepositoryBase<CategoryCollection>, ICategoryCollectionRepository
    {
        public CategoryCollectionRepository(IDatabaseFactory databaseFactory)
            : base(databaseFactory)
        {

        }
    }
}

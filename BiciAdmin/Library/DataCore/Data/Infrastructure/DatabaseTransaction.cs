﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Storage;

namespace DataCore.Data.Infrastructure
{
    public class DatabaseTransaction : IDatabaseTransaction
    {
        private readonly IDatabaseFactory _databaseFactory;
        //private IDbContextTransaction _dataContext;

        public DatabaseTransaction(IDatabaseFactory databaseFactory)
        {
            this._databaseFactory = databaseFactory;
        }

        public IDbContextTransaction DataContext
        {
            get { return _databaseFactory.Get().Database.BeginTransaction(); }
        }

        public void Commit()
        {
            DataContext.Commit();
        }

        public void Dispose()
        {
            DataContext.Dispose();
        }

        public void RollBack()
        {
            DataContext.Rollback();
        }
    }
}

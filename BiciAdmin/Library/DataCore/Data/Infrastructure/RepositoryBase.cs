﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore;

namespace DataCore.Data.Infrastructure
{
    public abstract class RepositoryBase<T> where T : class
    {
        private MainDBContext _dataContext;
        private readonly DbSet<T> _dbset;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="databaseFactory"></param>
        protected RepositoryBase(IDatabaseFactory databaseFactory)
        {
            DatabaseFactory = databaseFactory;
            _dbset = DataContext.Set<T>();
        }

        /// <summary>
        /// The database factor.
        /// </summary>
        protected IDatabaseFactory DatabaseFactory
        {
            get;
            private set;
        }

        /// <summary>
        /// Get datacontext by singleton.
        /// </summary>
        protected MainDBContext DataContext
        {
             get { return _dataContext ?? (_dataContext = DatabaseFactory.Get()); }
            //get { return DatabaseFactory.Get(); }
        }

        /// <summary>
        /// Execute command
        /// </summary>
        /// <param name="sql">Sql command</param>
        public virtual void ExecuteSql(string sql)
        {
            try
            {
                this.DataContext.Database.ExecuteSqlRaw(sql);
            }

            catch
            {
                return;
            }
        }

        /// <summary>
        /// Add entity to db context for inserting.
        /// </summary>
        /// <param name="entity">Model to insert</param>
        public virtual void Add(T entity)
        {
            try
            {
                _dbset.Add(entity);
            }

            catch
            {
                return;
            }
        }

        /// <summary>
        /// Add entiry to db context for updating.
        /// </summary>
        /// <param name="entity"></param>
        public virtual void Update(T entity)
        {
            try
            {
                _dbset.Update(entity);
                DataContext.Entry(entity).State = EntityState.Modified;
            }
            catch
            {
                return;
            }
        }

        /// <summary>
        /// Deleting model from DB by entity
        /// </summary>
        /// <param name="entity"></param>
        public virtual void Delete(T entity)
        {
            try
            {
                _dbset.Remove(entity);
            }
            catch
            {
                return;
            }
        }

        /// <summary>
        /// Delete database with where clause.
        /// </summary>
        /// <param name="where"></param>
        public virtual void Delete(Expression<Func<T, bool>> where)
        {
            try
            {
                IEnumerable<T> entities = _dbset.Where<T>(where).AsEnumerable();
                foreach (var entity in entities)
                {
                    _dbset.Remove(entity);
                }
            }
            catch
            {
                return;
            }
        }

        /// <summary>
        /// Get all data of table as queryable.
        /// </summary>
        /// <returns>Query for table</returns>
        public virtual IQueryable<T> GetAllQueryable()
        {
            return _dbset.AsQueryable();
        }

        /// <summary>
        /// Get data with wher clause.
        /// </summary>
        /// <param name="where">Condition</param>
        /// <returns>Query for table</returns>
        public virtual IQueryable<T> Query(Expression<Func<T, bool>> where)
        {
            return _dbset.Where(where);
        }
    }
}

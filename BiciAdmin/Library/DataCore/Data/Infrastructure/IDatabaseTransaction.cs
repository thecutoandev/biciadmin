﻿using System;

namespace DataCore.Data.Infrastructure
{
    public interface IDatabaseTransaction : IDisposable
    {
        void Commit();
        void RollBack();
    }
}

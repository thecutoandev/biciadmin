﻿using System;
using DataCore.Models;

namespace DataCore.Data.Infrastructure
{
    public interface IDatabaseFactory : IDisposable
    {
        MainDBContext Get();
    }
}

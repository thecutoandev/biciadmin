﻿using System;
using System.Linq;
using System.Linq.Expressions;

namespace DataCore.Data.Infrastructure
{
    public interface IRepository<T> where T : class
    {
        void ExecuteSql(string sql);
        void Add(T entity);
        void Update(T entity);
        void Delete(Expression<Func<T, bool>> where);
        void Delete(T entity);
        IQueryable<T> GetAllQueryable();
        IQueryable<T> Query(Expression<Func<T, bool>> where);
    }
}

﻿namespace DataCore.Data.Infrastructure
{
    public interface IUnitOfWork
    {
        void Commit();
    }
}

﻿using System;
using Microsoft.Extensions.Configuration;

namespace DataCore.Data.Infrastructure
{
    public class DatabaseFactory : Disposable, IDatabaseFactory
    {
        private readonly MainDBContext _dataContext;

        public DatabaseFactory(MainDBContext mainDBContext)
        {
            _dataContext = mainDBContext;
        }

        public MainDBContext Get()
        {
            return _dataContext;
        }

        protected override void DisposeCore()
        {
            if (_dataContext != null)
            {
                _dataContext.Dispose();
            }
        }
    }
}

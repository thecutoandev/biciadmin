﻿using DataCore.Models;
using DataCore.Models.Mappings;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System.Configuration;

namespace DataCore.Data
{
    public partial class MainDBContext:IdentityDbContext<User>
    {
        public DbSet<Collection> Collection { get; set; }
        public DbSet<UserAddress> UserAddress { get; set; }
        public DbSet<City> City { get; set; }
        public DbSet<CollectionMedia> CollectionMedia { get; set; }
        public DbSet<Category> Category { get; set; }
        public DbSet<CategoryMedia> CategoryMedia { get; set; }
        public DbSet<Brand> Brand { get; set; }
        public DbSet<GenericProduct> GenericProduct { get; set; }
        public DbSet<GenericProductMedia> GenericProductMedia { get; set; }
        public DbSet<ProductCollection> ProductCollection { get; set; }
        public DbSet<Product> Product { get; set; }
        public DbSet<ProductMedia> ProductMedia { get; set; }
        public DbSet<ProductAttributeType> ProductAttributeType { get; set; }
        public DbSet<ProductAttribute> ProductAttribute { get; set; }
        public DbSet<District> District { get; set; }
        public DbSet<Area> Area { get; set; }
        public DbSet<AreaDistrict> AreaDistrict { get; set; }
        public DbSet<PaymentMethod> PaymentMethod { get; set; }
        public DbSet<Order> Order { get; set; }
        public DbSet<OrderItem> OrderItem { get; set; }
        public DbSet<OrderSummary> OrderSummary { get; set; }
        public DbSet<ProductRating> ProductRating { get; set; }
        public DbSet<Blog> Blog { get; set; }
        public DbSet<BlogCollection> BlogCollection { get; set; }
        public DbSet<CollectionBlog> CollectionBlog { get; set; }
        public DbSet<Inventory> Inventory { get; set; }
        public DbSet<Banner> Banner { get; set; }
        public DbSet<BranchPickPriority> BranchPickPriority { get; set; }
        public DbSet<BatchJobs> BatchJobs { get; set; }
        public DbSet<ProductType> ProductType { get; set; }
        public DbSet<CategoryCollection> CategoryCollection { get; set; }

        public MainDBContext(DbContextOptions<MainDBContext> options)
        : base(options)
        {
            //Database.Migrate();
        }

        public MainDBContext() : base()
        {
        }

        //public MainDBContext(IConfiguration configuration)
        //{
        //    this.configuration = configuration;
        //}

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);
            builder.ApplyConfiguration(new CollectionConfiguration());
            builder.ApplyConfiguration(new UserAddressConfiguration());
            builder.ApplyConfiguration(new CityConfiguration());
            builder.ApplyConfiguration(new CollectionMediaConfiguration());
            builder.ApplyConfiguration(new CategoryConfiguration());
            builder.ApplyConfiguration(new CategoryMediaConfiguration());
            builder.ApplyConfiguration(new BrandConfiguration());
            builder.ApplyConfiguration(new GenericProductConfiguration());
            builder.ApplyConfiguration(new GenericProductMediaConfiguration());
            builder.ApplyConfiguration(new ProductCollectionConfiguration());
            builder.ApplyConfiguration(new ProductConfiguration());
            builder.ApplyConfiguration(new ProductMediaConfiguration());
            builder.ApplyConfiguration(new ProductAttributeTypeConfiguration());
            builder.ApplyConfiguration(new ProductAttributeConfiguration());
            builder.ApplyConfiguration(new DistrictConfiguration());
            builder.ApplyConfiguration(new AreaConfiguration());
            builder.ApplyConfiguration(new AreaDistrictConfiguration());
            builder.ApplyConfiguration(new PaymentMethodConfiguration());
            builder.ApplyConfiguration(new OrderConfiguration());
            builder.ApplyConfiguration(new OrderItemConfiguration());
            builder.ApplyConfiguration(new OrderSummaryConfiguration());
            builder.ApplyConfiguration(new ProductRatingConfiguration());
            builder.ApplyConfiguration(new BlogConfiguration());
            builder.ApplyConfiguration(new BlogCollectionConfiguration());
            builder.ApplyConfiguration(new CollectionBlogConfiguration());
            builder.ApplyConfiguration(new InventoryConfiguration());
            builder.ApplyConfiguration(new BannerConfiguration());
            builder.ApplyConfiguration(new BranchPickPriorityConfiguration());
            builder.ApplyConfiguration(new BatchJobsConfiguration());
            builder.ApplyConfiguration(new ProductTypeConfiguration());
            builder.ApplyConfiguration(new CategoryCollectionConfiguration());
            builder.Entity<User>().ToTable("Users");
            builder.Entity<IdentityRole<string>>().ToTable("UserRoles");
            builder.Entity<IdentityUserLogin<string>>().ToTable("UserLogins");
            builder.Entity<IdentityUserClaim<string>>().ToTable("UserClaims");
            builder.Entity<IdentityRole>().ToTable("Roles");

            builder.Seed();
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            //optionsBuilder.UseSqlServer("Data Source=localhost;Initial Catalog=DataApp;Persist Security Info=True;User ID=sa;Password=Password1!;MultipleActiveResultSets=True");
            base.OnConfiguring(optionsBuilder);
        }

        public virtual void Commit()
        {
            base.SaveChanges();
        }
    }
}

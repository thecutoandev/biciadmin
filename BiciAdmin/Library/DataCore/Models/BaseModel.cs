﻿using System;

namespace DataCore.Models
{
    public class BaseModel
    {
        public DateTime? created_date { get; set; } = DateTime.Now;
        public string created_by { get; set; } = "BiciWeb";
        public DateTime? updated_date { get; set; } = DateTime.Now;
        public string updated_by { get; set; } = "BiciWeb";
    }
}

using System;

namespace DataCore.Models
{
    public class ProductAttributeType : BaseModel
    {
        public int id {get; set;}
        public string name {get; set;}

    }
}
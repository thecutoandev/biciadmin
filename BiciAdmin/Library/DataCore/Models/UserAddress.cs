﻿using System;
namespace DataCore.Models
{
    public class UserAddress : BaseModel
    {
        public int id { get; set; }
        public string username { get; set; }
        public byte location_type { get; set; }
        public string address { get; set; }
        public string receiver_name { get; set; }
        public string receiver_phoneno { get; set; }
        public int district_id { get; set; }
        public int city_id { get; set; }
        public bool isdefault { get; set; }
    }
}

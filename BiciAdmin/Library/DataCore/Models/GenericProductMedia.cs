using System;

namespace DataCore.Models
{
    public class GenericProductMedia 
    {
        public int id {get; set;}
        public int generic_product_id {get; set;}
        public string image_link {get; set;}
        public int ref_wed_id { get; set; }
        public int position { get; set; }
    }
}
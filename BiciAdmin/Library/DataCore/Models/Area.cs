using System;

namespace DataCore.Models
{
    public class Area : BaseModel
    {
        public int id {get; set;}
        public string name {get; set;}
        public Decimal limit_transaction_amount {get; set;}
        public decimal fee_amount {get; set;}

    }
}
using System;

namespace DataCore.Models
{
    public class PaymentMethod : BaseModel
    {
        public int id {get; set;}
        public string name {get; set;}
        public string description {get; set;}
        public bool is_cod {get; set;}
        public decimal cod_fee_amount {get; set;}

    }
}
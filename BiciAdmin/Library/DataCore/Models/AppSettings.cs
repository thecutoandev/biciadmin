﻿using System;
using DataCore.Models.Auth;
namespace DataCore.Models
{
    public class AppSettings
    {
        public string JwtKey { get; set; }
        public string FolderFiles { get; set; }
        public string BaseFileEndPoint { get; set; }
        public string JobPickingTime { get; set; }
        public string DefaultPassWord { get; set; }
        public FaceBookAuthSettings FacebookAuthSettings { get; set; }
        public AccountKitAuthSettings AccountKitAuthSettings { get; set; }
        public KiotVietSettings KiotVietSettings { get; set; }
        public HaravanSettings HaravanSettings { get; set; }
    }

    public class HaravanSettings
    {
        public string TokenPass { get; set; }
        public string UrlBase { get; set; }
    }
}

using System;

namespace DataCore.Models
{
    public class Brand : BaseModel
    {
        public int id {get; set;}
        public string name {get; set;}
        public int ref_brand_id {get; set;}

    }
}
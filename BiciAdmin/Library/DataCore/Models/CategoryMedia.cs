using System;

namespace DataCore.Models
{
    public class CategoryMedia 
    {
        public int id {get; set;}
        public int category_id {get; set;}
        public string image_link {get; set;}

    }
}
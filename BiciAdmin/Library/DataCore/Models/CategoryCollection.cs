﻿using System;
namespace DataCore.Models
{
    public class CategoryCollection
    {
        public int id { get; set; }
        public int category_id { get; set; }
        public int product_type_id { get; set; }
        public int product_id { get; set; }
        public int position { get; set; }
        public string sort_value { get; set; }
    }
}

using System;

namespace DataCore.Models
{
    public class ProductMedia 
    {
        public int id {get; set;}
        public int product_id {get; set;}
        public string image_link {get; set;}
        public int position { get; set; }
    }
}
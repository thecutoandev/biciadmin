﻿using System;
namespace DataCore.Models
{
    public class BatchJobs
    {
        public int id { get; set; }
        public DateTime BeginAt { get; set; }
        public DateTime EndAt { get; set; }
        public byte Status { get; set; }
        public long NumberOfRecords { get; set; }
        public long SuccessRecords { get; set; }
        public string JobName { get; set; }
    }
}

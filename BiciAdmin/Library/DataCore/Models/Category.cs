using System;

namespace DataCore.Models
{
    public class Category : BaseModel
    {
        public int id {get; set;}
        public string name {get; set;}
        public int parent_category_id {get; set;}
        public byte status {get; set;}
        public int ref_category_id {get; set;}

    }
}
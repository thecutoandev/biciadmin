using System;

namespace DataCore.Models
{
    public class BlogCollection 
    {
        public int id {get; set;}
        public string name {get; set;}
        public string image_link {get; set;}
        public int ref_web_id { get; set; } = 0;
        public int parent_collection_id { get; set; } = 0;
    }
}
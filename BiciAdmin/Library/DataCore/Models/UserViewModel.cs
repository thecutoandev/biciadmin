﻿using System;
namespace DataCore.Models
{
    public class UserViewModel : User
    {
        public string PassWord { get; set; }
        public string Token{ get; set; }
        public string BranchName { get; set; }
        public UserViewModel()
        {
            this.Status = 1;
        }
    }
}

using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Microsoft.EntityFrameworkCore;

namespace DataCore.Models.Mappings
{
    public class ProductAttributeConfiguration : IEntityTypeConfiguration<ProductAttribute>
    {

        public void Configure(EntityTypeBuilder<ProductAttribute> entity)
        {
            // Define Columns
             entity.HasKey(e => e.id);
            entity.Property(e => e.id).IsRequired().ValueGeneratedOnAdd();
            entity.Property(e => e.product_id);
            entity.Property(e => e.attribute_id);
            entity.Property(e => e.value).IsUnicode().HasMaxLength(200);


            entity.ToTable("ProductAttribute");
        }
    }
}

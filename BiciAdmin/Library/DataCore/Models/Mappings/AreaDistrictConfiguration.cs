using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Microsoft.EntityFrameworkCore;

namespace DataCore.Models.Mappings
{
    public class AreaDistrictConfiguration : IEntityTypeConfiguration<AreaDistrict>
    {

        public void Configure(EntityTypeBuilder<AreaDistrict> entity)
        {
            // Define Columns
             entity.HasKey(e => e.id);
            entity.Property(e => e.id).IsRequired().ValueGeneratedOnAdd();
            entity.Property(e => e.area_id);
            entity.Property(e => e.district_id);


            entity.ToTable("AreaDistrict");
        }
    }
}

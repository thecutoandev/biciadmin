using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Microsoft.EntityFrameworkCore;

namespace DataCore.Models.Mappings
{
    public class ProductMediaConfiguration : IEntityTypeConfiguration<ProductMedia>
    {

        public void Configure(EntityTypeBuilder<ProductMedia> entity)
        {
            // Define Columns
             entity.HasKey(e => e.id);
            entity.Property(e => e.id).IsRequired().ValueGeneratedOnAdd();
            entity.Property(e => e.product_id);
            entity.Property(e => e.image_link).IsUnicode();
            entity.Property(e => e.position);


            entity.ToTable("ProductMedia");
        }
    }
}

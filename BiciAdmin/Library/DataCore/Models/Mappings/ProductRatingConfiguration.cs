using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Microsoft.EntityFrameworkCore;

namespace DataCore.Models.Mappings
{
    public class ProductRatingConfiguration : IEntityTypeConfiguration<ProductRating>
    {

        public void Configure(EntityTypeBuilder<ProductRating> entity)
        {
            // Define Columns
             entity.HasKey(e => e.id);
            entity.Property(e => e.id).IsRequired().ValueGeneratedOnAdd();
            entity.Property(e => e.product_id);
            entity.Property(e => e.order_id);
            entity.Property(e => e.rating_product_star).HasColumnType("decimal");
            entity.Property(e => e.rating_content).IsUnicode();


            entity.ToTable("ProductRating");
        }
    }
}

using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Microsoft.EntityFrameworkCore;

namespace DataCore.Models.Mappings
{
    public class DistrictConfiguration : IEntityTypeConfiguration<District>
    {

        public void Configure(EntityTypeBuilder<District> entity)
        {
            // Define Columns
            entity.HasKey(e => e.id);
            entity.HasIndex(e => new { e.city_id, e.id }).IsUnique();
            entity.Property(e => e.id).IsRequired();
            entity.Property(e => e.name).IsUnicode().HasMaxLength(100);
            entity.Property(e => e.city_id);
            entity.ToTable("District");
        }
    }
}

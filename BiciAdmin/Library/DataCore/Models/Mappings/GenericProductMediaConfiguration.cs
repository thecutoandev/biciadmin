using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Microsoft.EntityFrameworkCore;

namespace DataCore.Models.Mappings
{
    public class GenericProductMediaConfiguration : IEntityTypeConfiguration<GenericProductMedia>
    {

        public void Configure(EntityTypeBuilder<GenericProductMedia> entity)
        {
            // Define Columns
            entity.HasKey(e => e.id);
            entity.HasIndex(e => e.generic_product_id).HasDatabaseName("Generic_media_index");
            entity.Property(e => e.id).IsRequired().ValueGeneratedOnAdd();
            entity.Property(e => e.generic_product_id);
            entity.Property(e => e.image_link).IsUnicode();
            entity.Property(e => e.ref_wed_id);
            entity.Property(e => e.position);

            entity.ToTable("GenericProductMedia");
        }
    }
}

using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Microsoft.EntityFrameworkCore;

namespace DataCore.Models.Mappings
{
    public class OrderConfiguration : IEntityTypeConfiguration<Order>
    {

        public void Configure(EntityTypeBuilder<Order> entity)
        {
            // Define Columns
            entity.HasKey(e => e.id);
            entity.HasIndex(e => e.code).IsUnique();
            entity.Property(e => e.id).IsRequired().ValueGeneratedOnAdd();
            entity.Property(e => e.code).HasMaxLength(100);
            entity.Property(e => e.bank_transfer_code).HasMaxLength(1000);
            entity.Property(e => e.user_id).HasMaxLength(256);
            entity.Property(e => e.user_address_id);
            entity.Property(e => e.payment_method_id);
            entity.Property(e => e.note).IsUnicode();
            entity.Property(e => e.title).IsUnicode();
            entity.Property(e => e.image).IsUnicode();
            entity.Property(e => e.status).IsRequired();
            entity.Property(e => e.branch_id).IsRequired();
            entity.Property(e => e.employee).HasMaxLength(450);
            entity.Property(e => e.ref_web_id);

            entity.Property(e => e.created_by)
                    .HasMaxLength(50)
                    .IsRequired()
                    .IsUnicode(false);

            entity.Property(e => e.updated_by)
                    .HasMaxLength(50)
                    .IsUnicode(false);

            entity.Property(e => e.created_date)
                  .HasColumnType("datetime2");

            entity.Property(e => e.updated_date)
                  .HasColumnType("datetime2");
            entity.ToTable("Order");
        }
    }
}

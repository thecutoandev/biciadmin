using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Microsoft.EntityFrameworkCore;

namespace DataCore.Models.Mappings
{
    public class OrderItemConfiguration : IEntityTypeConfiguration<OrderItem>
    {

        public void Configure(EntityTypeBuilder<OrderItem> entity)
        {
            // Define Columns
            entity.HasKey(e => e.id);
            entity.Property(e => e.id).IsRequired().ValueGeneratedOnAdd();
            entity.Property(e => e.order_id);
            entity.Property(e => e.product_id);
            entity.Property(e => e.quantity);
            entity.Property(e => e.unit_price).HasColumnType("decimal");
            entity.Property(e => e.amount).HasColumnType("decimal");


            entity.ToTable("OrderItem");
        }
    }
}

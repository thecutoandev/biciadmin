using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Microsoft.EntityFrameworkCore;

namespace DataCore.Models.Mappings
{
    public class OrderSummaryConfiguration : IEntityTypeConfiguration<OrderSummary>
    {

        public void Configure(EntityTypeBuilder<OrderSummary> entity)
        {
            // Define Columns
            entity.Property(e => e.id);
            entity.HasIndex(e => e.order_id)
                  .HasDatabaseName("Order_Summary_Index");
            entity.Property(e => e.order_id);
            entity.Property(e => e.sub_total).HasColumnType("decimal");
            entity.Property(e => e.shipping_fee_amount).HasColumnType("decimal");
            entity.Property(e => e.service_fee_amount).HasColumnType("decimal");
            entity.Property(e => e.rating_order_star).HasColumnType("decimal");
            entity.Property(e => e.rating_content).IsUnicode();
            entity.Property(e => e.total_amount).HasColumnType("decimal");
            entity.Property(e => e.note).IsUnicode(true);
            entity.Property(e => e.approver).IsUnicode(false).HasMaxLength(200);

            entity.ToTable("OrderSummary");
        }
    }
}

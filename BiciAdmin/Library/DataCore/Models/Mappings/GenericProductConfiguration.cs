using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Microsoft.EntityFrameworkCore;

namespace DataCore.Models.Mappings
{
    public class GenericProductConfiguration : IEntityTypeConfiguration<GenericProduct>
    {

        public void Configure(EntityTypeBuilder<GenericProduct> entity)
        {
            // Define Columns
            entity.HasKey(e => e.id);
            entity.HasIndex(e => e.ref_web_id)
                  .HasDatabaseName("Generic_Product_Index")
                  .IsUnique();
            entity.Property(e => e.id).IsRequired().ValueGeneratedOnAdd();
            entity.Property(e => e.name).IsUnicode();
            entity.Property(e => e.category_id);
            entity.Property(e => e.brand_id);
            entity.Property(e => e.product_name).IsUnicode();
            entity.Property(e => e.product_content).IsUnicode();
            entity.Property(e => e.short_description).IsUnicode();
            entity.Property(e => e.product_price).HasColumnType("decimal");
            entity.Property(e => e.link).IsUnicode();
            entity.Property(e => e.is_free_ship);
            entity.Property(e => e.ref_web_id);
            entity.Property(e => e.hidden);

            entity.Property(e => e.created_by)
                    .HasMaxLength(50)
                    .IsRequired()
                    .IsUnicode(false);

            entity.Property(e => e.updated_by)
                    .HasMaxLength(50)
                    .IsUnicode(false);

            entity.Property(e => e.created_date)
                  .HasColumnType("datetime2");

            entity.Property(e => e.updated_date)
                  .HasColumnType("datetime2");
            entity.ToTable("GenericProduct");
        }
    }
}

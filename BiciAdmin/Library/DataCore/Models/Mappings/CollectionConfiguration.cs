﻿using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Microsoft.EntityFrameworkCore;

namespace DataCore.Models.Mappings
{
    public class CollectionConfiguration : IEntityTypeConfiguration<Collection>
    {

        public void Configure(EntityTypeBuilder<Collection> entity)
        {
            // Define table name
            //entity.ToTable("Collection");

            // Config key
            entity.HasKey(e => e.id);

            // Config columns
            entity.Property(col => col.id)
                    .IsRequired()
                    .ValueGeneratedOnAdd();

            entity.Property(e => e.name)
                    .IsUnicode()
                    .IsRequired()
                    .HasMaxLength(100);

            entity.Property(e => e.priority)
                    .IsRequired();

            entity.Property(e => e.status)
                    .IsRequired();

            entity.Property(e => e.type)
                    .IsRequired();

            entity.Property(e => e.limit_product)
                    .IsRequired();

            entity.Property(e => e.display_column)
                    .IsRequired();
            entity.Property(e => e.ref_web_id);

            entity.Property(e => e.created_by)
                    .HasMaxLength(50)
                    .IsRequired()
                    .IsUnicode(false);

            entity.Property(e => e.updated_by)
                    .HasMaxLength(50)
                    .IsUnicode(false);

            entity.Property(e => e.created_date)
                  .HasColumnType("datetime2");

            entity.Property(e => e.updated_date)
                  .HasColumnType("datetime2");

            entity.ToTable("Collection");
        }
    }
}

using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Microsoft.EntityFrameworkCore;

namespace DataCore.Models.Mappings
{
    public class CollectionBlogConfiguration : IEntityTypeConfiguration<CollectionBlog>
    {

        public void Configure(EntityTypeBuilder<CollectionBlog> entity)
        {
            // Define Columns
             entity.HasKey(e => e.id);
            entity.Property(e => e.id).IsRequired().ValueGeneratedOnAdd();
            entity.Property(e => e.collection_id);
            entity.Property(e => e.blog_id);
            entity.Property(e => e.ref_collection_id);
            entity.Property(e => e.ref_blog_id);

            entity.ToTable("CollectionBlog");
        }
    }
}

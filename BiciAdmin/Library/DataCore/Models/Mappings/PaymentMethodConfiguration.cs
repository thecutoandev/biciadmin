using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Microsoft.EntityFrameworkCore;

namespace DataCore.Models.Mappings
{
    public class PaymentMethodConfiguration : IEntityTypeConfiguration<PaymentMethod>
    {

        public void Configure(EntityTypeBuilder<PaymentMethod> entity)
        {
            // Define Columns
             entity.HasKey(e => e.id);
            entity.Property(e => e.id).IsRequired().ValueGeneratedOnAdd();
            entity.Property(e => e.name).IsUnicode().HasMaxLength(1000);
            entity.Property(e => e.description).IsUnicode().HasMaxLength(2000);
            entity.Property(e => e.is_cod);
            entity.Property(e => e.cod_fee_amount).HasColumnType("decimal");

            entity.Property(e => e.created_by)
                    .HasMaxLength(50)
                    .IsRequired()
                    .IsUnicode(false);

            entity.Property(e => e.updated_by)
                    .HasMaxLength(50)
                    .IsUnicode(false);

            entity.Property(e => e.created_date)
                  .HasColumnType("datetime2");

            entity.Property(e => e.updated_date)
                  .HasColumnType("datetime2");
            entity.ToTable("PaymentMethod");
        }
    }
}

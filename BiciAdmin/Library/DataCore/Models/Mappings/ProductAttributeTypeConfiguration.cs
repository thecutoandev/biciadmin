using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Microsoft.EntityFrameworkCore;

namespace DataCore.Models.Mappings
{
    public class ProductAttributeTypeConfiguration : IEntityTypeConfiguration<ProductAttributeType>
    {

        public void Configure(EntityTypeBuilder<ProductAttributeType> entity)
        {
            // Define Columns
            entity.HasKey(e => e.id);
            entity.Property(e => e.id).IsRequired();
            entity.Property(e => e.name).IsUnicode().HasMaxLength(100);

            entity.Property(e => e.created_by)
                    .HasMaxLength(50)
                    .IsRequired()
                    .IsUnicode(false);

            entity.Property(e => e.updated_by)
                    .HasMaxLength(50)
                    .IsUnicode(false);

            entity.Property(e => e.created_date)
                  .HasColumnType("datetime2");

            entity.Property(e => e.updated_date)
                  .HasColumnType("datetime2");
            entity.ToTable("ProductAttributeType");
        }
    }
}

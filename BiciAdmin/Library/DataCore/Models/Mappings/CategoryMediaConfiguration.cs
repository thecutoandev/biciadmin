using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Microsoft.EntityFrameworkCore;

namespace DataCore.Models.Mappings
{
    public class CategoryMediaConfiguration : IEntityTypeConfiguration<CategoryMedia>
    {

        public void Configure(EntityTypeBuilder<CategoryMedia> entity)
        {
            // Define Columns
             entity.HasKey(e => e.id);
            entity.Property(e => e.id).IsRequired().ValueGeneratedOnAdd();
            entity.Property(e => e.category_id);
            entity.Property(e => e.image_link).IsUnicode();


            entity.ToTable("CategoryMedia");
        }
    }
}

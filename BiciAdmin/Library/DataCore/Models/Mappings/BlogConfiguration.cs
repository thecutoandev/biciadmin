using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Microsoft.EntityFrameworkCore;

namespace DataCore.Models.Mappings
{
    public class BlogConfiguration : IEntityTypeConfiguration<Blog>
    {

        public void Configure(EntityTypeBuilder<Blog> entity)
        {
            // Define Columns
            entity.HasKey(e => e.id);
            entity.Property(e => e.id).IsRequired().ValueGeneratedOnAdd();
            entity.Property(e => e.ref_web_id).IsRequired(true);
            entity.Property(e => e.name).IsUnicode().HasMaxLength(300);
            entity.Property(e => e.content).IsUnicode();
            entity.Property(e => e.link).IsUnicode();

            entity.Property(e => e.created_by)
                    .HasMaxLength(50)
                    .IsRequired()
                    .IsUnicode(false);

            entity.Property(e => e.updated_by)
                    .HasMaxLength(50)
                    .IsUnicode(false);

            entity.Property(e => e.created_date)
                  .HasColumnType("datetime2");

            entity.Property(e => e.updated_date)
                  .HasColumnType("datetime2");
            entity.ToTable("Blog");
        }
    }
}

using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Microsoft.EntityFrameworkCore;

namespace DataCore.Models.Mappings
{
    public class ProductConfiguration : IEntityTypeConfiguration<Product>
    {

        public void Configure(EntityTypeBuilder<Product> entity)
        {
            // Define Columns
            entity.HasKey(e => e.id);
            entity.HasIndex(e => new { e.generic_product_id, e.ref_web_id })
                  .HasDatabaseName("Product_Variant_Index");
            entity.Property(e => e.id).IsRequired().ValueGeneratedOnAdd();
            entity.Property(e => e.product_code).HasMaxLength(100);
            entity.Property(e => e.product_name).IsUnicode().HasMaxLength(2000);
            entity.Property(e => e.product_price).HasColumnType("decimal");
            entity.Property(e => e.selling_price).HasColumnType("decimal");
            entity.Property(e => e.generic_product_id);
            entity.Property(e => e.link).IsUnicode();
            entity.Property(e => e.kiot_product_code).IsUnicode().HasMaxLength(100);
            entity.Property(e => e.ref_web_id);

            entity.Property(e => e.created_by)
                    .HasMaxLength(50)
                    .IsRequired()
                    .IsUnicode(false);

            entity.Property(e => e.updated_by)
                    .HasMaxLength(50)
                    .IsUnicode(false);

            entity.Property(e => e.created_date)
                  .HasColumnType("datetime2");

            entity.Property(e => e.updated_date)
                  .HasColumnType("datetime2");
            entity.ToTable("Product");
        }
    }
}

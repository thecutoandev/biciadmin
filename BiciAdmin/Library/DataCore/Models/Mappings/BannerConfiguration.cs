﻿using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Microsoft.EntityFrameworkCore;

namespace DataCore.Models.Mappings
{
    public class BannerConfiguration : IEntityTypeConfiguration<Banner>
    {
        public void Configure(EntityTypeBuilder<Banner> entity)
        {
            // Define Columns
            entity.HasKey(e => e.id);
            entity.Property(e => e.id).IsRequired().ValueGeneratedOnAdd();

            entity.Property(e => e.image)
                    .HasMaxLength(1000)
                    .IsRequired()
                    .IsUnicode(true);

            entity.Property(e => e.url)
                    .HasMaxLength(1000)
                    .IsRequired()
                    .IsUnicode(true);

            entity.Property(e => e.type).IsRequired();

            entity.Property(e => e.isActive).IsRequired();

            entity.Property(e => e.created_by)
                    .HasMaxLength(50)
                    .IsRequired()
                    .IsUnicode(false);

            entity.Property(e => e.updated_by)
                    .HasMaxLength(50)
                    .IsUnicode(false);

            entity.Property(e => e.created_date)
                  .HasColumnType("datetime2");

            entity.Property(e => e.updated_date)
                  .HasColumnType("datetime2");

            entity.ToTable("Banner");
        }
    }
}

using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Microsoft.EntityFrameworkCore;

namespace DataCore.Models.Mappings
{
    public class BlogCollectionConfiguration : IEntityTypeConfiguration<BlogCollection>
    {

        public void Configure(EntityTypeBuilder<BlogCollection> entity)
        {
            // Define Columns
             entity.HasKey(e => e.id);
            entity.Property(e => e.id).IsRequired().ValueGeneratedOnAdd();
            entity.Property(e => e.name).IsUnicode().HasMaxLength(100);
            entity.Property(e => e.image_link).IsUnicode();
            entity.Property(e => e.ref_web_id).IsRequired(true);
            entity.Property(e => e.parent_collection_id).IsRequired(true);


            entity.ToTable("BlogCollection");
        }
    }
}

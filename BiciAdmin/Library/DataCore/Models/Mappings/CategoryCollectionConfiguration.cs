﻿using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Microsoft.EntityFrameworkCore;

namespace DataCore.Models.Mappings
{
    public class CategoryCollectionConfiguration: IEntityTypeConfiguration<CategoryCollection>
    {
        public void Configure(EntityTypeBuilder<CategoryCollection> entity)
        {
            // Define Columns
            entity.HasKey(e => e.id);
            entity.Property(e => e.id).IsRequired().ValueGeneratedOnAdd();
            entity.Property(e => e.category_id);
            entity.Property(e => e.product_type_id);
            entity.Property(e => e.product_id);
            entity.Property(e => e.position);
            entity.Property(e => e.sort_value);

            entity.ToTable("CategoryCollection");
        }
    }
}

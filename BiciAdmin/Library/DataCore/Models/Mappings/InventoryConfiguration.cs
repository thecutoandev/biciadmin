using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Microsoft.EntityFrameworkCore;

namespace DataCore.Models.Mappings
{
    public class InventoryConfiguration : IEntityTypeConfiguration<Inventory>
    {

        public void Configure(EntityTypeBuilder<Inventory> entity)
        {
            // Define Columns
             entity.HasKey(e => e.id);
            entity.Property(e => e.id).IsRequired().ValueGeneratedOnAdd();
            entity.Property(e => e.name).IsUnicode().HasMaxLength(100);
            entity.Property(e => e.address).IsUnicode().HasMaxLength(200);


            entity.ToTable("Inventory");
        }
    }
}

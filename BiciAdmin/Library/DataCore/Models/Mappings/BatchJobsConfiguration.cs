﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace DataCore.Models.Mappings
{
    public class BatchJobsConfiguration : IEntityTypeConfiguration<BatchJobs>
    {
        public void Configure(EntityTypeBuilder<BatchJobs> entity)
        {
            // Define Columns
            entity.HasKey(e => e.id);
            entity.Property(e => e.id).IsRequired().ValueGeneratedOnAdd();
            entity.Property(e => e.JobName).IsUnicode().HasMaxLength(100);
            entity.Property(e => e.BeginAt).HasColumnType("datetime");
            entity.Property(e => e.EndAt).HasColumnType("datetime");
            entity.Property(e => e.Status).HasColumnType("tinyint");
            entity.Property(e => e.NumberOfRecords).HasColumnType("bigint");
            entity.Property(e => e.SuccessRecords).HasColumnType("bigint");

            entity.ToTable("BatchJobs");
        }
    }
}

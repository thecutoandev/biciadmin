using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Microsoft.EntityFrameworkCore;

namespace DataCore.Models.Mappings
{
    public class CollectionMediaConfiguration : IEntityTypeConfiguration<CollectionMedia>
    {

        public void Configure(EntityTypeBuilder<CollectionMedia> entity)
        {
            // Define Columns
             entity.HasKey(e => e.id);
            entity.Property(e => e.id).IsRequired().ValueGeneratedOnAdd();
            entity.Property(e => e.collection_id);
            entity.Property(e => e.image_link).IsUnicode();


            entity.ToTable("CollectionMedia");
        }
    }
}

using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Microsoft.EntityFrameworkCore;

namespace DataCore.Models.Mappings
{
    public class UserAddressConfiguration : IEntityTypeConfiguration<UserAddress>
    {

        public void Configure(EntityTypeBuilder<UserAddress> entity)
        {
            // Define Columns
            entity.HasKey(e => e.id);
            entity.HasIndex(e => new { e.receiver_phoneno, e.receiver_name });
            entity.Property(e => e.id).IsRequired().ValueGeneratedOnAdd();
            entity.Property(e => e.username).HasMaxLength(256);
            entity.Property(e => e.location_type).HasColumnType("tinyint");
            entity.Property(e => e.address).IsUnicode().HasMaxLength(1000);
            entity.Property(e => e.receiver_name).IsUnicode().HasMaxLength(1000);
            entity.Property(e => e.receiver_phoneno).IsUnicode().HasMaxLength(50);
            entity.Property(e => e.district_id);
            entity.Property(e => e.city_id);
            entity.Property(e => e.isdefault);

            entity.Property(e => e.created_by)
                    .HasMaxLength(50)
                    .IsRequired()
                    .IsUnicode(false);

            entity.Property(e => e.updated_by)
                    .HasMaxLength(50)
                    .IsUnicode(false);

            entity.Property(e => e.created_date)
                  .HasColumnType("datetime2");

            entity.Property(e => e.updated_date)
                  .HasColumnType("datetime2");
            entity.ToTable("UserAddress");
        }
    }
}

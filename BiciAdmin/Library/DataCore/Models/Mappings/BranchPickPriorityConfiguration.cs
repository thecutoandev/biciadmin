﻿using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Microsoft.EntityFrameworkCore;
namespace DataCore.Models.Mappings
{
    public class BranchPickPriorityConfiguration : IEntityTypeConfiguration<BranchPickPriority>
    {
        public void Configure(EntityTypeBuilder<BranchPickPriority> entity)
        {
            // Define Columns
            entity.HasKey(e => e.id);
            entity.Property(e => e.id).IsRequired().ValueGeneratedOnAdd();
            entity.Property(e => e.branch_id).IsRequired(true);
            entity.Property(e => e.priority).IsRequired(true);
            entity.Property(e => e.max_auto_pick).IsRequired(true);

            entity.ToTable("BranchPriority");
        }
    }
}

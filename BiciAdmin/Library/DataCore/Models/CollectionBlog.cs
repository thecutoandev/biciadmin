using System;

namespace DataCore.Models
{
    public class CollectionBlog 
    {
        public int id {get; set;}
        public int collection_id {get; set;}
        public int blog_id {get; set;}
        public int ref_collection_id { get; set; } = 0;
        public int ref_blog_id { get; set; } = 0;
    }
}
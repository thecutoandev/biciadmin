﻿using System;
namespace DataCore.Models
{
    public class ProductType : BaseModel
    {
        public int id { get; set; }
        public string name { get; set; }
    }
}

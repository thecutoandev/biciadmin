using System;

namespace DataCore.Models
{
    public class District 
    {
        public int id {get; set;}
        public string name {get; set;}
        public int city_id {get; set;}

    }
}
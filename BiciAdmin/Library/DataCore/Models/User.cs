﻿using Microsoft.AspNetCore.Identity;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DataCore.Models
{
    public class User : IdentityUser
    {
        [Column(TypeName = "varchar(50)")]
        [StringLength(50)]
        public string FBEmail { get; set; }
        [Column(TypeName = "nvarchar(max)")]
        public string Avatar { get; set; }
        [Column(TypeName = "tinyint")]
        public byte Gender { get; set; }
        [Column(TypeName = "datetime2")]
        public DateTime? BirthDay { get; set; }
        [Column(TypeName = "int")]
        public int RoleId { get; set; }
        [Column(TypeName ="nvarchar(200)")]
        public string FullName { get; set; }
        [Column(TypeName = "bigint")]
        public long BranchId { get; set; }
        [Column(TypeName = "tinyint")]
        public byte Status { get; set; } // 1: actived, 2: deactived, 3: deleted
    }
}

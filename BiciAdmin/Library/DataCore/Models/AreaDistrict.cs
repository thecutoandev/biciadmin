using System;

namespace DataCore.Models
{
    public class AreaDistrict 
    {
        public int id {get; set;}
        public int area_id {get; set;}
        public int district_id {get; set;}

    }
}
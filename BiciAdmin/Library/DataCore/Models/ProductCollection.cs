using System;

namespace DataCore.Models
{
    public class ProductCollection : BaseModel
    {
        public int id {get; set;}
        public int collection_id {get; set;}
        public int product_id {get; set;}

    }
}
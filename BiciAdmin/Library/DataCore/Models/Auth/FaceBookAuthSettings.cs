﻿using System;
using Newtonsoft.Json;
namespace DataCore.Models.Auth
{
    public class FaceBookAuthSettings
    {
        public string AppId { get; set; }
        public string AppSecret { get; set; }
        public string AppTokenEndPoint { get; set; }
        public string ValidateTokenEndPoint { get; set; }
        public string UserInfoEndPoint { get; set; }
    }
}

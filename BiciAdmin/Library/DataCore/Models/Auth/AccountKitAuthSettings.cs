﻿using System;
namespace DataCore.Models.Auth
{
    public class AccountKitAuthSettings
    {
        public string AuthorizeTokenEndPoint { get; set; }
    }
}

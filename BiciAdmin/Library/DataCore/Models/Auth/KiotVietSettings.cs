﻿using System;
namespace DataCore.Models.Auth
{
    public class KiotVietSettings
    {
        public string TokenEndPoint { get; set; }
        public string PublicAPIEndPoint { get; set; }
        public string ClientId { get; set; }
        public string ClientSecret { get; set; }
        public string Retail { get; set; }
        public long CashierId { get; set; }
        public TokenInfo TokenGlobal { get; set; }
    }

    public class TokenInfo
    {
        public string access_token { get; set; }
        public int expires_in { get; set; }
        public string token_type { get; set; }
        public DateTime token_time { get; set; }
    }
}

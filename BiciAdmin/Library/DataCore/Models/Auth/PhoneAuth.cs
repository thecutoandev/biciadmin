﻿using System;
using Newtonsoft.Json;
namespace DataCore.Models.Auth
{
    public class PhoneAuth
    {
        [JsonProperty("id")]
        public string Id { get; set; }
        [JsonProperty("application")]
        public AccountKitApplication Application { get; set; }
        [JsonProperty("phone")]
        public AccountKitPhone Phone { get; set; }
    }

    public class AccountKitApplication
    {
        [JsonProperty("id")]
        public string Id { get; set; }
    }

    public class AccountKitPhone
    {
        [JsonProperty("number")]
        public string Number { get; set; }
        [JsonProperty("country_prefix")]
        public string CountryCode { get; set; }
        [JsonProperty("national_number")]
        public string NationalNumber { get; set; }
    }
}

using System;

namespace DataCore.Models
{
    public class CollectionMedia 
    {
        public int id {get; set;}
        public int collection_id {get; set;}
        public string image_link {get; set;}

    }
}
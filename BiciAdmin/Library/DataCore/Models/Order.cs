using System;

namespace DataCore.Models
{
    public class Order : BaseModel
    {
        public int id {get; set;}
        public string code {get; set;}
        public string bank_transfer_code {get; set;}
        public string user_id {get; set;}
        public int user_address_id {get; set;}
        public int payment_method_id {get; set;}
        public string note {get; set;}
        public string source {get; set;}
        public string title { get; set; }
        public string image { get; set; }
        // 1: wait for processing, 2: Processing, 3: Prepare for delivery, 4: completed
        public byte status { get; set; }
        public long branch_id { get; set; }
        public string employee { get; set; }
        public long ref_web_id { get; set; }
        public Order()
        {
            status = 1;
        }
    }
}
﻿using System;
namespace DataCore.Models
{
    public class Banner : BaseModel
    {
        public int id { get; set; }
        public string image { get; set; }
        public byte type { get; set; }
        public string url { get; set; }
        public bool  isActive { get; set; }
    }
}

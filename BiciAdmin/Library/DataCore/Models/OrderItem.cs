using System;

namespace DataCore.Models
{
    public class OrderItem 
    {
        public int id {get; set;}
        public int order_id {get; set;}
        public int product_id {get; set;}
        public int quantity {get; set;}
        public decimal unit_price {get; set;}
        public decimal amount {get; set;}

    }
}
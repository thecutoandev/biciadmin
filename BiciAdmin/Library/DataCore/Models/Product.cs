using System;

namespace DataCore.Models
{
    public class Product : BaseModel
    {
        public int id {get; set;}
        public string product_code {get; set;}
        public string product_name {get; set;}
        public decimal product_price {get; set;}
        public decimal selling_price { get; set; }
        public int generic_product_id {get; set;}
        public string link {get; set;}
        public string kiot_product_code {get; set;}
        public int ref_web_id { get; set; } = 0;

    }
}
using System;

namespace DataCore.Models
{
    public class ProductRating 
    {
        public int id {get; set;}
        public int product_id {get; set;}
        public int order_id {get; set;}
        public decimal rating_product_star {get; set;}
        public string rating_content {get; set;}

    }
}
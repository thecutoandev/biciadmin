﻿using System;
namespace DataCore.Models
{
    public class BranchPickPriority
    {
        public int id { get; set; }
        public long branch_id { get; set; }
        public int priority { get; set; }
        public int max_auto_pick { get; set; }
    }
}

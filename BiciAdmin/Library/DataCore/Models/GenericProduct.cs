using System;

namespace DataCore.Models
{
    public class GenericProduct : BaseModel
    {
        public int id {get; set;}
        public string name {get; set;}
        public int category_id {get; set;}
        public int brand_id {get; set;}
        public string product_name {get; set;}
        public string product_content {get; set;}
        public decimal product_price {get; set;}
        public string link {get; set;}
        public bool is_free_ship {get; set;}
        public int ref_web_id { get; set; }
        public string short_description { get; set; }
        public bool hidden { get; set; }
        public string product_type { get; set; }
        public GenericProduct()
        {
            hidden = false;
        }
    }
}
using System;

namespace DataCore.Models
{
    public class Blog : BaseModel
    {
        public int id {get; set;}
        public string name {get; set;}
        public string content {get; set;}
        public string link {get; set;}
        public int ref_web_id { get; set; } = 0;
    }
}
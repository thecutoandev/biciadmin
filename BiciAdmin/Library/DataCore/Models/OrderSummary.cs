using System;

namespace DataCore.Models
{
    public class OrderSummary 
    {
        public int id {get; set;}
        public int order_id {get; set;}
        public decimal sub_total {get; set;}
        public decimal shipping_fee_amount {get; set;}
        public decimal service_fee_amount {get; set;}
        public decimal rating_order_star {get; set;}
        public string rating_content {get; set;}
        public decimal total_amount {get; set;}
        public string note { get; set; }
        public string approver { get; set; }
    }
}
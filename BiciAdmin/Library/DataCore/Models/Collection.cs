﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DataCore.Models
{
    public class Collection : BaseModel
    {
        public int id { get; set; }
        public string name { get; set; }
        public int priority { get; set; }
        public byte status { get; set; }
        public byte type { get; set; }
        public int limit_product { get; set; }
        public int display_column { get; set; }
        public int ref_web_id { get; set; }
    }
}

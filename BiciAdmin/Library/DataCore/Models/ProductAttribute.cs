using System;

namespace DataCore.Models
{
    public class ProductAttribute 
    {
        public int id {get; set;}
        public int product_id {get; set;}
        public int attribute_id {get; set;}
        public string value {get; set;}

    }
}
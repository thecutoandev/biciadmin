﻿Migration data 
// 2019-07-01
 => dotnet ef migrations add AddNewFieldForBlogColletion --startup-project ../../Sites/WebAPI

 // 2019-07-02
 => dotnet ef migrations add AddRefWebIDForBlog --startup-project ../../Sites/WebAPI

// 2019-07-02
 => dotnet ef migrations add AddRefWebIDForCollectionBlog --startup-project ../../Sites/WebAPI
// 2019-07-09
 => dotnet ef migrations add AddRefWebIDForGenericProduct --startup-project ../../Sites/WebAPI
 => dotnet ef migrations add AddRefWebIDForGenericProductMedia --startup-project ../../Sites/WebAPI
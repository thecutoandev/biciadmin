﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace DataCore.Migrations
{
    public partial class InitDatabase : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Area",
                columns: table => new
                {
                    id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    created_date = table.Column<DateTime>(type: "datetime2", nullable: true),
                    created_by = table.Column<string>(unicode: false, maxLength: 50, nullable: false),
                    updated_date = table.Column<DateTime>(type: "datetime2", nullable: true),
                    updated_by = table.Column<string>(unicode: false, maxLength: 50, nullable: true),
                    name = table.Column<string>(maxLength: 100, nullable: true),
                    limit_transaction_amount = table.Column<decimal>(type: "decimal", nullable: false),
                    fee_amount = table.Column<decimal>(type: "decimal", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Area", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "AreaDistrict",
                columns: table => new
                {
                    id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    area_id = table.Column<int>(nullable: false),
                    district_id = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AreaDistrict", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "Banner",
                columns: table => new
                {
                    id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    created_date = table.Column<DateTime>(type: "datetime2", nullable: true),
                    created_by = table.Column<string>(unicode: false, maxLength: 50, nullable: false),
                    updated_date = table.Column<DateTime>(type: "datetime2", nullable: true),
                    updated_by = table.Column<string>(unicode: false, maxLength: 50, nullable: true),
                    image = table.Column<string>(maxLength: 1000, nullable: false),
                    type = table.Column<byte>(nullable: false),
                    url = table.Column<string>(maxLength: 1000, nullable: false),
                    isActive = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Banner", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "BatchJobs",
                columns: table => new
                {
                    id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    BeginAt = table.Column<DateTime>(type: "datetime", nullable: false),
                    EndAt = table.Column<DateTime>(type: "datetime", nullable: false),
                    Status = table.Column<byte>(type: "tinyint", nullable: false),
                    NumberOfRecords = table.Column<long>(type: "bigint", nullable: false),
                    SuccessRecords = table.Column<long>(type: "bigint", nullable: false),
                    JobName = table.Column<string>(maxLength: 100, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BatchJobs", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "Blog",
                columns: table => new
                {
                    id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    created_date = table.Column<DateTime>(type: "datetime2", nullable: true),
                    created_by = table.Column<string>(unicode: false, maxLength: 50, nullable: false),
                    updated_date = table.Column<DateTime>(type: "datetime2", nullable: true),
                    updated_by = table.Column<string>(unicode: false, maxLength: 50, nullable: true),
                    name = table.Column<string>(maxLength: 300, nullable: true),
                    content = table.Column<string>(nullable: true),
                    link = table.Column<string>(nullable: true),
                    ref_web_id = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Blog", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "BlogCollection",
                columns: table => new
                {
                    id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    name = table.Column<string>(maxLength: 100, nullable: true),
                    image_link = table.Column<string>(nullable: true),
                    ref_web_id = table.Column<int>(nullable: false),
                    parent_collection_id = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BlogCollection", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "BranchPriority",
                columns: table => new
                {
                    id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    branch_id = table.Column<long>(nullable: false),
                    priority = table.Column<int>(nullable: false),
                    max_auto_pick = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BranchPriority", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "Brand",
                columns: table => new
                {
                    id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    created_date = table.Column<DateTime>(type: "datetime2", nullable: true),
                    created_by = table.Column<string>(unicode: false, maxLength: 50, nullable: false),
                    updated_date = table.Column<DateTime>(type: "datetime2", nullable: true),
                    updated_by = table.Column<string>(unicode: false, maxLength: 50, nullable: true),
                    name = table.Column<string>(maxLength: 2000, nullable: true),
                    ref_brand_id = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Brand", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "Category",
                columns: table => new
                {
                    id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    created_date = table.Column<DateTime>(type: "datetime2", nullable: true),
                    created_by = table.Column<string>(unicode: false, maxLength: 50, nullable: false),
                    updated_date = table.Column<DateTime>(type: "datetime2", nullable: true),
                    updated_by = table.Column<string>(unicode: false, maxLength: 50, nullable: true),
                    name = table.Column<string>(maxLength: 2000, nullable: true),
                    parent_category_id = table.Column<int>(nullable: false),
                    status = table.Column<byte>(type: "tinyint", nullable: false),
                    ref_category_id = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Category", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "CategoryCollection",
                columns: table => new
                {
                    id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    category_id = table.Column<int>(nullable: false),
                    product_type_id = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CategoryCollection", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "CategoryMedia",
                columns: table => new
                {
                    id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    category_id = table.Column<int>(nullable: false),
                    image_link = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CategoryMedia", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "City",
                columns: table => new
                {
                    id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    name = table.Column<string>(maxLength: 100, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_City", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "Collection",
                columns: table => new
                {
                    id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    created_date = table.Column<DateTime>(type: "datetime2", nullable: true),
                    created_by = table.Column<string>(unicode: false, maxLength: 50, nullable: false),
                    updated_date = table.Column<DateTime>(type: "datetime2", nullable: true),
                    updated_by = table.Column<string>(unicode: false, maxLength: 50, nullable: true),
                    name = table.Column<string>(maxLength: 100, nullable: false),
                    priority = table.Column<int>(nullable: false),
                    status = table.Column<byte>(nullable: false),
                    type = table.Column<byte>(nullable: false),
                    limit_product = table.Column<int>(nullable: false),
                    display_column = table.Column<int>(nullable: false),
                    ref_web_id = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Collection", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "CollectionBlog",
                columns: table => new
                {
                    id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    collection_id = table.Column<int>(nullable: false),
                    blog_id = table.Column<int>(nullable: false),
                    ref_collection_id = table.Column<int>(nullable: false),
                    ref_blog_id = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CollectionBlog", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "CollectionMedia",
                columns: table => new
                {
                    id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    collection_id = table.Column<int>(nullable: false),
                    image_link = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CollectionMedia", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "District",
                columns: table => new
                {
                    id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    name = table.Column<string>(maxLength: 100, nullable: true),
                    city_id = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_District", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "GenericProduct",
                columns: table => new
                {
                    id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    created_date = table.Column<DateTime>(type: "datetime2", nullable: true),
                    created_by = table.Column<string>(unicode: false, maxLength: 50, nullable: false),
                    updated_date = table.Column<DateTime>(type: "datetime2", nullable: true),
                    updated_by = table.Column<string>(unicode: false, maxLength: 50, nullable: true),
                    name = table.Column<string>(nullable: true),
                    category_id = table.Column<int>(nullable: false),
                    brand_id = table.Column<int>(nullable: false),
                    product_name = table.Column<string>(nullable: true),
                    product_content = table.Column<string>(nullable: true),
                    product_price = table.Column<decimal>(type: "decimal", nullable: false),
                    link = table.Column<string>(nullable: true),
                    is_free_ship = table.Column<bool>(nullable: false),
                    ref_web_id = table.Column<int>(nullable: false),
                    short_description = table.Column<string>(nullable: true),
                    hidden = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_GenericProduct", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "GenericProductMedia",
                columns: table => new
                {
                    id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    generic_product_id = table.Column<int>(nullable: false),
                    image_link = table.Column<string>(nullable: true),
                    ref_wed_id = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_GenericProductMedia", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "Inventory",
                columns: table => new
                {
                    id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    name = table.Column<string>(maxLength: 100, nullable: true),
                    address = table.Column<string>(maxLength: 200, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Inventory", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "Order",
                columns: table => new
                {
                    id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    created_date = table.Column<DateTime>(type: "datetime2", nullable: true),
                    created_by = table.Column<string>(unicode: false, maxLength: 50, nullable: false),
                    updated_date = table.Column<DateTime>(type: "datetime2", nullable: true),
                    updated_by = table.Column<string>(unicode: false, maxLength: 50, nullable: true),
                    code = table.Column<string>(maxLength: 100, nullable: true),
                    bank_transfer_code = table.Column<string>(maxLength: 1000, nullable: true),
                    user_id = table.Column<string>(maxLength: 256, nullable: true),
                    user_address_id = table.Column<int>(nullable: false),
                    payment_method_id = table.Column<int>(nullable: false),
                    note = table.Column<string>(nullable: true),
                    source = table.Column<string>(nullable: true),
                    title = table.Column<string>(nullable: true),
                    image = table.Column<string>(nullable: true),
                    status = table.Column<byte>(nullable: false),
                    branch_id = table.Column<long>(nullable: false),
                    employee = table.Column<string>(maxLength: 450, nullable: true),
                    ref_web_id = table.Column<long>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Order", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "OrderItem",
                columns: table => new
                {
                    id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    order_id = table.Column<int>(nullable: false),
                    product_id = table.Column<int>(nullable: false),
                    quantity = table.Column<int>(nullable: false),
                    unit_price = table.Column<decimal>(type: "decimal", nullable: false),
                    amount = table.Column<decimal>(type: "decimal", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_OrderItem", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "OrderSummary",
                columns: table => new
                {
                    id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    order_id = table.Column<int>(nullable: false),
                    sub_total = table.Column<decimal>(type: "decimal", nullable: false),
                    shipping_fee_amount = table.Column<decimal>(type: "decimal", nullable: false),
                    service_fee_amount = table.Column<decimal>(type: "decimal", nullable: false),
                    rating_order_star = table.Column<decimal>(type: "decimal", nullable: false),
                    rating_content = table.Column<string>(nullable: true),
                    total_amount = table.Column<decimal>(type: "decimal", nullable: false),
                    note = table.Column<string>(nullable: true),
                    approver = table.Column<string>(unicode: false, maxLength: 200, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_OrderSummary", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "PaymentMethod",
                columns: table => new
                {
                    id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    created_date = table.Column<DateTime>(type: "datetime2", nullable: true),
                    created_by = table.Column<string>(unicode: false, maxLength: 50, nullable: false),
                    updated_date = table.Column<DateTime>(type: "datetime2", nullable: true),
                    updated_by = table.Column<string>(unicode: false, maxLength: 50, nullable: true),
                    name = table.Column<string>(maxLength: 1000, nullable: true),
                    description = table.Column<string>(maxLength: 2000, nullable: true),
                    is_cod = table.Column<bool>(nullable: false),
                    cod_fee_amount = table.Column<decimal>(type: "decimal", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PaymentMethod", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "Product",
                columns: table => new
                {
                    id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    created_date = table.Column<DateTime>(type: "datetime2", nullable: true),
                    created_by = table.Column<string>(unicode: false, maxLength: 50, nullable: false),
                    updated_date = table.Column<DateTime>(type: "datetime2", nullable: true),
                    updated_by = table.Column<string>(unicode: false, maxLength: 50, nullable: true),
                    product_code = table.Column<string>(maxLength: 100, nullable: true),
                    product_name = table.Column<string>(maxLength: 2000, nullable: true),
                    product_price = table.Column<decimal>(type: "decimal", nullable: false),
                    selling_price = table.Column<decimal>(type: "decimal", nullable: false),
                    generic_product_id = table.Column<int>(nullable: false),
                    link = table.Column<string>(nullable: true),
                    kiot_product_code = table.Column<string>(maxLength: 100, nullable: true),
                    ref_web_id = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Product", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "ProductAttribute",
                columns: table => new
                {
                    id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    product_id = table.Column<int>(nullable: false),
                    attribute_id = table.Column<int>(nullable: false),
                    value = table.Column<string>(maxLength: 200, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ProductAttribute", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "ProductAttributeType",
                columns: table => new
                {
                    id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    created_date = table.Column<DateTime>(type: "datetime2", nullable: true),
                    created_by = table.Column<string>(unicode: false, maxLength: 50, nullable: false),
                    updated_date = table.Column<DateTime>(type: "datetime2", nullable: true),
                    updated_by = table.Column<string>(unicode: false, maxLength: 50, nullable: true),
                    name = table.Column<string>(maxLength: 100, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ProductAttributeType", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "ProductCollection",
                columns: table => new
                {
                    id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    created_date = table.Column<DateTime>(type: "datetime2", nullable: true),
                    created_by = table.Column<string>(unicode: false, maxLength: 50, nullable: false),
                    updated_date = table.Column<DateTime>(type: "datetime2", nullable: true),
                    updated_by = table.Column<string>(unicode: false, maxLength: 50, nullable: true),
                    collection_id = table.Column<int>(nullable: false),
                    product_id = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ProductCollection", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "ProductMedia",
                columns: table => new
                {
                    id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    product_id = table.Column<int>(nullable: false),
                    image_link = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ProductMedia", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "ProductRating",
                columns: table => new
                {
                    id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    product_id = table.Column<int>(nullable: false),
                    order_id = table.Column<int>(nullable: false),
                    rating_product_star = table.Column<decimal>(type: "decimal", nullable: false),
                    rating_content = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ProductRating", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "ProductType",
                columns: table => new
                {
                    id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    created_date = table.Column<DateTime>(type: "datetime2", nullable: true),
                    created_by = table.Column<string>(unicode: false, maxLength: 50, nullable: false),
                    updated_date = table.Column<DateTime>(type: "datetime2", nullable: true),
                    updated_by = table.Column<string>(unicode: false, maxLength: 50, nullable: true),
                    name = table.Column<string>(maxLength: 1000, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ProductType", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "Roles",
                columns: table => new
                {
                    Id = table.Column<string>(nullable: false),
                    Name = table.Column<string>(maxLength: 256, nullable: true),
                    NormalizedName = table.Column<string>(maxLength: 256, nullable: true),
                    ConcurrencyStamp = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Roles", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "UserAddress",
                columns: table => new
                {
                    id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    created_date = table.Column<DateTime>(type: "datetime2", nullable: true),
                    created_by = table.Column<string>(unicode: false, maxLength: 50, nullable: false),
                    updated_date = table.Column<DateTime>(type: "datetime2", nullable: true),
                    updated_by = table.Column<string>(unicode: false, maxLength: 50, nullable: true),
                    username = table.Column<string>(maxLength: 256, nullable: true),
                    location_type = table.Column<byte>(type: "tinyint", nullable: false),
                    address = table.Column<string>(maxLength: 1000, nullable: true),
                    receiver_name = table.Column<string>(maxLength: 1000, nullable: true),
                    receiver_phoneno = table.Column<string>(maxLength: 50, nullable: true),
                    district_id = table.Column<int>(nullable: false),
                    city_id = table.Column<int>(nullable: false),
                    isdefault = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UserAddress", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "UserRoles",
                columns: table => new
                {
                    Id = table.Column<string>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    NormalizedName = table.Column<string>(nullable: true),
                    ConcurrencyStamp = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UserRoles", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Users",
                columns: table => new
                {
                    Id = table.Column<string>(nullable: false),
                    UserName = table.Column<string>(maxLength: 256, nullable: true),
                    NormalizedUserName = table.Column<string>(maxLength: 256, nullable: true),
                    Email = table.Column<string>(maxLength: 256, nullable: true),
                    NormalizedEmail = table.Column<string>(maxLength: 256, nullable: true),
                    EmailConfirmed = table.Column<bool>(nullable: false),
                    PasswordHash = table.Column<string>(nullable: true),
                    SecurityStamp = table.Column<string>(nullable: true),
                    ConcurrencyStamp = table.Column<string>(nullable: true),
                    PhoneNumber = table.Column<string>(nullable: true),
                    PhoneNumberConfirmed = table.Column<bool>(nullable: false),
                    TwoFactorEnabled = table.Column<bool>(nullable: false),
                    LockoutEnd = table.Column<DateTimeOffset>(nullable: true),
                    LockoutEnabled = table.Column<bool>(nullable: false),
                    AccessFailedCount = table.Column<int>(nullable: false),
                    FBEmail = table.Column<string>(type: "varchar(50)", maxLength: 50, nullable: true),
                    Avatar = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Gender = table.Column<byte>(type: "tinyint", nullable: false),
                    BirthDay = table.Column<DateTime>(type: "datetime2", nullable: true),
                    RoleId = table.Column<int>(type: "int", nullable: false),
                    FullName = table.Column<string>(type: "nvarchar(200)", nullable: true),
                    BranchId = table.Column<long>(type: "bigint", nullable: false),
                    Status = table.Column<byte>(type: "tinyint", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Users", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "AspNetRoleClaims",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    RoleId = table.Column<string>(nullable: false),
                    ClaimType = table.Column<string>(nullable: true),
                    ClaimValue = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetRoleClaims", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AspNetRoleClaims_Roles_RoleId",
                        column: x => x.RoleId,
                        principalTable: "Roles",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUserRoles",
                columns: table => new
                {
                    UserId = table.Column<string>(nullable: false),
                    RoleId = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUserRoles", x => new { x.UserId, x.RoleId });
                    table.ForeignKey(
                        name: "FK_AspNetUserRoles_Roles_RoleId",
                        column: x => x.RoleId,
                        principalTable: "Roles",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_AspNetUserRoles_Users_UserId",
                        column: x => x.UserId,
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUserTokens",
                columns: table => new
                {
                    UserId = table.Column<string>(nullable: false),
                    LoginProvider = table.Column<string>(nullable: false),
                    Name = table.Column<string>(nullable: false),
                    Value = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUserTokens", x => new { x.UserId, x.LoginProvider, x.Name });
                    table.ForeignKey(
                        name: "FK_AspNetUserTokens_Users_UserId",
                        column: x => x.UserId,
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "UserClaims",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    UserId = table.Column<string>(nullable: false),
                    ClaimType = table.Column<string>(nullable: true),
                    ClaimValue = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UserClaims", x => x.Id);
                    table.ForeignKey(
                        name: "FK_UserClaims_Users_UserId",
                        column: x => x.UserId,
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "UserLogins",
                columns: table => new
                {
                    LoginProvider = table.Column<string>(nullable: false),
                    ProviderKey = table.Column<string>(nullable: false),
                    ProviderDisplayName = table.Column<string>(nullable: true),
                    UserId = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UserLogins", x => new { x.LoginProvider, x.ProviderKey });
                    table.ForeignKey(
                        name: "FK_UserLogins_Users_UserId",
                        column: x => x.UserId,
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.InsertData(
                table: "City",
                columns: new[] { "id", "name" },
                values: new object[,]
                {
                    { 1, "Thành phố Hà Nội" },
                    { 52, "Tỉnh Bình Định" },
                    { 54, "Tỉnh Phú Yên" },
                    { 56, "Tỉnh Khánh Hòa" },
                    { 58, "Tỉnh Ninh Thuận" },
                    { 60, "Tỉnh Bình Thuận" },
                    { 62, "Tỉnh Kon Tum" },
                    { 64, "Tỉnh Gia Lai" },
                    { 66, "Tỉnh Đắk Lắk" },
                    { 67, "Tỉnh Đắk Nông" },
                    { 68, "Tỉnh Lâm Đồng" },
                    { 70, "Tỉnh Bình Phước" },
                    { 72, "Tỉnh Tây Ninh" },
                    { 74, "Tỉnh Bình Dương" },
                    { 51, "Tỉnh Quảng Ngãi" },
                    { 75, "Tỉnh Đồng Nai" },
                    { 79, "Thành phố Hồ Chí Minh" },
                    { 80, "Tỉnh Long An" },
                    { 82, "Tỉnh Tiền Giang" },
                    { 84, "Tỉnh Trà Vinh" },
                    { 86, "Tỉnh Vĩnh Long" },
                    { 87, "Tỉnh Đồng Tháp" },
                    { 89, "Tỉnh An Giang" },
                    { 91, "Tỉnh Kiên Giang" },
                    { 92, "Thành phố Cần Thơ" },
                    { 93, "Tỉnh Hậu Giang" },
                    { 94, "Tỉnh Sóc Trăng" },
                    { 95, "Tỉnh Bạc Liêu" },
                    { 96, "Tỉnh Cà Mau" },
                    { 77, "Tỉnh Bà Rịa - Vũng Tàu" },
                    { 49, "Tỉnh Quảng Nam" },
                    { 83, "Tỉnh Bến Tre" },
                    { 46, "Tỉnh Thừa Thiên Huế" },
                    { 48, "Thành phố Đà Nẵng" },
                    { 2, "Tỉnh Hà Giang" },
                    { 4, "Tỉnh Cao Bằng" },
                    { 6, "Tỉnh Bắc Kạn" },
                    { 8, "Tỉnh Tuyên Quang" },
                    { 10, "Tỉnh Lào Cai" },
                    { 12, "Tỉnh Lai Châu" },
                    { 14, "Tỉnh Sơn La" },
                    { 15, "Tỉnh Yên Bái" },
                    { 17, "Tỉnh Hoà Bình" },
                    { 19, "Tỉnh Thái Nguyên" },
                    { 20, "Tỉnh Lạng Sơn" },
                    { 22, "Tỉnh Quảng Ninh" },
                    { 24, "Tỉnh Bắc Giang" },
                    { 11, "Tỉnh Điện Biên" },
                    { 26, "Tỉnh Vĩnh Phúc" },
                    { 45, "Tỉnh Quảng Trị" },
                    { 25, "Tỉnh Phú Thọ" },
                    { 44, "Tỉnh Quảng Bình" },
                    { 42, "Tỉnh Hà Tĩnh" },
                    { 40, "Tỉnh Nghệ An" },
                    { 37, "Tỉnh Ninh Bình" },
                    { 36, "Tỉnh Nam Định" },
                    { 38, "Tỉnh Thanh Hóa" },
                    { 34, "Tỉnh Thái Bình" },
                    { 33, "Tỉnh Hưng Yên" },
                    { 31, "Thành phố Hải Phòng" },
                    { 30, "Tỉnh Hải Dương" },
                    { 27, "Tỉnh Bắc Ninh" },
                    { 35, "Tỉnh Hà Nam" }
                });

            migrationBuilder.InsertData(
                table: "District",
                columns: new[] { "id", "city_id", "name" },
                values: new object[,]
                {
                    { 643, 66, "Thành phố Buôn Ma Thuột" },
                    { 649, 66, "Huyện Krông Búk" },
                    { 639, 64, "Huyện Chư Pưh" },
                    { 644, 66, "Thị Xã Buôn Hồ" },
                    { 645, 66, "Huyện Ea H'leo" },
                    { 646, 66, "Huyện Ea Súp" },
                    { 647, 66, "Huyện Buôn Đôn" },
                    { 648, 66, "Huyện Cư M'gar" },
                    { 650, 66, "Huyện Krông Năng" },
                    { 656, 66, "Huyện Lắk" },
                    { 652, 66, "Huyện M'Đrắk" },
                    { 653, 66, "Huyện Krông Bông" },
                    { 654, 66, "Huyện Krông Pắc" },
                    { 655, 66, "Huyện Krông A Na" },
                    { 657, 66, "Huyện Cư Kuin" },
                    { 660, 67, "Thị xã Gia Nghĩa" },
                    { 638, 64, "Huyện Phú Thiện" },
                    { 661, 67, "Huyện Đăk Glong" },
                    { 651, 66, "Huyện Ea Kar" },
                    { 637, 64, "Huyện Krông Pa" },
                    { 618, 62, "Huyện Ia H' Drai" },
                    { 634, 64, "Huyện Đăk Pơ" },
                    { 662, 67, "Huyện Cư Jút" },
                    { 613, 62, "Huyện Kon Plông" },
                    { 614, 62, "Huyện Kon Rẫy" },
                    { 615, 62, "Huyện Đắk Hà" },
                    { 616, 62, "Huyện Sa Thầy" },
                    { 617, 62, "Huyện Tu Mơ Rông" },
                    { 622, 64, "Thành phố Pleiku" },
                    { 623, 64, "Thị xã An Khê" },
                    { 635, 64, "Huyện Ia Pa" },
                    { 624, 64, "Thị xã Ayun Pa" },
                    { 626, 64, "Huyện Đăk Đoa" },
                    { 627, 64, "Huyện Chư Păh" },
                    { 628, 64, "Huyện Ia Grai" },
                    { 629, 64, "Huyện Mang Yang" },
                    { 630, 64, "Huyện Kông Chro" },
                    { 631, 64, "Huyện Đức Cơ" },
                    { 632, 64, "Huyện Chư Prông" },
                    { 633, 64, "Huyện Chư Sê" },
                    { 625, 64, "Huyện KBang" },
                    { 663, 67, "Huyện Đắk Mil" },
                    { 708, 72, "Huyện Châu Thành" },
                    { 665, 67, "Huyện Đắk Song" },
                    { 697, 70, "Huyện Chơn Thành" },
                    { 698, 70, "Huyện Phú Riềng" },
                    { 703, 72, "Thành phố Tây Ninh" },
                    { 705, 72, "Huyện Tân Biên" },
                    { 706, 72, "Huyện Tân Châu" },
                    { 707, 72, "Huyện Dương Minh Châu" },
                    { 612, 62, "Huyện Đắk Tô" },
                    { 709, 72, "Huyện Hòa Thành" },
                    { 696, 70, "Huyện Bù Đăng" },
                    { 710, 72, "Huyện Gò Dầu" },
                    { 712, 72, "Huyện Trảng Bàng" },
                    { 718, 74, "Thành phố Thủ Dầu Một" },
                    { 719, 74, "Huyện Bàu Bàng" },
                    { 720, 74, "Huyện Dầu Tiếng" },
                    { 721, 74, "Thị xã Bến Cát" },
                    { 722, 74, "Huyện Phú Giáo" },
                    { 723, 74, "Thị xã Tân Uyên" },
                    { 724, 74, "Thị xã Dĩ An" },
                    { 711, 72, "Huyện Bến Cầu" },
                    { 695, 70, "Huyện Đồng Phú" },
                    { 694, 70, "Huyện Hớn Quản" },
                    { 693, 70, "Huyện Bù Đốp" },
                    { 666, 67, "Huyện Đắk R'Lấp" },
                    { 667, 67, "Huyện Tuy Đức" },
                    { 672, 68, "Thành phố Đà Lạt" },
                    { 673, 68, "Thành phố Bảo Lộc" },
                    { 674, 68, "Huyện Đam Rông" },
                    { 675, 68, "Huyện Lạc Dương" },
                    { 676, 68, "Huyện Lâm Hà" },
                    { 677, 68, "Huyện Đơn Dương" },
                    { 678, 68, "Huyện Đức Trọng" },
                    { 679, 68, "Huyện Di Linh" },
                    { 680, 68, "Huyện Bảo Lâm" },
                    { 681, 68, "Huyện Đạ Huoai" },
                    { 682, 68, "Huyện Đạ Tẻh" },
                    { 683, 68, "Huyện Cát Tiên" },
                    { 688, 70, "Thị xã Phước Long" },
                    { 689, 70, "Thành phố Đồng Xoài" },
                    { 690, 70, "Thị xã Bình Long" },
                    { 691, 70, "Huyện Bù Gia Mập" },
                    { 692, 70, "Huyện Lộc Ninh" },
                    { 664, 67, "Huyện Krông Nô" },
                    { 611, 62, "Huyện Ngọc Hồi" },
                    { 561, 54, "Huyện Sông Hinh" },
                    { 608, 62, "Thành phố Kon Tum" },
                    { 517, 49, "Huyện Núi Thành" },
                    { 518, 49, "Huyện Phú Ninh" },
                    { 519, 49, "Huyện Nông Sơn" },
                    { 522, 51, "Thành phố Quảng Ngãi" },
                    { 524, 51, "Huyện Bình Sơn" },
                    { 525, 51, "Huyện Trà Bồng" },
                    { 526, 51, "Huyện Tây Trà" },
                    { 527, 51, "Huyện Sơn Tịnh" },
                    { 516, 49, "Huyện Nam Trà My" },
                    { 528, 51, "Huyện Tư Nghĩa" },
                    { 530, 51, "Huyện Sơn Tây" },
                    { 531, 51, "Huyện Minh Long" },
                    { 532, 51, "Huyện Nghĩa Hành" },
                    { 533, 51, "Huyện Mộ Đức" },
                    { 534, 51, "Huyện Đức Phổ" },
                    { 535, 51, "Huyện Ba Tơ" },
                    { 536, 51, "Huyện Lý Sơn" },
                    { 540, 52, "Thành phố Qui Nhơn" },
                    { 529, 51, "Huyện Sơn Hà" },
                    { 515, 49, "Huyện Bắc Trà My" },
                    { 514, 49, "Huyện Tiên Phước" },
                    { 513, 49, "Huyện Thăng Bình" },
                    { 490, 48, "Quận Liên Chiểu" },
                    { 491, 48, "Quận Thanh Khê" },
                    { 492, 48, "Quận Hải Châu" },
                    { 493, 48, "Quận Sơn Trà" },
                    { 494, 48, "Quận Ngũ Hành Sơn" },
                    { 495, 48, "Quận Cẩm Lệ" },
                    { 497, 48, "Huyện Hòa Vang" },
                    { 498, 48, "Huyện Hoàng Sa" },
                    { 502, 49, "Thành phố Tam Kỳ" },
                    { 503, 49, "Thành phố Hội An" },
                    { 504, 49, "Huyện Tây Giang" },
                    { 505, 49, "Huyện Đông Giang" },
                    { 506, 49, "Huyện Đại Lộc" },
                    { 507, 49, "Thị xã Điện Bàn" },
                    { 508, 49, "Huyện Duy Xuyên" },
                    { 509, 49, "Huyện Quế Sơn" },
                    { 510, 49, "Huyện Nam Giang" },
                    { 511, 49, "Huyện Phước Sơn" },
                    { 512, 49, "Huyện Hiệp Đức" },
                    { 542, 52, "Huyện An Lão" },
                    { 610, 62, "Huyện Đắk Glei" },
                    { 543, 52, "Huyện Hoài Nhơn" },
                    { 545, 52, "Huyện Phù Mỹ" },
                    { 576, 56, "Huyện Trường Sa" },
                    { 582, 58, "Thành phố Phan Rang-Tháp Chàm" },
                    { 584, 58, "Huyện Bác Ái" },
                    { 585, 58, "Huyện Ninh Sơn" },
                    { 586, 58, "Huyện Ninh Hải" },
                    { 587, 58, "Huyện Ninh Phước" },
                    { 588, 58, "Huyện Thuận Bắc" },
                    { 589, 58, "Huyện Thuận Nam" },
                    { 575, 56, "Huyện Khánh Sơn" },
                    { 593, 60, "Thành phố Phan Thiết" },
                    { 595, 60, "Huyện Tuy Phong" },
                    { 596, 60, "Huyện Bắc Bình" },
                    { 597, 60, "Huyện Hàm Thuận Bắc" },
                    { 598, 60, "Huyện Hàm Thuận Nam" },
                    { 599, 60, "Huyện Tánh Linh" },
                    { 600, 60, "Huyện Đức Linh" },
                    { 601, 60, "Huyện Hàm Tân" },
                    { 602, 60, "Huyện Phú Quí" },
                    { 594, 60, "Thị xã La Gi" },
                    { 574, 56, "Huyện Diên Khánh" },
                    { 573, 56, "Huyện Khánh Vĩnh" },
                    { 572, 56, "Thị xã Ninh Hòa" },
                    { 546, 52, "Huyện Vĩnh Thạnh" },
                    { 547, 52, "Huyện Tây Sơn" },
                    { 548, 52, "Huyện Phù Cát" },
                    { 549, 52, "Thị xã An Nhơn" },
                    { 550, 52, "Huyện Tuy Phước" },
                    { 551, 52, "Huyện Vân Canh" },
                    { 555, 54, "Thành phố Tuy Hoà" },
                    { 557, 54, "Thị xã Sông Cầu" },
                    { 558, 54, "Huyện Đồng Xuân" },
                    { 559, 54, "Huyện Tuy An" },
                    { 560, 54, "Huyện Sơn Hòa" },
                    { 725, 74, "Thị xã Thuận An" },
                    { 562, 54, "Huyện Tây Hoà" },
                    { 563, 54, "Huyện Phú Hoà" },
                    { 564, 54, "Huyện Đông Hòa" },
                    { 568, 56, "Thành phố Nha Trang" },
                    { 569, 56, "Thành phố Cam Ranh" },
                    { 570, 56, "Huyện Cam Lâm" },
                    { 571, 56, "Huyện Vạn Ninh" },
                    { 544, 52, "Huyện Hoài Ân" },
                    { 726, 74, "Huyện Bắc Tân Uyên" },
                    { 771, 79, "Quận 10" },
                    { 732, 75, "Thành phố Long Khánh" },
                    { 891, 89, "Huyện Tri Tôn" },
                    { 892, 89, "Huyện Châu Thành" },
                    { 893, 89, "Huyện Chợ Mới" },
                    { 894, 89, "Huyện Thoại Sơn" },
                    { 899, 91, "Thành phố Rạch Giá" },
                    { 900, 91, "Thành phố Hà Tiên" },
                    { 902, 91, "Huyện Kiên Lương" },
                    { 903, 91, "Huyện Hòn Đất" },
                    { 890, 89, "Huyện Tịnh Biên" },
                    { 904, 91, "Huyện Tân Hiệp" },
                    { 906, 91, "Huyện Giồng Riềng" },
                    { 907, 91, "Huyện Gò Quao" },
                    { 908, 91, "Huyện An Biên" },
                    { 909, 91, "Huyện An Minh" },
                    { 910, 91, "Huyện Vĩnh Thuận" },
                    { 911, 91, "Huyện Phú Quốc" },
                    { 912, 91, "Huyện Kiên Hải" },
                    { 913, 91, "Huyện U Minh Thượng" },
                    { 905, 91, "Huyện Châu Thành" },
                    { 914, 91, "Huyện Giang Thành" },
                    { 889, 89, "Huyện Châu Phú" },
                    { 887, 89, "Thị xã Tân Châu" },
                    { 861, 86, "Thị xã Bình Minh" },
                    { 862, 86, "Huyện Trà Ôn" },
                    { 863, 86, "Huyện Bình Tân" },
                    { 866, 87, "Thành phố Cao Lãnh" },
                    { 867, 87, "Thành phố Sa Đéc" },
                    { 868, 87, "Thị xã Hồng Ngự" },
                    { 869, 87, "Huyện Tân Hồng" },
                    { 870, 87, "Huyện Hồng Ngự" },
                    { 888, 89, "Huyện Phú Tân" },
                    { 871, 87, "Huyện Tam Nông" },
                    { 873, 87, "Huyện Cao Lãnh" },
                    { 874, 87, "Huyện Thanh Bình" },
                    { 875, 87, "Huyện Lấp Vò" },
                    { 876, 87, "Huyện Lai Vung" },
                    { 877, 87, "Huyện Châu Thành" },
                    { 883, 89, "Thành phố Long Xuyên" },
                    { 884, 89, "Thành phố Châu Đốc" },
                    { 886, 89, "Huyện An Phú" },
                    { 872, 87, "Huyện Tháp Mười" },
                    { 916, 92, "Quận Ninh Kiều" },
                    { 917, 92, "Quận Ô Môn" },
                    { 918, 92, "Quận Bình Thuỷ" },
                    { 950, 94, "Thị xã Vĩnh Châu" },
                    { 951, 94, "Huyện Trần Đề" },
                    { 954, 95, "Thành phố Bạc Liêu" },
                    { 956, 95, "Huyện Hồng Dân" },
                    { 957, 95, "Huyện Phước Long" },
                    { 958, 95, "Huyện Vĩnh Lợi" },
                    { 959, 95, "Thị xã Giá Rai" },
                    { 960, 95, "Huyện Đông Hải" },
                    { 949, 94, "Huyện Thạnh Trị" },
                    { 961, 95, "Huyện Hoà Bình" },
                    { 966, 96, "Huyện U Minh" },
                    { 967, 96, "Huyện Thới Bình" },
                    { 968, 96, "Huyện Trần Văn Thời" },
                    { 969, 96, "Huyện Cái Nước" },
                    { 970, 96, "Huyện Đầm Dơi" },
                    { 971, 96, "Huyện Năm Căn" },
                    { 972, 96, "Huyện Phú Tân" },
                    { 973, 96, "Huyện Ngọc Hiển" },
                    { 964, 96, "Thành phố Cà Mau" },
                    { 948, 94, "Thị xã Ngã Năm" },
                    { 947, 94, "Huyện Mỹ Xuyên" },
                    { 946, 94, "Huyện Long Phú" },
                    { 919, 92, "Quận Cái Răng" },
                    { 923, 92, "Quận Thốt Nốt" },
                    { 924, 92, "Huyện Vĩnh Thạnh" },
                    { 925, 92, "Huyện Cờ Đỏ" },
                    { 926, 92, "Huyện Phong Điền" },
                    { 927, 92, "Huyện Thới Lai" },
                    { 930, 93, "Thành phố Vị Thanh" },
                    { 931, 93, "Thị xã Ngã Bảy" },
                    { 932, 93, "Huyện Châu Thành A" },
                    { 933, 93, "Huyện Châu Thành" },
                    { 934, 93, "Huyện Phụng Hiệp" },
                    { 935, 93, "Huyện Vị Thuỷ" },
                    { 936, 93, "Huyện Long Mỹ" },
                    { 937, 93, "Thị xã Long Mỹ" },
                    { 941, 94, "Thành phố Sóc Trăng" },
                    { 942, 94, "Huyện Châu Thành" },
                    { 943, 94, "Huyện Kế Sách" },
                    { 944, 94, "Huyện Mỹ Tú" },
                    { 945, 94, "Huyện Cù Lao Dung" },
                    { 860, 86, "Huyện Tam Bình" },
                    { 859, 86, "Huyện  Vũng Liêm" },
                    { 858, 86, "Huyện Mang Thít" },
                    { 857, 86, "Huyện Long Hồ" },
                    { 766, 79, "Quận Tân Bình" },
                    { 767, 79, "Quận Tân Phú" },
                    { 768, 79, "Quận Phú Nhuận" },
                    { 769, 79, "Quận 2" },
                    { 770, 79, "Quận 3" },
                    { 483, 46, "Huyện Nam Đông" },
                    { 772, 79, "Quận 11" },
                    { 773, 79, "Quận 4" },
                    { 765, 79, "Quận Bình Thạnh" },
                    { 774, 79, "Quận 5" },
                    { 776, 79, "Quận 8" },
                    { 777, 79, "Quận Bình Tân" },
                    { 778, 79, "Quận 7" },
                    { 783, 79, "Huyện Củ Chi" },
                    { 784, 79, "Huyện Hóc Môn" },
                    { 785, 79, "Huyện Bình Chánh" },
                    { 786, 79, "Huyện Nhà Bè" },
                    { 787, 79, "Huyện Cần Giờ" },
                    { 775, 79, "Quận 6" },
                    { 764, 79, "Quận Gò Vấp" },
                    { 763, 79, "Quận 9" },
                    { 762, 79, "Quận Thủ Đức" },
                    { 734, 75, "Huyện Tân Phú" },
                    { 735, 75, "Huyện Vĩnh Cửu" },
                    { 736, 75, "Huyện Định Quán" },
                    { 737, 75, "Huyện Trảng Bom" },
                    { 738, 75, "Huyện Thống Nhất" },
                    { 739, 75, "Huyện Cẩm Mỹ" },
                    { 740, 75, "Huyện Long Thành" },
                    { 741, 75, "Huyện Xuân Lộc" },
                    { 742, 75, "Huyện Nhơn Trạch" },
                    { 747, 77, "Thành phố Vũng Tàu" },
                    { 748, 77, "Thành phố Bà Rịa" },
                    { 750, 77, "Huyện Châu Đức" },
                    { 751, 77, "Huyện Xuyên Mộc" },
                    { 752, 77, "Huyện Long Điền" },
                    { 753, 77, "Huyện Đất Đỏ" },
                    { 754, 77, "Thị xã Phú Mỹ" },
                    { 755, 77, "Huyện Côn Đảo" },
                    { 760, 79, "Quận 1" },
                    { 761, 79, "Quận 12" },
                    { 794, 80, "Thành phố Tân An" },
                    { 731, 75, "Thành phố Biên Hòa" },
                    { 795, 80, "Thị xã Kiến Tường" },
                    { 797, 80, "Huyện Vĩnh Hưng" },
                    { 831, 83, "Huyện Châu Thành" },
                    { 832, 83, "Huyện Chợ Lách" },
                    { 833, 83, "Huyện Mỏ Cày Nam" },
                    { 834, 83, "Huyện Giồng Trôm" },
                    { 835, 83, "Huyện Bình Đại" },
                    { 836, 83, "Huyện Ba Tri" },
                    { 837, 83, "Huyện Thạnh Phú" },
                    { 838, 83, "Huyện Mỏ Cày Bắc" },
                    { 829, 83, "Thành phố Bến Tre" },
                    { 842, 84, "Thành phố Trà Vinh" },
                    { 845, 84, "Huyện Cầu Kè" },
                    { 846, 84, "Huyện Tiểu Cần" },
                    { 847, 84, "Huyện Châu Thành" },
                    { 848, 84, "Huyện Cầu Ngang" },
                    { 849, 84, "Huyện Trà Cú" },
                    { 850, 84, "Huyện Duyên Hải" },
                    { 851, 84, "Thị xã Duyên Hải" },
                    { 855, 86, "Thành phố Vĩnh Long" },
                    { 844, 84, "Huyện Càng Long" },
                    { 825, 82, "Huyện Tân Phú Đông" },
                    { 824, 82, "Huyện Gò Công Đông" },
                    { 823, 82, "Huyện Gò Công Tây" },
                    { 798, 80, "Huyện Mộc Hóa" },
                    { 799, 80, "Huyện Tân Thạnh" },
                    { 800, 80, "Huyện Thạnh Hóa" },
                    { 801, 80, "Huyện Đức Huệ" },
                    { 802, 80, "Huyện Đức Hòa" },
                    { 803, 80, "Huyện Bến Lức" },
                    { 804, 80, "Huyện Thủ Thừa" },
                    { 805, 80, "Huyện Tân Trụ" },
                    { 806, 80, "Huyện Cần Đước" },
                    { 807, 80, "Huyện Cần Giuộc" },
                    { 808, 80, "Huyện Châu Thành" },
                    { 815, 82, "Thành phố Mỹ Tho" },
                    { 816, 82, "Thị xã Gò Công" },
                    { 817, 82, "Thị xã Cai Lậy" },
                    { 818, 82, "Huyện Tân Phước" },
                    { 819, 82, "Huyện Cái Bè" },
                    { 820, 82, "Huyện Cai Lậy" },
                    { 821, 82, "Huyện Châu Thành" },
                    { 822, 82, "Huyện Chợ Gạo" },
                    { 796, 80, "Huyện Tân Hưng" },
                    { 482, 46, "Huyện Phú Lộc" },
                    { 444, 42, "Huyện Hương Khê" },
                    { 480, 46, "Thị xã Hương Trà" },
                    { 137, 15, "Huyện Mù Căng Chải" },
                    { 138, 15, "Huyện Trấn Yên" },
                    { 139, 15, "Huyện Trạm Tấu" },
                    { 140, 15, "Huyện Văn Chấn" },
                    { 141, 15, "Huyện Yên Bình" },
                    { 148, 17, "Thành phố Hòa Bình" },
                    { 150, 17, "Huyện Đà Bắc" },
                    { 151, 17, "Huyện Kỳ Sơn" },
                    { 136, 15, "Huyện Văn Yên" },
                    { 152, 17, "Huyện Lương Sơn" },
                    { 154, 17, "Huyện Cao Phong" },
                    { 155, 17, "Huyện Tân Lạc" },
                    { 156, 17, "Huyện Mai Châu" },
                    { 157, 17, "Huyện Lạc Sơn" },
                    { 158, 17, "Huyện Yên Thủy" },
                    { 159, 17, "Huyện Lạc Thủy" },
                    { 164, 19, "Thành phố Thái Nguyên" },
                    { 165, 19, "Thành phố Sông Công" },
                    { 153, 17, "Huyện Kim Bôi" },
                    { 167, 19, "Huyện Định Hóa" },
                    { 135, 15, "Huyện Lục Yên" },
                    { 132, 15, "Thành phố Yên Bái" },
                    { 107, 12, "Huyện Mường Tè" },
                    { 108, 12, "Huyện Sìn Hồ" },
                    { 109, 12, "Huyện Phong Thổ" },
                    { 110, 12, "Huyện Than Uyên" },
                    { 111, 12, "Huyện Tân Uyên" },
                    { 112, 12, "Huyện Nậm Nhùn" },
                    { 116, 14, "Thành phố Sơn La" },
                    { 118, 14, "Huyện Quỳnh Nhai" },
                    { 133, 15, "Thị xã Nghĩa Lộ" },
                    { 119, 14, "Huyện Thuận Châu" },
                    { 121, 14, "Huyện Bắc Yên" },
                    { 122, 14, "Huyện Phù Yên" },
                    { 123, 14, "Huyện Mộc Châu" },
                    { 124, 14, "Huyện Yên Châu" },
                    { 125, 14, "Huyện Mai Sơn" },
                    { 126, 14, "Huyện Sông Mã" },
                    { 127, 14, "Huyện Sốp Cộp" },
                    { 128, 14, "Huyện Vân Hồ" },
                    { 120, 14, "Huyện Mường La" },
                    { 168, 19, "Huyện Phú Lương" },
                    { 169, 19, "Huyện Đồng Hỷ" },
                    { 170, 19, "Huyện Võ Nhai" },
                    { 203, 22, "Huyện Vân Đồn" },
                    { 204, 22, "Huyện Hoành Bồ" },
                    { 205, 22, "Thị xã Đông Triều" },
                    { 206, 22, "Thị xã Quảng Yên" },
                    { 207, 22, "Huyện Cô Tô" },
                    { 213, 24, "Thành phố Bắc Giang" },
                    { 215, 24, "Huyện Yên Thế" },
                    { 216, 24, "Huyện Tân Yên" },
                    { 202, 22, "Huyện Ba Chẽ" },
                    { 217, 24, "Huyện Lạng Giang" },
                    { 219, 24, "Huyện Lục Ngạn" },
                    { 220, 24, "Huyện Sơn Động" },
                    { 221, 24, "Huyện Yên Dũng" },
                    { 222, 24, "Huyện Việt Yên" },
                    { 223, 24, "Huyện Hiệp Hòa" },
                    { 227, 25, "Thành phố Việt Trì" },
                    { 228, 25, "Thị xã Phú Thọ" },
                    { 230, 25, "Huyện Đoan Hùng" },
                    { 218, 24, "Huyện Lục Nam" },
                    { 201, 22, "Huyện Hải Hà" },
                    { 200, 22, "Huyện Đầm Hà" },
                    { 199, 22, "Huyện Tiên Yên" },
                    { 171, 19, "Huyện Đại Từ" },
                    { 172, 19, "Thị xã Phổ Yên" },
                    { 173, 19, "Huyện Phú Bình" },
                    { 178, 20, "Thành phố Lạng Sơn" },
                    { 180, 20, "Huyện Tràng Định" },
                    { 181, 20, "Huyện Bình Gia" },
                    { 182, 20, "Huyện Văn Lãng" },
                    { 183, 20, "Huyện Cao Lộc" },
                    { 184, 20, "Huyện Văn Quan" },
                    { 185, 20, "Huyện Bắc Sơn" },
                    { 186, 20, "Huyện Hữu Lũng" },
                    { 187, 20, "Huyện Chi Lăng" },
                    { 188, 20, "Huyện Lộc Bình" },
                    { 189, 20, "Huyện Đình Lập" },
                    { 193, 22, "Thành phố Hạ Long" },
                    { 194, 22, "Thành phố Móng Cái" },
                    { 195, 22, "Thành phố Cẩm Phả" },
                    { 196, 22, "Thành phố Uông Bí" },
                    { 198, 22, "Huyện Bình Liêu" },
                    { 106, 12, "Huyện Tam Đường" },
                    { 105, 12, "Thành phố Lai Châu" },
                    { 103, 11, "Huyện Nậm Pồ" },
                    { 102, 11, "Huyện Mường Ảng" },
                    { 276, 1, "Huyện Thạch Thất" },
                    { 277, 1, "Huyện Chương Mỹ" },
                    { 278, 1, "Huyện Thanh Oai" },
                    { 279, 1, "Huyện Thường Tín" },
                    { 280, 1, "Huyện Phú Xuyên" },
                    { 281, 1, "Huyện Ứng Hòa" },
                    { 282, 1, "Huyện Mỹ Đức" },
                    { 24, 2, "Thành phố Hà Giang" },
                    { 275, 1, "Huyện Quốc Oai" },
                    { 26, 2, "Huyện Đồng Văn" },
                    { 28, 2, "Huyện Yên Minh" },
                    { 29, 2, "Huyện Quản Bạ" },
                    { 30, 2, "Huyện Vị Xuyên" },
                    { 31, 2, "Huyện Bắc Mê" },
                    { 32, 2, "Huyện Hoàng Su Phì" },
                    { 33, 2, "Huyện Xín Mần" },
                    { 34, 2, "Huyện Bắc Quang" },
                    { 35, 2, "Huyện Quang Bình" },
                    { 27, 2, "Huyện Mèo Vạc" },
                    { 274, 1, "Huyện Hoài Đức" },
                    { 273, 1, "Huyện Đan Phượng" },
                    { 272, 1, "Huyện Phúc Thọ" },
                    { 1, 1, "Quận Ba Đình" },
                    { 2, 1, "Quận Hoàn Kiếm" },
                    { 3, 1, "Quận Tây Hồ" },
                    { 4, 1, "Quận Long Biên" },
                    { 5, 1, "Quận Cầu Giấy" },
                    { 6, 1, "Quận Đống Đa" },
                    { 7, 1, "Quận Hai Bà Trưng" },
                    { 8, 1, "Quận Hoàng Mai" },
                    { 9, 1, "Quận Thanh Xuân" },
                    { 16, 1, "Huyện Sóc Sơn" },
                    { 17, 1, "Huyện Đông Anh" },
                    { 18, 1, "Huyện Gia Lâm" },
                    { 19, 1, "Quận Nam Từ Liêm" },
                    { 20, 1, "Huyện Thanh Trì" },
                    { 21, 1, "Quận Bắc Từ Liêm" },
                    { 250, 1, "Huyện Mê Linh" },
                    { 268, 1, "Quận Hà Đông" },
                    { 269, 1, "Thị xã Sơn Tây" },
                    { 271, 1, "Huyện Ba Vì" },
                    { 40, 4, "Thành phố Cao Bằng" },
                    { 231, 25, "Huyện Hạ Hoà" },
                    { 42, 4, "Huyện Bảo Lâm" },
                    { 44, 4, "Huyện Thông Nông" },
                    { 76, 8, "Huyện Sơn Dương" },
                    { 80, 10, "Thành phố Lào Cai" },
                    { 82, 10, "Huyện Bát Xát" },
                    { 83, 10, "Huyện Mường Khương" },
                    { 84, 10, "Huyện Si Ma Cai" },
                    { 85, 10, "Huyện Bắc Hà" },
                    { 86, 10, "Huyện Bảo Thắng" },
                    { 87, 10, "Huyện Bảo Yên" },
                    { 75, 8, "Huyện Yên Sơn" },
                    { 88, 10, "Huyện Sa Pa" },
                    { 94, 11, "Thành phố Điện Biên Phủ" },
                    { 95, 11, "Thị Xã Mường Lay" },
                    { 96, 11, "Huyện Mường Nhé" },
                    { 97, 11, "Huyện Mường Chà" },
                    { 98, 11, "Huyện Tủa Chùa" },
                    { 99, 11, "Huyện Tuần Giáo" },
                    { 100, 11, "Huyện Điện Biên" },
                    { 101, 11, "Huyện Điện Biên Đông" },
                    { 89, 10, "Huyện Văn Bàn" },
                    { 74, 8, "Huyện Hàm Yên" },
                    { 73, 8, "Huyện Chiêm Hóa" },
                    { 72, 8, "Huyện Na Hang" },
                    { 45, 4, "Huyện Hà Quảng" },
                    { 46, 4, "Huyện Trà Lĩnh" },
                    { 47, 4, "Huyện Trùng Khánh" },
                    { 48, 4, "Huyện Hạ Lang" },
                    { 49, 4, "Huyện Quảng Uyên" },
                    { 50, 4, "Huyện Phục Hoà" },
                    { 51, 4, "Huyện Hoà An" },
                    { 52, 4, "Huyện Nguyên Bình" },
                    { 53, 4, "Huyện Thạch An" },
                    { 58, 6, "Thành Phố Bắc Kạn" },
                    { 60, 6, "Huyện Pác Nặm" },
                    { 61, 6, "Huyện Ba Bể" },
                    { 62, 6, "Huyện Ngân Sơn" },
                    { 63, 6, "Huyện Bạch Thông" },
                    { 64, 6, "Huyện Chợ Đồn" },
                    { 65, 6, "Huyện Chợ Mới" },
                    { 66, 6, "Huyện Na Rì" },
                    { 70, 8, "Thành phố Tuyên Quang" },
                    { 71, 8, "Huyện Lâm Bình" },
                    { 43, 4, "Huyện Bảo Lạc" },
                    { 481, 46, "Huyện A Lưới" },
                    { 232, 25, "Huyện Thanh Ba" },
                    { 234, 25, "Huyện Yên Lập" },
                    { 401, 38, "Huyện Nga Sơn" },
                    { 402, 38, "Huyện Như Xuân" },
                    { 403, 38, "Huyện Như Thanh" },
                    { 404, 38, "Huyện Nông Cống" },
                    { 405, 38, "Huyện Đông Sơn" },
                    { 406, 38, "Huyện Quảng Xương" },
                    { 407, 38, "Huyện Tĩnh Gia" },
                    { 412, 40, "Thành phố Vinh" },
                    { 400, 38, "Huyện Hậu Lộc" },
                    { 413, 40, "Thị xã Cửa Lò" },
                    { 415, 40, "Huyện Quế Phong" },
                    { 416, 40, "Huyện Quỳ Châu" },
                    { 417, 40, "Huyện Kỳ Sơn" },
                    { 418, 40, "Huyện Tương Dương" },
                    { 419, 40, "Huyện Nghĩa Đàn" },
                    { 420, 40, "Huyện Quỳ Hợp" },
                    { 421, 40, "Huyện Quỳnh Lưu" },
                    { 422, 40, "Huyện Con Cuông" },
                    { 414, 40, "Thị xã Thái Hoà" },
                    { 423, 40, "Huyện Tân Kỳ" },
                    { 399, 38, "Huyện Hoằng Hóa" },
                    { 397, 38, "Huyện Triệu Sơn" },
                    { 376, 37, "Huyện Kim Sơn" },
                    { 377, 37, "Huyện Yên Mô" },
                    { 380, 38, "Thành phố Thanh Hóa" },
                    { 381, 38, "Thị xã Bỉm Sơn" },
                    { 382, 38, "Thành phố Sầm Sơn" },
                    { 384, 38, "Huyện Mường Lát" },
                    { 385, 38, "Huyện Quan Hóa" },
                    { 386, 38, "Huyện Bá Thước" },
                    { 398, 38, "Huyện Thiệu Hóa" },
                    { 387, 38, "Huyện Quan Sơn" },
                    { 389, 38, "Huyện Ngọc Lặc" },
                    { 390, 38, "Huyện Cẩm Thủy" },
                    { 391, 38, "Huyện Thạch Thành" },
                    { 392, 38, "Huyện Hà Trung" },
                    { 393, 38, "Huyện Vĩnh Lộc" },
                    { 394, 38, "Huyện Yên Định" },
                    { 395, 38, "Huyện Thọ Xuân" },
                    { 396, 38, "Huyện Thường Xuân" },
                    { 388, 38, "Huyện Lang Chánh" },
                    { 424, 40, "Huyện Anh Sơn" },
                    { 425, 40, "Huyện Diễn Châu" },
                    { 426, 40, "Huyện Yên Thành" },
                    { 456, 44, "Huyện Quảng Ninh" },
                    { 457, 44, "Huyện Lệ Thủy" },
                    { 458, 44, "Thị xã Ba Đồn" },
                    { 461, 45, "Thành phố Đông Hà" },
                    { 462, 45, "Thị xã Quảng Trị" },
                    { 464, 45, "Huyện Vĩnh Linh" },
                    { 465, 45, "Huyện Hướng Hóa" },
                    { 466, 45, "Huyện Gio Linh" },
                    { 455, 44, "Huyện Bố Trạch" },
                    { 467, 45, "Huyện Đa Krông" },
                    { 469, 45, "Huyện Triệu Phong" },
                    { 470, 45, "Huyện Hải Lăng" },
                    { 471, 45, "Huyện Cồn Cỏ" },
                    { 474, 46, "Thành phố Huế" },
                    { 476, 46, "Huyện Phong Điền" },
                    { 477, 46, "Huyện Quảng Điền" },
                    { 478, 46, "Huyện Phú Vang" },
                    { 479, 46, "Thị xã Hương Thủy" },
                    { 468, 45, "Huyện Cam Lộ" },
                    { 454, 44, "Huyện Quảng Trạch" },
                    { 453, 44, "Huyện Tuyên Hóa" },
                    { 452, 44, "Huyện Minh Hóa" },
                    { 427, 40, "Huyện Đô Lương" },
                    { 428, 40, "Huyện Thanh Chương" },
                    { 429, 40, "Huyện Nghi Lộc" },
                    { 430, 40, "Huyện Nam Đàn" },
                    { 431, 40, "Huyện Hưng Nguyên" },
                    { 432, 40, "Thị xã Hoàng Mai" },
                    { 436, 42, "Thành phố Hà Tĩnh" },
                    { 437, 42, "Thị xã Hồng Lĩnh" },
                    { 439, 42, "Huyện Hương Sơn" },
                    { 440, 42, "Huyện Đức Thọ" },
                    { 441, 42, "Huyện Vũ Quang" },
                    { 442, 42, "Huyện Nghi Xuân" },
                    { 443, 42, "Huyện Can Lộc" },
                    { 445, 42, "Huyện Thạch Hà" },
                    { 446, 42, "Huyện Cẩm Xuyên" },
                    { 447, 42, "Huyện Kỳ Anh" },
                    { 448, 42, "Huyện Lộc Hà" },
                    { 449, 42, "Thị xã Kỳ Anh" },
                    { 450, 44, "Thành Phố Đồng Hới" },
                    { 375, 37, "Huyện Yên Khánh" },
                    { 233, 25, "Huyện Phù Ninh" },
                    { 374, 37, "Huyện Hoa Lư" },
                    { 372, 37, "Huyện Nho Quan" },
                    { 288, 30, "Thành phố Hải Dương" },
                    { 290, 30, "Thành phố Chí Linh" },
                    { 291, 30, "Huyện Nam Sách" },
                    { 292, 30, "Huyện Kinh Môn" },
                    { 293, 30, "Huyện Kim Thành" },
                    { 294, 30, "Huyện Thanh Hà" },
                    { 295, 30, "Huyện Cẩm Giàng" },
                    { 296, 30, "Huyện Bình Giang" },
                    { 264, 27, "Huyện Lương Tài" },
                    { 297, 30, "Huyện Gia Lộc" },
                    { 299, 30, "Huyện Ninh Giang" },
                    { 300, 30, "Huyện Thanh Miện" },
                    { 303, 31, "Quận Hồng Bàng" },
                    { 304, 31, "Quận Ngô Quyền" },
                    { 305, 31, "Quận Lê Chân" },
                    { 306, 31, "Quận Hải An" },
                    { 307, 31, "Quận Kiến An" },
                    { 308, 31, "Quận Đồ Sơn" },
                    { 298, 30, "Huyện Tứ Kỳ" },
                    { 263, 27, "Huyện Gia Bình" },
                    { 262, 27, "Huyện Thuận Thành" },
                    { 261, 27, "Thị xã Từ Sơn" },
                    { 235, 25, "Huyện Cẩm Khê" },
                    { 236, 25, "Huyện Tam Nông" },
                    { 237, 25, "Huyện Lâm Thao" },
                    { 238, 25, "Huyện Thanh Sơn" },
                    { 239, 25, "Huyện Thanh Thuỷ" },
                    { 240, 25, "Huyện Tân Sơn" },
                    { 243, 26, "Thành phố Vĩnh Yên" },
                    { 244, 26, "Thành phố Phúc Yên" },
                    { 246, 26, "Huyện Lập Thạch" }
                });

            migrationBuilder.InsertData(
                table: "District",
                columns: new[] { "id", "city_id", "name" },
                values: new object[,]
                {
                    { 247, 26, "Huyện Tam Dương" },
                    { 248, 26, "Huyện Tam Đảo" },
                    { 249, 26, "Huyện Bình Xuyên" },
                    { 251, 26, "Huyện Yên Lạc" },
                    { 252, 26, "Huyện Vĩnh Tường" },
                    { 253, 26, "Huyện Sông Lô" },
                    { 256, 27, "Thành phố Bắc Ninh" },
                    { 258, 27, "Huyện Yên Phong" },
                    { 259, 27, "Huyện Quế Võ" },
                    { 260, 27, "Huyện Tiên Du" },
                    { 373, 37, "Huyện Gia Viễn" },
                    { 311, 31, "Huyện Thuỷ Nguyên" },
                    { 309, 31, "Quận Dương Kinh" },
                    { 313, 31, "Huyện An Lão" },
                    { 344, 34, "Huyện Vũ Thư" },
                    { 347, 35, "Thành phố Phủ Lý" },
                    { 349, 35, "Huyện Duy Tiên" },
                    { 350, 35, "Huyện Kim Bảng" },
                    { 351, 35, "Huyện Thanh Liêm" },
                    { 352, 35, "Huyện Bình Lục" },
                    { 353, 35, "Huyện Lý Nhân" },
                    { 356, 36, "Thành phố Nam Định" },
                    { 358, 36, "Huyện Mỹ Lộc" },
                    { 359, 36, "Huyện Vụ Bản" },
                    { 360, 36, "Huyện Ý Yên" },
                    { 361, 36, "Huyện Nghĩa Hưng" },
                    { 362, 36, "Huyện Nam Trực" },
                    { 363, 36, "Huyện Trực Ninh" },
                    { 364, 36, "Huyện Xuân Trường" },
                    { 365, 36, "Huyện Giao Thủy" },
                    { 366, 36, "Huyện Hải Hậu" },
                    { 369, 37, "Thành phố Ninh Bình" },
                    { 370, 37, "Thành phố Tam Điệp" },
                    { 343, 34, "Huyện Kiến Xương" },
                    { 312, 31, "Huyện An Dương" },
                    { 342, 34, "Huyện Tiền Hải" },
                    { 340, 34, "Huyện Đông Hưng" },
                    { 314, 31, "Huyện Kiến Thuỵ" },
                    { 315, 31, "Huyện Tiên Lãng" },
                    { 316, 31, "Huyện Vĩnh Bảo" },
                    { 317, 31, "Huyện Cát Hải" },
                    { 318, 31, "Huyện Bạch Long Vĩ" },
                    { 323, 33, "Thành phố Hưng Yên" },
                    { 325, 33, "Huyện Văn Lâm" },
                    { 326, 33, "Huyện Văn Giang" },
                    { 341, 34, "Huyện Thái Thụy" },
                    { 327, 33, "Huyện Yên Mỹ" },
                    { 329, 33, "Huyện Ân Thi" },
                    { 330, 33, "Huyện Khoái Châu" },
                    { 331, 33, "Huyện Kim Động" },
                    { 332, 33, "Huyện Tiên Lữ" },
                    { 333, 33, "Huyện Phù Cừ" },
                    { 336, 34, "Thành phố Thái Bình" },
                    { 338, 34, "Huyện Quỳnh Phụ" },
                    { 339, 34, "Huyện Hưng Hà" },
                    { 328, 33, "Thị xã Mỹ Hào" }
                });

            migrationBuilder.InsertData(
                table: "ProductAttributeType",
                columns: new[] { "id", "created_by", "created_date", "name", "updated_by", "updated_date" },
                values: new object[,]
                {
                    { 2, "InitData", new DateTime(2020, 2, 19, 19, 9, 18, 587, DateTimeKind.Local).AddTicks(260), "Image", "InitData", new DateTime(2020, 2, 19, 19, 9, 18, 587, DateTimeKind.Local).AddTicks(260) },
                    { 1, "InitData", new DateTime(2020, 2, 19, 19, 9, 18, 586, DateTimeKind.Local).AddTicks(8580), "Text", "InitData", new DateTime(2020, 2, 19, 19, 9, 18, 586, DateTimeKind.Local).AddTicks(9700) },
                    { 3, "InitData", new DateTime(2020, 2, 19, 19, 9, 18, 587, DateTimeKind.Local).AddTicks(270), "Color", "InitData", new DateTime(2020, 2, 19, 19, 9, 18, 587, DateTimeKind.Local).AddTicks(270) }
                });

            migrationBuilder.CreateIndex(
                name: "IX_AspNetRoleClaims_RoleId",
                table: "AspNetRoleClaims",
                column: "RoleId");

            migrationBuilder.CreateIndex(
                name: "IX_AspNetUserRoles_RoleId",
                table: "AspNetUserRoles",
                column: "RoleId");

            migrationBuilder.CreateIndex(
                name: "IX_District_city_id_id",
                table: "District",
                columns: new[] { "city_id", "id" },
                unique: true);

            migrationBuilder.CreateIndex(
                name: "Generic_Product_Index",
                table: "GenericProduct",
                column: "ref_web_id",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "Generic_media_index",
                table: "GenericProductMedia",
                column: "generic_product_id");

            migrationBuilder.CreateIndex(
                name: "IX_Order_code",
                table: "Order",
                column: "code",
                unique: true,
                filter: "[code] IS NOT NULL");

            migrationBuilder.CreateIndex(
                name: "Order_Summary_Index",
                table: "OrderSummary",
                column: "order_id");

            migrationBuilder.CreateIndex(
                name: "Product_Variant_Index",
                table: "Product",
                columns: new[] { "generic_product_id", "ref_web_id" });

            migrationBuilder.CreateIndex(
                name: "RoleNameIndex",
                table: "Roles",
                column: "NormalizedName",
                unique: true,
                filter: "[NormalizedName] IS NOT NULL");

            migrationBuilder.CreateIndex(
                name: "IX_UserAddress_receiver_phoneno_receiver_name",
                table: "UserAddress",
                columns: new[] { "receiver_phoneno", "receiver_name" });

            migrationBuilder.CreateIndex(
                name: "IX_UserClaims_UserId",
                table: "UserClaims",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_UserLogins_UserId",
                table: "UserLogins",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "EmailIndex",
                table: "Users",
                column: "NormalizedEmail");

            migrationBuilder.CreateIndex(
                name: "UserNameIndex",
                table: "Users",
                column: "NormalizedUserName",
                unique: true,
                filter: "[NormalizedUserName] IS NOT NULL");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Area");

            migrationBuilder.DropTable(
                name: "AreaDistrict");

            migrationBuilder.DropTable(
                name: "AspNetRoleClaims");

            migrationBuilder.DropTable(
                name: "AspNetUserRoles");

            migrationBuilder.DropTable(
                name: "AspNetUserTokens");

            migrationBuilder.DropTable(
                name: "Banner");

            migrationBuilder.DropTable(
                name: "BatchJobs");

            migrationBuilder.DropTable(
                name: "Blog");

            migrationBuilder.DropTable(
                name: "BlogCollection");

            migrationBuilder.DropTable(
                name: "BranchPriority");

            migrationBuilder.DropTable(
                name: "Brand");

            migrationBuilder.DropTable(
                name: "Category");

            migrationBuilder.DropTable(
                name: "CategoryCollection");

            migrationBuilder.DropTable(
                name: "CategoryMedia");

            migrationBuilder.DropTable(
                name: "City");

            migrationBuilder.DropTable(
                name: "Collection");

            migrationBuilder.DropTable(
                name: "CollectionBlog");

            migrationBuilder.DropTable(
                name: "CollectionMedia");

            migrationBuilder.DropTable(
                name: "District");

            migrationBuilder.DropTable(
                name: "GenericProduct");

            migrationBuilder.DropTable(
                name: "GenericProductMedia");

            migrationBuilder.DropTable(
                name: "Inventory");

            migrationBuilder.DropTable(
                name: "Order");

            migrationBuilder.DropTable(
                name: "OrderItem");

            migrationBuilder.DropTable(
                name: "OrderSummary");

            migrationBuilder.DropTable(
                name: "PaymentMethod");

            migrationBuilder.DropTable(
                name: "Product");

            migrationBuilder.DropTable(
                name: "ProductAttribute");

            migrationBuilder.DropTable(
                name: "ProductAttributeType");

            migrationBuilder.DropTable(
                name: "ProductCollection");

            migrationBuilder.DropTable(
                name: "ProductMedia");

            migrationBuilder.DropTable(
                name: "ProductRating");

            migrationBuilder.DropTable(
                name: "ProductType");

            migrationBuilder.DropTable(
                name: "UserAddress");

            migrationBuilder.DropTable(
                name: "UserClaims");

            migrationBuilder.DropTable(
                name: "UserLogins");

            migrationBuilder.DropTable(
                name: "UserRoles");

            migrationBuilder.DropTable(
                name: "Roles");

            migrationBuilder.DropTable(
                name: "Users");
        }
    }
}

﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace DataCore.Migrations
{
    public partial class InitDataV50 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "product_type",
                table: "GenericProduct",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "product_id",
                table: "CategoryCollection",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.UpdateData(
                table: "ProductAttributeType",
                keyColumn: "id",
                keyValue: 1,
                columns: new[] { "created_date", "updated_date" },
                values: new object[] { new DateTime(2020, 12, 9, 12, 0, 8, 317, DateTimeKind.Local).AddTicks(2100), new DateTime(2020, 12, 9, 12, 0, 8, 317, DateTimeKind.Local).AddTicks(3280) });

            migrationBuilder.UpdateData(
                table: "ProductAttributeType",
                keyColumn: "id",
                keyValue: 2,
                columns: new[] { "created_date", "updated_date" },
                values: new object[] { new DateTime(2020, 12, 9, 12, 0, 8, 317, DateTimeKind.Local).AddTicks(3890), new DateTime(2020, 12, 9, 12, 0, 8, 317, DateTimeKind.Local).AddTicks(3900) });

            migrationBuilder.UpdateData(
                table: "ProductAttributeType",
                keyColumn: "id",
                keyValue: 3,
                columns: new[] { "created_date", "updated_date" },
                values: new object[] { new DateTime(2020, 12, 9, 12, 0, 8, 317, DateTimeKind.Local).AddTicks(3900), new DateTime(2020, 12, 9, 12, 0, 8, 317, DateTimeKind.Local).AddTicks(3900) });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "product_type",
                table: "GenericProduct");

            migrationBuilder.DropColumn(
                name: "product_id",
                table: "CategoryCollection");

            migrationBuilder.UpdateData(
                table: "ProductAttributeType",
                keyColumn: "id",
                keyValue: 1,
                columns: new[] { "created_date", "updated_date" },
                values: new object[] { new DateTime(2020, 5, 8, 16, 43, 9, 472, DateTimeKind.Local).AddTicks(6550), new DateTime(2020, 5, 8, 16, 43, 9, 472, DateTimeKind.Local).AddTicks(7830) });

            migrationBuilder.UpdateData(
                table: "ProductAttributeType",
                keyColumn: "id",
                keyValue: 2,
                columns: new[] { "created_date", "updated_date" },
                values: new object[] { new DateTime(2020, 5, 8, 16, 43, 9, 472, DateTimeKind.Local).AddTicks(8470), new DateTime(2020, 5, 8, 16, 43, 9, 472, DateTimeKind.Local).AddTicks(8480) });

            migrationBuilder.UpdateData(
                table: "ProductAttributeType",
                keyColumn: "id",
                keyValue: 3,
                columns: new[] { "created_date", "updated_date" },
                values: new object[] { new DateTime(2020, 5, 8, 16, 43, 9, 472, DateTimeKind.Local).AddTicks(8490), new DateTime(2020, 5, 8, 16, 43, 9, 472, DateTimeKind.Local).AddTicks(8490) });
        }
    }
}

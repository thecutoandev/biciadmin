﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace DataCore.Migrations
{
    public partial class AddPositionMedia : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "position",
                table: "ProductMedia",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "position",
                table: "GenericProductMedia",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.UpdateData(
                table: "ProductAttributeType",
                keyColumn: "id",
                keyValue: 1,
                columns: new[] { "created_date", "updated_date" },
                values: new object[] { new DateTime(2020, 5, 8, 16, 43, 9, 472, DateTimeKind.Local).AddTicks(6550), new DateTime(2020, 5, 8, 16, 43, 9, 472, DateTimeKind.Local).AddTicks(7830) });

            migrationBuilder.UpdateData(
                table: "ProductAttributeType",
                keyColumn: "id",
                keyValue: 2,
                columns: new[] { "created_date", "updated_date" },
                values: new object[] { new DateTime(2020, 5, 8, 16, 43, 9, 472, DateTimeKind.Local).AddTicks(8470), new DateTime(2020, 5, 8, 16, 43, 9, 472, DateTimeKind.Local).AddTicks(8480) });

            migrationBuilder.UpdateData(
                table: "ProductAttributeType",
                keyColumn: "id",
                keyValue: 3,
                columns: new[] { "created_date", "updated_date" },
                values: new object[] { new DateTime(2020, 5, 8, 16, 43, 9, 472, DateTimeKind.Local).AddTicks(8490), new DateTime(2020, 5, 8, 16, 43, 9, 472, DateTimeKind.Local).AddTicks(8490) });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "position",
                table: "ProductMedia");

            migrationBuilder.DropColumn(
                name: "position",
                table: "GenericProductMedia");

            migrationBuilder.UpdateData(
                table: "ProductAttributeType",
                keyColumn: "id",
                keyValue: 1,
                columns: new[] { "created_date", "updated_date" },
                values: new object[] { new DateTime(2020, 2, 19, 19, 9, 18, 586, DateTimeKind.Local).AddTicks(8580), new DateTime(2020, 2, 19, 19, 9, 18, 586, DateTimeKind.Local).AddTicks(9700) });

            migrationBuilder.UpdateData(
                table: "ProductAttributeType",
                keyColumn: "id",
                keyValue: 2,
                columns: new[] { "created_date", "updated_date" },
                values: new object[] { new DateTime(2020, 2, 19, 19, 9, 18, 587, DateTimeKind.Local).AddTicks(260), new DateTime(2020, 2, 19, 19, 9, 18, 587, DateTimeKind.Local).AddTicks(260) });

            migrationBuilder.UpdateData(
                table: "ProductAttributeType",
                keyColumn: "id",
                keyValue: 3,
                columns: new[] { "created_date", "updated_date" },
                values: new object[] { new DateTime(2020, 2, 19, 19, 9, 18, 587, DateTimeKind.Local).AddTicks(270), new DateTime(2020, 2, 19, 19, 9, 18, 587, DateTimeKind.Local).AddTicks(270) });
        }
    }
}

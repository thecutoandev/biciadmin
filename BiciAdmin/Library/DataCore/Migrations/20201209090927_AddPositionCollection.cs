﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace DataCore.Migrations
{
    public partial class AddPositionCollection : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "position",
                table: "CategoryCollection",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<string>(
                name: "sort_value",
                table: "CategoryCollection",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.UpdateData(
                table: "ProductAttributeType",
                keyColumn: "id",
                keyValue: 1,
                columns: new[] { "created_date", "updated_date" },
                values: new object[] { new DateTime(2020, 12, 9, 16, 9, 26, 724, DateTimeKind.Local).AddTicks(60), new DateTime(2020, 12, 9, 16, 9, 26, 724, DateTimeKind.Local).AddTicks(1230) });

            migrationBuilder.UpdateData(
                table: "ProductAttributeType",
                keyColumn: "id",
                keyValue: 2,
                columns: new[] { "created_date", "updated_date" },
                values: new object[] { new DateTime(2020, 12, 9, 16, 9, 26, 724, DateTimeKind.Local).AddTicks(1850), new DateTime(2020, 12, 9, 16, 9, 26, 724, DateTimeKind.Local).AddTicks(1850) });

            migrationBuilder.UpdateData(
                table: "ProductAttributeType",
                keyColumn: "id",
                keyValue: 3,
                columns: new[] { "created_date", "updated_date" },
                values: new object[] { new DateTime(2020, 12, 9, 16, 9, 26, 724, DateTimeKind.Local).AddTicks(1860), new DateTime(2020, 12, 9, 16, 9, 26, 724, DateTimeKind.Local).AddTicks(1860) });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "position",
                table: "CategoryCollection");

            migrationBuilder.DropColumn(
                name: "sort_value",
                table: "CategoryCollection");

            migrationBuilder.UpdateData(
                table: "ProductAttributeType",
                keyColumn: "id",
                keyValue: 1,
                columns: new[] { "created_date", "updated_date" },
                values: new object[] { new DateTime(2020, 12, 9, 12, 0, 8, 317, DateTimeKind.Local).AddTicks(2100), new DateTime(2020, 12, 9, 12, 0, 8, 317, DateTimeKind.Local).AddTicks(3280) });

            migrationBuilder.UpdateData(
                table: "ProductAttributeType",
                keyColumn: "id",
                keyValue: 2,
                columns: new[] { "created_date", "updated_date" },
                values: new object[] { new DateTime(2020, 12, 9, 12, 0, 8, 317, DateTimeKind.Local).AddTicks(3890), new DateTime(2020, 12, 9, 12, 0, 8, 317, DateTimeKind.Local).AddTicks(3900) });

            migrationBuilder.UpdateData(
                table: "ProductAttributeType",
                keyColumn: "id",
                keyValue: 3,
                columns: new[] { "created_date", "updated_date" },
                values: new object[] { new DateTime(2020, 12, 9, 12, 0, 8, 317, DateTimeKind.Local).AddTicks(3900), new DateTime(2020, 12, 9, 12, 0, 8, 317, DateTimeKind.Local).AddTicks(3900) });
        }
    }
}

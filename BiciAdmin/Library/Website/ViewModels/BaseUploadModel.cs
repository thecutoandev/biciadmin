﻿using System;
using Microsoft.AspNetCore.Http;

namespace Website.ViewModels
{
    public class BaseUploadModel
    {
        public IFormFile file { get; set; }
        public string folder { get; set; }
    }
}

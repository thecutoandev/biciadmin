using Models = DataCore.Models;

namespace Website.ViewModels
{
    public class OrderItemViewModel : Models.OrderItem
    {
        // You can add field name to response to client.
        public string product_name { get; set; }
        public string variant_type { get; set; }
        public string variant_value { get; set; }
        public string kiot_code { get; set; }
        public int generic_product_id { get; set; }
        public string product_image { set; get; }
    }
}
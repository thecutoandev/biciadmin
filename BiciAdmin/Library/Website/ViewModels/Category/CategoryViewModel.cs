using Models = DataCore.Models;
using System.Collections.Generic;

namespace Website.ViewModels
{
    public class CategoryViewModel : Models.Category
    {
        // You can add field name to response to client.
        public List<string> medias { get; set; }
        public List<GenericProductViewModel> products { get; set; }
        public int has_childs { get; set; }
        public string parent_name { get; set; }
        //public List<Models.Category> childs { get; set; }
    }
}
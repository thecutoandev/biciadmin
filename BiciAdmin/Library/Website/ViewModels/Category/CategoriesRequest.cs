﻿using System;
namespace Website.ViewModels
{
    public class CategoriesRequest : PaggingBase
    {
        public int parrentId { get; set; }
        public bool isGetImages { get; set; } = true;
    }
}

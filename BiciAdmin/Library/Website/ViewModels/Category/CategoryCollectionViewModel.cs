﻿using System.Collections.Generic;
using Models = DataCore.Models;

namespace Website.ViewModels
{
    public class CategoryCollectionViewModel : Models.Category
    {
        public List<string> medias { get; set; }
        public List<ProductTypeViewModel> product_types { get; set; }
    }
}

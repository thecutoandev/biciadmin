﻿using System;
using System.Collections.Generic;
using Models = DataCore.Models;
namespace Website.ViewModels
{
    public class CategoryInfoViewModel : Models.Category
    {
        public List<GenericProductViewModel> products { get; set; }
        public List<string> medias { get; set; }
    }
}

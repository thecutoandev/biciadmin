﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Website.ViewModels
{
    public class UserRes
    {
        public string phoneNumber { get; set; }
        public DateTime? birthday { get; set; }
        public string email { get; set; }
        public byte gender { get; set; }
        public string fullName { get; set; }
    }
}

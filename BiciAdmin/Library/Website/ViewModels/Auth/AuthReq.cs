﻿using System;
namespace Website.ViewModels
{
    public class AuthReq
    {
        public string user_name { get; set; }
        public string password { get; set; }
    }
}

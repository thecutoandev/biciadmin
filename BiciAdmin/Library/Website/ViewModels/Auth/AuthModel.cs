﻿using System;
namespace Website.ViewModels
{
    public class AuthModel
    {
        public string name { get; set; }
        public string token { get; set; }
        public string avatar { get; set; }
        public DateTime? birthDay { get; set; }
        public string fbEmail { get; set; }
        public string phone { get; set; }
        public byte gender { get; set; }
        public int role { get; set; }
    }

    public class AdminAuthModel : AuthModel
    {
        public long branch_id { get; set; }
        public bool is_authenticated { get; set; }
    }
}

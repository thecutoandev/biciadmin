﻿using System;
namespace Website.ViewModels
{
    public class OrderConfirmation
    {
        public int order_id { get; set; }
        public string note { get; set; }
        public byte status { get; set; }
        public string approver { get; set; }
    }
}

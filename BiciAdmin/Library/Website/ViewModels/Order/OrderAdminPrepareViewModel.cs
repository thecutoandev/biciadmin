﻿using System.Collections.Generic;
using Models = DataCore.Models;

namespace Website.ViewModels
{
    public class OrderAdminPrepareViewModel
    {
        public string product_name { get; set; }
        public string variant { get; set; }
        public string sku { get; set; }
        public int qty { get; set; }
        public double stocking { get; set; }
        public int product_id { get; set; }
        public int generic_id { get; set; }
    }
}

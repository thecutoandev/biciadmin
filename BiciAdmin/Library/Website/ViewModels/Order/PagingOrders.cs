﻿using System;
using System.Collections.Generic;

namespace Website.ViewModels
{
    public class PagingOrder : PaggingBase
    {
        public int status { get; set; }
        public string source { get; set; }
        public long branch_id { get; set; }
        public List<int> ids { get; set; }
    }
}


﻿using System.Collections.Generic;
using Models = DataCore.Models;

namespace Website.ViewModels
{
    public class OrderAdminViewModel : Models.Order
    {
        public string customer_name { get; set; }
        public string area { get; set; }
        public decimal total_amount { get; set; }
        public decimal ship { get; set; }
        public decimal amount { get; set; }
        public string phone { get; set; }
        public string full_address { get; set; }
        public decimal cod_fee_amount { get; set; }
        public string payment_method_name { get; set; }
        public List<OrderAdminItemViewModel> items { get; set; }
        public string approve_note { get; set; }
    }
}
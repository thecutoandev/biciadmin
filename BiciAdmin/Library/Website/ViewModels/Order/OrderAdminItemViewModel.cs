﻿using System.Collections.Generic;
using Models = DataCore.Models;

namespace Website.ViewModels
{
    public class OrderAdminItemViewModel
    {
        public int id { get; set; }
        public string product_name { get; set; }
        public int qty { get; set; }
        public decimal amount { get; set; }
        public decimal price { get; set; }
        public int product_id { get; set; }
        public int generic_id { get; set; }
        public string sku { get; set; }
        public string variant_name { get; set; }
    }
}

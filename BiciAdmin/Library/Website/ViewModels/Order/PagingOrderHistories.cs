﻿using System;
using System.Collections.Generic;

namespace Website.ViewModels
{
    public class PagingOrderHistories
    {
        public int page { get; set; }
        public int itemPerPage { get; set; }
        public PagingOrderHistories()
        {
            page = 0;
            itemPerPage = -1;
        }
    }
}
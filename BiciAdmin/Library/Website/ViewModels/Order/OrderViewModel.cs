using System.Collections.Generic;
using Models = DataCore.Models;

namespace Website.ViewModels
{
    public class OrderViewModel : Models.Order
    {
        // You can add field name to response to client.
        public List<OrderItemViewModel> details { get; set; }
        public OrderSummaryViewModel summary { get; set; }
        public UserAddressViewModel delivery_address { get; set; }
    }
}
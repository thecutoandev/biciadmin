using Models = DataCore.Models;

namespace Website.ViewModels
{
    public class ProductAttributeViewModel : Models.ProductAttribute
    {
        // You can add field name to response to client.
        public string attribute_name { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;

namespace Website.ViewModels
{
    public class BaseDatatable<T> where T : class
    {
        // List of data
        public List<T> data { get; set; }
        // Count all data (base on where clause if use filter)
        public int total { get; set; }
        // Time for tracking
        //public DateTime time_stamp
        //{
        //    get
        //    {
        //        return DateTime.Now;
        //    }
        //}
    }
}

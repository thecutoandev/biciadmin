﻿using System;
namespace Website.ViewModels
{
    public class PaggingBase
    {
        public int page{ get; set; }
        public int itemPerPage { get; set; }
        public string sortBy { get; set; }
        public bool reverse { get; set; }
        public string searchValue { get; set; }
        public PaggingBase()
        {
            page = 1;
            itemPerPage = -1;
            sortBy = "id";
            reverse = false;
            searchValue = string.Empty;
        }
    }
}

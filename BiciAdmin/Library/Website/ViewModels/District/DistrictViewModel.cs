using Models = DataCore.Models;

namespace Website.ViewModels
{
    public class DistrictViewModel : Models.District
    {
        // You can add field name to response to client.
        public string city_name { get; set; }
    }
}
﻿using System;
namespace Website.ViewModels
{
    public class KiotStocking
    {
        public long kiot_id { get; set; }
        public double quantity { get; set; }
        public string product_name { get; set; }
        public string  product_code { get; set; }
    }
}

using Models = DataCore.Models;
using System.Collections.Generic;
namespace Website.ViewModels
{
    public class ProductViewModel : Models.Product
    {
        // You can add field name to response to client.
        //public ProductAttributeViewModel variant { get; set; }
        public List<string> medias { get; set; }
        public string varirant_type { get; set; }
        public string variant_value { get; set; }
    }
}
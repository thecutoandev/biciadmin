using Models = DataCore.Models;

namespace Website.ViewModels
{
    public class UserAddressViewModel : Models.UserAddress
    {
        // You can add field name to response to client.
        public string district_name { get; set; }
        public string city_name { get; set; }
    }
}
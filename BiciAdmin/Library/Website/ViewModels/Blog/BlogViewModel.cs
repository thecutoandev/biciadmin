using Models = DataCore.Models;
using System.Collections.Generic;

namespace Website.ViewModels
{
    public class BlogViewModel : Models.Blog
    {
        // You can add field name to response to client.
        public List<BlogCollectionViewModel> collections { get; set; }
    }
}
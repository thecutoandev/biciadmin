﻿using System;
namespace Website.ViewModels
{
    public class UserInfoViewModel
    {
        public string user_name { get; set; }
        public string phone { get; set; }
        public string full_name { get; set; }
        public string avatar { get; set; }
        public string email { get; set; }
        public byte gender { get; set; }
        public DateTime? birth_day { get; set; }
        public int role_id { get; set; }
        public long branch_id { set; get; }
    }
}

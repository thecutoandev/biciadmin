using Models = DataCore.Models;
using System.Collections.Generic;

namespace Website.ViewModels
{
    public class AreaViewModel : Models.Area
    {
        // You can add field name to response to client.
        public List<DistrictViewModel> districts { get; set; }
    }
}
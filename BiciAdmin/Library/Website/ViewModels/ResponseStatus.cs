﻿using System;
using System.Linq;
using System.Collections.Generic;

namespace Website.ViewModels
{
    public class ResponseListModel<T> where T : class
    {
        public bool successfull { get; set; }
        public string message { get; set; }
        public List<T> dataset { get; set; }
        public int total { get; set; }
        public DateTime? time_stamp
        {
            get
            {
                return DateTime.Now;
            }
        }
        public ResponseListModel()
        {
            this.successfull = true;
            this.message = "Data powered by Wao Solutions.";
        }
    }

    public class ResponseSignleModel<T> where T : class
    {
        public bool successfull { get; set; }
        public string message { get; set; }
        public T dataset { get; set; }
        public DateTime? time_stamp
        {
            get
            {
                return DateTime.Now;
            }
        }
        public ResponseSignleModel()
        {
            this.successfull = true;
            this.message = "Data powered by Wao Solutions.";
        }
    }

    public class ResponseStatus
    {
        public bool successfull { get; set; }
        public string message { get; set; }
        public object dataset { get; set; }
        public DateTime? time_stamp
        {
            get
            {
                return DateTime.Now;
            }
        }
        public ResponseStatus()
        {
            this.successfull = false;
            this.message = "Data powered by Wao Solutions.";
        }
    }
}

﻿using System;
namespace Website.ViewModels
{
    public class CollectionRequest : PaggingBase
    {
        public byte type { get; set; } = 99;
        public bool isGetImages { get; set; } = true;
    }
}

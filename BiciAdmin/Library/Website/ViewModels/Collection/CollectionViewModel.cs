﻿using System;
using System.Collections.Generic;
using Models = DataCore.Models;

namespace Website.ViewModels
{
    public class CollectionViewModel : Models.Collection
    {
        // You can add field name to response to client.
        public List<GenericProductViewModel> generic_products { get; set; }
        public List<string> medias { get; set; }
    }
}
 
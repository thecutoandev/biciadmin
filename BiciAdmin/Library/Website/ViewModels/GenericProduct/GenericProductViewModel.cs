using Models = DataCore.Models;
using System.Collections.Generic;

namespace Website.ViewModels
{
    public class GenericProductViewModel : Models.GenericProduct
    {
        // You can add field name to response to client.
        public  List<string> medias{ get; set; }
        public List<ProductViewModel> products { get; set; }
        public string category_name { get; set; }
        public string brand_name { get; set; }
    }
}
﻿using System;
namespace Website.ViewModels
{
    public class GenericProductRequestBase : PaggingBase
    {
        public int collection_id { get; set; }
        public int category_id { get; set; }
        public int brand_id { get; set; }
        public bool isGetImages { get; set; }
        public GenericProductRequestBase() : base()
        {
            collection_id = 0;
            category_id = 0;
            brand_id = 0;
            isGetImages = true;
        }
    }

    public class GenericProductRequest : GenericProductRequestBase
    {
        public bool is_get_hidden { get; set; }
        public GenericProductRequest()
        {
            isGetImages = true;
            is_get_hidden = false;
        }

        public GenericProductRequest(GenericProductRequestBase context)
        {
            this.brand_id = context.brand_id;
            this.category_id = context.category_id;
            this.collection_id = context.collection_id;
            this.itemPerPage = context.itemPerPage;
            this.page = context.page;
            this.reverse = context.reverse;
            this.searchValue = context.searchValue;
            this.sortBy = context.sortBy;
            this.isGetImages = context.isGetImages;
            this.is_get_hidden = false;
        }
    }
}

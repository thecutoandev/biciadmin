﻿using System;
using System.Collections.Generic;

namespace Website.ViewModels
{
    public class BranchPickUpdate
    {
        public List<BranchPickViewModel> branches { get; set; }
    }
}

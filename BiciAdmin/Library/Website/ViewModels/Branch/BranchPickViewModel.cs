﻿using System;
using Models = DataCore.Models;
namespace Website.ViewModels
{
    public class BranchPickViewModel : Models.BranchPickPriority
    {
        public string branch_name { get; set; }
    }
}
